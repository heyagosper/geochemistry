package au.gov.ga.oemd.plugin.geochemistry.action;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.dm.geochemistry.domain.Job;
import au.gov.ga.oemd.dm.geochemistry.domain.SampleAnalysisResult;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.dm.geochemistry.service.search.AnalysisExportCriteria;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.controller.FileHelper;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Component("analysesExportAction")
public class AnalysesExportAction {
    //    private static final Logger log = LoggerFactory.getLogger(AnalysesExportAction.class);
    public static final String CONTENT_TYPE_CSV = "application/vnd.ns-csv";

    public static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
    public static final String CONTENT_TYPE_XML = "text/xml";
    public static final String ENCODING = System.getProperty("file.encoding");
    public static final String FILE_NAME = "analysesResults";

    @Autowired
    private GeochemistryService geochemistryService;

    @Autowired
    private FileHelper fileHelper;

    @Autowired
    private ValidationMessageAdapter validationMessageAdapter;

    public Boolean processAnalysesForExport(RequestContext context,
            MessageContext messageContext) {
        List<SimpleMessageBean> simpleMessageBeanList = new ArrayList<SimpleMessageBean>();
        AnalysisExportCriteria analysisExportCriteria = (AnalysisExportCriteria) context.getFlowScope()
                .get("analysisExportCriteria",
                        AnalysisExportCriteria.class);
        Assert.notNull(analysisExportCriteria);
        Job job = (Job) context.getFlowScope()
                .get("job",
                        Job.class);
        List<SampleAnalysisResult> analysesForExportList = this.geochemistryService.findAnalysesByExportCriteria(analysisExportCriteria);
        if (CollectionUtils.isNotEmpty(analysesForExportList)) {
            OutputStream outputStream = null;
            String exportType = "";
            if (analysisExportCriteria.isPropertiesOnlyExport()) {
                exportType = "Properties";
            } else if (analysisExportCriteria.isPropertiesAndMethodsExport()) {
                exportType = "Methods";
            } else if (analysisExportCriteria.isReferenceMaterialsExport()) {
                exportType = "Reference";
            } else if (analysisExportCriteria.isKnownReferenceExport()) {
                exportType = "KnownReferences";
            } else if (analysisExportCriteria.isPropertiesWithReferencesExport()) {
                exportType = "PropertiesAndRef";
            } else if (analysisExportCriteria.isPropertiesAndMethodsWithReferenceExport()) {
                exportType = "MethodsAndRef";
            }
            String fileName = FILE_NAME
                    + exportType
                    + analysisExportCriteria.getJobNo()
                    + analysisExportCriteria.getFileExtension();

            if (analysisExportCriteria.isCsvExport()) {
                outputStream = this.fileHelper.openOutputStream(context,
                        fileName,
                        CONTENT_TYPE_CSV);

            } else if (analysisExportCriteria.isExcelExport()) {
                outputStream = this.fileHelper.openOutputStream(context,
                        fileName,
                        CONTENT_TYPE_EXCEL);

            } else if (analysisExportCriteria.isXmlExport()) {
                outputStream = this.fileHelper.openOutputStream(context,
                        fileName,
                        CONTENT_TYPE_XML);

            }
            this.geochemistryService.processAnalysesForExport(analysesForExportList,
                    analysisExportCriteria,
                    job,
                    outputStream);
            if (outputStream != null) {
                this.fileHelper.closeOutputStream(context,
                        outputStream);
                try {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                } catch (IOException e) {
                    simpleMessageBeanList.add(new SimpleMessageBean("Error closing file output stream ["
                            + e
                            + "]"));
                }
            }

        }
        this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                messageContext);
        if (messageContext.hasErrorMessages()) {
            context.getFlowScope()
                    .put("isValidJobExport",
                            Boolean.FALSE);
            context.getFlowScope()
                    .put("fileDownloaded",
                            Boolean.FALSE);
            return Boolean.FALSE;
        }
        context.getFlowScope()
                .put("isValidJobExport",
                        Boolean.TRUE);
        context.getFlowScope()
                .put("fileDownloaded",
                        Boolean.TRUE);
        //        downloadAnalyses();
        return Boolean.TRUE;

    }

}
