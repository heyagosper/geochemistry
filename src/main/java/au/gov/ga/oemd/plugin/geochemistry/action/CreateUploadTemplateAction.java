package au.gov.ga.oemd.plugin.geochemistry.action;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.dm.geochemistry.domain.Method;
import au.gov.ga.oemd.dm.geochemistry.domain.Property;
import au.gov.ga.oemd.dm.geochemistry.service.search.CreateUploadTemplateCriteria;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.controller.FileHelper;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

import com.Ostermiller.util.ExcelCSVPrinter;

@Component("createUploadTemplateAction")
public class CreateUploadTemplateAction {

    public static final String CONTENT_TYPE_CSV = "application/vnd.ns-csv";
    public static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
    public static final String EMPTY_STRING = "";
    public static final String FILE_NAME = "Upload";

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    private FileHelper fileHelper;

    @Autowired
    private Properties mappingProperties;

    public List<String[]> constructGravPropertyRows(String techId,
            List<Property> propertyList) {

        List<String[]> rows = new ArrayList<String[]>();
        List<String> columnHeaders = new ArrayList<String>();

        String gravSampleNameLookup = this.getProperty(techId
                + ".sample.lookup");
        String gravCrucibleWeightLookup = this.getProperty(techId
                + ".crucibleWeight.lookup");
        String gravCombinedWeightLookup = this.getProperty(techId
                + ".combinedWeight.lookup");
        String gravFinalWeightLookup = this.getProperty(techId
                + ".finalWeight.lookup");
        String gravLossOnIgnitionLookup = this.getProperty(techId
                + ".lossOnIgnition.lookup");
        // add empty column
        columnHeaders.add(EMPTY_STRING);
        columnHeaders.add(gravSampleNameLookup);
        columnHeaders.add(gravCrucibleWeightLookup);
        columnHeaders.add(gravCombinedWeightLookup);
        columnHeaders.add(gravFinalWeightLookup);
        columnHeaders.add(gravLossOnIgnitionLookup);

        String[] strColumnHeaders = new String[columnHeaders.size()];
        strColumnHeaders = columnHeaders.toArray(strColumnHeaders);
        rows.add(strColumnHeaders);

        return rows;
    }

    public List<String[]> constructHeader(Method method,
            CreateUploadTemplateCriteria createUploadTemplateCriteria) {

        List<String[]> headerList = new ArrayList<String[]>();

        String[] sessionDateTimeHeader = new String[] {
                "Session date-time:",
                EMPTY_STRING }; // A1
        String instrumentIdHeader = EMPTY_STRING;
        if (method.getDefaultInstrument() != null) {
            instrumentIdHeader = method.getDefaultInstrument()
                    .getInstrumentId();
        }
        String[] instrumentHeader = new String[] {
                "Instrument:",
                instrumentIdHeader }; // A2 & B2
        String[] numberOfResHeader = new String[] {
                "Number of Results:",
                EMPTY_STRING }; // A3

        headerList.add(sessionDateTimeHeader);
        headerList.add(instrumentHeader);
        headerList.add(numberOfResHeader);
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });

        return headerList;
    }

    public List<String[]> constructIcpmsPropertyRows(String techId,
            List<Property> propertyList) {

        List<String[]> rows = new ArrayList<String[]>();
        List<String> propertyIds = new ArrayList<String>();
        List<String> propertyUnits = new ArrayList<String>();

        String icpmsPropertyHeaderLookup = this.getProperty(techId
                + ".propertyHeader.lookup");
        String icpmsDateTimeLookup = this.getProperty(techId
                + ".dateTime.lookup");
        String icpmsTypeLookup = this.getProperty(techId
                + ".type.lookup");
        String icpmsSubsampleIdLookup = this.getProperty(techId
                + ".subsampleId.lookup");
        String icpmsSampleNameLookup = this.getProperty(techId
                + ".sample.lookup");

        propertyUnits.add(icpmsDateTimeLookup);
        propertyUnits.add(icpmsTypeLookup);
        propertyUnits.add(icpmsSubsampleIdLookup);
        propertyUnits.add(icpmsSampleNameLookup);

        propertyIds.add(icpmsPropertyHeaderLookup);
        for (int i = 0; i < propertyUnits.size() - 1; i++) {
            propertyIds.add(EMPTY_STRING);
        }

        for (Property ma : propertyList) {
            if (ma.getPropertyId() != null) {
                propertyIds.add(ma.getPropertyId());

                if (ma.getDeliveryUnits() != null) {
                    propertyUnits.add(ma.getDeliveryUnits());
                } else {
                    propertyUnits.add(EMPTY_STRING);
                }
            }
        }

        String[] strPropertyIds = new String[propertyIds.size()];
        strPropertyIds = propertyIds.toArray(strPropertyIds);
        String[] strPropertyUnits = new String[propertyIds.size()];
        strPropertyUnits = propertyUnits.toArray(strPropertyUnits);
        rows.add(strPropertyIds);
        rows.add(strPropertyUnits);

        return rows;
    }

    public List<String[]> constructPropertyRows(List<Property> propertyList) {

        List<String[]> rows = new ArrayList<String[]>();
        List<String> propertyIds = new ArrayList<String>();
        List<String> propertyUnits = new ArrayList<String>();

        propertyIds.add("Seq.");
        propertyIds.add("Date-Time");
        propertyIds.add("Sample");
        propertyIds.add("Type");
        propertyIds.add("Subsample");

        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);

        for (Property ma : propertyList) {
            if (ma.getPropertyId() != null) {
                propertyIds.add(ma.getPropertyId());

                if (ma.getDeliveryUnits() != null) {
                    propertyUnits.add(ma.getDeliveryUnits());
                } else {
                    propertyUnits.add(EMPTY_STRING);
                }
            }
        }

        String[] strPropertyIds = new String[propertyIds.size()];
        strPropertyIds = propertyIds.toArray(strPropertyIds);
        String[] strPropertyUnits = new String[propertyIds.size()];
        strPropertyUnits = propertyUnits.toArray(strPropertyUnits);
        rows.add(strPropertyIds);
        rows.add(strPropertyUnits);

        return rows;
    }

    public List<String[]> constructTitrPropertyRows(String techId,
            List<Property> propertyList) {

        List<String[]> rows = new ArrayList<String[]>();
        List<String> columnHeaders = new ArrayList<String>();

        String titrSampleNameLookup = this.getProperty(techId
                + ".sample.lookup");
        String titrFeoWeightLookup = this.getProperty(techId
                + ".feoWeight.lookup");
        String titrFeoVolumeLookup = this.getProperty(techId
                + ".feoVolume.lookup");
        String titrFeoPercentLookup = this.getProperty(techId
                + ".feoPercent.lookup");
        String titrTitrantFactorLookup = this.getProperty(techId
                + ".titrantFactor.lookup");

        String[] titrantFactorCol = {
                "",
                titrTitrantFactorLookup };

        // add empty column
        columnHeaders.add(EMPTY_STRING);
        columnHeaders.add(titrSampleNameLookup);
        columnHeaders.add(titrFeoWeightLookup);
        columnHeaders.add(titrFeoVolumeLookup);
        columnHeaders.add(titrFeoPercentLookup);

        String[] strColumnHeaders = new String[columnHeaders.size()];
        strColumnHeaders = columnHeaders.toArray(strColumnHeaders);
        rows.add(titrantFactorCol);
        rows.add(strColumnHeaders);

        return rows;
    }

    public List<String[]> constructXrfHeader(Method method,
            CreateUploadTemplateCriteria createUploadTemplateCriteria) {
        List<String[]> headerList = new ArrayList<String[]>();
        String techId = method.getTechnique()
                .getTechniqueId();
        String[] sessionDateTimeHeader = new String[] {
                "<Session date-time here>",
                EMPTY_STRING }; // A1
        String instrumentIdHeader = EMPTY_STRING;
        if (method.getDefaultInstrument() != null) {
            instrumentIdHeader = method.getDefaultInstrument()
                    .getInstrumentId();
        }
        String[] instrumentHeader = new String[] { instrumentIdHeader }; // A2 & B2

        String xrfMethodLookup = this.getProperty(techId
                + ".method.lookup");

        String[] methodHeader = new String[] {
                xrfMethodLookup,
                EMPTY_STRING };

        String xrfTotalLookup = this.getProperty(techId
                + ".total.lookup");

        String[] numberOfResHeader = new String[] {
                xrfTotalLookup,
                EMPTY_STRING }; // A3

        headerList.add(sessionDateTimeHeader);
        headerList.add(instrumentHeader);
        headerList.add(methodHeader);
        headerList.add(numberOfResHeader);
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        headerList.add(new String[] {
                EMPTY_STRING,
                EMPTY_STRING });
        return headerList;
    }

    public List<String[]> constructXrfPropertyRows(String techId,
            List<Property> propertyList,
            boolean sampleWeightRequired) {

        List<String[]> rows = new ArrayList<String[]>();
        List<String> propertyIds = new ArrayList<String>();
        List<String> propertyUnits = new ArrayList<String>();
        String xrfInitialWeightLookup = null;
        String xrfFinalWeightLookup = null;

        String xrfSequenceNoLookup = this.getProperty(techId
                + ".sequenceNo.lookup");
        String xrfSampleNameLookup = this.getProperty(techId
                + ".sampleName.lookup");
        String xrfMeasurementDateTimeLookup = this.getProperty(techId
                + ".measurementDateTime.lookup");
        if (sampleWeightRequired) {
            xrfInitialWeightLookup = this.getProperty(techId
                    + ".initialWeight.lookup");
            xrfFinalWeightLookup = this.getProperty(techId
                    + ".finalWeight.lookup");
        }
        String xrfSumOfConcentrationLookup = this.getProperty(techId
                + ".sumOfConcentration.lookup");
        String xrfResultTypeLookup = this.getProperty(techId
                + ".resultType.lookup");
        String xrfSubsampleIdLookup = this.getProperty(techId
                + ".subsampleId.lookup");

        propertyIds.add(xrfSequenceNoLookup);
        propertyIds.add(xrfSampleNameLookup);
        propertyIds.add(xrfMeasurementDateTimeLookup);
        if (sampleWeightRequired) {
            propertyIds.add(xrfInitialWeightLookup);
            propertyIds.add(xrfFinalWeightLookup);
        }
        propertyIds.add(xrfSumOfConcentrationLookup);
        propertyIds.add(xrfResultTypeLookup);
        propertyIds.add(xrfSubsampleIdLookup);

        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);
        if (sampleWeightRequired) {
            propertyUnits.add(propertyIds.indexOf(xrfInitialWeightLookup),
                    "weight (g)");
            propertyUnits.add(propertyIds.indexOf(xrfFinalWeightLookup),
                    "weight (g)");
        }
        propertyUnits.add(propertyIds.indexOf(xrfSumOfConcentrationLookup),
                "of conc. %");
        propertyUnits.add(EMPTY_STRING);
        propertyUnits.add(EMPTY_STRING);

        for (Property ma : propertyList) {
            if (ma.getPropertyId() != null) {
                propertyIds.add(ma.getPropertyId());

                if (ma.getDeliveryUnits() != null) {
                    propertyUnits.add(ma.getDeliveryUnits());
                } else {
                    propertyUnits.add(EMPTY_STRING);
                }
            }
        }

        String[] strPropertyIds = new String[propertyIds.size()];
        strPropertyIds = propertyIds.toArray(strPropertyIds);
        String[] strPropertyUnits = new String[propertyIds.size()];
        strPropertyUnits = propertyUnits.toArray(strPropertyUnits);
        rows.add(strPropertyIds);
        rows.add(strPropertyUnits);

        return rows;
    }

    public OutputStream createCsvOutput(List<String[]> dataRows,
            OutputStream outputStream,
            List<SimpleMessageBean> simpleMessageBeanList) {

        try {
            ExcelCSVPrinter ecsv = new ExcelCSVPrinter(outputStream);
            for (String[] line : dataRows) {
                ecsv.writeln(line);
            }
        } catch (Exception e) {
            simpleMessageBeanList.add(new SimpleMessageBean("Error creating file output stream ["
                    + e
                    + "]"));
        }
        return outputStream;
    }

    public OutputStream createExcelOutput(String methodId,
            List<String[]> dataRows,
            OutputStream outputStream,
            List<SimpleMessageBean> simpleMessageBeanList) {

        try {
            Workbook analysesWorkbook = new HSSFWorkbook();
            Sheet analysesSheet = analysesWorkbook.createSheet("Upload");
            Row row;
            int lineCount = 0;

            for (String[] line : dataRows) {
                row = analysesSheet.createRow((short) lineCount);
                for (int col = 0; col < line.length; col++) {
                    try {
                        Double dCell = Double.valueOf(line[col])
                                .doubleValue();
                        row.createCell((short) col)
                                .setCellValue(dCell);
                    } catch (NumberFormatException ex) { // it's a string 
                        row.createCell((short) col)
                                .setCellValue(line[col]);
                    }
                }
                lineCount++;
            }
            analysesWorkbook.write(outputStream);
        } catch (Exception e) {
            simpleMessageBeanList.add(new SimpleMessageBean("Error creating file output stream ["
                    + e
                    + "]"));
        }
        return outputStream;
    }

    public Boolean process(RequestContext context,
            Method method,
            CreateUploadTemplateCriteria createUploadTemplateCriteria,
            MessageContext messageContext) {

        List<SimpleMessageBean> simpleMessageBeanList = new ArrayList<SimpleMessageBean>();
        List<String[]> headerList = new ArrayList<String[]>();
        List<String[]> rows = null;

        Assert.notNull(method);
        Assert.notNull(createUploadTemplateCriteria);

        List<Property> propertyList = new ArrayList<Property>();
        if (createUploadTemplateCriteria.isActiveIncludedUpload()) {
            propertyList.addAll(method.getActivePropertyList());
        }
        if (createUploadTemplateCriteria.isInactiveIncludedUpload()) {
            propertyList.addAll(method.getInactivePropertyList());
        }

        if (CollectionUtils.isNotEmpty(propertyList)) {
            if (createUploadTemplateCriteria.isPropertyOrderAlpha()) {
                Collections.sort(propertyList,
                        Property.alphaComparator);
            } else {
                Collections.sort(propertyList,
                        Property.standardComparator);
            }
        }

        if (method.getTechnique()
                .isXrf()) {
            headerList = this.constructXrfHeader(method,
                    createUploadTemplateCriteria);
            rows = this.constructXrfPropertyRows(method.getTechnique()
                    .getTechniqueId(),
                    propertyList,
                    method.isXrfSampleWeightRequired());
        } else if (method.getTechnique()
                .isIcpms()) {
            rows = this.constructIcpmsPropertyRows(method.getTechnique()
                    .getTechniqueId(),
                    propertyList);
        } else if (method.getTechnique()
                .isGravimetry()) {
            rows = this.constructGravPropertyRows(method.getTechnique()
                    .getTechniqueId(),
                    propertyList);
        } else if (method.getTechnique()
                .isTitration()) {
            rows = this.constructTitrPropertyRows(method.getTechnique()
                    .getTechniqueId(),
                    propertyList);
        } else {
            headerList = this.constructHeader(method,
                    createUploadTemplateCriteria);
            rows = this.constructPropertyRows(propertyList);
        }

        //
        OutputStream outputStream = null;
        if (!rows.isEmpty()) {
            for (int i = 0; i < headerList.size(); i++) {
                rows.add(i,
                        headerList.get(i));
            }

            String fileName = FILE_NAME
                    + method.getMethodId()
                    + createUploadTemplateCriteria.getFileExtension();

            if (createUploadTemplateCriteria.isCsvUpload()) {
                outputStream = this.fileHelper.openOutputStream(context,
                        fileName,
                        CONTENT_TYPE_CSV);
                outputStream = this.createCsvOutput(rows,
                        outputStream,
                        simpleMessageBeanList);
            } else if (createUploadTemplateCriteria.isExcelUpload()) {
                outputStream = this.fileHelper.openOutputStream(context,
                        fileName,
                        CONTENT_TYPE_EXCEL);
                outputStream = this.createExcelOutput(method.getMethodId(),
                        rows,
                        outputStream,
                        simpleMessageBeanList);
            }

            if (outputStream != null) {
                this.fileHelper.closeOutputStream(context,
                        outputStream);
                try {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                } catch (IOException e) {
                    simpleMessageBeanList.add(new SimpleMessageBean("Error closing file output stream ["
                            + e
                            + "]"));
                }
            } else {
                simpleMessageBeanList.add(new SimpleMessageBean("No methodProperty result data found"));
            }
        } else {
            simpleMessageBeanList.add(new SimpleMessageBean("Unable to construct method properties")); // problems with data
        }

        this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                messageContext);
        if (messageContext.hasErrorMessages()) {
            context.getFlowScope()
                    .put("isValidCreateUploadTemplate",
                            Boolean.FALSE);
            context.getFlowScope()
                    .put("fileDownloaded",
                            Boolean.FALSE);
            return Boolean.FALSE;
        }
        context.getFlowScope()
                .put("isValidCreateUploadTemplate",
                        Boolean.TRUE);
        context.getFlowScope()
                .put("fileDownloaded",
                        Boolean.TRUE);

        return Boolean.TRUE;
    }

    private String getProperty(String code) {
        return this.mappingProperties.getProperty(code);
    }
}
