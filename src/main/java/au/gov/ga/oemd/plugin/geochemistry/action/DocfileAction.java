package au.gov.ga.oemd.plugin.geochemistry.action;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.dm.geochemistry.domain.Docfile;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Component("docfileAction")
public class DocfileAction {
    private static final Logger log = LoggerFactory.getLogger(DocfileAction.class);

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    public Boolean processUpload(Docfile docfile,
            RequestContext context,
            MessageContext messageContext) {
        MultipartFile file = context.getRequestParameters()
                .getRequiredMultipartFile("file");
        List<SimpleMessageBean> simpleMessageBeanList = new ArrayList<SimpleMessageBean>();
        if (file != null
                && file.getSize() > 0) {
            Assert.notNull(docfile);
            try {
                docfile.parse(file);
            } catch (Throwable e) {
                log.error("Throwable caught uploading file ",
                        e);
                String msg = "The file: "
                        + docfile
                                .getTitle()
                        + " was invalid and could not be processed";
                simpleMessageBeanList.add(new SimpleMessageBean(msg).error());
                this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                        messageContext);
                return Boolean.FALSE;
            }

            if (messageContext.hasErrorMessages()) {
                context.getFlowScope()
                        .put("isValidDocfile",
                                Boolean.FALSE);
                return Boolean.FALSE;
            }
            context.getFlowScope()
                    .put("isValidDocfile",
                            Boolean.TRUE);
            context.getFlowScope()
                    .put("fileUploaded",
                            Boolean.TRUE);
            return Boolean.TRUE; // if we found a file we consider it valid even if there are messages displayed 
        } else {
            String msg = "The file to be uploaded was missing or empty. Please load a valid file.";
            simpleMessageBeanList.add(new SimpleMessageBean(msg).error());
            this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                    messageContext);
            return Boolean.FALSE;
        }
    }

}
