package au.gov.ga.oemd.plugin.geochemistry.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.dm.geochemistry.domain.MethodInstrumentCalibration;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodInstrumentCalibrationFile;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Component("methodInstrumentCalibrationFileAction")
public class MethodInstrumentCalibrationFileAction {
    private static final Logger log = LoggerFactory.getLogger(MethodInstrumentCalibrationFileAction.class);

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    public Boolean processUpload(MethodInstrumentCalibration methodInstrumentCalibration,
            RequestContext context,
            MessageContext messageContext) {
        MultipartFile file = context.getRequestParameters()
                .getRequiredMultipartFile("file");
        if (file != null
                && file.getSize() > 0) {
            List<SimpleMessageBean> simpleMessageBeanList = new ArrayList<SimpleMessageBean>();

            MethodInstrumentCalibrationFile sourceFile = new MethodInstrumentCalibrationFile(methodInstrumentCalibration);
            if (!methodInstrumentCalibration.hasMethodInstrumentCalibrationFile(file.getOriginalFilename())) {
                methodInstrumentCalibration.getMethodInstrumentCalibrationFileList()
                        .add(sourceFile);
                try {
                    sourceFile.parse(file);
                } catch (IOException e) {
                    log.error("IOException caught in processUpload",
                            e);
                    String msg = "The file: "
                            + sourceFile.getName()
                            + " was invalid and could not be processed";
                    simpleMessageBeanList.add(new SimpleMessageBean(msg).error());
                    this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                            messageContext);
                    return Boolean.FALSE;
                }
            } else {
                String msg = "The file: "
                    + file.getOriginalFilename()
                    + " has already been uploaded";
            simpleMessageBeanList.add(new SimpleMessageBean(msg).info());
            }
            this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                    messageContext);
            return Boolean.TRUE; // if we found a file we consider it valid even if there are messages displayed 
        } else {
            return Boolean.FALSE;
        }
    }
}
