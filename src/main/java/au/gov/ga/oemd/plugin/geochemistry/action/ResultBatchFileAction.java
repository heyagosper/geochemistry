package au.gov.ga.oemd.plugin.geochemistry.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.framework.controller.FileHelper;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatch;
import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatchSourceFile;
import au.gov.ga.oemd.dm.geochemistry.service.ResultBatchTransferService;

@Component("resultBatchFileAction")
public class ResultBatchFileAction {
    private static final Logger log = LoggerFactory.getLogger(ResultBatchFileAction.class);
    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    private FileHelper fileHelper;

    @Autowired
    private ResultBatchTransferService resultBatchTransferService;

    public Boolean processDownload(RequestContext context,
            MessageContext messageContext) {
        ResultBatchSourceFile sourceFile = (ResultBatchSourceFile) context.getFlashScope()
                .get("resultBatchSourceFile",
                        ResultBatchSourceFile.class);
        if (sourceFile == null) {
            ResultBatch resultBatch = (ResultBatch) context.getConversationScope()
                    .get("resultBatch",
                            ResultBatch.class);
            Assert.notNull(resultBatch);
            sourceFile = resultBatch.getSourceFile();
        }
        this.fileHelper.streamFile(context,
                sourceFile);

        return Boolean.TRUE;
    }

    public Boolean processUpload(ResultBatch resultBatch,
            RequestContext context,
            MessageContext messageContext) {
        MultipartFile file = context.getRequestParameters()
                .getRequiredMultipartFile("file");
        List<SimpleMessageBean> simpleMessageBeanList = new ArrayList<SimpleMessageBean>();
        if (file != null
                && file.getSize() > 0) {
            Assert.notNull(resultBatch);
            ResultBatchSourceFile sourceFile = resultBatch.getSourceFile();
            if (sourceFile == null) {
                sourceFile = new ResultBatchSourceFile(resultBatch);
                resultBatch.setSourceFile(sourceFile);
            }

            try {
                // reinstate versionno which may have been lost if we switched methods
                sourceFile.setVersionNo(resultBatch.getSourceFileVersionNo());
                // mark the blob as dirty in case we need to delete a previous one
                sourceFile.setSourceFileBlobNo(null);
                sourceFile.parse(file);
                this.resultBatchTransferService.processResultBatchTransferData(sourceFile.getInputStream(),
                        resultBatch,
                        simpleMessageBeanList);
            } catch (IOException e) {
                log.error("IOException caught in processUpload",
                        e);
                String msg = "The file: "
                        + resultBatch.getSourceFile()
                                .getName()
                        + " was invalid and could not be processed";
                simpleMessageBeanList.add(new SimpleMessageBean(msg).error());
                this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                        messageContext);
                return Boolean.FALSE;
            }
            //            if (resultBatch.getResultBatchTransferFile() == null
            //                    || resultBatch.getResultBatchTransferFile()
            //                            .getAnalysisRecordList()
            //                            .isEmpty()) {
            //                String msg = "The file: "
            //                        + resultBatch.getSourceFile()
            //                                .getName()
            //                        + " was invalid and could not be processed";
            //                simpleMessageBeanList.add(new SimpleMessageBean(msg).error());
            //                this.validationMessageAdapter.addMessages(simpleMessageBeanList,
            //                        messageContext);
            //            }
            this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                    messageContext);
            if (messageContext.hasErrorMessages()) {
                context.getFlowScope()
                        .put("isValidResultBatchData",
                                Boolean.FALSE);
                return Boolean.FALSE;
            }
            context.getFlowScope()
                    .put("isValidResultBatchData",
                            Boolean.TRUE);
            context.getFlowScope()
                    .put("fileUploaded",
                            Boolean.TRUE);
            return Boolean.TRUE; // if we found a file we consider it valid even if there are messages displayed 
        } else {
            String msg = "The file to be uploaded was missing or empty. Please load a valid file.";
            simpleMessageBeanList.add(new SimpleMessageBean(msg).error());
            this.validationMessageAdapter.addMessages(simpleMessageBeanList,
                    messageContext);
            return Boolean.FALSE;
        }
    }

}
