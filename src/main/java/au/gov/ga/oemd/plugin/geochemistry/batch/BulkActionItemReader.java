/*
 * Copyright 2009-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.gov.ga.oemd.plugin.geochemistry.batch;

import groovy.lang.Binding;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import au.gov.ga.oemd.dm.geochemistry.domain.Job;
import au.gov.ga.oemd.dm.geochemistry.transfer.BulkJobActionBean;
import au.gov.ga.oemd.framework.common.domain.XmlToStringBuilder;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.context.SystemContext;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

/**
 * {@link ItemReader} that reads and returns geochem Job records (one at a time)
 * eligible for bulkaction processing at a time
 */
public class BulkActionItemReader
        implements ItemReader<Job>, ApplicationContextAware, StepExecutionListener {

    private static final Logger log = LoggerFactory.getLogger(BulkActionItemReader.class);

    private ApplicationContext applicationContext;

    private BulkJobActionBean bulkJobActionBean = new BulkJobActionBean();

    public BulkJobActionBean.Request getRequest() {
        return bulkJobActionBean.getRequest();
    }

    public void setRequest(BulkJobActionBean.Request request) {
        bulkJobActionBean.setRequest(request);
    }

    private Binding binding;

    private Script script;

    private StepExecution stepExecution;

    public BulkActionItemReader() {
        super();
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        this.stepExecution = null;
        return null;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    /**
     * Reads next record from phase
     */
    @SuppressWarnings({ "unchecked" })
    @Override
    public synchronized Job read() throws Exception {
        Validate.notNull(this.stepExecution,
                "stepExecution was not injected. Was this configured as a step listener?");
        if (this.script == null) {
            bulkJobActionBean.getRequest()
                    .setMaxProcessingTime(null); // no limit on execution time
            List<SimpleMessageBean> messages = new ArrayList<SimpleMessageBean>();
            this.openBulkAction(bulkJobActionBean,
                    messages);
            if (SimpleMessageBean.hasErrorMessages(messages)) {
                throw new IllegalArgumentException("Error processing input file "
                        + ValidationMessageAdapter.toString(messages,
                                true));
            }
            Job resp = (Job) this.script.invokeMethod("read",
                    new Object[] {});
            if (messages != null
                    && !messages.isEmpty()) {
                // add the messages to the step context so downstream processors can see it
                List<SimpleMessageBean> contextMessages = (List<SimpleMessageBean>) this.stepExecution.getExecutionContext()
                        .get("messages");
                if (contextMessages == null) {
                    contextMessages = messages;
                } else {
                    contextMessages.addAll(messages);
                }
                this.stepExecution.getExecutionContext()
                        .put("messages",
                                contextMessages);
            }
            return resp;

        } else {
            return (Job) this.script.invokeMethod("read",
                    new Object[] {});
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    protected void openBulkAction(BulkJobActionBean bulkJobActionBean,
            List<SimpleMessageBean> messages) throws IOException,
            ResourceException,
            ScriptException {
        String groovyDir = SystemContext.getInstance()
                .getConfigProperty("appDataDir")
                + "groovy"
                + File.separator;
        log.debug("actioning groovy scripts from {}",
                groovyDir);
        GroovyScriptEngine gse = new GroovyScriptEngine(groovyDir);
        this.binding = new Binding();
        this.binding.setVariable("bulkJobActionBean",
                bulkJobActionBean);
        this.binding.setVariable("messages",
                messages);
        this.binding.setVariable("applicationContext",
                this.applicationContext);
        this.script = gse.createScript("BulkActioner.groovy",
                this.binding);

        this.script.invokeMethod("init",
                new Object[] {});
        this.script.invokeMethod("processAction",
                new Object[] {
                        bulkJobActionBean,
                        messages });
        log.info(XmlToStringBuilder.getInstance()
                .toString(messages));
    }
}
