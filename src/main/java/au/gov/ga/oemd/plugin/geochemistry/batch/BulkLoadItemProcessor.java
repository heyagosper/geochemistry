/*
 * Copyright 2009-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.gov.ga.oemd.plugin.geochemistry.batch;

import groovy.lang.Binding;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import au.gov.ga.oemd.dm.geochemistry.transfer.BulkLoadBean;
import au.gov.ga.oemd.framework.common.domain.XmlToStringBuilder;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.context.SystemContext;

public class BulkLoadItemProcessor
        implements ItemProcessor<Object[], List<SimpleMessageBean>>, ApplicationContextAware, StepExecutionListener {

    private static final Logger log = LoggerFactory.getLogger(BulkLoadItemProcessor.class);

    private ApplicationContext applicationContext;
    private Binding binding;
    private BulkLoadBean bulkLoadBean;
    private int jobCount = 0;
    private Script script;

    private StepExecution stepExecution;

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        this.stepExecution = null;
        return null;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SimpleMessageBean> process(Object[] item) throws Exception {
        Validate.notNull(this.stepExecution,
                "stepExecution was not injected. Was this configured as a step listener?");
        List<SimpleMessageBean> messages = (List<SimpleMessageBean>) this.stepExecution.getExecutionContext()
                .get("messages");
        if (messages == null) {
            messages = new ArrayList<SimpleMessageBean>();
        } else { // we have messages from an upstream process that we will merge with ours (so can be removed from the context now)
            this.stepExecution.getExecutionContext()
                    .put("messages",
                            null);
        }

        if (this.script == null) {
            this.bulkLoadBean = new BulkLoadBean();
            this.bulkLoadBean.getRequest()
                    .setMaxProcessingTime(null); // no limit on execution time
            this.openBulkLoad(this.bulkLoadBean,
                    messages);
        }
        this.script.invokeMethod("init",
                new Object[] {});

        this.jobCount++;

        //public int processJob(BulkLoadBean bulkLoadBean,Object[] jobData,int jobCount,List<SimpleMessageBean> messages) {
        Integer resp = (Integer) this.script.invokeMethod("processJob",
                new Object[] {
                        this.bulkLoadBean,
                        item,
                        this.jobCount,
                        messages });

        log.info("Execution status: {} ",
                resp);
        if (log.isDebugEnabled()) {
            log.info(XmlToStringBuilder.getInstance()
                    .toString(messages));
        }
        return messages;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    protected void openBulkLoad(BulkLoadBean bulkLoadBean,
            List<SimpleMessageBean> messages) throws IOException,
            ResourceException,
            ScriptException {
        String groovyDir = SystemContext.getInstance()
                .getConfigProperty("appDataDir")
                + "groovy"
                + File.separator;
        log.debug("loading groovy scripts from {}",
                groovyDir);
        GroovyScriptEngine gse = new GroovyScriptEngine(groovyDir);
        this.binding = new Binding();
        this.binding.setVariable("bulkLoadBean",
                bulkLoadBean);
        this.binding.setVariable("messages",
                messages);
        this.binding.setVariable("applicationContext",
                this.applicationContext);
        this.script = gse.createScript("BulkLoader.groovy",
                this.binding);
    }
}
