/*
 * Copyright 2009-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.gov.ga.oemd.plugin.geochemistry.batch;

import groovy.lang.Binding;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.binding.message.MessageContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

import au.gov.ga.oemd.dm.geochemistry.transfer.BulkLoadBean;
import au.gov.ga.oemd.framework.common.domain.XmlToStringBuilder;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.context.SystemContext;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

/**
 * {@link ItemReader} that reads a bulkload input file and returns it one job at
 * a time
 */
public class BulkLoadItemReader
        implements ItemReader<Object[]>, ApplicationContextAware, StepExecutionListener {

    private static final Logger log = LoggerFactory.getLogger(BulkLoadItemReader.class);

    private ApplicationContext applicationContext;

    private Binding binding;

    private Resource resource = null;

    private Script script;

    private StepExecution stepExecution;

    public BulkLoadItemReader() {
        super();
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        this.stepExecution = null;
        return null;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    /**
     * Reads next record from phase
     */
    @SuppressWarnings({ "unchecked" })
    @Override
    public synchronized Object[] read() throws Exception {
        Validate.notNull(this.stepExecution,
                "stepExecution was not injected. Was this configured as a step listener?");
        if (this.script == null) {
            Validate.notNull(this.resource);
            BulkLoadBean bulkLoadBean = new BulkLoadBean();
            bulkLoadBean.getRequest()
                    .setMaxProcessingTime(null); // no limit on execution time
            List<SimpleMessageBean> messages = new ArrayList<SimpleMessageBean>();
            this.openBulkLoad(bulkLoadBean,
                    null,
                    this.resource.getInputStream(),
                    this.resource.getFilename(),
                    messages);
            if (SimpleMessageBean.hasErrorMessages(messages)) {
                throw new IllegalArgumentException("Error processing input file "
                        + ValidationMessageAdapter.toString(messages,
                                true));
            }
            Object[] resp = (Object[]) this.script.invokeMethod("read",
                    new Object[] {});
            if (messages != null
                    && !messages.isEmpty()) {
                // add the messages to the step context so downstream processors can see it
                List<SimpleMessageBean> contextMessages = (List<SimpleMessageBean>) this.stepExecution.getExecutionContext()
                        .get("messages");
                if (contextMessages == null) {
                    contextMessages = messages;
                } else {
                    contextMessages.addAll(messages);
                }
                this.stepExecution.getExecutionContext()
                        .put("messages",
                                contextMessages);
            }
            return resp;

        } else {
            return (Object[]) this.script.invokeMethod("read",
                    new Object[] {});
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    protected void openBulkLoad(BulkLoadBean bulkLoadBean,
            MessageContext messageContext,
            InputStream inputStream,
            String filename,
            List<SimpleMessageBean> messages) throws IOException,
            ResourceException,
            ScriptException {
        String groovyDir = SystemContext.getInstance()
                .getConfigProperty("appDataDir")
                + "groovy"
                + File.separator;
        log.debug("loading groovy scripts from {}",
                groovyDir);
        GroovyScriptEngine gse = new GroovyScriptEngine(groovyDir);
        this.binding = new Binding();
        this.binding.setVariable("bulkLoadBean",
                bulkLoadBean);
        this.binding.setVariable("inputStream",
                inputStream);
        this.binding.setVariable("fileName",
                filename);
        this.binding.setVariable("messages",
                messages);
        this.binding.setVariable("messageContext",
                messageContext);
        this.binding.setVariable("applicationContext",
                this.applicationContext);
        this.script = gse.createScript("BulkLoader.groovy",
                this.binding);

        this.script.invokeMethod("init",
                new Object[] {});
        //this.script.run();
        //this.processFile(bulkLoadBean,fileName,inputStream,messages)
        this.script.invokeMethod("processFile",
                new Object[] {
                        bulkLoadBean,
                        filename,
                        inputStream,
                        messages });
        log.info(XmlToStringBuilder.getInstance()
                .toString(messages));
        //        log.info(XmlToStringBuilder.getInstance()
        //                .toString(this.binding.getVariable("messages")));
    }
}
