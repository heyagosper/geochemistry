/*
 * Copyright 2009-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.gov.ga.oemd.plugin.geochemistry.batch;

import groovy.lang.Binding;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.binding.message.MessageContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

import au.gov.ga.oemd.dm.geochemistry.transfer.BulkLoadBean;
import au.gov.ga.oemd.framework.common.domain.XmlToStringBuilder;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.context.SystemContext;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

/**
 * crude batch task that processes an entire file in one go
 * 
 */
public class BulkLoadTask
        implements Tasklet, ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(BulkLoadTask.class);

    private ApplicationContext applicationContext;

    private Binding binding;
    private Resource resource = null;
    private Script script;

    public BulkLoadTask() {
        super();
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
            ChunkContext chunkContext) throws Exception {
        if (this.resource == null) {
            return null; // discard once we have processed it
        }
        BulkLoadBean bulkLoadBean = new BulkLoadBean();
        bulkLoadBean.getRequest()
                .setMaxProcessingTime(null); // not limit on execution time
        List<SimpleMessageBean> messages = new ArrayList<SimpleMessageBean>();
        this.executeBulkLoad(bulkLoadBean,
                null,
                this.resource.getInputStream(),
                this.resource.getFilename(),
                messages);
        if (SimpleMessageBean.hasErrorMessages(messages)) {
            stepContribution.setExitStatus(new ExitStatus("FAILED",
                    ValidationMessageAdapter.toString(messages,
                            true)));
            throw new IllegalArgumentException("Error processing input file "
                    + messages);
        }
        stepContribution.setExitStatus(new ExitStatus("COMPLETED",
                ValidationMessageAdapter.toString(messages,
                        false)));

        @SuppressWarnings("unchecked")
        List<String[]> resp = (List<String[]>) this.script.invokeMethod("read",
                new Object[] {});
        if (log.isDebugEnabled()) {
            log.info(XmlToStringBuilder.getInstance()
                    .toString(resp));
        }
        log.info(XmlToStringBuilder.getInstance()
                .toString(this.binding.getVariable("messages")));

        return RepeatStatus.FINISHED;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    protected void executeBulkLoad(BulkLoadBean bulkLoadBean,
            MessageContext messageContext,
            InputStream inputStream,
            String filename,
            List<SimpleMessageBean> messages) throws IOException,
            ResourceException,
            ScriptException {
        String groovyDir = SystemContext.getInstance()
                .getConfigProperty("appDataDir")
                + "groovy"
                + File.separator;
        log.debug("loading groovy scripts from {}",
                groovyDir);
        GroovyScriptEngine gse = new GroovyScriptEngine(groovyDir);
        this.binding = new Binding();
        this.binding.setVariable("bulkLoadBean",
                bulkLoadBean);
        this.binding.setVariable("inputStream",
                inputStream);
        this.binding.setVariable("fileName",
                filename);
        this.binding.setVariable("messages",
                messages);
        this.binding.setVariable("messageContext",
                messageContext);
        this.binding.setVariable("applicationContext",
                this.applicationContext);
        this.script = gse.createScript("BulkLoader.groovy",
                this.binding);

        this.script.run();
        log.info(XmlToStringBuilder.getInstance()
                .toString(messages));
    }

}
