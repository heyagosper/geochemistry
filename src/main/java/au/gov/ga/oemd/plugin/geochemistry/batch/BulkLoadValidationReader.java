/*
 * Copyright 2009-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.gov.ga.oemd.plugin.geochemistry.batch;

import groovy.lang.Binding;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.binding.message.MessageContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

import au.gov.ga.oemd.dm.geochemistry.transfer.BulkLoadBean;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.context.SystemContext;

/**
 * {@link ItemReader} that reads a bulkload input file and returns a list of
 * validation messages
 */
public class BulkLoadValidationReader
        implements ItemReader<List<SimpleMessageBean>>, ApplicationContextAware {

    private static final Logger log = LoggerFactory.getLogger(BulkLoadValidationReader.class);

    private ApplicationContext applicationContext;

    private Binding binding;

    private Resource resource = null;

    private Script script;

    public BulkLoadValidationReader() {
        super();
    }

    /**
     * Reads next record (and there will only be one)
     */
    @Override
    public synchronized List<SimpleMessageBean> read() throws Exception {
        if (this.script == null) {
            Validate.notNull(this.resource);
            BulkLoadBean bulkLoadBean = new BulkLoadBean();
            bulkLoadBean.getRequest()
                    .setValidateOnly(true);
            bulkLoadBean.getRequest()
                    .setStopOnFirstError(false);
            bulkLoadBean.getRequest()
                    .setMaxProcessingTime(null); // no limit on execution time
            List<SimpleMessageBean> messages = new ArrayList<SimpleMessageBean>();
            this.openBulkLoad(bulkLoadBean,
                    null,
                    this.resource.getInputStream(),
                    this.resource.getFilename(),
                    messages);
            return messages;
        } else {
            return null; // supposed to be one iteration only
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    protected void openBulkLoad(BulkLoadBean bulkLoadBean,
            MessageContext messageContext,
            InputStream inputStream,
            String filename,
            List<SimpleMessageBean> messages) throws IOException,
            ResourceException,
            ScriptException {
        String groovyDir = SystemContext.getInstance()
                .getConfigProperty("appDataDir")
                + "groovy"
                + File.separator;
        log.debug("loading groovy scripts from {}",
                groovyDir);
        GroovyScriptEngine gse = new GroovyScriptEngine(groovyDir);
        this.binding = new Binding();
        this.binding.setVariable("bulkLoadBean",
                bulkLoadBean);
        this.binding.setVariable("inputStream",
                inputStream);
        this.binding.setVariable("fileName",
                filename);
        this.binding.setVariable("messages",
                messages);
        this.binding.setVariable("messageContext",
                messageContext);
        this.binding.setVariable("applicationContext",
                this.applicationContext);
        this.script = gse.createScript("BulkLoader.groovy",
                this.binding);

        this.script.run();
    }
}
