package au.gov.ga.oemd.plugin.geochemistry.controller;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.dm.geochemistry.transfer.BulkJobActionBean;
import au.gov.ga.oemd.framework.common.domain.XmlToStringBuilder;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.context.SecurityContextHelper;
import au.gov.ga.oemd.framework.context.SystemContext;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Controller("bulkJobActionController")
public class BulkJobActionController {
    private static final Logger log = LoggerFactory.getLogger(BulkJobActionController.class);

    @Autowired(required = true)
    ApplicationContext applicationContext;
    @Autowired(required = true)
    GeochemistryService appService;
    @Autowired(required = true)
    ValidationMessageAdapter validationMessageAdapter;
    @Autowired(required = true)
    JobLauncher jobLauncher;

    @Autowired(required = true)
    JobRegistry jobRegistry;

    public BulkJobActionController() {
        super();
    }

    /*
     * note: there's no real reason to do this in groovy at this stage other
     * than hedging bets that complications could occur (as they are quicker to
     * fix)
     */
    @Transactional
    @Profiled
    public Boolean processAction(BulkJobActionBean bulkJobActionBean,
            RequestContext context,
            MessageContext messageContext) {
        if (bulkJobActionBean.getRequest()
                .isValidateOnly()) {
            TransactionAspectSupport.currentTransactionStatus()
                    .setRollbackOnly(); // ensure we don't even get accidental updates
        }
        List<SimpleMessageBean> messages = new ArrayList<SimpleMessageBean>();
        Validate.notNull(bulkJobActionBean);

        try {
            if (bulkJobActionBean.getRequest()
                    .isProcessModeAsync()) {
                this.submitBackgroundLoad(bulkJobActionBean);
                messages.add(new SimpleMessageBean("The request has been submitted for background processing").info());
            } else {
                execForegroundAction(bulkJobActionBean,
                        messageContext,
                        messages);
            }
        } catch (Throwable e) {
            TransactionAspectSupport.currentTransactionStatus()
                    .setRollbackOnly(); // ensure we back everything out (except the sequences)
            log.error("Exception caught in processAction",
                    e);
            this.validationMessageAdapter.addMessages(messages,
                    messageContext);
            return Boolean.FALSE;
        }
        this.validationMessageAdapter.addMessages(messages,
                messageContext);
        if (messageContext.hasErrorMessages()) {
            context.getFlowScope()
                    .put("isValidBulkJobAction",
                            Boolean.FALSE);
            return Boolean.FALSE;
        }
        context.getFlowScope()
                .put("isValidBulkJobAction",
                        Boolean.TRUE);
        return Boolean.TRUE; // if we found a file we consider it valid even if there are messages displayed 
    }

    protected void execForegroundAction(BulkJobActionBean bulkJobActionBean,
            MessageContext messageContext,
            List<SimpleMessageBean> messages) throws IOException,
            ResourceException,
            ScriptException {
        String groovyDir = SystemContext.getInstance()
                .getConfigProperty("appDataDir")
                + "groovy"
                + File.separator;
        log.debug("loading groovy scripts from {}",
                groovyDir);
        GroovyScriptEngine gse = new GroovyScriptEngine(groovyDir);

        Binding binding = new Binding();
        binding.setVariable("bulkJobActionBean",
                bulkJobActionBean);
        binding.setVariable("messages",
                messages);
        binding.setVariable("messageContext",
                messageContext);
        binding.setVariable("applicationContext",
                this.applicationContext);
        gse.run("BulkJobActioner.groovy",
                binding);
        log.info(XmlToStringBuilder.getInstance()
                .toString(binding.getVariable("messages")));
    }

    protected void submitBackgroundLoad(BulkJobActionBean bulkJobActionBean) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
        String filename = "file:///bulkaction/"
                + SecurityContextHelper.getCurrentUser()
                        .getUsername()
                + "_"
                + df.format(new Date());
        JobParameters jobParameters = new JobParametersBuilder()//
        .addLong("fromJobNo",
                bulkJobActionBean.getRequest()
                        .getFromJobNo())
                .addLong("toJobNo",
                        bulkJobActionBean.getRequest()
                                .getToJobNo())
                .addLong("fromJobNo",
                        bulkJobActionBean.getRequest()
                                .getFromJobNo())
                .addString("allEligibleJobs",
                        String.valueOf(bulkJobActionBean.getRequest()
                                .isAllEligibleJobs()))
                .addString("actionCompleteLabAnalyses",
                        String.valueOf(bulkJobActionBean.getRequest()
                                .isActionCompleteLabAnalyses()))
                .addString("actionCompleteLabQA",
                        String.valueOf(bulkJobActionBean.getRequest()
                                .isActionCompleteLabQA()))
                .addString("actionCompleteClientApproval",
                        String.valueOf(bulkJobActionBean.getRequest()
                                .isActionCompleteClientApproval()))
                .addString("actionFinalise",
                        String.valueOf(bulkJobActionBean.getRequest()
                                .isActionFinalise()))
                .addDate("actionDateTime",
                        bulkJobActionBean.getRequest()
                                .getActionDateTime())
                .addString("report.file",
                        filename
                                + "_report.csv")
                //
                .toJobParameters();

        Job job;
        try {
            job = this.jobRegistry.getJob("bulkaction");
        } catch (NoSuchJobException e) {
            log.error("NoSuchJobException caught in submitBackgroundLoad",
                    e);
            throw new RuntimeException("no job defined");
        }
        if (job.getJobParametersIncrementer() != null) {
            jobParameters = job.getJobParametersIncrementer()
                    .getNext(jobParameters);
        }
        try {
            jobLauncher.run(job,
                    jobParameters);
        } catch (JobExecutionAlreadyRunningException e) {
            log.error("JobExecutionAlreadyRunningException caught in submitBackgroundLoad",
                    e);
            throw new RuntimeException(e);
        } catch (JobRestartException e) {
            log.error("JobRestartException caught in submitBackgroundLoad",
                    e);
            throw new RuntimeException(e);
        } catch (JobInstanceAlreadyCompleteException e) {
            log.error("JobInstanceAlreadyCompleteException caught in submitBackgroundLoad",
                    e);
            throw new RuntimeException(e);
        } catch (JobParametersInvalidException e) {
            log.error("JobParametersInvalidException caught in submitBackgroundLoad",
                    e);
            throw new RuntimeException(e);
        }
    }
}
