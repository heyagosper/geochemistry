package au.gov.ga.oemd.plugin.geochemistry.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.webflow.execution.RequestContext;

import au.gov.ga.oemd.dm.geochemistry.service.BulkLoadService;
import au.gov.ga.oemd.dm.geochemistry.transfer.BulkLoadBean;
import au.gov.ga.oemd.framework.common.service.validation.SimpleMessageBean;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Controller("bulkLoadController")
public class BulkLoadController {
    private static final Logger log = LoggerFactory.getLogger(BulkLoadController.class);

    @Autowired
    BulkLoadService bulkLoadService;

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    public BulkLoadController() {
        super();
    }

    @Profiled
    public Boolean processUpload(BulkLoadBean bulkLoadBean,
            RequestContext context,
            MessageContext messageContext) {
        MultipartFile file = context.getRequestParameters()
                .getRequiredMultipartFile("file");

        List<SimpleMessageBean> messages = new ArrayList<SimpleMessageBean>();
        if (file != null
                && file.getSize() > 0) {
            Validate.notNull(bulkLoadBean);
            String originalFilename = file.getOriginalFilename();

            try {
                InputStream inputStream = file.getInputStream();
                if (bulkLoadBean.getRequest()
                        .isProcessModeAsync()) {
                    this.bulkLoadService.submitBackgroundLoad(bulkLoadBean,
                            file,
                            originalFilename);
                    messages.add(new SimpleMessageBean("The file: "
                            + file.getName()
                            + " has been submitted for background processing").info());
                } else {
                    this.bulkLoadService.execForegroundLoad(bulkLoadBean,
                            inputStream,
                            originalFilename,
                            messages);
                }
            } catch (Throwable e) {
                log.error("Exception caught in processUpload",
                        e);
                messages.add(new SimpleMessageBean("The file: "
                        + file.getName()
                        + " was invalid and could not be processed due to:\n"
                        + e).error());
                this.validationMessageAdapter.addMessages(messages,
                        messageContext);
                return Boolean.FALSE;
            }
            this.validationMessageAdapter.addMessages(messages,
                    messageContext);
            if (messageContext.hasErrorMessages()) {
                context.getFlowScope()
                        .put("isValidBulkLoad",
                                Boolean.FALSE);
                return Boolean.FALSE;
            }
            context.getFlowScope()
                    .put("isValidBulkLoad",
                            Boolean.TRUE);
            context.getFlowScope()
                    .put("fileUploaded",
                            Boolean.TRUE);
            return Boolean.TRUE; // if we found a file we consider it valid even if there are messages displayed 
        } else {
            String msg = "The file to be uploaded was missing or empty. Please load a valid file.";
            messages.add(new SimpleMessageBean(msg).error());
            this.validationMessageAdapter.addMessages(messages,
                    messageContext);
            return Boolean.FALSE;
        }
    }

    @Autowired
    public void setBulkLoadService(BulkLoadService bulkLoadService) {
        this.bulkLoadService = bulkLoadService;
    }

}
