package au.gov.ga.oemd.plugin.geochemistry.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.gov.ga.oemd.dm.geochemistry.domain.Docfile;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.framework.controller.FileHelper;

@Controller
public class DocfileController {

    @Autowired
    private GeochemistryService appService;

    @Autowired
    private FileHelper fileHelper;

    public DocfileController() {
        super();
    }

    /**
     * retrieve a Docfile
     * 
     * @param docfileNo
     * @param model
     * @return
     */
    @RequestMapping(value = "/docfiles/{docfileNo}/download", method = RequestMethod.GET)
    public String getDocfile(@PathVariable Integer docfileNo,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {
        Docfile docfile = this.appService.findDocfileById(docfileNo, true);
        model.addAttribute("docfile",
                docfile);
        this.fileHelper.streamFile(response,
                docfile);

        return null;
    }
}