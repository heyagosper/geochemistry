package au.gov.ga.oemd.plugin.geochemistry.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.geochemistry.domain.Analyst;
import au.gov.ga.oemd.dm.geochemistry.domain.Instrument;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatch;
import au.gov.ga.oemd.dm.geochemistry.domain.KnownReferenceResult;
import au.gov.ga.oemd.dm.geochemistry.domain.Laboratory;
import au.gov.ga.oemd.dm.geochemistry.domain.LocationCode;
import au.gov.ga.oemd.dm.geochemistry.domain.Method;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodInstrument;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodProperty;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodReferenceMaterial;
import au.gov.ga.oemd.dm.geochemistry.domain.Property;
import au.gov.ga.oemd.dm.geochemistry.domain.Property.PropertyGroupCode;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceMaterial;
import au.gov.ga.oemd.dm.geochemistry.domain.Technique;
import au.gov.ga.oemd.dm.geochemistry.domain.TechniqueInstrument;
import au.gov.ga.oemd.dm.geochemistry.domain.TechniqueLaboratory;
import au.gov.ga.oemd.dm.geochemistry.domain.Unit;
import au.gov.ga.oemd.dm.geochemistry.domain.UnitGroup;
import au.gov.ga.oemd.dm.geochemistry.domain.UnitGroupLink;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.dm.geochemistry.service.search.JobBatchSampleSearchCriteria;
import au.gov.ga.oemd.framework.common.domain.ActivationStatusCode;
import au.gov.ga.oemd.framework.common.domain.CompletionStatusCode;
import au.gov.ga.oemd.framework.common.domain.EntryTypeCode;
import au.gov.ga.oemd.framework.controller.ReferenceDataHelper;
import au.gov.ga.oemd.framework.faces.convert.SelectItemConverter;

@Component("referenceData")
public class ReferenceData extends ReferenceDataHelper
        implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private GeochemistryService appService;

    public ReferenceData() {
    }

    public Laboratory findLaboratoryByCode(String code) {
        return this.appService.findLaboratoryByCode(code);
    }

    public Method findMethodByCode(String code) {
        return this.appService.findMethodByCode(code);
    }

    public Method findMethodById(Integer id) {
        return this.appService.findMethodById(id);
    }

    public Technique findTechniqueByCode(String code) {
        return this.appService.findTechniqueByCode(code);
    }

    public Technique findTechniqueById(Integer id) {
        return this.appService.findTechniqueById(id);
    }

    public List<SelectItem> getActivationStatusCodeSelectionList() {
        return this.asSelectionList(ActivationStatusCode.values());
    }

    public List<SelectItem> getActiveAnalysisLabSelectionList() {
        return this.getActiveAnalysisLabSelectionList(null);
    }

    public List<SelectItem> getActiveAnalysisLabSelectionList(Laboratory currentLab) {
        return this.asSelectionList(this.appService.findActiveAnalysisLabs(),
                currentLab);
    }

    public List<SelectItem> getActiveAnalystSelectionList() {
        return this.getActiveAnalystSelectionList((Analyst) null);
    }

    @SuppressWarnings("unchecked")
    public List<SelectItem> getActiveAnalystSelectionList(Analyst currentAnalyst) {
        return this.asSelectionList((List<Analyst>) this.selectActive(this.appService.selectAnalystAll()),
                currentAnalyst);
    }

    @SuppressWarnings("unchecked")
    public List<SelectItem> getActiveAnalystSelectionList(List<Analyst> currentAnalysts) {
        return this.asSelectionList((List<Analyst>) this.selectActive(this.appService.selectAnalystAll()),
                currentAnalysts);
    }

    public List<Instrument> getActiveInstrumentList(Laboratory laboratory,
            Technique technique,
            Method method) {
        List<Instrument> res = new ArrayList<Instrument>();
        if (technique == null) {
            // then no instruments
        } else if (laboratory == null) {
            // then no instruments
        } else if (method == null) {
            return technique.getActiveInstrumentList(laboratory);
        } else {
            return method.getActiveInstrumentList(laboratory);
        }
        return res;
    }
    
    public List<SelectItem> getActiveMethodSelectionList(Laboratory laboratory,
            Technique technique,
            Method currentMethod) {
        return this.asSelectionList(technique == null ? null : technique.getActiveMethodList(laboratory),
                currentMethod);
    }

    /**
     * An overloaded version of getActiveMethodSelectionList() for filtering on
     * the create batch screen. It searches based on related job batches.
     * 
     * @param laboratory
     * @param technique
     * @param currentMethod
     * @param includedJobBatches
     * @return a list of selectable methods
     */
    public List<SelectItem> getActiveMethodSelectionList(Laboratory laboratory,
            Technique technique,
            Method currentMethod,
            List<String> includedJobBatches) {
        if (includedJobBatches == null) {
            return this.getActiveMethodSelectionList(laboratory,
                    technique, 
                    currentMethod);
        } else { 
            if (includedJobBatches.isEmpty()) {
                return this.getActiveMethodSelectionList(laboratory,
                        technique, 
                        currentMethod);
            } else {
                List<JobBatch> jobBatches = this.appService.getJobBatches(includedJobBatches, 
                        true, 
                        false);
        
                List<Method> methodList = new ArrayList<Method>();
                if (jobBatches != null) {
                    for (JobBatch jb : jobBatches) {
                        if (jb != null) {
                            methodList.addAll(jb.getMethodList());
                        }
                    }
                }
                    
                if (!methodList.isEmpty()) {
                    return this.asSelectionList(technique == null ? null : technique.getActiveMethodList(laboratory,
                            methodList),
                            currentMethod);
                } else {
                    return this.asSelectionList(technique == null ? null : technique.getActiveMethodList(laboratory),
                            currentMethod);
                }
            }
        }
    }

    public List<SelectItem> getActiveMethodSelectionList(Technique technique) {
        return this.getActiveMethodSelectionList(null,
                technique,
                null);
    }

    public List<SelectItem> getActivePreparationLabSelectionList() {
        return this.asSelectionList(this.appService.findActivePreparationLabs());
    }

    public List<SelectItem> getActivePreparationLabSelectionList(Technique technique) {
        // note we are ignoring the technique as we don't (yet?) have a technique-lab mapping
        return this.asSelectionList(this.appService.findActivePreparationLabs());
    }

    public List<SelectItem> getActiveTechniqueSelectionList() {
        return this.getActiveTechniqueSelectionList(null,
                null);
    }

    public List<SelectItem> getActiveTechniqueSelectionList(Laboratory laboratory,
            Technique currentTechnique) {
        if (laboratory == null) {
            return this.asSelectionList(this.appService.findActiveTechniques());
        } else {
            if (currentTechnique != null
                    && !currentTechnique.isPractisedAt(laboratory)) {
                currentTechnique = null;
            }
            return this.asSelectionList(laboratory.getActiveTechniqueList(),
                    currentTechnique);
        }
    }

    /**
     * An overloaded version of getActiveTechniqueSelectionList() for filtering
     * on the create batch screen. It searches based on related job batches.
     * 
     * @param laboratory
     * @param currentTechnique
     * @param includedJobBatches
     * @return a list of selectable techniques
     */
    public List<SelectItem> getActiveTechniqueSelectionList(Laboratory laboratory,
            Technique currentTechnique,
            List<String> includedJobBatches) {
        if (includedJobBatches == null) {
            return this.getActiveTechniqueSelectionList(laboratory,
                    currentTechnique);
        } else { 
            if (includedJobBatches.isEmpty()) {
                return this.getActiveTechniqueSelectionList(laboratory,
                        currentTechnique);
            }else {
                List<JobBatch> jobBatches = this.appService.getJobBatches(includedJobBatches,
                        true,
                        false);
                
                if (laboratory == null) {
                    return this.asSelectionList(this.appService.findActiveTechniques());
                } else {
                    if (currentTechnique != null
                            && !currentTechnique.isPractisedAt(laboratory)) {
                        currentTechnique = null;
                    }
        
                    List<Technique> techniqueList = new ArrayList<Technique>();
                    if (jobBatches != null) {
                        for (JobBatch jb : jobBatches) {
                            if (jb != null) {
                                for (Method m : jb.getMethodList()) {
                                    if (!techniqueList.contains(m.getTechnique())){
                                        techniqueList.add(m.getTechnique());
                                    }
                                }
                            }
                        }
                    }
                    if (techniqueList.isEmpty()) {
                        techniqueList = laboratory.getActiveTechniqueList();
                    }
                    
                    return this.asSelectionList(techniqueList,
                            currentTechnique);
                }
            }
        }
    }

    public List<SelectItem> getAllInstrumentsSelectionList() {
        return this.asSelectionList(this.appService.findAllInstruments());
    }

    public List<SelectItem> getAllMethodsSelectionList() {
        return this.asSelectionList(this.appService.findAllMethods());
    }

    public List<SelectItem> getAllReferenceMaterialsSelectionList() {
        return this.asSelectionList(this.appService.findAllReferenceMaterials());
    }

    public List<SelectItem> getAnalysisLabSelectionList() {
        return this.asSelectionList(this.appService.findAllAnalysisLabs());
    }

    public List<SelectItem> getAnalysisLabSelectionList(Technique technique) {
        // note we are ignoring the technique as we don't (yet?) have a technique-lab mapping
        return this.asSelectionList(this.appService.findAllAnalysisLabs());
    }

    public List<SelectItem> getAnalystSelectionList() {
        return this.asSelectionList(this.appService.selectAnalystAll());
    }

    public List<SelectItem> getCompletionStatusCodeSelectionList() {
        return this.asSelectionList(CompletionStatusCode.values());
    }

    public List<SelectItem> getConcentrationMeasurementUnitSelectionList() {
        return this.asSelectionList(this.appService.findAllConcentrationMeasureUnits(),
                new SelectItemConverter() {
                    @Override
                    public SelectItem convert(Object value) {
                        UnitGroupLink item = (UnitGroupLink) value;
                        return new SelectItem(item.getUnit()
                                .getUnitId(),
                                item.getUnit()
                                        .getUnitId());
                    }
                });
    }

    public Laboratory getDefaultAnalysisLab() {
        return this.appService.getDefaultAnalysisLab();
    }

    public Laboratory getDefaultPreparationLab() {
        return this.appService.getDefaultPreparationLab();
    }

    public List<SelectItem> getEntryTypeCodeSelectionList() {
        return this.asSelectionList(EntryTypeCode.values());
    }

    public Laboratory getGeochemLab() {
        return this.appService.getGeochemLab();
    }

    public Technique getGravimetryTechnique() {
        return this.appService.getGravimetryTechnique();
    }

    public Technique getIcpmsTechnique() {
        return this.appService.getIcpmsTechnique();
    }

    public List<SelectItem> getInstrumentSelectionList(Technique technique) {
        return this.asSelectionList(technique == null ? null : technique.getInstrumentList());
    }

    public List<SelectItem> getInstrumentSelectionListByMethod(Method method) {
        return this.asSelectionList(method == null ? null : method.getInstrumentList());
    }

    public List<SelectItem> getIntegerSelectionList() {
        List<SelectItem> list = new ArrayList<SelectItem>();
        for (int i = 1; i <= 10; i++) {
            list.add(new SelectItem(Integer.valueOf(i),
                    String.valueOf(i)));
        }
        return list;
    }

    public List<SelectItem> getJobBatchSampleSearchResultTypeSelectionList() {
        return this.asRadioSelectionList(JobBatchSampleSearchCriteria.ResultTypeCode.values());
    }

    public List<SelectItem> getKnownReferenceResultSelectionList(List<KnownReferenceResult> knownReferenceResultList) {
        return this.asSelectionList(knownReferenceResultList,
                new SelectItemConverter() {
                    @Override
                    public SelectItem convert(Object value) {
                        KnownReferenceResult item = (KnownReferenceResult) value;
                        return new SelectItem(item,
                                item.getResultValue() != null ? item.getResultValue()
                                        .toPlainString() : "");
                    }
                });
    }

    public List<SelectItem> getLaboratoryLocationCodeSelectionList() {
        return this.asSelectionList(LocationCode.values());
    }

    public Analyst getLegacyAnalyst() {
        return this.appService.getLegacyAnalyst();
    }

    public List<SelectItem> getMeasurementUnitSelectionList() {
        return this.asSelectionList(this.appService.findAllUnits(),
                new SelectItemConverter() {
                    @Override
                    public SelectItem convert(Object value) {
                        Unit item = (Unit) value;
                        return new SelectItem(item.getUnitId(),
                                item.getUnitId());
                    }
                });
    }

    public List<MethodInstrument> getMethodInstrumentList(Method method) {
        List<MethodInstrument> methodInstrumentList = new ArrayList<MethodInstrument>();
        methodInstrumentList.addAll(method.getMethodInstrumentList());
        List<Instrument> instrumentList = this.appService.findAllInstruments();
        for (Instrument instrument : instrumentList) {
            MethodInstrument methodInstrument = new MethodInstrument(instrument,
                    method);
            if (!methodInstrumentList.contains(methodInstrument)) {
                methodInstrumentList.add(methodInstrument);
            }
        }
        Collections.sort(methodInstrumentList);
        return methodInstrumentList;
    }

    public List<MethodProperty> getMethodPropertyList(Method method) {
        List<MethodProperty> methodPropertyList = new ArrayList<MethodProperty>();
        methodPropertyList.addAll(method.getMethodPropertyList());
        List<Property> propertyList = this.appService.findAllProperties();
        for (Property property : propertyList) {
            MethodProperty methodProperty = new MethodProperty(method,
                    property);
            if (!methodPropertyList.contains(methodProperty)) {
                methodPropertyList.add(methodProperty);
            }
        }
        Collections.sort(methodPropertyList);
        return methodPropertyList;
    }

    public List<MethodReferenceMaterial> getMethodReferenceMaterialList(Method method) {
        List<MethodReferenceMaterial> methodReferenceMaterialList = new ArrayList<MethodReferenceMaterial>();
        methodReferenceMaterialList.addAll(method.getMethodReferenceMaterialList());
        List<ReferenceMaterial> referenceMaterialList = this.appService.findAllReferenceMaterials();
        for (ReferenceMaterial referenceMaterial : referenceMaterialList) {
            MethodReferenceMaterial methodReferenceMaterial = new MethodReferenceMaterial(method,
                    referenceMaterial);
            if (!methodReferenceMaterialList.contains(methodReferenceMaterial)) {
                methodReferenceMaterialList.add(methodReferenceMaterial);
            }
        }
        Collections.sort(methodReferenceMaterialList);
        return methodReferenceMaterialList;
    }

    public List<SelectItem> getMethodSelectionList(Technique technique) {
        return this.asSelectionList(technique == null ? null : technique.getMethodList());
    }

    public List<SelectItem> getPreparationLabSelectionList(Technique technique) {
        // note we are ignoring the technique as we don't (yet?) have a technique-lab mapping
        return this.asSelectionList(this.appService.findAllPreparationLabs());
    }

    public List<SelectItem> getPropertyGroupCodeSelectionList() {
        return this.asSelectionList(PropertyGroupCode.values());
    }

    //    // should be public List<SelectItem> getPropertySelectionList(Method method) {
    //    public List<SelectItem> getPropertySelectionListByMethod(Method method) {
    //        return this.asSelectionList(method == null ? null : method.getPropertyList());
    //    }

    public List<SelectItem> getPropertyIdSelectionList() {
        return this.asSelectionList(this.appService.findAllProperties());
    }

    public List<ReferenceMaterial> getReferenceMaterialList(Method method) {
        List<ReferenceMaterial> referenceMaterialList = new ArrayList<ReferenceMaterial>();
        if (method != null) {
            for (MethodReferenceMaterial methodReferenceMaterial : method.getMethodReferenceMaterialList()) {
                referenceMaterialList.add(methodReferenceMaterial.getReferenceMaterial());
            }
        }
        return referenceMaterialList;
    }

    public List<TechniqueInstrument> getTechniqueInstrumentList(Instrument instrument) {
        List<TechniqueInstrument> techniqueInstrumentList = new ArrayList<TechniqueInstrument>();
        techniqueInstrumentList.addAll(instrument.getTechniqueInstrumentList());
        for (Technique technique : this.appService.findAllTechniques()) {
            TechniqueInstrument techniqueInstrument = new TechniqueInstrument(technique,
                    instrument);
            if (!techniqueInstrumentList.contains(techniqueInstrument)) {
                techniqueInstrumentList.add(techniqueInstrument);
            }
        }
        Collections.sort(techniqueInstrumentList);
        return techniqueInstrumentList;
    }

    public List<TechniqueInstrument> getTechniqueInstrumentList(Technique technique) {
        List<TechniqueInstrument> techniqueInstrumentList = new ArrayList<TechniqueInstrument>();
        techniqueInstrumentList.addAll(technique.getTechniqueInstrumentList());
        for (Instrument instrument : this.appService.findAllInstruments()) {
            TechniqueInstrument techniqueInstrument = new TechniqueInstrument(technique,
                    instrument);
            if (!techniqueInstrumentList.contains(techniqueInstrument)) {
                techniqueInstrumentList.add(techniqueInstrument);
            }
        }
        Collections.sort(techniqueInstrumentList);
        return techniqueInstrumentList;
    }

    public List<TechniqueLaboratory> getTechniqueLaboratoryList(Laboratory laboratory) {
        List<TechniqueLaboratory> techniqueLaboratoryList = new ArrayList<TechniqueLaboratory>();
        techniqueLaboratoryList.addAll(laboratory.getTechniqueLaboratoryList());
        for (Technique technique : this.appService.findAllTechniques()) {
            TechniqueLaboratory techniqueLaboratory = new TechniqueLaboratory(technique,
                    laboratory);
            if (!techniqueLaboratoryList.contains(techniqueLaboratory)) {
                techniqueLaboratoryList.add(techniqueLaboratory);
            }
        }
        Collections.sort(techniqueLaboratoryList);
        return techniqueLaboratoryList;
    }

    public List<SelectItem> getTechniqueSelectionList() {
        return this.asSelectionList(this.appService.findAllTechniques());
    }

    public Technique getTitrationTechnique() {
        return this.appService.getTitrationTechnique();
    }

    public List<SelectItem> getUncertaintyTypeCodeList() {
        return this.asSelectionList(this.appService.findAllUncertaintyTypes());
    }

    public List<SelectItem> getUnitGroupIdSelectionList() {
        return this.asSelectionList(this.appService.findAllUnitGroups(),
                new SelectItemConverter() {
                    @Override
                    public SelectItem convert(Object value) {
                        UnitGroup item = (UnitGroup) value;
                        return new SelectItem(item.getUnitGroupId(),
                                item.getGroupName());
                    }
                });
    }

    public List<UnitGroupLink> getUnitGroupLinkList(UnitGroup unitGroup) {
        List<UnitGroupLink> unitGroupLinkList = new ArrayList<UnitGroupLink>();
        unitGroupLinkList.addAll(unitGroup.getUnitGroupLinkList());
        for (Unit unit : this.appService.findAllUnits()) {
            UnitGroupLink unitGroupLink = new UnitGroupLink(unitGroup,
                    unit);
            if (!unitGroupLinkList.contains(unitGroupLink)) {
                unitGroupLinkList.add(unitGroupLink);
            }
        }
        Collections.sort(unitGroupLinkList);
        return unitGroupLinkList;
    }

    public List<SelectItem> getUnitGroupSelectionList() {
        return this.asSelectionList(this.appService.findAllUnitGroups());
    }

    public List<SelectItem> getUnitIdSelectionList() {
        //        return this.asSelectionList(this.appService.findAllUnits());
        //    }

        List<Unit> unitList = this.appService.findAllUnits();
        List<SelectItem> unitIds = new ArrayList<SelectItem>();
        for (Unit unit : unitList) {
            unitIds.add(new SelectItem(unit.getUnitId(),
                    unit.getUnitId()));
        }
        this.sort(unitIds);
        return unitIds;
    }

    public List<SelectItem> getUnitIdSelectionList(String id) {
        List<SelectItem> selectionList = new ArrayList<SelectItem>();
        List<UnitGroupLink> itemList = new ArrayList<UnitGroupLink>();
        UnitGroup unitGroup = this.appService.findUnitGroupById(id);
        itemList.addAll(unitGroup.getUnitGroupLinkList());
        for (UnitGroupLink item : itemList) {
            selectionList.add(new SelectItem(item.getUnit()
                    .getUnitId(),
                    item.getUnit()
                            .getUnitId()));
        }
        return selectionList;
    }

    public List<SelectItem> getUnitSelectionList() {
        List<SelectItem> selectionList = new ArrayList<SelectItem>();
        List<UnitGroup> itemList = this.appService.findAllUnitGroups();
        for (UnitGroup item : itemList) {
            selectionList.add(new SelectItem(item,
                    item.getGroupName()));
        }
        return selectionList;
    }
    public Technique getXrfTechnique() {
        return this.appService.getXrfTechnique();
    }

}
