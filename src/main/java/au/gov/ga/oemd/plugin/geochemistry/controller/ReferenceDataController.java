package au.gov.ga.oemd.plugin.geochemistry.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.gov.ga.oemd.dm.geochemistry.domain.Instrument;
import au.gov.ga.oemd.dm.geochemistry.domain.Method;
import au.gov.ga.oemd.dm.geochemistry.domain.Technique;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;

@Controller
public class ReferenceDataController {

    @Autowired
    private GeochemistryService appService;
    @SuppressWarnings("unused")
    @Autowired
    private ReferenceData referenceData;

    public ReferenceDataController() {
        super();
    }

    /**
     * 
     * @param instrumentNo
     * @param model
     * @return
     */
    @RequestMapping(value = "/instruments/{instrumentNo}/details", method = RequestMethod.GET)
    public String getInstrument(@PathVariable Integer instrumentNo,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {
        Instrument instrument = this.appService.findInstrumentById(instrumentNo);
        model.addAttribute("instrument",
                instrument);
        return "flows/geochemistry/admin/instruments/viewInstrument/viewInstrumentDetails-view";
    }

    /**
     * 
     * @param methodNo
     * @param model
     * @return
     */
    @RequestMapping(value = "/methods/{methodNo}/details", method = RequestMethod.GET)
    public String getMethod(@PathVariable Integer methodNo,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {
        Method method = this.appService.findMethodById(methodNo);
        model.addAttribute("method",
                method);
        model.addAttribute("technique",
                method.getTechnique());
        return "flows/geochemistry/admin/methods/viewMethod/viewMethodDetails-view";
    }

    /**
     * 
     * @param techniqueNo
     * @param model
     * @return
     */
    @RequestMapping(value = "/techniques/{techniqueNo}/details", method = RequestMethod.GET)
    public String getTechnique(@PathVariable Integer techniqueNo,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {
        Technique technique = this.appService.findTechniqueById(techniqueNo);
        model.addAttribute("technique",
                technique);
        return "flows/geochemistry/admin/techniques/viewTechnique/viewTechnique-panel";
    }
}