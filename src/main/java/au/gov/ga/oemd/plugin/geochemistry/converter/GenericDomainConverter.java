package au.gov.ga.oemd.plugin.geochemistry.converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.common.service.ReferenceDataService;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.framework.faces.convert.AbstractGenericDomainConverter;

/**
 * @author u19176 This class is a generic domain object converter for use in the
 *         Raging Spot geochem-sa-web project
 */
@Component
public class GenericDomainConverter extends AbstractGenericDomainConverter {

    @Autowired
    private GeochemistryService appService;

    @Autowired
    private ReferenceDataService referenceDataService;

    @PostConstruct
    public void initialize() {
        List<Object> servicesList = new ArrayList<Object>();
        servicesList.add(this.appService);
        servicesList.add(this.referenceDataService);
        this.setFinderServicesList(servicesList);
    }

    @Override
    protected Object getFinderService() {
        return this.appService;
    }

}
