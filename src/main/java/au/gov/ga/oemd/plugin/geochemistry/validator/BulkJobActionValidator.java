package au.gov.ga.oemd.plugin.geochemistry.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.geochemistry.transfer.BulkJobActionBean;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Component
public class BulkJobActionValidator {

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    private Validator validator;

    public boolean validate(BulkJobActionBean bulkJobActionBean,
            MessageContext messageContext) {
        Validate.notNull(bulkJobActionBean);
        Validate.notNull(bulkJobActionBean.getRequest());
        int jobSelectionType = 0;
        if (bulkJobActionBean.getRequest()
                .getFromJobNo() != null) {
            jobSelectionType++;
        }

        if (bulkJobActionBean.getRequest()
                .isAllEligibleJobs() == true) {
            jobSelectionType++;
        }
        if (!bulkJobActionBean.getRequest()
                .getJobNoList()
                .isEmpty()) {
            jobSelectionType++;
        }
        if (jobSelectionType == 0) {
            this.validationMessageAdapter.addMessage("validator.bulkJobAction.noJobs",
                    messageContext);
        } else if (jobSelectionType > 1) {
            this.validationMessageAdapter.addMessage("validator.bulkJobAction.multiJobs",
                    messageContext);
        }
        if (!bulkJobActionBean.getRequest()
                .isActionCompleteLabAnalyses()
                && !bulkJobActionBean.getRequest()
                        .isActionCompleteLabQA()
                && !bulkJobActionBean.getRequest()
                        .isActionCompleteClientApproval()
                && !bulkJobActionBean.getRequest()
                        .isActionFinalise()) {
            this.validationMessageAdapter.addMessage("validator.bulkJobAction.noAction",
                    messageContext);
        }

        this.addMessages(this.validator.validate(bulkJobActionBean),
                messageContext,
                "validator.bulkJobAction.");

        return !messageContext.hasErrorMessages();
    }

    public boolean validateAfter(BulkJobActionBean bulkJobActionBean,
            MessageContext messageContext) {
        Validate.notNull(bulkJobActionBean);
        Validate.notNull(bulkJobActionBean.getRequest());
        this.addMessages(this.validator.validate(bulkJobActionBean),
                messageContext,
                "validator.bulkJobAction.");

        return !messageContext.hasErrorMessages();
    }

    /**
     * @param constraintViolations
     * @param messageContext
     * @param parentContextPath
     */
    private void addMessages(Set<ConstraintViolation<BulkJobActionBean>> constraintViolations,
            MessageContext messageContext,
            String parentContextPath) {
        if (constraintViolations != null
                && !constraintViolations.isEmpty()) {
            for (ConstraintViolation<?> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        parentContextPath);
            }
        }
    }
}
