package au.gov.ga.oemd.plugin.geochemistry.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.geochemistry.transfer.BulkLoadBean;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Component
public class BulkLoadValidator {

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    private Validator validator;

    public boolean validateAfter(BulkLoadBean bulkLoadBean,
            MessageContext messageContext) {
        Validate.notNull(bulkLoadBean);
        this.addMessages(this.validator.validate(bulkLoadBean),
                messageContext,
                "validator.bulkLoad.");
        return !messageContext.hasErrorMessages();
    }

    public boolean validate(BulkLoadBean bulkLoadBean,
            MessageContext messageContext) {
        Validate.notNull(bulkLoadBean);
        Validate.notNull(bulkLoadBean.getRequest());
        this.addMessages(this.validator.validate(bulkLoadBean),
                messageContext,
                "validator.bulkLoad.");

        return !messageContext.hasErrorMessages();
    }

    /**
     * @param constraintViolations
     * @param messageContext
     * @param parentContextPath
     */
    private void addMessages(Set<ConstraintViolation<BulkLoadBean>> constraintViolations,
            MessageContext messageContext,
            String parentContextPath) {
        if (constraintViolations != null
                && !constraintViolations.isEmpty()) {
            for (ConstraintViolation<?> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        parentContextPath);
            }
        }
    }
}
