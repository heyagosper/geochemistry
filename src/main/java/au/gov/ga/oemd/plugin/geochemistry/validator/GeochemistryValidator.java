package au.gov.ga.oemd.plugin.geochemistry.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.faces.model.OneSelectionTrackingListDataModel;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.common.domain.a.Sample;
import au.gov.ga.oemd.dm.common.service.SampleService;
import au.gov.ga.oemd.dm.geochemistry.domain.Analyst;
import au.gov.ga.oemd.dm.geochemistry.domain.Docfile;
import au.gov.ga.oemd.dm.geochemistry.domain.Instrument;
import au.gov.ga.oemd.dm.geochemistry.domain.Job;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatch;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatchPreparation;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatchSample;
import au.gov.ga.oemd.dm.geochemistry.domain.KnownReferenceResult;
import au.gov.ga.oemd.dm.geochemistry.domain.Laboratory;
import au.gov.ga.oemd.dm.geochemistry.domain.Method;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodInstrument;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodInstrumentCalibration;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodInstrumentCalibrationResult;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodProperty;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodReferenceMaterial;
import au.gov.ga.oemd.dm.geochemistry.domain.PreparationProcedure;
import au.gov.ga.oemd.dm.geochemistry.domain.Property;
import au.gov.ga.oemd.dm.geochemistry.domain.PublishedSampleAnalysisResult;
import au.gov.ga.oemd.dm.geochemistry.domain.PublishingUIBean;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceMaterial;
import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatch;
import au.gov.ga.oemd.dm.geochemistry.domain.SampleAnalysis;
import au.gov.ga.oemd.dm.geochemistry.domain.SampleAnalysisResult;
import au.gov.ga.oemd.dm.geochemistry.domain.SubsampleType;
import au.gov.ga.oemd.dm.geochemistry.domain.Technique;
import au.gov.ga.oemd.dm.geochemistry.domain.TechniqueInstrument;
import au.gov.ga.oemd.dm.geochemistry.domain.TechniqueLaboratory;
import au.gov.ga.oemd.dm.geochemistry.domain.UncertaintyType;
import au.gov.ga.oemd.dm.geochemistry.domain.Unit;
import au.gov.ga.oemd.dm.geochemistry.domain.UnitGroup;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.dm.geochemistry.service.ResultBatchService;
import au.gov.ga.oemd.dm.geochemistry.service.search.AnalysisExportCriteria;
import au.gov.ga.oemd.framework.controller.ReferenceDataHelper;
import au.gov.ga.oemd.framework.validator.TextValidator;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

@Component
public class GeochemistryValidator extends TextValidator {

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    Validator validator;

    @Autowired
    private GeochemistryService appService;

    @Autowired
    private ResultBatchService resultBatchService;

    @Autowired
    private SampleService sampleService;

    public boolean validateAddJobBatchSamples(JobBatch jobBatch,
            MessageContext messageContext) {
        Long sampleNo = null;
        if (StringUtils.isNotBlank(jobBatch.getSampleNumbers())) {
            // split around comma,space or tab 
            String[] sampleNumbers = StringUtils.split(jobBatch.getSampleNumbers(),
                    ", \t");
            for (String sampleNoString : sampleNumbers) {
                try {
                    sampleNo = Long.valueOf(this.trimToNull(sampleNoString));
                } catch (NumberFormatException e) {
                    messageContext.addMessage(new MessageBuilder().info()
                            .code("validator.sample.invalidSampleNo")
                            .resolvableArg(sampleNoString)
                            .build());
                    continue;
                }
                // check to see if we already have one prepared 
                JobBatchSample jobBatchSample = jobBatch.getJobBatchSample(sampleNo);
                if (jobBatchSample == null) {
                    Sample sample = this.sampleService.findSampleById(sampleNo);
                    if (sample == null) {
                        messageContext.addMessage(new MessageBuilder().info()
                                .code("validator.sample.sampleNotFound")
                                .resolvableArg(sampleNoString)
                                .build());
                    } else {
                        jobBatchSample = new JobBatchSample(jobBatch,
                                sample);
                        jobBatch.getJobBatchSampleList()
                                .add(jobBatchSample);
                    }
                }
            }
        }
        jobBatch.setSampleNumbers(null);
        return !messageContext.hasErrorMessages();
    }

    public boolean validateAfterJobClientApproval(Job job,
            MessageContext messageContext) {
        if (job.isClientApprovalComplete() == false
                || job.getClientApprovalCompletionDate() == null
                || job.getClientApprovalCompletionBy() == null) {
            this.validationMessageAdapter.addMessage("validator.job.clientApprovalComplete",
                    messageContext,
                    job.getJobNo());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateAfterJobLabAnalysis(Job job,
            MessageContext messageContext) {
        if (job.isLabAnalysisComplete() == false
                || job.getLabAnalysisCompletionDate() == null
                || job.getLabAnalysisCompletionBy() == null) {
            this.validationMessageAdapter.addMessage("validator.job.labAnalysisComplete",
                    messageContext,
                    job.getJobNo());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateAfterJobLabQa(Job job,
            MessageContext messageContext) {
        if (job.isLabQaComplete() == false
                || job.getLabQaCompletionDate() == null
                || job.getLabQaCompletionBy() == null) {
            this.validationMessageAdapter.addMessage("validator.job.labQaComplete",
                    messageContext,
                    job.getJobNo());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateAnalysisExportCriteria(AnalysisExportCriteria analysisExportCriteria,
            Job job,
            List<Method> jobMethodList,
            MessageContext messageContext) {
        analysisExportCriteria.setJobNo(job.getJobNo());
        analysisExportCriteria.getMethodList()
                .clear();

        for (Method method : jobMethodList) {
            if (method.isSelected()) {
                analysisExportCriteria.getMethodList()
                        .add(method);
            }
        }
        if (analysisExportCriteria.getMethodList()
                .isEmpty()) {
            this.validationMessageAdapter.addMessage("validator.analysisExport.method.invalid",
                    messageContext);
        } else {
            List<SampleAnalysisResult> analysesForExportList = this.appService.findAnalysesByExportCriteria(analysisExportCriteria);
            if (analysesForExportList.isEmpty()) {
                this.validationMessageAdapter.addMessage("validator.analysisExport.noAnalyses",
                        messageContext);
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateAnalyst(Analyst analyst,
            MessageContext messageContext) {
        analyst.setAnalystSearchName(this.buildAnalystSearchName(analyst.getAnalystLastName(),
                analyst.getAnalystFirstName()));

        Set<ConstraintViolation<Analyst>> constraintViolations = this.validator.validate(analyst);
        for (ConstraintViolation<Analyst> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.analyst.");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateCalibration(MethodInstrumentCalibration methodInstrumentCalibration,
            MessageContext messageContext) {
        methodInstrumentCalibration.setComment(this.validateText(methodInstrumentCalibration.getComment()));
        Set<ConstraintViolation<MethodInstrumentCalibration>> constraintViolations = this.validator.validate(methodInstrumentCalibration);
        for (ConstraintViolation<MethodInstrumentCalibration> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.methodInstrumentCalibration.");
        }
        List<MethodInstrumentCalibration> methodInstrumentCalibrationList = this.appService.findMethodInstrumentCalibrationList(methodInstrumentCalibration.getInstrument(),
                methodInstrumentCalibration.getMethod());

        for (MethodInstrumentCalibration ic : methodInstrumentCalibrationList) {
            if (new Date(ic.getCalibrationDateTime()
                    .getTime()).equals(methodInstrumentCalibration.getCalibrationDateTime())
                    && !ic.getMethodInstrumentCalibrationNo()
                            .equals(methodInstrumentCalibration.getMethodInstrumentCalibrationNo())) {
                this.validationMessageAdapter.addMessage("validator.methodInstrumentCalibration.dateNonUnique",
                        messageContext);
            }
        }

        return !messageContext.hasErrorMessages();
    }

    public boolean validateCalibrationResults(List<MethodInstrumentCalibrationResult> methodInstrumentCalibrationResultList,
            MessageContext messageContext) {
        for (MethodInstrumentCalibrationResult methodInstrumentCalibrationResult : methodInstrumentCalibrationResultList) {
            Set<ConstraintViolation<MethodInstrumentCalibrationResult>> constraintViolations = this.validator.validate(methodInstrumentCalibrationResult);
            for (ConstraintViolation<MethodInstrumentCalibrationResult> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        "validator.methodInstrumentCalibrationResult.");
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateCalibrations(OneSelectionTrackingListDataModel instrumentCalibrations,
            MessageContext messageContext) {
        if (instrumentCalibrations == null
                || instrumentCalibrations.getRowCount() == 0) {
            this.validationMessageAdapter.addMessage("validator.search.noResultsFound",
                    messageContext);
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateDocfile(Docfile docfile,
            MessageContext messageContext) {
        Set<ConstraintViolation<Docfile>> constraintViolations = this.validator.validate(docfile);
        for (ConstraintViolation<Docfile> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.docfile.");
        }
        if (docfile.getDocfileId() != null) {
            docfile.setDocfileId(this.trimToNullAndUpperCase(docfile.getDocfileId()));
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateEnteredSampleNo(ReferenceMaterial referenceMaterial,
            MessageContext messageContext) {

        if (referenceMaterial.getSampleNo() == null
                || (referenceMaterial.getSampleNo() != null && referenceMaterial.getSampleNo()
                        .longValue() == 0)) {
            referenceMaterial.setSample(null);
        }

        if (referenceMaterial.getSampleNo() != null
                && referenceMaterial.getSampleNo()
                        .longValue() != 0) {
            Sample sample = this.sampleService.findSampleById(referenceMaterial.getSampleNo()
                    .longValue());
            if (sample == null) {
                messageContext.addMessage(new MessageBuilder().error()
                        .code("validator.sample.noSampleFound")
                        .resolvableArg(referenceMaterial.getSampleNo())
                        .build());
            }

            referenceMaterial.setSample(sample);
        }

        return !messageContext.hasErrorMessages();
    }

    public boolean validateInstrument(Instrument instrument,
            MessageContext messageContext) {
        instrument.setInstrumentId(this.trimToNullAndUpperCase(instrument.getInstrumentId()));
        instrument.setComment(this.validateText(instrument.getComment()));
        Set<ConstraintViolation<Instrument>> constraintViolations = this.validator.validate(instrument);
        for (ConstraintViolation<Instrument> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.instrument.");
        }
        if (instrument.getInstrumentId() != null) {
            instrument.setInstrumentId(this.trimToNullAndUpperCase(instrument.getInstrumentId()));
        }
        this.validateTechniqueInstrument(instrument.getTechniqueInstrumentList(),
                instrument,
                messageContext);

        return !messageContext.hasErrorMessages();
    }

    public boolean validateJob(Job job,
            MessageContext messageContext) {
        for (JobBatch jobBatch : job.getJobBatchList()) {
            for (Method method : jobBatch.getUiMethodList()) {
                if (method.isSelected()
                        && !jobBatch.getMethodList()
                                .contains(method)) {
                    jobBatch.getMethodList()
                            .add(method);
                } else if (!method.isSelected()
                        && jobBatch.getMethodList()
                                .contains(method)) {
                    jobBatch.getMethodList()
                            .remove(method);
                }
            }
            for (JobBatchPreparation jobBatchPreparation : jobBatch.getUiJobBatchPreparationList()) {
                if (jobBatchPreparation.isSelected()
                        && !jobBatch.getJobBatchPreparationList()
                                .contains(jobBatchPreparation)) {
                    jobBatch.getJobBatchPreparationList()
                            .add(jobBatchPreparation);
                } else if (!jobBatchPreparation.isSelected()
                        && jobBatch.getJobBatchPreparationList()
                                .contains(jobBatchPreparation)) {
                    jobBatch.getJobBatchPreparationList()
                            .remove(jobBatchPreparation);
                }
            }
            for (JobBatchPreparation jobBatchPreparation : jobBatch.getJobBatchPreparationList()) {
                for (Method method : jobBatchPreparation.getProcedure()
                        .getMethodList()) {
                    if (jobBatch.getMethodList()
                            .contains(method)) {
                        this.validationMessageAdapter.addMessage("validator.job.invalidPrepProcedureMethodCombination",
                                messageContext,
                                jobBatch.getJobBatchId(),
                                jobBatchPreparation.getProcedure()
                                        .getEffect(),
                                method.getTechnique()
                                        .getTechniqueName(),
                                method.getMethodName());
                    }
                }
            }
            if (StringUtils.isNotBlank(jobBatch.getSampleNumbers())) {
                this.validateAddJobBatchSamples(jobBatch,
                        messageContext);
            }
            List<SubsampleType> subsampleTypeList = this.appService.findAllSubsampleTypes();
            boolean subsampleIdMatched = false;
            for (JobBatchSample jobBatchSample : jobBatch.getJobBatchSampleList()) {
                subsampleIdMatched = false;
                if (StringUtils.isNotBlank(jobBatchSample.getSubsampleId())) {
                    for (SubsampleType subsampleType : subsampleTypeList) {
                        if (subsampleType.getMatchRegex() != null
                                && jobBatchSample.getSubsampleId()
                                        .matches(subsampleType.getMatchRegex())) {
                            if (subsampleType.getReplaceRegex() != null) {
                                jobBatchSample.setSubsampleId(jobBatchSample.getSubsampleId()
                                        .replaceAll(subsampleType.getMatchRegex(),
                                                subsampleType.getReplaceRegex()));
                            }
                            subsampleIdMatched = true;
                            break;
                        }
                    }
                    if (subsampleIdMatched == false) {
                        this.validationMessageAdapter.addMessage("validator.job.invalidSubsampleId",
                                messageContext,
                                jobBatchSample.getSubsampleId(),
                                jobBatchSample.getSample()
                                        .getSampleNo());
                    }
                }
            }
            if (job.getAccessCode() == null) {
                job.setAccessCode(ReferenceDataHelper.OPEN_ACCESS);
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobClientApproval(Job job,
            MessageContext messageContext) {
        if (job.isLabAnalysisComplete() == false
                || job.isLabQaComplete() == false) {
            this.validationMessageAdapter.addMessage("validator.job.action.invalidState",
                    messageContext,
                    job.getJobNo(),
                    "Client Approval Completion");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobFinalise(Job job,
            PublishingUIBean publishBean,
            MessageContext messageContext) {
        if (!job.isFinaliseActionAllowed()) {
            this.validationMessageAdapter.addMessage("validator.finaliseJob.noSampleAnalyses",
                    messageContext,
                    job.getJobNo());
        }
        List<ResultBatch> resultBatchList = this.resultBatchService.findBatchesByJob(job);
        if (resultBatchList != null) {
            for (ResultBatch resultBatch : resultBatchList) {
                if (resultBatch.isStatusIncomplete()) {
                    this.validationMessageAdapter.addMessage("validator.finaliseJob.incompleteResultBatches",
                            messageContext,
                            job.getJobNo(),
                            resultBatch.getResultBatchNo());
                }
            }
        }
        // Validate preferred Sample Analysis Result selection
        for (JobBatch jobBatch : job.getJobBatchList()) {
            for (JobBatchSample jobBatchSample : jobBatch.getJobBatchSampleList()) {
                for (String subsampleId : jobBatchSample.getSubsampleIdList()) {
                    for (Property property : publishBean.getPropertyList(jobBatch,
                            jobBatchSample.getSample(),
                            subsampleId)) {
                        SampleAnalysisResult sampleAnalysisResult = publishBean.getSampleAnalysisResult(jobBatch,
                                jobBatchSample.getSample(),
                                property,
                                subsampleId);
                        PublishedSampleAnalysisResult prevSampleAnalysisResult = publishBean.getPreviousPreferredPublishedResult(jobBatchSample.getSample(),
                                property,
                                subsampleId);
                        if (sampleAnalysisResult != null
                                && prevSampleAnalysisResult != null) {
                            if (sampleAnalysisResult.isPreferred()
                                    && prevSampleAnalysisResult.getSampleAnalysisResult()
                                            .isPreferred()) {
                                this.validationMessageAdapter.addMessage("validator.finaliseJob.moreThanOnePreferredResult",
                                        messageContext,
                                        jobBatch.getJobBatchId(),
                                        property.getPropertyId(),
                                        jobBatchSample.getSample()
                                                .getSampleNo());
                            }
                        }
                    }
                }
            }
        }

        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobLabAnalysis(Job job,
            MessageContext messageContext) {
        this.validateJobQa(job,
                messageContext);
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobLabQa(Job job,
            MessageContext messageContext) {
        if (job.isLabAnalysisComplete() == false) {
            this.validationMessageAdapter.addMessage("validator.job.action.invalidState",
                    messageContext,
                    job.getJobNo(),
                    "Lab QA Completion");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobPublish(Job job,
            MessageContext messageContext) {
        return this.validateJobPublish(job,
                messageContext,
                false,
                false);
    }

    public boolean validateJobPublish(Job job,
            MessageContext messageContext,
            boolean verbose,
            boolean ignoreStatus) {

        if (!this.appService.isPublishAllowed()) {
            this.validationMessageAdapter.addMessage("validator.publishJob.disabled",
                    messageContext,
                    job.getJobNo());
        } else if (!job.isStatusFinalised()
                && !ignoreStatus) {
            this.validationMessageAdapter.addMessage("validator.publishJob.notFinalised",
                    messageContext,
                    job.getJobNo());
        } else if (!job.isOpenAccess()) {
            this.validationMessageAdapter.addMessage("validator.publishJob.notOpen",
                    messageContext,
                    job.getJobNo());
        } else if (!job.isAnyOpenAccess()) { // any open jobbatchsamples?
            this.validationMessageAdapter.addMessage("validator.publishJob.noOpen",
                    messageContext,
                    job.getJobNo());
        }
        if (!messageContext.hasErrorMessages()) {
            if (!job.isAllOpenAccess()) { // all open jobbatchsamples?
                this.validationMessageAdapter.addInfoMessage("validator.publishJob.someOpen",
                        messageContext,
                        job.getJobNo());
            }
        }
        if (!messageContext.hasErrorMessages()
                && verbose) {
            this.validationMessageAdapter.addInfoMessage("validator.publishJob.notify",
                    messageContext,
                    job.getJobNo());
        }

        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobQa(Job job,
            MessageContext messageContext) {
        List<ResultBatch> resultBatchList = this.resultBatchService.findBatchesByJob(job);
        if (resultBatchList.isEmpty()) {
            this.validationMessageAdapter.addMessage("validator.job.noResultBatchesAttached",
                    messageContext,
                    job.getJobNo());
        } else {
            for (ResultBatch rs : resultBatchList) {
                if (rs.isStatusIncomplete()) {
                    this.validationMessageAdapter.addMessage("validator.job.resultBatch.incomplete",
                            messageContext,
                            rs.getResultBatchNo());
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobSearchResults(OneSelectionTrackingListDataModel searchResults,
            MessageContext messageContext) {
        if (searchResults == null
                || searchResults.getRowCount() == 0) {
            this.validationMessageAdapter.addMessage("validator.search.noResultsFound",
                    messageContext);
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobViewOnEntry(Job job,
            MessageContext messageContext) {
        int messageCount = messageContext.getAllMessages().length;
        if (job == null) {
            this.validationMessageAdapter.addMessage("validator.job.jobNotFound",
                    messageContext);
        }
        return messageContext.getAllMessages().length == messageCount;
    }

    public boolean validateKnownReferenceResult(KnownReferenceResult knownReferenceResult,
            MessageContext messageContext) {

        Set<ConstraintViolation<KnownReferenceResult>> constraintViolations = this.validator.validate(knownReferenceResult);
        for (ConstraintViolation<KnownReferenceResult> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.knownReferenceResult.");
        }
        if (this.appService.findUnitById(this.trimToNull(knownReferenceResult.getResultUnits())) == null) {
            this.validationMessageAdapter.addMessage("validator.knownReferenceResult.unknownResultUnits",
                    messageContext,
                    knownReferenceResult.getResultUnits());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateLaboratory(Laboratory laboratory,
            MessageContext messageContext) {
        laboratory.setLaboratoryId(this.trimToNullAndUpperCase(laboratory.getLaboratoryId()));
        laboratory.setDescription(this.validateText(laboratory.getDescription()));
        Set<ConstraintViolation<Laboratory>> constraintViolations = this.validator.validate(laboratory);
        for (ConstraintViolation<Laboratory> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.laboratory.");
        }
        if (laboratory.getLaboratoryId() != null) {
            laboratory.setLaboratoryId(this.trimToNullAndUpperCase(laboratory.getLaboratoryId()));
        }

        // Check another laboratory isn't default analysis laboratory
        if (laboratory.isAnalysisDefaultValue()
                && laboratory.getAnalysisStatus()
                        .isActive()) {
            Laboratory defaultAnalysisLab = this.appService.findDefaultAnalysisLaboratory();
            if (defaultAnalysisLab != null
                    && !defaultAnalysisLab.getLaboratoryNo()
                            .equals(laboratory.getLaboratoryNo())) {
                this.validationMessageAdapter.addMessage("validator.laboratory.analysisDefaultAlreadyExists",
                        messageContext,
                        defaultAnalysisLab.getLaboratoryName());
            }
        }

        // Check another laboratory isn't default preparation laboratory
        if (laboratory.isPreparationDefaultValue()
                && laboratory.getPreparationStatus()
                        .isActive()) {
            Laboratory defaultPrepLab = this.appService.findDefaultPreparationLaboratory();
            if (defaultPrepLab != null
                    && !defaultPrepLab.getLaboratoryNo()
                            .equals(laboratory.getLaboratoryNo())) {
                this.validationMessageAdapter.addMessage("validator.laboratory.preparationDefaultAlreadyExists",
                        messageContext,
                        defaultPrepLab.getLaboratoryName());
            }
        }
        this.validateTechniqueLaboratory(laboratory.getTechniqueLaboratoryList(),
                laboratory,
                messageContext);
        return !messageContext.hasErrorMessages();
    }

    public boolean validateMethod(Method method,
            MessageContext messageContext) {
        method.setMethodId(this.trimToNullAndUpperCase(method.getMethodId()));
        method.setDescription(this.validateText(method.getDescription()));
        Method existingMethod = this.appService.findMethodByCode(method.getMethodId());
        if (existingMethod != null
                && !existingMethod.getMethodNo()
                        .equals(method.getMethodNo())) {
            this.validationMessageAdapter.addMessage("validator.method.methodIdAlreadyUsed",
                    messageContext,
                    method.getMethodId());
        }

        if (method.isDefaultValue()) {
            if (!method.isActive()) {
                this.validationMessageAdapter.addMessage("validator.method.defaultMustBeActive",
                        messageContext);
            } else if (method.getTechnique() != null) {
                List<Method> methodList = this.appService.findMethodByTechnique(method.getTechnique());
                for (Method m : methodList) {
                    if (!m.getMethodNo()
                            .equals(method.getMethodNo())
                            && m.isDefaultValue()
                            && m.isActive()) {
                        this.validationMessageAdapter.addMessage("validator.method.defaultAlreadyExists",
                                messageContext,
                                m.getMethodId(),
                                m.getTechnique()
                                        .getTechniqueId());
                    }
                }
            }
        }

        Set<ConstraintViolation<Method>> constraintViolations = this.validator.validate(method);
        for (ConstraintViolation<Method> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.method.");
        }
        this.validateMethodProperty(method.getMethodPropertyList(),
                messageContext);
        this.validateMethodInstrument(method.getMethodInstrumentList(),
                messageContext);
        this.validateMethodReferenceMaterial(method.getMethodReferenceMaterialList(),
                messageContext);

        return !messageContext.hasErrorMessages();
    }

    public boolean validatePreparationProcedure(PreparationProcedure preparationProcedure,
            MessageContext messageContext) {
        preparationProcedure.setPreparationProcedureId(this.trimToNullAndUpperCase(preparationProcedure.getPreparationProcedureId()));
        Set<ConstraintViolation<PreparationProcedure>> constraintViolations = this.validator.validate(preparationProcedure);
        for (ConstraintViolation<PreparationProcedure> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.preparationProcedure.");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateProperty(Property property,
            MessageContext messageContext) {
        property.setPropertyId(this.trimToNull(property.getPropertyId()));
        Set<ConstraintViolation<Property>> constraintViolations = this.validator.validate(property);
        for (ConstraintViolation<Property> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.property.");
        }
        if (property.isLoiIncludeInd()
                && property.getLoiFactor() == null) {
            this.validationMessageAdapter.addMessage("validator.property.noLoiFactor",
                    messageContext);
        }
        if (property.isRestIncludeInd()
                && property.getRestFactor() == null) {
            this.validationMessageAdapter.addMessage("validator.property.noRestFactor",
                    messageContext);
        }
        if (StringUtils.isNotBlank(property.getDeliveryUnits())
                && this.appService.findUnitById(this.trimToNull(property.getDeliveryUnits())) == null) {
            this.validationMessageAdapter.addMessage("validator.property.unknownDeliveryUnits",
                    messageContext,
                    property.getDeliveryUnits());
        }
        if (StringUtils.isNotBlank(property.getStorageUnits())
                && this.appService.findUnitById(this.trimToNull(property.getStorageUnits())) == null) {
            this.validationMessageAdapter.addMessage("validator.property.unknownStorageUnits",
                    messageContext,
                    property.getStorageUnits());
        }
        if (StringUtils.isNotBlank(property.getOxideForm())) {
            if (this.appService.findPropertyById(property.getOxideForm()) == null
                    || property.getOxideForm()
                            .equals(property.getPropertyId())) {
                this.validationMessageAdapter.addMessage("validator.property.invalidOxideForm",
                        messageContext,
                        property.getOxideForm());
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateReferenceMaterial(ReferenceMaterial referenceMaterial,
            MessageContext messageContext) {
        referenceMaterial.setReferenceMaterialId(this.trimToNullAndUpperCase(referenceMaterial.getReferenceMaterialId()));
        referenceMaterial.setDescription(this.validateText(referenceMaterial.getDescription()));
        Set<ConstraintViolation<ReferenceMaterial>> constraintViolations = this.validator.validate(referenceMaterial);
        for (ConstraintViolation<ReferenceMaterial> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.referenceMaterial.");
        }

        return !messageContext.hasErrorMessages();
    }

    public boolean validateRemoveJobBatchSample(JobBatch jobBatch,
            Long sampleNo,
            MessageContext messageContext) {
        JobBatchSample jobBatchSample = jobBatch.getJobBatchSample(sampleNo);
        if (jobBatchSample != null) { // deletion is idempotent so notfoudn is ok
            if (CollectionUtils.isNotEmpty(jobBatchSample.getSampleAnalysisList())) {
                // get rid of the data first!
                this.validationMessageAdapter.addMessage("validator.job.cannotRemoveSample",
                        messageContext,
                        sampleNo);
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateSampleAnalysesPreferredValues(JobBatchSample jobBatchSample,
            MessageContext messageContext) {
        if (CollectionUtils.isNotEmpty(jobBatchSample.getGroupedSampleAnalysisList())) {
            for (List<SampleAnalysis> sampleAnalysisList : jobBatchSample.getGroupedSampleAnalysisList()) {
                if (sampleAnalysisList.size() == 1) { // the most common case is just one per method which must be preferred
                    sampleAnalysisList.get(0)
                            .setPreferredValue(true);
                } else {
                    boolean preferredValueNotFound = true;
                    List<String> prefSubsampleList = new ArrayList<String>();
                    SampleAnalysis currSampleAnalysis = null;
                    for (SampleAnalysis sampleAnalysis : sampleAnalysisList) {
                        currSampleAnalysis = sampleAnalysis;
                        if (sampleAnalysis.isPreferredValue()
                                && preferredValueNotFound) {
                            preferredValueNotFound = false;
                            prefSubsampleList.add(sampleAnalysis.getSubsampleId());
                        } else if (sampleAnalysis.isPreferredValue()) {
                            if (!prefSubsampleList.contains(sampleAnalysis.getSubsampleId())) {
                                prefSubsampleList.add(sampleAnalysis.getSubsampleId()); // found one but different subsample
                            } else {
                                // More than one preferred value selected
                                this.validationMessageAdapter.addMessage("validator.jobBatchSample.sampleAnalysis.moreThanOnePreferredResult",
                                        messageContext,
                                        sampleAnalysis.getResultBatch()
                                                .getMethod()
                                                .getMethodName());
                                break;
                            }
                        }
                    }
                    if (preferredValueNotFound) {
                        // No preferred value selected
                        this.validationMessageAdapter.addMessage("validator.jobBatchSample.sampleAnalysis.noPreferredResult",
                                messageContext,
                                currSampleAnalysis.getResultBatch()
                                        .getMethod()
                                        .getMethodName());
                    }
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateSampleAnalysesViewOnEntry(JobBatchSample jobBatchSample,
            MessageContext messageContext) {
        int messageCount = messageContext.getAllMessages().length;
        if (jobBatchSample == null) {
            // add a message here
            this.validationMessageAdapter.addMessage("validator.jobBatchSample.recordNotFound",
                    messageContext);
        }
        return messageContext.getAllMessages().length == messageCount;
    }

    public boolean validateSearchResults(List<?> searchResults,
            MessageContext messageContext) {
        if (searchResults == null
                || searchResults.size() == 0) {
            messageContext.addMessage(new MessageBuilder().info()
                    .code("validator.search.noResultsFound")
                    .build());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateSearchResults(OneSelectionTrackingListDataModel searchResults,
            MessageContext messageContext) {
        if (searchResults == null
                || searchResults.getRowCount() == 0) {
            messageContext.addMessage(new MessageBuilder().info()
                    .code("validator.search.noResultsFound")
                    .build());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateSubsampleType(SubsampleType subsampleType,
            MessageContext messageContext) {
        subsampleType.setSubsampleTypeId(this.trimToNull(subsampleType.getSubsampleTypeId()));
        Set<ConstraintViolation<SubsampleType>> constraintViolations = this.validator.validate(subsampleType);
        for (ConstraintViolation<SubsampleType> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.subsampleType.");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateTechnique(Technique technique,
            MessageContext messageContext) {
        technique.setTechniqueId(this.trimToNullAndUpperCase(technique.getTechniqueId()));
        technique.setDescription(this.validateText(technique.getDescription()));
        Technique existingTechnique = this.appService.findTechniqueByCode(technique.getTechniqueId());
        if (existingTechnique != null
                && !existingTechnique.getTechniqueNo()
                        .equals(technique.getTechniqueNo())) {
            this.validationMessageAdapter.addMessage("validator.technique.techniqueIdAlreadyUsed",
                    messageContext,
                    technique.getTechniqueId());
        }

        Set<ConstraintViolation<Technique>> constraintViolations = this.validator.validate(technique);
        for (ConstraintViolation<Technique> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.technique.");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateUncertaintyType(UncertaintyType uncertaintyType,
            MessageContext messageContext) {
        uncertaintyType.setUncertaintyTypeId(this.trimToNullAndUpperCase(uncertaintyType.getUncertaintyTypeId()));
        uncertaintyType.setUncertaintyTypeCode(this.trimToNullAndUpperCase(uncertaintyType.getUncertaintyTypeCode()));
        if (uncertaintyType.getUncertaintyTypeCode() != null) {
            UncertaintyType existingUncertaintyType = this.appService.findUncertaintyTypeByCode(uncertaintyType.getUncertaintyTypeCode());
            if (existingUncertaintyType != null
                    && !existingUncertaintyType.getUncertaintyTypeNo()
                            .equals(uncertaintyType.getUncertaintyTypeNo())) {
                this.validationMessageAdapter.addMessage("validator.uncertaintyType.uncertaintyTypeCodeAlreadyUsed",
                        messageContext,
                        uncertaintyType.getUncertaintyTypeCode());
            }
        }
        if (uncertaintyType.getUncertaintyTypeId() != null) {
            UncertaintyType existingUncertaintyType = this.appService.findUncertaintyTypeByUncertaintyTypeId(uncertaintyType.getUncertaintyTypeId());
            if (existingUncertaintyType != null
                    && !existingUncertaintyType.getUncertaintyTypeNo()
                            .equals(uncertaintyType.getUncertaintyTypeNo())) {
                this.validationMessageAdapter.addMessage("validator.uncertaintyType.uncertaintyTypeIdAlreadyUsed",
                        messageContext,
                        uncertaintyType.getUncertaintyTypeId());
            }
        }
        Set<ConstraintViolation<UncertaintyType>> constraintViolations = this.validator.validate(uncertaintyType);
        for (ConstraintViolation<UncertaintyType> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.uncertaintyType.");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateUnit(Unit unit,
            MessageContext messageContext) {
        unit.setUnitId(this.trimToNull(unit.getUnitId()));
        unit.setUnitName(this.trimToNull(unit.getUnitName()));
        unit.setHtmlUnitId(this.trimToNull(unit.getHtmlUnitId()));
        unit.setHtmlUnitName(this.trimToNull(unit.getHtmlUnitName()));
        unit.setXmlUnitId(this.trimToNull(unit.getXmlUnitId()));
        Set<ConstraintViolation<Unit>> constraintViolations = this.validator.validate(unit);
        for (ConstraintViolation<Unit> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.unit.");
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateUnitGroup(UnitGroup unitGroup,
            MessageContext messageContext) {
        unitGroup.setUnitGroupId(this.trimToNullAndUpperCase(unitGroup.getUnitGroupId()));
        if (unitGroup.getValueType() == null) {
            unitGroup.setValueType(UnitGroup.ValueTypeCode.SCALAR);
        }
        unitGroup.setStandardUnitId(this.trimToNull(unitGroup.getStandardUnitId()));
        if (unitGroup.getStandardUnitId() != null) {
            if (unitGroup.getUnitGroupLink(unitGroup.getStandardUnitId()) == null) {
                this.validationMessageAdapter.addMessage("validator.unitGroup.standardUnitInvalid",
                        messageContext,
                        unitGroup.getStandardUnitId());
            }
        }
        unitGroup.setGroupName(this.trimToNull(unitGroup.getGroupName()));
        Set<ConstraintViolation<UnitGroup>> constraintViolations = this.validator.validate(unitGroup);
        for (ConstraintViolation<UnitGroup> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.unitGroup.");
        }
        return !messageContext.hasErrorMessages();
    }

    protected String trimToNull(String val) {
        return StringUtils.trimToNull(val);
    }

    protected String trimToNullAndUpperCase(String val) {
        return StringUtils.upperCase(StringUtils.trimToNull(val));
    }

    private String buildAnalystSearchName(String analystLastName,
            String analystFirstName) {
        StringBuilder resp = new StringBuilder(analystLastName);
        if (StringUtils.isNotEmpty(analystFirstName)) {
            if (resp.length() > 0) {
                resp.append(", ");
            }
            resp.append(analystFirstName);
        }
        return resp.length() > 0 ? resp.toString() : null;
    }

    private void validateMethodInstrument(List<MethodInstrument> selectedInstrumentList,
            MessageContext messageContext) {
        for (MethodInstrument methodInstrument : selectedInstrumentList) {
            if (methodInstrument.isDefaultValue()) {
                if (!methodInstrument.isActive()) {
                    this.validationMessageAdapter.addMessage("validator.method.defaultInstrumentNotActive",
                            messageContext,
                            methodInstrument.getInstrument()
                                    .getInstrumentId());
                }
            }

            Set<ConstraintViolation<MethodInstrument>> constraintViolations = this.validator.validate(methodInstrument);
            for (ConstraintViolation<MethodInstrument> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        "validator.method.methodInstrument.");
            }
        }
    }

    private void validateMethodProperty(List<MethodProperty> selectedPropertyList,
            MessageContext messageContext) {
        for (MethodProperty methodProperty : selectedPropertyList) {
            Set<ConstraintViolation<MethodProperty>> constraintViolations = this.validator.validate(methodProperty);
            for (ConstraintViolation<MethodProperty> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        "validator.method.methodProperty.");
            }
        }
    }

    private void validateMethodReferenceMaterial(List<MethodReferenceMaterial> selectedReferenceMaterialList,
            MessageContext messageContext) {
        for (MethodReferenceMaterial methodReferenceMaterial : selectedReferenceMaterialList) {
            if (methodReferenceMaterial.isDefaultValue()) {
                if (!methodReferenceMaterial.isActive()) {
                    this.validationMessageAdapter.addMessage("validator.method.defaultReferenceMaterialNotActive",
                            messageContext,
                            methodReferenceMaterial.getReferenceMaterial()
                                    .getReferenceMaterialId());
                }
            }

            Set<ConstraintViolation<MethodReferenceMaterial>> constraintViolations = this.validator.validate(methodReferenceMaterial);
            for (ConstraintViolation<MethodReferenceMaterial> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        "validator.method.methodReferenceMaterial.");
            }
        }
    }

    private void validateTechniqueInstrument(List<TechniqueInstrument> selectedInstrumentList,
            Instrument instrument,
            MessageContext messageContext) {
        for (TechniqueInstrument techniqueInstrument : selectedInstrumentList) {
            if (techniqueInstrument.isDefaultValue()) {
                if (!techniqueInstrument.isActive()) {
                    this.validationMessageAdapter.addMessage("validator.instrument.defaultTechniqueNotActive",
                            messageContext,
                            techniqueInstrument.getInstrument()
                                    .getInstrumentId());
                }
            }

            Set<ConstraintViolation<TechniqueInstrument>> constraintViolations = this.validator.validate(techniqueInstrument);
            for (ConstraintViolation<TechniqueInstrument> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        "validator.instrument.techniqueInstrument.");
            }
        }
        // check that we have matching instrumentTechnique links for each of the method.techniques
        for (Method method : instrument.getMethodList()) {
            boolean instrumentHasTechnique = false;
            for (TechniqueInstrument techniqueInstrument : selectedInstrumentList) {
                if (techniqueInstrument.getTechnique()
                        .equals(method.getTechnique())) {
                    instrumentHasTechnique = true;
                    break;
                }
            }
            if (!instrumentHasTechnique) {
                this.validationMessageAdapter.addMessage("validator.instrument.techniqueRequired",
                        messageContext,
                        method.getTechnique()
                                .getTechniqueId(),
                        method.getMethodId());
            }
        }
    }

    private void validateTechniqueLaboratory(List<TechniqueLaboratory> selectedLaboratoryList,
            Laboratory laboratory,
            MessageContext messageContext) {
        for (TechniqueLaboratory techniqueLaboratory : selectedLaboratoryList) {
            if (techniqueLaboratory.isDefaultValue()) {
                if (!techniqueLaboratory.isActive()) {
                    this.validationMessageAdapter.addMessage("validator.laboratory.defaultTechniqueNotActive",
                            messageContext,
                            techniqueLaboratory.getLaboratory()
                                    .getLaboratoryId());
                }
            }

            Set<ConstraintViolation<TechniqueLaboratory>> constraintViolations = this.validator.validate(techniqueLaboratory);
            for (ConstraintViolation<TechniqueLaboratory> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        "validator.laboratory.techniqueLaboratory.");
            }
        }
        // check that we are not about to orphan an instrument-technique
        for (Instrument instrument : laboratory.getInstrumentList()) {
            for (TechniqueInstrument techniqueInstrument : instrument.getTechniqueInstrumentList()) {
                boolean laboratoryHasTechnique = false;
                for (TechniqueLaboratory techniqueLaboratory : selectedLaboratoryList) {
                    if (techniqueLaboratory.getTechnique()
                            .equals(techniqueInstrument.getTechnique())) {
                        laboratoryHasTechnique = true;
                        break;
                    }
                }
                if (!laboratoryHasTechnique) {
                    this.validationMessageAdapter.addMessage("validator.laboratory.techniqueRequired",
                            messageContext,
                            techniqueInstrument.getTechnique()
                                    .getTechniqueId(),
                            instrument.getInstrumentId());
                }
            }
        }
    }
}
