package au.gov.ga.oemd.plugin.geochemistry.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.faces.model.OneSelectionTrackingListDataModel;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.geochemistry.service.search.InstrumentSearchCriteria;

@Component
public class InstrumentValidator {
    private static final Logger log = LoggerFactory.getLogger(InstrumentValidator.class);

    @Autowired
    private Validator validator;

    public boolean validateInstrumentSearchCriteria(InstrumentSearchCriteria searchCriteria,
            MessageContext messageContext) {
        Set<ConstraintViolation<InstrumentSearchCriteria>> constraintViolations = this.validator.validate(searchCriteria);
        for (ConstraintViolation<InstrumentSearchCriteria> violation : constraintViolations) {
            log.debug(violation.getPropertyPath()
                    .toString()
                    + " "
                    + violation.getMessage());
            messageContext.addMessage(new MessageBuilder().error()
                    .code("validator.instrument."
                            + violation.getPropertyPath()
                                    .toString())
                    .resolvableArg(violation.getMessage())
                    .defaultText(violation.getPropertyPath()
                            .toString()
                            + " "
                            + violation.getMessage())
                    .build());
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateSearchResults(OneSelectionTrackingListDataModel searchResults,
            MessageContext messageContext) {
        if (searchResults == null
                || searchResults.getRowCount() == 0) {
            messageContext.addMessage(new MessageBuilder().error()
                    .code("validator.search.noResultsFound")
                    .build());
        }
        return !messageContext.hasErrorMessages();
    }

}
