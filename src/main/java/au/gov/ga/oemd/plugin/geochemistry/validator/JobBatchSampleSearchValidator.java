package au.gov.ga.oemd.plugin.geochemistry.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatch;
import au.gov.ga.oemd.dm.geochemistry.service.search.JobBatchSampleSearchCriteria;
import au.gov.ga.oemd.framework.common.service.search.SearchCriteria;
import au.gov.ga.oemd.framework.validator.SearchValidator;

@Component("jobBatchSampleSearchValidator")
public class JobBatchSampleSearchValidator extends SearchValidator {
    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(JobBatchSampleSearchValidator.class);

    @Override
    public boolean validateSearchCriteria(SearchCriteria searchCriteria,
            MessageContext messageContext) {
        if (searchCriteria != null) {
            JobBatchSampleSearchCriteria sc = (JobBatchSampleSearchCriteria) searchCriteria;
            sc.setJobBatchId(ResultBatch.parseJobBatchId(sc.getJobBatchId()));
        }
        super.validateSearchCriteria(searchCriteria,
                messageContext);
        return !messageContext.hasErrorMessages();
    }

}
