package au.gov.ga.oemd.plugin.geochemistry.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatch;
import au.gov.ga.oemd.dm.geochemistry.service.search.ResultBatchSearchCriteria;
import au.gov.ga.oemd.framework.common.service.search.SearchCriteria;
import au.gov.ga.oemd.framework.validator.SearchValidator;

@Component("resultBatchSearchValidator")
public class ResultBatchSearchValidator extends SearchValidator {
    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(ResultBatchSearchValidator.class);

    @Override
    public boolean validateSearchCriteria(SearchCriteria searchCriteria,
            MessageContext messageContext) {
        if (searchCriteria != null) {
            ResultBatchSearchCriteria sc = (ResultBatchSearchCriteria) searchCriteria;
            sc.setJobBatchId(ResultBatch.parseJobBatchId(sc.getJobBatchId()));
        }
        super.validateSearchCriteria(searchCriteria,
                messageContext);
        return !messageContext.hasErrorMessages();
    }

}
