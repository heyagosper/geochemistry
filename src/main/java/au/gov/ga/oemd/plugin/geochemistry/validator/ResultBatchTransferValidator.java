package au.gov.ga.oemd.plugin.geochemistry.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import au.gov.ga.oemd.dm.common.domain.a.Sample;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatchSample;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceAnalysis.AnalysisPurposeCode;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceMaterial;
import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatch;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.dm.geochemistry.transfer.ResultBatchTransferFile;
import au.gov.ga.oemd.dm.geochemistry.transfer.TransferAnalysisRecord;
import au.gov.ga.oemd.dm.geochemistry.transfer.TransferAnalysisResultRecord;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

/**
 * We will probably need validators for each technique but we will use a single
 * one until we actually need some more
 */
@Component
public class ResultBatchTransferValidator {

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    private GeochemistryService appService;

    @Autowired
    private Validator validator;

    public boolean validateResultBatchTransferFile(ResultBatch resultBatch,
            MessageContext messageContext) {
        //        InputAttributeHelper inputAttributeHelper = InputAttributeHelper.getInstance(this.appService);
        //        List<String> processedSampleSubsampleNumbers = new ArrayList<String>();
        Validate.notNull(resultBatch,
                "the resultbatch is null");
        if (resultBatch.getResultBatchTransferFile() == null
                || resultBatch.getResultBatchTransferFile()
                        .getAnalysisRecordList() == null
                || resultBatch.getResultBatchTransferFile()
                        .getAnalysisRecordList()
                        .isEmpty()) {
            this.validationMessageAdapter.addMessage("validator.resultBatchTransferFile.empty",
                    messageContext);
        } else {
            this.addMessages(this.validator.validate(resultBatch.getResultBatchTransferFile()),
                    messageContext,
                    "validator.resultBatchTransferFile.");

            for (TransferAnalysisRecord transferAnalysisRecord : resultBatch.getResultBatchTransferFile()
                    .getAnalysisRecordList()) {
                Sample sample = null;
                ReferenceMaterial rm = null;
                if (transferAnalysisRecord.getSampleNo() != null) {
                    JobBatchSample jobBatchSample = resultBatch.getJobBatchSample(transferAnalysisRecord.getSampleNo());
                    if (jobBatchSample != null) {
                        sample = jobBatchSample.getSample();
                        transferAnalysisRecord.setAnalysisPurpose(null);
                    }
                }
                if (sample == null) { // wasn't a valid Sample Analysis
                    rm = resultBatch.getReferenceMaterial(transferAnalysisRecord.getSampleName());
                    if (rm != null) {
                        transferAnalysisRecord.setSampleNo(new Long(rm.getReferenceMaterialNo()
                                .intValue()));
                        if (transferAnalysisRecord.getAnalysisPurpose() == null) {
                            transferAnalysisRecord.setAnalysisPurpose(AnalysisPurposeCode.RS);
                        }
                    } else {
                        this.validationMessageAdapter.addMessage("validator.resultBatchTransferFile.sampleName",
                                messageContext,
                                transferAnalysisRecord.getSampleName());
                    }
                    //TODO - do we need this in Geochem??
                    //                } else {
                    //                    String sampleCombo = analysisRecord.getSampleNo()
                    //                            + "^"
                    //                            + analysisRecord.getSubsampleId();
                    //                    if (processedSampleSubsampleNumbers.indexOf(sampleCombo) != -1) {
                    //                        this.validationMessageAdapter.addMessage("validator.resultBatchTransferFile.sampleCombination",
                    //                                messageContext,
                    //                                analysisRecord.getSampleNo(),
                    //                                analysisRecord.getSubsampleId());
                    //                    } else {
                    //                        processedSampleSubsampleNumbers.add(sampleCombo);
                    //                    }
                    //                }
                    //                List<AnalysisRecordAttributes> analysisRecordAttributesList = analysisRecord.getAnalysisRecordAttributesList();
                    //                for (AnalysisRecordAttributes analysisRecordAttribute : analysisRecordAttributesList) {
                    //                    String replacementName = inputAttributeHelper.getReplacementStringForAnalysisContext(analysisRecordAttribute.getName());
                    //                    if (replacementName != null) {
                    //                        this.validationMessageAdapter.addMessage("validator.inputAttribute.name",
                    //                                messageContext,
                    //                                analysisRecordAttribute.getName(),
                    //                                "Analysis");
                    //                    } else {
                    //                        analysisRecordAttribute.setName(replacementName);
                    //                    }

                }
                for (TransferAnalysisResultRecord transferAnalysisResultRecord : transferAnalysisRecord.getAnalysisResultList()) {
                    if (resultBatch.getMethodProperty(transferAnalysisResultRecord.getPropertyId()) == null) {
                        this.validationMessageAdapter.addMessage("validator.resultBatchTransferFile.methodProperty",
                                messageContext,
                                transferAnalysisResultRecord.getPropertyId(),
                                resultBatch.getMethod()
                                        .getMethodName());
                    }
                    if (transferAnalysisResultRecord.getResultUnits() == null) {
                        this.validationMessageAdapter.addMessage("validator.resultBatchTransferFile.noResultUnits",
                                messageContext,
                                transferAnalysisResultRecord.getPropertyId());
                    } else if (this.appService.findUnitById(transferAnalysisResultRecord.getResultUnits()
                            .trim()) == null) {
                        this.validationMessageAdapter.addMessage("validator.resultBatchTransferFile.resultUnits",
                                messageContext,
                                transferAnalysisResultRecord.getResultUnits(),
                                transferAnalysisResultRecord.getPropertyId());
                    }
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    /**
     * @param constraintViolations
     * @param messageContext
     * @param parentContextPath
     */
    private void addMessages(Set<ConstraintViolation<ResultBatchTransferFile>> constraintViolations,
            MessageContext messageContext,
            String parentContextPath) {
        if (constraintViolations != null
                && !constraintViolations.isEmpty()) {
            for (ConstraintViolation<?> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        parentContextPath);
            }
        }
    }
}
