package au.gov.ga.oemd.plugin.geochemistry.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import au.gov.ga.oemd.dm.geochemistry.domain.Analysis;
import au.gov.ga.oemd.dm.geochemistry.domain.AnalysisResult;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatch;
import au.gov.ga.oemd.dm.geochemistry.domain.JobBatchSample;
import au.gov.ga.oemd.dm.geochemistry.domain.KnownReferenceResult;
import au.gov.ga.oemd.dm.geochemistry.domain.MethodProperty;
import au.gov.ga.oemd.dm.geochemistry.domain.Property;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceAnalysis;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceAnalysis.AnalysisPurposeCode;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceAnalysisResult;
import au.gov.ga.oemd.dm.geochemistry.domain.ReferenceMaterial;
import au.gov.ga.oemd.dm.geochemistry.domain.ResultBatch;
import au.gov.ga.oemd.dm.geochemistry.domain.SampleAnalysis;
import au.gov.ga.oemd.dm.geochemistry.domain.SampleAnalysisResult;
import au.gov.ga.oemd.dm.geochemistry.service.GeochemistryService;
import au.gov.ga.oemd.dm.geochemistry.service.ResultBatchService;
import au.gov.ga.oemd.framework.validator.ValidationMessageAdapter;

/**
 * We will probably need validators for each technique but we will use a single
 * one until we actually need some more
 */
@Component
public class ResultBatchValidator {
    private static final BigDecimal BD_ZERO = BigDecimal.ZERO;

    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(ResultBatchValidator.class);

    @Autowired
    ValidationMessageAdapter validationMessageAdapter;

    @Autowired
    private GeochemistryService appService;

    @Autowired
    private ResultBatchService resultBatchService;

    @Autowired
    private Validator validator;

    public boolean validateBatch(ResultBatch resultBatch,
            MessageContext messageContext) {
        this.addMessages(this.validator.validate(resultBatch),
                messageContext,
                "validator.resultBatch.");

        return !messageContext.hasErrorMessages();
    }

    public boolean validateBatchSessionDetails(ResultBatch form,
            MessageContext messageContext) {
        this.validateProperties(form,
                "validator.resultBatch.",
                messageContext,
                "technique",
                "method",
                "laboratory",
                "instrument",
                "analystList",
                "sessionDateTime",
                "methodInstrumentCalibration",
                "includedJobBatches");
        if (form.getTechnique()
                .isTitration()) {
            this.validateProperties(form,
                    "validator.resultBatch.",
                    messageContext,
                    "titrantFactor");
        }

        return !messageContext.hasErrorMessages();
    }

    public boolean validateDerivedResults(ResultBatch resultBatch,
            MessageContext messageContext) {
        if (!resultBatch.isRecalculateAllowed()) {
            this.validationMessageAdapter.addMessage("validator.resultBatch.recalculateIsNotAllowed",
                    messageContext);
        }
        if (!messageContext.hasErrorMessages()) {
            MethodProperty mp = resultBatch.getMethod()
                    .getMethodProperty(Property.ID_LOI);
            if (mp != null
                    && mp.isDerived()) {
                for (ReferenceAnalysis analysis : resultBatch.getReferenceAnalysisList()) {
                    this.validateLossOnIgnitionAnalysisWeights(analysis,
                            false,
                            analysis.getReferenceMaterial()
                                    .getReferenceMaterialId(),
                            messageContext);
                }
                for (SampleAnalysis analysis : resultBatch.getSampleAnalysisList()) {
                    this.validateLossOnIgnitionAnalysisWeights(analysis,
                            false,
                            analysis.getSample()
                                    .getSampleNo()
                                    .toString(),
                            messageContext);
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobBatchMethods(ResultBatch resultBatch,
            MessageContext messageContext) {
        if (resultBatch.getSessionDateTime() != null
                & resultBatch.getSessionDateTime()
                        .before(DateUtils.addMonths(new Date(),
                                -12))) {
            return true; // skip this rule for legacy data
        }
        return this.validateJobBatchMethods(resultBatch,
                messageContext,
                false);
    }

    public boolean validateJobBatchMethods(ResultBatch resultBatch,
            MessageContext messageContext,
            boolean infoMessage) {
        if (CollectionUtils.isNotEmpty(resultBatch.getIncludedJobBatchIdList())) {
            for (String jobBatchId : resultBatch.getIncludedJobBatchIdList()) {
                JobBatch jobBatch = this.appService.findJobBatchByJobBatchId(jobBatchId,
                        false,
                        false);
                if (!jobBatch.getMethodList()
                        .contains(resultBatch.getMethod())) {
                    this.validationMessageAdapter.addMessage("validator.resultBatch.methodNotInJobBatch",
                            messageContext,
                            infoMessage,
                            jobBatchId,
                            resultBatch.getMethod()
                                    .getMethodId());
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    @Transactional(readOnly = true)
    public boolean validateJobBatchNumbers(ResultBatch resultBatch,
            MessageContext messageContext) {
        resultBatch.getIncludedJobBatchList()
                .clear();

        if (CollectionUtils.isNotEmpty(resultBatch.getIncludedJobBatchIdList())) {
            StringBuilder idList = new StringBuilder();
            for (String jobBatchId : resultBatch.getIncludedJobBatchIdList()) {
                JobBatch jobBatch = this.appService.findJobBatchByJobBatchId(jobBatchId,
                        true,
                        true);
                if (jobBatch == null) {
                    this.validationMessageAdapter.addMessage("validator.resultBatch.jobBatchNotFound",
                            messageContext,
                            jobBatchId);
                } else if (jobBatch.isStatusFinalised()) {
                    this.validationMessageAdapter.addMessage("validator.resultBatch.jobIsFinalised",
                            messageContext,
                            jobBatch.getJob()
                                    .getJobNo());
                } else {
                    if (jobBatchId.endsWith(".1")
                            && jobBatch.getJob()
                                    .getJobBatchList()
                                    .size() == 1) {
                        jobBatchId = jobBatchId.substring(0,
                                jobBatchId.length() - 2);// truncate the redundant ".1"
                    } else {
                        if (idList.length() > 0) {
                            idList.append(",");
                        }
                    }
                    resultBatch.getIncludedJobBatchList()
                            .add(jobBatch);
                }
                if (idList.length() > 0) {
                    idList.append(",");
                }
                idList.append(jobBatchId);
            }
            resultBatch.setIncludedJobBatches(idList.toString());
        } else {
            this.validationMessageAdapter.addMessage("validator.resultBatch.noJobBatchesEntered",
                    messageContext);
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobBatchSamples(ResultBatch resultBatch,
            MessageContext messageContext) {
        // Make sure there is at least one sample selected for each included bulkaction batch
        if (CollectionUtils.isNotEmpty(resultBatch.getSampleAnalysisList())) {
            for (JobBatch jobBatch : resultBatch.getIncludedJobBatchList()) {
                boolean hasAnalyses = false;
                for (SampleAnalysis sampleAnalysis : resultBatch.getSampleAnalysisList()) {
                    if (sampleAnalysis.getJobBatch()
                            .equals(jobBatch)) {
                        hasAnalyses = true;
                        break;
                    }
                }
                if (hasAnalyses == false) {
                    this.validationMessageAdapter.addMessage("validator.resultBatch.noSampleSelectedForJobBatch",
                            messageContext,
                            jobBatch.getJobBatchId());
                }
            }
        } else {
            this.validationMessageAdapter.addMessage("validator.resultBatch.noSamplesSelected",
                    messageContext);
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateJobBatchSampleSelection(ResultBatch resultBatch,
            MessageContext messageContext) {
        boolean newSampleAnalysis = true;
        // If editing result batch make sure that existing sample analyses are preserved and not duplicated
        if (CollectionUtils.isNotEmpty(resultBatch.getSampleAnalysisList())) {
            // Edit of Result Batch
            List<SampleAnalysis> tempList = new ArrayList<SampleAnalysis>(resultBatch.getSampleAnalysisList());

            // Remove Orphaned sample analyses if bulkaction has been removed
            for (SampleAnalysis sampleAnalysis : tempList) {
                if (!resultBatch.getIncludedJobBatchList()
                        .contains(sampleAnalysis.getJobBatch())) {
                    resultBatch.getSampleAnalysisList()
                            .remove(sampleAnalysis);
                }
            }

            for (JobBatch jobBatch : resultBatch.getIncludedJobBatchList()) {
                for (JobBatchSample jobBatchSample : jobBatch.getJobBatchSampleList()) {
                    if (jobBatchSample.isSelected()) {
                        newSampleAnalysis = true;
                        for (SampleAnalysis resultBatchSampleAnalysis : tempList) {
                            if (resultBatchSampleAnalysis.getSample()
                                    .equals(jobBatchSample.getSample())) {
                                newSampleAnalysis = false;
                                break;
                            }
                        }
                        if (newSampleAnalysis) {
                            //Add new sample analysis to list of existing sample analyses
                            this.createNewSampleAnalysis(resultBatch,
                                    jobBatch,
                                    jobBatchSample);
                        }
                    } else { // Sample may have been removed on edit
                        for (SampleAnalysis resultBatchSampleAnalysis : tempList) {
                            if (resultBatchSampleAnalysis.getSample()
                                    .equals(jobBatchSample.getSample())) {
                                // Sample deselected during edit
                                resultBatch.getSampleAnalysisList()
                                        .remove(resultBatchSampleAnalysis);
                            }
                        }
                    }
                }
            }
        } else {// Create Result Batch 
            for (JobBatch jobBatch : resultBatch.getIncludedJobBatchList()) {
                for (JobBatchSample jobBatchSample : jobBatch.getJobBatchSampleList()) {
                    if (jobBatchSample.isSelected()) {
                        this.createNewSampleAnalysis(resultBatch,
                                jobBatch,
                                jobBatchSample);
                    }
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    public boolean validateLaboratoryMetadata(ResultBatch resultBatch,
            MessageContext messageContext) {
        this.constrainMethodInstrumentProperties(resultBatch);
        this.validateProperties(resultBatch,
                "validator.resultBatch.",
                messageContext,
                "laboratory");
        return !messageContext.hasErrorMessages();
    }

    /**
     * 
     * @param form
     * @param messageContext
     * @return
     * 
     *         Check the values selected from method list and if valid rebuild
     *         instrument list according to values selected.
     */

    public boolean validateMethodMetadata(ResultBatch resultBatch,
            MessageContext messageContext) {
        this.constrainMethodInstrumentProperties(resultBatch);
        this.validateProperties(resultBatch,
                "validator.resultBatch.",
                messageContext,
                "laboratory",
                "technique",
                "method");
        return !messageContext.hasErrorMessages();
    }

    public boolean validateReferenceMaterialSelection(ResultBatch resultBatch,
            List<ReferenceMaterial> referenceMaterialList,
            MessageContext messageContext) {
        // If editing result batch make sure that existing reference analyses are preserved and not duplicated
        if (CollectionUtils.isNotEmpty(resultBatch.getReferenceAnalysisList())) {
            boolean newReferenceAnalysis = true;
            List<ReferenceAnalysis> tempList = new ArrayList<ReferenceAnalysis>(resultBatch.getReferenceAnalysisList());
            for (ReferenceMaterial referenceMaterial : referenceMaterialList) {
                if (referenceMaterial.isSelected()) {
                    newReferenceAnalysis = true;
                    for (ReferenceAnalysis resultBatchRefAnalysis : tempList) {
                        if (resultBatchRefAnalysis.getReferenceMaterial()
                                .equals(referenceMaterial)) {
                            newReferenceAnalysis = false;
                            this.addRemoveReferenceAnalysisObjects(resultBatch,
                                    referenceMaterial);
                        }
                    }
                    if (newReferenceAnalysis) {
                        // Add new reference analysis to list of existing reference analyses
                        for (int i = 0; i < referenceMaterial.getNoTimesReferenceMaterialUsed(); i++) {
                            this.createNewReferenceAnalysis(resultBatch,
                                    referenceMaterial);
                        }
                    }
                } else { // Reference analysis may have been removed on edit
                    for (ReferenceAnalysis resultBatchReferenceAnalysis : tempList) {
                        if (resultBatchReferenceAnalysis.getReferenceMaterial()
                                .equals(referenceMaterial)) {
                            // Reference analysis deselected during edit
                            resultBatch.getReferenceAnalysisList()
                                    .remove(resultBatchReferenceAnalysis);
                        }
                    }
                }
            }
        } else {// Create New Result Batch 
            for (ReferenceMaterial referenceMaterial : referenceMaterialList) {
                if (referenceMaterial.isSelected()) {
                    for (int i = 0; i < referenceMaterial.getNoTimesReferenceMaterialUsed(); i++) {
                        this.createNewReferenceAnalysis(resultBatch,
                                referenceMaterial);
                    }
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    @Transactional(readOnly = true)
    public boolean validateResultBatch(ResultBatch resultBatch,
            MessageContext messageContext) {
        if ((resultBatch.getTechnique()
                .isTitration() || resultBatch.getTechnique()
                .isGravimetry())
                && resultBatch.getVersionNo() == null) {
            if (resultBatch.isRemoveSourceFile()) {
                resultBatch.setSourceFile(null);
            }
        }
        this.validateJobBatchNumbers(resultBatch,
                messageContext);
        if (resultBatch.getMethod()
                .hasDerivedProperties()) {
            this.resultBatchService.calculateDerivedResultValues(resultBatch);
        }
        Set<ConstraintViolation<ResultBatch>> constraintViolations = this.validator.validate(resultBatch);
        for (ConstraintViolation<ResultBatch> violation : constraintViolations) {
            this.validationMessageAdapter.addMessage(violation,
                    messageContext,
                    "validator.resultBatch.");
        }
        if (resultBatch.getInstrument() == null) {
            this.validationMessageAdapter.addMessage("validator.resultBatch.instrumentRequired",
                    messageContext);
        }
        if (resultBatch.getMethodInstrumentCalibration() == null) {
            this.validationMessageAdapter.addMessage("validator.resultBatch.methodInstrumentCalibrationNotFound",
                    messageContext);
        }
        return !messageContext.hasErrorMessages();
    }

    //    public boolean validateBatchRefMaterials(ResultBatch resultBatch,
    //            MessageContext messageContext) {
    //        List<ReferenceAnalysis> referenceAnalysisList = new ArrayList<ReferenceAnalysis>();
    //        for (MethodReferenceMaterial methodReferenceMaterial : resultBatch.getMethod()
    //                .getMethodReferenceMaterialList()) {
    //            if (methodReferenceMaterial.getReferenceMaterial()
    //                    .isSelected()) {
    //                ReferenceAnalysis referenceAnalysis = new ReferenceAnalysis(resultBatch,
    //                        resultBatch.getMethod(),
    //                        methodReferenceMaterial.getReferenceMaterial());
    //                referenceAnalysisList.add(referenceAnalysis);
    //            }
    //        }
    //        resultBatch.setReferenceAnalysisList(referenceAnalysisList);
    //
    //        return !messageContext.hasErrorMessages();
    //    }

    //    public boolean validateBatchSamples(ResultBatch resultBatch,
    //            List<LabSubSample> resultBatchSampleSelectedList,
    //            MessageContext messageContext) {
    //        if (resultBatchSampleSelectedList.isEmpty()) {
    //            messageContext.addMessage(new MessageBuilder().error()
    //                    .code("validator.createBatch.noSampleSelected")
    //                    .build());
    //        }
    //
    //        resultBatch.getResultBatchSampleList()
    //                .clear();
    //        for (LabSubSample labSubSample : resultBatchSampleSelectedList) {
    //            resultBatch.getResultBatchSampleList()
    //                    .addAll(labSubSample.getSelectedResultBatchSampleList());
    //        }
    //        return !messageContext.hasErrorMessages();
    //    }

    //    public boolean validateResultBatchRefConcentrations(ResultBatch resultBatch,
    //            MessageContext messageContext) {
    //
    //        if (CollectionUtils.isEmpty(resultBatch.getResultBatchRefConcentrationList())) {
    //            messageContext.addMessage(new MessageBuilder().error()
    //                    .code("validator.resultBatch.refMaterial.noRefMaterial")
    //                    .build());
    //        } else {
    //            for (ResultBatchRefConcentration fbrc : resultBatch.getResultBatchRefConcentrationList()) {
    //                if (fbrc.getTitrationVolume() == null) {
    //                    messageContext.addMessage(new MessageBuilder().error()
    //                            .code("validator.resultBatch.refMaterial.noTitrationVolume")
    //                            .build());
    //                }
    //                if (fbrc.getWeight() == null) {
    //                    messageContext.addMessage(new MessageBuilder().error()
    //                            .code("validator.resultBatch.refMaterial.noWeight")
    //                            .build());
    //                }
    //            }
    //        }
    //        return !messageContext.hasErrorMessages();
    //    }

    /**
     * This validates a result batch on entry to the editBatch flow. It should
     * whinge and complain if we failed to find the batch
     * 
     * @param form
     * @param messageContext
     * @return
     */
    public boolean validateResultBatchEditOnEntry(ResultBatch resultBatch,
            MessageContext messageContext) {
        int messageCount = messageContext.getAllMessages().length;
        if (resultBatch == null) {
            // batch should have been created by now
            messageContext.addMessage(new MessageBuilder().error()
                    .code("validator.resultBatch.resultBatchNotFound")
                    .build());
        }
        return messageContext.getAllMessages().length == messageCount;
    }

    public boolean validateResultBatchForComplete(ResultBatch resultBatch,
            MessageContext messageContext) {
        List<Integer> sequenceList = new ArrayList<Integer>();
        MethodProperty mpLoi = resultBatch.getMethod()
                .getMethodProperty(Property.ID_MLOI);

        for (ReferenceAnalysis analysis : resultBatch.getReferenceAnalysisList()) {
            if (analysis.getSequenceNo() == null) {
                this.validationMessageAdapter.addMessage("validator.resultBatch.noSequenceNo",
                        messageContext);
            } else if (sequenceList.contains(analysis.getSequenceNo())) {
                this.validationMessageAdapter.addMessage("validator.resultBatch.duplicateSequenceNo",
                        messageContext);
            }
            if (mpLoi != null
                    && mpLoi.isDerived()) {
                this.validateLossOnIgnitionAnalysisWeights(analysis,
                        true,
                        analysis.getReferenceMaterial()
                                .getReferenceMaterialId(),
                        messageContext);
            }
            if (resultBatch.getTechnique()
                    .isTitration()) {
                this.validateTitrationAnalysisWeights(analysis,
                        true,
                        messageContext);
            }
            this.validateAnalysisResults(analysis.getReferenceAnalysisResultList(),
                    analysis.getReferenceMaterial()
                            .getReferenceMaterialId(),
                    messageContext,
                    "validator.resultBatch.referenceAnalysis");
            sequenceList.add(analysis.getSequenceNo());
        }

        for (SampleAnalysis analysis : resultBatch.getSampleAnalysisList()) {
            if (analysis.getSequenceNo() == null) {
                this.validationMessageAdapter.addMessage("validator.resultBatch.noSequenceNo",
                        messageContext);
            } else if (sequenceList.contains(analysis.getSequenceNo())) {
                this.validationMessageAdapter.addMessage("validator.resultBatch.duplicateSequenceNo",
                        messageContext);
            }
            if (mpLoi != null
                    && mpLoi.isDerived()) {
                this.validateLossOnIgnitionAnalysisWeights(analysis,
                        true,
                        analysis.getSample()
                                .getSampleNo()
                                .toString(),
                        messageContext);
            }
            if (resultBatch.getTechnique()
                    .isTitration()) {
                this.validateTitrationAnalysisWeights(analysis,
                        false,
                        messageContext);
            }
            this.validateAnalysisResults(analysis.getSampleAnalysisResultList(),
                    analysis.getSample()
                            .getSampleNo()
                            .toString(),
                    messageContext,
                    "validator.resultBatch.sampleAnalysis");

            sequenceList.add(analysis.getSequenceNo());
        }
        return !messageContext.hasErrorMessages();
    }

    /**
     * This validates a calibration batch on entry to the viewBatch flow. It
     * should whinge and complain if we failed to find the batch
     * 
     * @param form
     * @param messageContext
     * @return
     */
    public boolean validateResultBatchViewOnEntry(ResultBatch batch,
            MessageContext messageContext) {
        int messageCount = messageContext.getAllMessages().length;
        if (batch == null) {
            // add a message here
            messageContext.addMessage(new MessageBuilder().error()
                    .code("validator.resultBatch.jobBatchNotFound")
                    .build());
        }
        return messageContext.getAllMessages().length == messageCount;
    }

    public boolean validateTechniqueMetadata(ResultBatch resultBatch,
            MessageContext messageContext) {
        this.constrainMethodInstrumentProperties(resultBatch);
        this.validateProperties(resultBatch,
                "validator.resultBatch.",
                messageContext,
                "laboratory",
                "technique");
        return !messageContext.hasErrorMessages();
    }

    //    public boolean validateSearchResults(OneSelectionTrackingListDataModel searchResults,
    //            MessageContext messageContext) {
    //        if (searchResults == null
    //                || searchResults.getRowCount() == 0) {
    //            this.validationMessageAdapter.addMessage("validator.search.noResultsFound",
    //                    messageContext);
    //        }
    //        return !messageContext.hasErrorMessages();
    //    }

    protected void constrainMethodInstrumentProperties(ResultBatch resultBatch) {
        // order of precedence is lab, technique, method, instrument
        if (resultBatch.getTechnique() != null
                && resultBatch.getLaboratory() != null
                && !resultBatch.getTechnique()
                        .isPractisedAt(resultBatch.getLaboratory())) {
            resultBatch.setTechnique(null);// technique is no longer valid
        }
        if (resultBatch.getMethod() != null
                && resultBatch.getTechnique() != null
                && !resultBatch.getMethod()
                        .getTechnique()
                        .equals(resultBatch.getTechnique())) {
            resultBatch.setMethod(null);// method is no longer valid
        }
        if (resultBatch.getInstrument() != null
                && resultBatch.getLaboratory() != null
                && !resultBatch.getInstrument()
                        .getLaboratory()
                        .equals(resultBatch.getLaboratory())) {
            resultBatch.setInstrument(null);// instrument is no longer valid
        }
        if (resultBatch.getInstrument() != null
                && resultBatch.getTechnique() != null
                && !resultBatch.getTechnique()
                        .isPractisedOn(resultBatch.getInstrument())) {
            resultBatch.setInstrument(null);// instrument is no longer valid
        }
        if (resultBatch.getInstrument() != null
                && resultBatch.getMethod() != null
                && !resultBatch.getMethod()
                        .isPractisedOn(resultBatch.getInstrument())) {
            resultBatch.setInstrument(null);// instrument is no longer valid
        }
        // try to set some default values
        if (resultBatch.getLaboratory() != null) {
            if (resultBatch.getTechnique() == null) {
                resultBatch.setTechnique(resultBatch.getLaboratory()
                        .getDefaultTechnique());
            }
            if (resultBatch.getTechnique() != null) {
                if (resultBatch.getMethod() == null) {
                    resultBatch.setMethod(resultBatch.getTechnique()
                            .getDefaultMethod(resultBatch.getLaboratory()));
                }
                if (resultBatch.getMethod() != null) {
                    if (resultBatch.getInstrument() != null
                            && !resultBatch.getMethod()
                                    .isPractisedOn(resultBatch.getInstrument())) {
                        resultBatch.setInstrument(resultBatch.getMethod()
                                .getDefaultInstrument());// instrument is no longer valid
                    }
                    if (resultBatch.getInstrument() == null) {
                        resultBatch.setInstrument(resultBatch.getMethod()
                                .getDefaultInstrument(resultBatch.getLaboratory()));
                    }
                }
            }
        }
        if (resultBatch.getLaboratory() == null
                || resultBatch.getTechnique() == null
                || resultBatch.getMethod() == null
                || resultBatch.getInstrument() == null) {
            resultBatch.clearAnalyses();
        }
    }

    protected void validateAnalysisResults(List<? extends AnalysisResult> resultList,
            String subjectId,
            MessageContext messageContext,
            String parentContextPath) {
        for (AnalysisResult result : resultList) {
            if (result.getResultValue() == null
                    || result.getResultUnits() == null) {
                this.validationMessageAdapter.addMessage(parentContextPath
                        + ".incomplete",
                        messageContext);
            } else if (this.appService.findUnitById(StringUtils.trimToNull(result.getResultUnits())) == null) {
                //TODO-refactor this. This could result in thousands of sql queries lookups for large resultbatch
                this.validationMessageAdapter.addMessage(parentContextPath
                        + ".unknownResultUnits",
                        messageContext,
                        result.getResultUnits(),
                        result.getProperty()
                                .getPropertyId(),
                        subjectId);
            }
        }
    }

    /**
     * @param constraintViolations
     * @param messageContext
     * @param parentContextPath
     */
    private void addMessages(Set<ConstraintViolation<ResultBatch>> constraintViolations,
            MessageContext messageContext,
            String parentContextPath) {
        if (constraintViolations != null
                && !constraintViolations.isEmpty()) {
            for (ConstraintViolation<?> violation : constraintViolations) {
                this.validationMessageAdapter.addMessage(violation,
                        messageContext,
                        parentContextPath);
            }
        }
    }

    private void addRemoveReferenceAnalysisObjects(ResultBatch resultBatch,
            ReferenceMaterial referenceMaterial) {
        // Check the specified number of reference analysis objects exist in list
        int count = 0;
        int numTimesUsed = referenceMaterial.getNoTimesReferenceMaterialUsed()
                .intValue();
        for (Iterator<ReferenceAnalysis> iter = resultBatch.getReferenceAnalysisList()
                .iterator(); iter.hasNext();) {
            ReferenceAnalysis referenceAnalysis = iter.next();
            if (referenceAnalysis.getReferenceMaterial()
                    .equals(referenceMaterial)) {
                if (count < numTimesUsed) {
                    count++;
                } else {
                    // Remove excess reference analysis objects
                    iter.remove();
                }
            }
        }
        // If there aren't enough add extra reference analysis objects
        if (count < numTimesUsed) {
            for (int i = 0; i < numTimesUsed
                    - count; i++) {
                this.createNewReferenceAnalysis(resultBatch,
                        referenceMaterial);
            }
        }
    }

    private void createNewReferenceAnalysis(ResultBatch resultBatch,
            ReferenceMaterial referenceMaterial) {
        ReferenceAnalysis referenceAnalysis = new ReferenceAnalysis(resultBatch,
                referenceMaterial);

        referenceAnalysis.setAnalysisPurpose(AnalysisPurposeCode.RS);
        referenceAnalysis.setSequenceNo(Integer.valueOf(resultBatch.getAnalysisList()
                .size() + 1));
        referenceAnalysis.setAnalysisDatetime(resultBatch.getSessionDateTime());
        for (Property property : resultBatch.getMethod()
                .getActiveAndDerivedPropertyList()) {
            KnownReferenceResult knownReferenceResult = resultBatch.getMethodInstrumentCalibration()
                    .getKnownReferenceResult(referenceMaterial,
                            property);
            ReferenceAnalysisResult referenceAnalysisResult = new ReferenceAnalysisResult();
            referenceAnalysisResult.setReferenceAnalysis(referenceAnalysis);
            referenceAnalysisResult.setProperty(property);
            referenceAnalysisResult.setKnownReferenceResult(knownReferenceResult);
            referenceAnalysisResult.setResultUnits(property.getStorageUnits());
            referenceAnalysis.getReferenceAnalysisResultList()
                    .add(referenceAnalysisResult);
        }
        resultBatch.getReferenceAnalysisList()
                .add(referenceAnalysis);
    }

    private void createNewSampleAnalysis(ResultBatch resultBatch,
            JobBatch jobBatch,
            JobBatchSample jobBatchSample) {
        SampleAnalysis sampleAnalysis = new SampleAnalysis(resultBatch,
                jobBatch,
                jobBatchSample.getSample());
        sampleAnalysis.setAnalysisDatetime(resultBatch.getSessionDateTime());
        sampleAnalysis.setSequenceNo(resultBatch.getAnalysisList()
                .size() + 1);
        sampleAnalysis.setSubsampleId(jobBatchSample.getSubsampleId());
        for (Property property : resultBatch.getMethod()
                .getActiveAndDerivedPropertyList()) {
            SampleAnalysisResult sampleAnalysisResult = new SampleAnalysisResult();
            sampleAnalysisResult.setProperty(property);
            sampleAnalysisResult.setSampleAnalysis(sampleAnalysis);
            sampleAnalysisResult.setResultUnits(property.getStorageUnits());
            MethodProperty mp = resultBatch.getMethod()
                    .getMethodProperty(property);
            if (mp != null
                    && mp.isDerived()) {
                if (resultBatch.getMethod()
                        .getTechnique()
                        .isTitration()) {
                    sampleAnalysisResult.setTitrationVolume(BD_ZERO);
                    sampleAnalysisResult.setWeight(BD_ZERO);
                }
            }
            sampleAnalysis.getSampleAnalysisResultList()
                    .add(sampleAnalysisResult);
        }
        resultBatch.getSampleAnalysisList()
                .add(sampleAnalysis);

    }

    private boolean validateLossOnIgnitionAnalysisWeights(Analysis analysis,
            boolean required,
            String subjectId,
            MessageContext messageContext) {
        if (analysis.getInitialWeight() != null
                && analysis.getIncludedWeight() != null
                && analysis.getFinalWeight() != null) {
            if (analysis.getInitialWeight()
                    .doubleValue() < analysis.getIncludedWeight()
                    .doubleValue()) {
                this.validationMessageAdapter.addMessage("validator.resultBatch.incorrectInitialWeight",
                        messageContext,
                        subjectId);
            }
            if (analysis.getInitialWeight()
                    .doubleValue() < analysis.getFinalWeight()
                    .doubleValue()) {
                messageContext.addMessage(new MessageBuilder().info()
                        .code("validator.resultBatch.incorrectFinalWeight")
                        .resolvableArg(analysis.isReferenceAnalysis() ? ((ReferenceAnalysis) analysis).getReferenceMaterial()
                                .getName() : ((SampleAnalysis) analysis).getSample()
                                .getSampleNo())
                        .build());
            }
        } else if (required) {
            this.validationMessageAdapter.addMessage("validator.resultBatch.referenceAnalysis.loiwcValuesIncomplete",
                    messageContext);
        }
        return !messageContext.hasErrorMessages();
    }

    /**
     * There has to be a way to genericize this so it's not locked to
     * ResultBatch????
     * 
     * @param form
     * @param parentContextPath
     * @param messageContext
     * @param properties
     *            (a list of one or more property names)
     * 
     */
    private void validateProperties(ResultBatch form,
            String parentContextPath,
            MessageContext messageContext,
            String... properties) {
        Validate.notEmpty(properties);
        for (String property : properties) {
            this.addMessages(this.validator.validateProperty(form,
                    property),
                    messageContext,
                    parentContextPath);
        }
    }

    private boolean validateTitrationAnalysisWeights(ReferenceAnalysis analysis,
            boolean required,
            MessageContext messageContext) {
        if (!analysis.getResultBatch()
                .getTechnique()
                .isTitration()) {
            return true;
        }
        for (ReferenceAnalysisResult result : analysis.getReferenceAnalysisResultList()) {
            Property property = result.getProperty();
            if (property != null) {
                MethodProperty mp = analysis.getResultBatch()
                        .getMethodProperty(property.getPropertyId());
                if (mp != null
                        && mp.isDerived()) {
                    if (result.getTitrationVolume() != null
                            && result.getWeight() != null
                            && analysis.getResultBatch()
                                    .getTitrantFactor() != null) {
                        // check for unreasonable values here
                    } else if (required) {
                        this.validationMessageAdapter.addMessage("validator.resultBatch.referenceAnalysis.titrationValuesIncomplete",
                                messageContext,
                                analysis.getReferenceMaterial()
                                        .getReferenceMaterialId(),
                                property.getPropertyId());
                    }
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

    private boolean validateTitrationAnalysisWeights(SampleAnalysis analysis,
            boolean required,
            MessageContext messageContext) {
        if (!analysis.getResultBatch()
                .getTechnique()
                .isTitration()) {
            return true;
        }
        for (SampleAnalysisResult result : analysis.getSampleAnalysisResultList()) {
            Property property = result.getProperty();
            if (property != null) {
                MethodProperty mp = analysis.getResultBatch()
                        .getMethodProperty(property.getPropertyId());
                if (mp != null
                        && mp.isDerived()) {
                    if (result.getTitrationVolume() != null
                            && result.getWeight() != null
                            && analysis.getResultBatch()
                                    .getTitrantFactor() != null) {
                        // check for unreasonable values here
                    } else if (required) {
                        this.validationMessageAdapter.addMessage("validator.resultBatch.sampleAnalysis.titrationValuesIncomplete",
                                messageContext,
                                analysis.getSample()
                                        .getSampleNo(),
                                property.getPropertyId());
                    }
                }
            }
        }
        return !messageContext.hasErrorMessages();
    }

}
