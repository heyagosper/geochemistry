<%-- This was once an XML file, it is now a set of variables to be used in the page generation based on perf testing --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- Start: Process Browser Information --%>
	<c:set scope="page" var="_versearch" value="" />
	<c:set scope="page" var="_versearchext" value="" />
	<c:set scope="page" var="_verend" value="" />
	
	<%-- Chrome --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Chrome') > -1}">
			<c:set var="agtag_browser" value="Chrome" />
			<c:set var="_versearch" value="Chrome/" />
			<c:set var="_verend" value="\ " />
		</c:if>
	</c:if>
	
	<%-- OmniWeb --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'OmniWeb') > -1}">
			<c:set var="agtag_browser" value="Chrome" />
			<c:set var="_versearch" value="OmniWeb" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- Safari --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Apple') > -1}">
			<c:set var="agtag_browser" value="Safari" />
			<c:set var="_versearch" value="Version/" />
			<c:set var="_verend" value="\ " />
		</c:if>
	</c:if>

	<%-- Safari --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Google Inc.') > -1}">
			<c:set var="agtag_browser" value="Safari" />
			<c:set var="_versearch" value="Version" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- Opera --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Opera') > -1}">
			<c:set var="agtag_browser" value="Opera" />
			<c:set var="_versearch" value="Version" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- iCab --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'iCab') > -1}">
			<c:set var="agtag_browser" value="iCab" />
			<c:set var="_versearch" value="default" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- Konqueror --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'KDE') > -1}">
			<c:set var="agtag_browser" value="Konqueror" />
			<c:set var="_versearch" value="default" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- Firefox --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Firefox') > -1}">
			<c:set var="agtag_browser" value="Firefox" />
			<c:set var="_versearch" value="Firefox/" />
			<c:set var="_verend" value="." />
		</c:if>
	</c:if>

	<%-- Camino --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Camino') > -1}">
			<c:set var="agtag_browser" value="Camino" />
			<c:set var="_versearch" value="default" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- Netscape --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Netscape') > -1}">
			<c:set var="agtag_browser" value="Netscape" />
			<c:set var="_versearch" value="default" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- Explorer --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'MSIE') > -1}">
			<c:set var="agtag_browser" value="Explorer" />
			<c:set var="_versearch" value="MSIE" />
			<c:set var="_versearchext" value="\ " />
			<c:set var="_verend" value=";" />
		</c:if>
	</c:if>

	<%-- Mozilla --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Gecko') > -1}">
			<c:set var="agtag_browser" value="Mozilla" />
			<c:set var="_versearch" value="rv:" />
			<c:set var="_verend" value=")" />
		</c:if>
	</c:if>

	<%-- Netscape --%>
	<c:if test="${fn:length(agtag_browser) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Mozilla') > -1}">
			<c:set var="agtag_browser" value="Netscape" />
			<c:set var="_versearch" value="Mozilla" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>
<%-- End: Process Browser Information --%>

<%-- Start: Process Browser Version Information --%>
	<%-- If a version is found --%>
	<c:if test="${fn:length(_versearch) > 0}">
		<c:set var="_verstart">
			${(fn:indexOf(_agent, _versearch) + fn:length(_versearch) )}
		</c:set>
		<c:set var="_versubstr">
			${fn:substring(_agent, _verstart, fn:length(_agent) )}
		</c:set>
		<c:set var="agtag_bversion">
	  	${fn:substring(_versubstr, 0, fn:indexOf(_versubstr, _verend ) )}
		</c:set>
		<c:set var="agtag_bversion">
	  	${fn:replace(agtag_bversion, "_", ".")}
		</c:set>
		<c:set var="agtag_bversion">
			<fmt:formatNumber value="${fn:substring(agtag_bversion, 0, fn:indexOf(agtag_bversion, '.' ) )}" type="number" maxIntegerDigits="3" />
		</c:set>
	</c:if>
<%-- End: Process Browser Version Information --%>


<%-- Start: Process OS Version Information --%>
	<c:set scope="page" var="_versearch" value="" />
	<c:set scope="page" var="_versearchext" value="" />
	<c:set scope="page" var="_verend" value="" />

	<%-- MOBILE: Android --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Android') > -1}">
			<c:set var="agtag_os" value="Mobile Droid" />
			<c:set var="_versearch" value="Android " />
			<c:set var="_verend" value=";" />
		</c:if>
	</c:if>

	<%-- TABLET: Kindle --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Cloud9') > -1}">
			<c:set var="agtag_os" value="Mobile Droid KindleFire" />
			<c:set var="_versearch" value="Silk/" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- TABLET: Kindle --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Silk-Accelerated') > -1}">
			<c:set var="agtag_os" value="Mobile Droid KindleFire" />
			<c:set var="_versearch" value="Silk/" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>

	<%-- MOBILE: iPhone --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'iPhone') > -1}">
			<c:set var="agtag_os" value="mobile iPhone IOS" />
			<c:set var="_versearch" value="CPU OS " />
			<c:set var="_verend" value=" like" />
		</c:if>
	</c:if>

	<%-- TABLET: iPad --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'iPad') > -1}">
			<c:set var="agtag_os" value="tablet mobile iPad IOS" />
			<c:set var="_versearch" value="CPU OS " />
			<c:set var="_verend" value=" like" />
		</c:if>
	</c:if>

	<%-- MOBILE: iPod --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'iPod') > -1}">
			<c:set var="agtag_os" value="mobile iPod IOS" />
			<c:set var="_versearch" value="CPU OS " />
			<c:set var="_verend" value=" like" />
		</c:if>
	</c:if>

	<%-- Windows --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Win') > -1}">
			<c:set var="agtag_os" value="Windows" />
			<c:set var="_versearch" value="Windows NT" />
			<c:set var="_verend" value=";" />
		</c:if>
	</c:if>

	<%-- Mac --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Mac') > -1}">
			<c:set var="agtag_os" value="Mac" />
			<c:set var="_versearch" value=" OS X" />
			<c:set var="_verend" value=";" />
		</c:if>
	</c:if>

	<%-- Linux --%>
	<c:if test="${fn:length(agtag_os) == 0}">
		<c:if test="${fn:indexOf(_agent, 'Linux') > -1}">
			<c:set var="agtag_os" value="Linux" />
			<c:set var="_versearch" value="" />
			<c:set var="_verend" value="" />
		</c:if>
	</c:if>
<%-- End: Process OS Version Information --%>


<%-- Start: Process OS Version Information --%>
	<%-- If a version is found --%>
	<c:if test="${fn:length(_versearch) > 0}">
		<c:set var="_verstart">
			${(fn:indexOf(_agent, _versearch) + fn:length(_versearch) )}
		</c:set>
		<c:set var="_versubstr">
			${fn:substring(_agent, _verstart, fn:length(_agent) ) }
		</c:set>
		<c:set var="agtag_oversion">
	  	${fn:substring(_versubstr, 0, fn:indexOf(_versubstr, _verend) )}
		</c:set>
		<c:set var="agtag_oversion">
	  	${fn:replace(agtag_oversion, "_", ".")}
		</c:set>
		<c:set var="agtag_oversion">
	  	${fn:substring(agtag_oversion, 0, fn:indexOf(agtag_oversion, '.')+2 )}
		</c:set>
	</c:if>
<%-- End: Process OS Version Information --%>


<%-- Start: Extend Process OS Version Information --%>
	<c:if test="${fn:length(agtag_oversion) > 0}">
		<%-- Process OS version - Windows --%>
		<c:if test="${agtag_os == 'Windows'}">
			<c:choose>
		    <c:when test="${agtag_oversion == 5}"  > <c:set var="agtag_oversion" value="Windows2k" /> </c:when>
		    <c:when test="${agtag_oversion == 5.1}"> <c:set var="agtag_oversion" value="WindowsXP" /> </c:when>
		    <c:when test="${agtag_oversion == 5.2}"> <c:set var="agtag_oversion" value="WindowsXP" /> </c:when> <%-- XP64bit --%>
		    <c:when test="${agtag_oversion == 6.0}"> <c:set var="agtag_oversion" value="WindowsVista" /> </c:when>
		    <c:when test="${agtag_oversion == 6.1}"> <c:set var="agtag_oversion" value="Windows7" /> </c:when>
		    <c:otherwise><c:set var="agtag_oversion" value="Windows7Plus" /></c:otherwise>
			</c:choose>
		</c:if>
	
		<%-- Process OS version - Mac --%>
		<c:if test="${agtag_os == 'Mac'}">
			<c:choose>
		    <c:when test="${agtag_oversion == 10.0}"  > <c:set var="agtag_oversion" value="osxCheetah" /> </c:when>
		    <c:when test="${agtag_oversion == 10.1}"  > <c:set var="agtag_oversion" value="osxPuma" /> </c:when>
		    <c:when test="${agtag_oversion == 10.2}"  > <c:set var="agtag_oversion" value="osxJaguar" /> </c:when>
		    <c:when test="${agtag_oversion == 10.3}"  > <c:set var="agtag_oversion" value="osxPanther" /> </c:when>
		    <c:when test="${agtag_oversion == 10.4}"  > <c:set var="agtag_oversion" value="osxTiger" /> </c:when>
		    <c:when test="${agtag_oversion == 10.5}"  > <c:set var="agtag_oversion" value="osxLeopard" /> </c:when>
		    <c:when test="${agtag_oversion == 10.6}"  > <c:set var="agtag_oversion" value="osxSnowLeopard" /> </c:when>
		    <c:when test="${agtag_oversion == 10.7}"  > <c:set var="agtag_oversion" value="osxLion" /> </c:when>
		    <c:when test="${agtag_oversion == 10.8}"  > <c:set var="agtag_oversion" value="osxMountainLion" /> </c:when>
		    <c:otherwise><c:set var="agtag_oversion" value="osxPlus" /></c:otherwise>
			</c:choose>
		</c:if>
	
		<%-- Process OS version - Android --%>
		<c:if test="${agtag_os == 'Droid'}">
			<c:choose>
		    <c:when test="${agtag_oversion == 2.3}"  > <c:set var="agtag_oversion" value="Gingerbread" /> </c:when>
		    <c:otherwise><c:set var="agtag_oversion" value="Froyo" /></c:otherwise>
			</c:choose>
		</c:if>
	
	
		<%-- Process IOS version - I(pad/phone/pod) --%>
		<c:set var="_check">${fn:endsWith(agtag_os, " IOS") || fn:endsWith(agtag_os, " KindleFire") }</c:set>
			
		<c:if test="${_check == 'true'}">
			<c:set var="agtag_os">
				${agtag_os}${fn:substring(agtag_oversion, 0, fn:indexOf(agtag_oversion, '.') )}
			</c:set>
			<c:set var="agtag_oversion" value="" />
		</c:if>
	</c:if>
<%-- End: Extend Process OS Version Information --%>
