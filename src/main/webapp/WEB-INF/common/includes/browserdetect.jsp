<%-- browser detection code from http://knoll.pro/2012/08/server-side-browser-detection/
 set the following pagescope variables
  _agent containing the user-agent header value
  agtag_browser with the name of the browser eg Chrome, Explorer 
  agtag_bversion with the browser version name/number
  agtag_os with the client OS name
  agtag_oversion with the client OS version
   
--%>
<%--JSP specific imports--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Set Defaults --%>
<%-- -- Set User Agent --%>
<c:set var="_agent"> <%= request.getHeader("user-agent") %> </c:set>

<%-- -- Set Default Browser Information --%>
<c:set var="agtag_browser" value="" />
<c:set var="agtag_bversion" value="" />

<%-- -- Set Default OS Information --%>
<c:set var="agtag_os" value="" />
<c:set var="agtag_oversion" value="" />
    
<%@ include file="/WEB-INF/common/includes/browserdetect-info.jsp" %>


<%-- Create an exception for IE intranet issues --%>
<c:if test="${fn:indexOf(_agent, 'Trident') > -1}">
    <c:set var="_verstart">
        ${(fn:indexOf(_agent, "Trident/") + 8 )}
    </c:set>
    
    <c:set var="_versubstr">
        ${fn:substring(_agent, _verstart, fn:length(_agent) )}
    </c:set>
    
    <c:set var="_checkversion">
        ${fn:substring(_versubstr, 0, fn:indexOf(_versubstr, ';' ) )}
    </c:set>
    
  <c:if test="${_checkversion == 4.0}"> <c:set var="agtag_bversion" value="8" /> </c:if>
  <c:if test="${_checkversion == 5.0}"> <c:set var="agtag_bversion" value="9" /> </c:if>
</c:if>
<%-- create convenience classnames eg for inclusion in the body element --%>
<c:set var="browserdetectinfo" value="${agtag_browser} ${agtag_browser}${agtag_bversion} ${agtag_os} ${agtag_oversion}" />
