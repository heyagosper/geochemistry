/* jQuery Setup
 * Hand the $ variable back to it's original owner, to avoid conflicts with Dojo, A4J, etc.
 */

jQuery.noConflict();

/*
 * gaFramework initiation
 * 
 */

if (!gaFramework) {
    var gaFramework = {};
}

/*
 * Custom event for when the document is ready This event is called *after* all
 * the files are loaded by the lazy loader on page load, and is never called
 * again (until the next page is loaded, obviously.) You really shouldn't
 * trigger the ready event manually.
 * 
 * To bind functionality to the gaFrameworkReady event, use the following:
 * gaFramework.ready(function(){ // Do something here });
 * 
 * To trigger the modified event, use the following: gaFramework.ready();
 * 
 */

gaFramework.ready = function(fn) {
    if (typeof fn == 'function')
        jQuery(document).bind('gaFrameworkReady', fn);
    else if (!fn) {
        jQuery(document).trigger('gaFrameworkReady');
    }
    return this;
};

/*
 * Custom event for when the document is modified This event is triggered by
 * various AJAX calls and automatically triggers the lazy load.
 * 
 * To bind functionality to the modified event, use the following:
 * gaFramework.modified(function(){ // Do something here });
 * 
 * To trigger the modified event, use the following: gaFramework.modified();
 * 
 */

gaFramework.modified = function(fn) {
    if (typeof fn == 'function')
        jQuery(document).bind('modified', fn);
    else if (!fn)
        jQuery(document).trigger('modified');
    return this;
};

/*
 * Firebug Console What we're doing here is overriding the console variable, so
 * that browsers don't error out when Firebug doesn't exist. This makes for
 * easier testing of the javascript.
 */
(function(window) {
    if (typeof window.console === "undefined") {
        window.console = {};
    }
    if (typeof console === "undefined") {
        var console = window.console;
    }
    var names = [ 'log', 'debug', 'info', 'warn', 'error', 'assert', 'dir', 'dirxml', 'group', 'groupEnd', 'time', 'timeEnd', 'count', 'trace',
            'profile', 'profileEnd' ];
    for ( var i = 0; i < names.length; ++i) {
        if (window.console[names[i]] == null || typeof window.console[names[i]] === "undefined") {
            window.console[names[i]] = function() {
            };
        }
        ;
    }
})(this);

/*
 * String Left Pad Pads the string out to a preset length with whatever string
 * is passed to it.
 */

String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};

/*
 * String Right Pad Pads the string out to a preset length with whatever string
 * is passed to it.
 */

String.prototype.rpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
};

/*
 * Get the index of an object in an array Some browsers don't have this method,
 * so we're providing our own version here to work around that.
 */

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj, fromIndex) {
        if (fromIndex == null) {
            fromIndex = 0;
        } else if (fromIndex < 0) {
            fromIndex = Math.max(0, this.length + fromIndex);
        }
        for ( var i = fromIndex, j = this.length; i < j; i++) {
            if (this[i] === obj)
                return i;
        }
        return -1;
    };
}

/*
 * Dynamic Javascript Lazyloader
 * 
 */

gaFramework.dynamicJS = {

    // Variables
    frameworkTag : null,
    jsDirectory : '',
    queue : [],
    loaded : [],
    lazyLoadCallback : function() {
    },

    // Initialise the Javascript loader
    init : function() {
        // Variables
        var dynamicJS = gaFramework.dynamicJS;
        // Get the location of the framework.js script
        jQuery.each(jQuery('script'), function(i, script) {
            var src = jQuery(script).attr('src');
            if (src && src.match(/(.*?)framework\.js$/)) {
                var matches = src.match(/(.*?)framework\.js$/);
                dynamicJS.frameworkTag = jQuery(script);
                dynamicJS.jsDirectory = matches[1];
            }
        });
    },

    // Initialise the Javascript loader
    doLazyLoad : function(fn) {
        // Variables
        var dynamicJS = gaFramework.dynamicJS;
        var loadUnconditionally = true; // set this to false for
        // conditional/just-in-time loading of
        // js files
        //
        if (fn) {
            dynamicJS.lazyLoadCallback = fn;
        }
        // Load jQuery plugins
        dynamicJS.doQueue('jquery.md5.js');
        dynamicJS.doQueue('jquery.textchange.js');
        dynamicJS.doQueue('jquery.metadata.js');
        dynamicJS.doQueue('jquery.preload.js', function() {
            jQuery.preloadCssImages();
        });
        // Default scripts to load
        dynamicJS.doQueue('ga.utilities.js');
        dynamicJS.doQueue('ga.workflow.js');
        dynamicJS.doQueue('ga.modal.js');
        dynamicJS.doQueue('ga.richfaces.js');
        dynamicJS.doQueue('ga.jquery-ui.js');
        // Conditional loading
        // Various table tidbits
        if (loadUnconditionally || jQuery('table').length) {
            // Identify table cells and rows.
            dynamicJS.doQueue('ga.table.identify.js');

            // Alternate row colours for tables.
            dynamicJS.doQueue('ga.table.rowAlterColors.js');
            // Various table tidbits
            dynamicJS.doQueue('ga.table.fixedHeader.js');
        }
        // Form validation
        if (loadUnconditionally || jQuery('form,input,textarea,select').length) {
            dynamicJS.doQueue('ga.form.validate.js');
        }
        // Form validation
        if (loadUnconditionally || jQuery('input.selectAll[type=checkbox],input.changeAll').length) {
            dynamicJS.doQueue('ga.form.selectall.js');
        }
        // Date and Time
        if (loadUnconditionally || jQuery('.form_input.date,.form_input.time,.form_input.dateTime').length) {
            dynamicJS.doQueue('ga.dropdown.js');
            dynamicJS.doQueue('ga.form.datetime.js');
        }
        // Autosuggest
        // if( loadUnconditionally || jQuery('input.autosuggest').length ) {
        // dynamicJS.doQueue('ga.dropdown.js');
        // dynamicJS.doQueue('ga.autosuggest.js',function(){
        // jQuery.autosuggest.init();
        // });
        // }
        // Accordion Panels
        // if( loadUnconditionally || jQuery('.accordionGroup').length ) {
        dynamicJS.doQueue('ga.accordion.js');
        // }
        // radioCheckbox
        if (loadUnconditionally || jQuery('.radioCheckboxGroup').length) {
            dynamicJS.doQueue('ga.table.radioCheckbox.js');
        }
        // Slider
        if (loadUnconditionally || jQuery('input.slider').length) {
            dynamicJS.doQueue('ga.form.slider.js');
        }
        // Placeholder (HTML5)
        if (loadUnconditionally || jQuery('input[placeholder]').length) {
            dynamicJS.doQueue('ga.form.placeholder.js');
        }
        // Search (HTML5)
        if (loadUnconditionally || jQuery('input[type="search"]').length) {
            dynamicJS.doQueue('ga.form.search.js');
        }
        // Image thumbnails
        if (loadUnconditionally || jQuery('.ga-thumbnail-container').length) {
            dynamicJS.doQueue('ga.image.thumbnails.js');
        }
        // Tables
        if (loadUnconditionally || jQuery('table').length) {
            dynamicJS.doQueue('jquery.tablednd.js');
            dynamicJS.doQueue('ga.table.options.js');
            dynamicJS.doQueue('ga.menu.js');
            dynamicJS.doQueue('ga.table.grid.js');
            dynamicJS.doQueue('ga.table.rowAlterColors.js');
            dynamicJS.doQueue('jquery.scrollTo-1.4.2.js');
        }
        /*
         * / Expandable Tables if( loadUnconditionally ||
         * jQuery('table.expandable').length ) {
         * dynamicJS.doQueue('ga.table.expander.js',function(){
         * jQuery.tableExpander.init(); }); } //
         */
        // OpenLayers support
        if (loadUnconditionally || jQuery('#map').length) {
            dynamicJS.doQueue('ga.map.js');
        }
        // popup displays (depends on dojo dialog)
        if (loadUnconditionally || jQuery('a.popupLink').length) {
            dynamicJS.doQueue('ga.popupLink.js');
        }

    },

    // Load an external script
    doQueue : function(file, fn) {
        // Variables
        var dynamicJS = gaFramework.dynamicJS;
        var element = null;
        // Start the timer.
        if (!dynamicJS.startTime) {
            dynamicJS.startTime = new Date();
        }
        // Add the jsDirectory if we only have the filename
        if (file.indexOf('/') < 0) {
            file = dynamicJS.jsDirectory + file;
        }
        // Find this script in the queue
        for ( var i = 0; i < dynamicJS.queue.length; i++) {
            var obj = dynamicJS.queue[i];
            if (obj.file == file) {
                element = obj;
            }
        }
        // If the script object doesn't exist...
        if (!element) {
            // Log the element
            dynamicJS.queue.push(new gaScript(file));
            // Apply the onload function if it exists
            if (fn) {
                var pos = dynamicJS.queue.length - 1;
                dynamicJS.queue[pos].onload = fn;
            }
        }
    },

    // Load an framework script
    loading : function() {
        // Variables
        var loading = 0;
        var dynamicJS = gaFramework.dynamicJS;
        // Find this script in the queue
        for ( var i = 0; i < dynamicJS.queue.length; i++) {
            var obj = dynamicJS.queue[i];
            if (obj.loaded != true) {
                loading++;
            }
        }
        // Output
        return loading;
    }
};

/*
 * gaScript Class This is a custom class used by the lazyloader to manage the
 * scripts it loads.
 */

var gaScript = function(file) {
    // Variables
    var dynamicJS = gaFramework.dynamicJS;
    var self = this;
    // Define the file
    this.file = file;
    // Allow the user to set an onload function
    this.onload = function() {
    };
    // Create the node
    this.element = document.createElement('script');
    this.element.setAttribute("src", file);
    this.element.setAttribute("type", "text/javascript");
    // Add it to the content
    document.getElementsByTagName("head")[0].appendChild(this.element);
    // Do some stuff when the script loads
    this.element.onload = function() {
        // Deincrement the count of loading files
        self.loaded = true;
        // Deincrement the count of loading files
        dynamicJS.loaded.push(self);
        // Run the onload function set by the developer.
        if (self.onload) {
            self.onload();
        }
        // If everything is loaded...
        if (!dynamicJS.loading()) {
            // Log the time to the console.
            var endTime = new Date();
            // Log the files loaded
            console.info('Files Loaded Dynamically (' + (endTime - dynamicJS.startTime) + 'ms): ' + dynamicJS.loaded.toString().replace(/,/g, ', '));
            // Run the lazyload callback and then reset it.
            dynamicJS.lazyLoadCallback();
            dynamicJS.lazyLoadCallback = function() {
            };
            // Empty the loaded array
            dynamicJS.loaded = [];
        }
    };
    // Since the code above doesn't work in IE, the following code should kick
    // things off.
    this.element.onreadystatechange = function() {
        if (this.readyState == 'complete' || this.readyState == 'loaded') {
            self.element.onload();
        }
    };
    // Define the toString method
    gaScript.prototype.toString = function() {
        // Variables
        var modFile = this.file;
        modFile = (modFile.indexOf('?') >= 0) ? modFile.substr(0, modFile.indexOf('?')) : modFile;
        modFile = (modFile.indexOf('/') >= 0) ? modFile.substr(modFile.lastIndexOf('/') + 1) : modFile;
        return modFile;
    };
};

/*
 * Custom filter for richfaces autocomplete component. This ensures an empty
 * select item is always available in the drop down list. This is here because
 * ga.richfaces.js was not loaded in time for it to be used.
 */
gaFramework.autoCompleteFilter = function(subString, value) {
    if (subString.length >= 1) {
        if (value.startsWith(subString) || value == "") {
            return true;
        }
    }
    return false;
};

/*
 * Initialise the lazy loader
 */

gaFramework.dynamicJS.init();

/*
 * On document modified...
 */

gaFramework.modified(function() {
    // Load the files
    gaFramework.dynamicJS.doLazyLoad();
});

/*
 * On document ready...
 */

jQuery(document).ready(function() {
    // Load the files
    gaFramework.dynamicJS.doLazyLoad(function() {
        gaFramework.ready();
    });
});
