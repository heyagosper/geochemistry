(function(jQuery) {
	
	/* Set up the accordion namespace
	 *
	 */	
		
	jQuery.accordion = {};
			
	/* 
	 *
	 */	
		
	// override this to change the default accordion open/close speed
	if (gaFramework.accordionDefaultSpeed === undefined) {
	   gaFramework.accordionDefaultSpeed = 0;
	}
	
    /* 
     * restyle accordion groups
     *  by default we mark the accordionGroup itself to indiate it has been styled
     *  to have it on a different child element use
     *  add the pseudo class {accordionMarkerElem:'<selector>'}
     *  eg {accordionMarker:'> tbody'} to mark the tbody in a table (useful with a jsf datascroller)
     *    
     */   
          
    jQuery.fn.accordion = function() {
     // Iterate through the panel groups
          jQuery(this).each( function() {
          var menu = jQuery(this);
          var metadata = menu.metadata();
          var marker = metadata.accordionMarker; // parse accordionMarker key value 
          if (marker == null){ // default is to put the marker on the accordionGroup
             if (menu.hasClass('hasAccordion')){
                return true; // skip to next group
             }else {
                menu.addClass('hasAccordion');
             }
          } else {
             var markerElem = menu.find(marker); 
             if (markerElem.hasClass('hasAccordion')){
                  return true; // skip to next group
             } else {
                  markerElem.addClass('hasAccordion');
             }
           }
		 // accordion panels need to start visible in case anything goes wrong in the js-tier.
		 // so hide them now and reopen some or all as required
			if( ! menu.hasClass('closed') ) {
				menu.find(".accordionPanel .panelBody").hide();
			}
		 // Setup some basic stuff for the panels
			if( ! jQuery( '.accordionPanel .panelHead div.marker', menu ).length ) {
				jQuery( '.accordionPanel .panelHead', menu ).append('<div class="marker"></div>');
			}
		 // Event listener to toggle specific panels
			jQuery('.panelHead',menu).click(function(e){
				var target = jQuery(e.target);
				if( target.is('a') ) {					
					return; // leave it alone
				}

				//console.time('accordion.js.panelhead.click');
			 // We don't animate for options links
				if( jQuery(e.target).hasClass('options') || ! jQuery(e.target).parents('.options').length ) {
				 // Variables
					var panel				=	jQuery(this).closest('.accordionPanel');
				 // Open the panel on a forced accordion
					if( ! panel.hasClass('selected') && menu.hasClass('force') ) {
						jQuery.accordion.closePanel( jQuery('.accordionPanel',menu) );
						jQuery.accordion.openPanel(panel);
					}
				 // Open the panel if it's closed
					else if( ! panel.hasClass('selected') && ! menu.hasClass('force') ) {
						jQuery.accordion.openPanel(panel);
					}
				 // Close the panel if its open
					else if( panel.hasClass('selected') && ! menu.hasClass('force') ) {
						jQuery.accordion.closePanel(panel);
					}
				}
				//console.timeEnd('accordion.js.panelhead.click');
			});
		 // For menus that have more than one panel...
			if( jQuery('.accordionPanel',menu).length > 1 && ! menu.hasClass('force') ) {
			 // Setup the post links
				var postLinks				=	jQuery('<div></div>').addClass('postLinks').appendTo(menu);
			 // Link to contract all the panels
				var contractAll = jQuery('<a></a>')
					.addClass('contractAll')
					.html('Contract All')
					.bind('click',function(){
						var togglePos = Math.floor(contractAll.position().top);
						var scrollTop = jQuery(window).scrollTop();
						var scrollOffset = togglePos - scrollTop;
						jQuery.accordion.closePanel( jQuery('.accordionPanel.selected',menu),0 );
						var newTogglePos = Math.floor(contractAll.position().top);
						var newTop=newTogglePos - scrollOffset;
						// skip reposition until we can test with accordions inside a div with scrollbars, eg AFGN
						//console.log('adjusting toggle'+', togglePos='+togglePos+', scrollTop='+scrollTop+', scrollOffset='+scrollOffset+', newTogglePos='+newTogglePos+', newTop='+newTop+', height='+jQuery(window).height());
						//jQuery(window).scrollTop(newTop); // reposition the page to put the toggle back in place
					})
					.appendTo(postLinks);
			 // Hide this link if there are no panels closed
				if( ! jQuery('.accordionPanel.selected',menu).length ) {
					contractAll.hide();
				}
			 // Expand all causes all kinds of craziness on ajax loaded panels
				if( ! jQuery('.panelBody',this).children('a.panelBodyContent').length ) {
				 // Link to expand all the panels
					var expandAll = jQuery('<a></a>')
						.addClass('expandAll')
						.html('Expand All')
						.bind('click',function(){
							var togglePos = Math.floor(expandAll.position().top);
							var scrollTop = jQuery(window).scrollTop();
							var scrollOffset = togglePos - scrollTop;
							jQuery.accordion.openPanel( jQuery('.accordionPanel',menu).not('.selected'),0 );
							var newTogglePos = Math.floor(expandAll.position().top);
							var newTop=newTogglePos - scrollOffset;
							// skip reposition until we can test with accordions inside a div with scrollbars, eg AFGN
							//console.log('adjusting toggle'+', togglePos='+togglePos+', scrollTop='+scrollTop+', scrollOffset='+scrollOffset+', newTogglePos='+newTogglePos+', newTop='+newTop+', height='+jQuery(window).height());
							//jQuery(window).scrollTop(newTop); // reposition the page to put the toggle back in place
						})
						.appendTo(postLinks);
				 // Hide this link if there are no panels open
					if( ! jQuery('.accordionPanel',menu).not('.selected').length ) {
						expandAll.hide();
					}
				}
			}
		 // Open any panels set that way by default
			var openPanels = menu.find('.accordionPanel.open');
			if (openPanels.length) {
				jQuery.accordion.openPanel( openPanels, 0 );
				openPanels.removeClass('open');
			}
		});
	 // Return the accordion panels
		return this;
	};
	
	/* 
	 *
	 */	
		
	jQuery.accordion.openPanel = function( accordion, openSpeed, asyncron ) {
		if (typeof asyncron === 'undefined') {
			asyncron = true;
		}
		
		jQuery(accordion).each( function() {
			if( jQuery(this).hasClass('accordionPanel') ) {
			 // Variables
				var menu					=	jQuery(this).closest('.accordionGroup');
				var panel					=	jQuery(this);
				var postOptions				=	jQuery('.postLinks',menu);
				var speed 					=	openSpeed ? openSpeed : gaFramework.accordionDefaultSpeed;
			 // If we have a contentlink in the body, we load the content via ajax
				if( jQuery('.panelBody',this).children('a.panelBodyContent').length ) {
				 // Variables
					var href				=	jQuery('.panelBody',panel).children('a.panelBodyContent').attr('href');
					if(href.length) {
					 // Turn on the loading throbber
						jQuery('.panelHead div.marker',this).addClass('loading');
					 // Load the content
						jQuery.ajax({
							async: 		asyncron,
							url:		href,
							dataType:	'text',
							cache:		false,
							success: 	function( html, textStatus, xhr ) {
							 // Variables
								var html		=	jQuery( html );
							 // Load the content
								var panelbody		=	jQuery('.panelBody',panel);
								var hrefBase	=	href.match(/^[\w\W\/]*\//i);
								var currentUrl		=	window.location.toString();
								var currentBase		=	currentUrl.substr(0,currentUrl.lastIndexOf('/')+1);
							 // Loop through the elements with links to outside content
								jQuery.each( jQuery('*[href],*[src]',html), function( i, text ) {
								 // Variables
									var attrtype	=	( jQuery(this).attr('href') ) ? 'href' : 'src';
								 // For relative urls...
									if( ! jQuery(this).attr(attrtype).replace(currentBase,'').match(/^([\w]+:|\/)/i) ) {
										var url 	=	jQuery(this).attr(attrtype).replace(currentBase,'').replace(/^\.\//, "" );
										jQuery(this).attr(attrtype,hrefBase+url);
										}
									});
							 // Place the content
								var content			=	( jQuery('.container-pad',html).length ) ? jQuery('.container-pad',html).html() : jQuery('body',html).html();
								jQuery(panelbody).html( content );
								
								jQuery(panel).addClass('selected');
								jQuery('.panelHead',panel).addClass('selected');
								// Animate the open
								jQuery('.panelBody:hidden',panel).slideDown( speed, function() {
									jQuery(document).modified();
									jQuery(window).resize();
								}); // End of slideDown
							},
							error:		function( xhr, textStatus, error ) {
								alert( 'An error occured while attempting to get the panel content: "'+xhr.status+' '+xhr.statusText+'".' );
							},
							complete: 	function( xhr, textStatus ) {
								jQuery('.panelHead div.marker',panel).removeClass('loading');
							}
						}); // End of ajax call
					} // End of if
					else {
						 // Add the selected class to the panel
						jQuery(panel).addClass('selected');
						jQuery('.panelHead',panel).addClass('selected');
						// Animate the open
						jQuery('.panelBody:hidden',panel).slideDown( speed, function() {
							jQuery(document).modified();
							jQuery(window).resize();
						}); // End of slideDown
					}
				}
				else {
				 // Add the selected class to the panel
					jQuery(panel).addClass('selected');
					jQuery('.panelHead',panel).addClass('selected');
				 // Animate the open
					if( speed === 0 ) {
				 			jQuery('.panelBody:hidden',this).show();
				 		jQuery(document).modified();
						jQuery(window).resize();
					}
					else {
						jQuery('.panelBody:hidden',this).slideDown( speed, function(){
							jQuery(document).modified();
							jQuery(window).resize();
						});
					}
				}
				 // Update the 'Expand All' link.
				jQuery('a.contractAll:hidden',postOptions).show();
				if( ! jQuery('.accordionPanel',menu).not('.selected').length ) {
					jQuery('a.expandAll',postOptions).hide();
				}
			}
		});
		//console.timeEnd('accordion.js.openPanel');
	};
	
	/* 
	 *
	 */	
		
	jQuery.accordion.closePanel = function( accordion, closeSpeed ) {
		//console.time('accordion.js.closePanel');
	 // Define a function to close a panel
		jQuery(accordion).map( function() {
			if( jQuery(this).hasClass('accordionPanel') ) {
			 // Variables
				var menu			=	jQuery(this).parents('.accordionGroup');
				var postOptions		=	jQuery('.postLinks',menu);
				var speed 			=	closeSpeed ? closeSpeed : gaFramework.accordionDefaultSpeed;
			 // Remove the selected class from the panel
				jQuery(this).removeClass('selected');
				jQuery('.panelHead',this).removeClass('selected');
			 // Update the 'Expand All' link.
				jQuery('a.expandAll:hidden',postOptions).show();
				if( ! jQuery('.accordionPanel.selected',menu).length ) {
					jQuery('a.contractAll',postOptions).hide();
				}
			 // Animate the close
				if (speed === 0) {
						jQuery('.panelBody:visible',this).hide();
				}else {
					jQuery('.panelBody:visible',this).slideUp( speed, function(){
						jQuery(window).resize();
					});
				}
			}
		});
		//console.time('accordion.js.closePanel');
	};
	
	/* 
	 *
	 */	
		
	jQuery.accordion.init = function() {
		jQuery('.accordionGroup').accordion();
		gaFramework.modified(function(){
			jQuery('.accordionGroup').accordion();
		});
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.accordion.init();
});	
