(function(jQuery) {
	
	/* Connect a generic dropdown to a text field
	 * Setting autoEmpty to true will automatically empty the dropdown when it is closed
	 */	

	jQuery.fn.dropdown = function() {
		if( jQuery(this).length ) {
		 // Variables
			var input								=	jQuery(this).attr({ autocomplete: 'off' });
			var autoSubmit							=	( input.hasClass('autoSubmit') || input.hasClass('autosubmit') );
			var form = input.parents('form');
			var id = jQuery(this).id();
			if( ! id ) {
				id = gaFramework.identifier( this );
				input.id( id );
			}
		 // Where is the input, relative to the form?
			var inputTop = input.offset().top - form.offset().top;
			var inputLeft = input.offset().left - form.offset().left;
		 // Setup the dropdown menu
			var dropdown							=	jQuery('<div></div>'+"\n")
				.id(id + '-dropdown')
				.css({
					display: 'none',
					top: inputTop + input.outerHeight(),
					left: inputLeft,
					width: input.innerWidth()
				})
				.addClass('dropdown')
				.appendTo(form);
		 //	Define a special function for hiding the dropdown, so we can autoEmpty it
			dropdown.close = function( speed ) {
				dropdown.hide( speed ? speed : 100 );
			};
			// flag to tracing if the up/down key pressed
			dropdown.keymoved = false;
		 // When the user presses down on a key...
			input.keydown(function( e ) {
			 // Hide the dropdown
				var timeVal = input.val();
				switch( e.keyCode ) {
					case 9 : // Tab Key							 
					case 13: // Enter key	
						if( ( timeVal == '' || dropdown.keymoved ) && jQuery('li.keyhover a.option',dropdown).length ) {
							jQuery('li.keyhover a.option',dropdown).click();
						}
						break;
					case 37: // Left key
					case 39: // Right key
					 // We don't want anything happening when we try to move side to side.
						break;    
					case 38: // Up key
						if( ! jQuery('ul li:first',dropdown).hasClass('keyhover') )
							jQuery('li.keyhover',dropdown).removeClass('keyhover').prev().addClass('keyhover');
							dropdown.keymoved = true;
						return false;
						break;	
					case 40: // Down key
						if( ! jQuery('ul li:last',dropdown).hasClass('keyhover') )
							jQuery('li.keyhover',dropdown).removeClass('keyhover').next().addClass('keyhover');
							dropdown.keymoved = true;
						return false;
						break;
					default:
						break;
				}
			});
		 // Close the dropdown when the user tabs away.
			input.bind('keydown keyup keypress', function( e ) {
				if( e.keyCode == 9 )
					dropdown.close();
			});
		 // set keymove flag to default value (false) after keymove action
			input.bind('blur', function( e ) {
				dropdown.keymoved = false;
			});
		 // When the user exits the field...
			jQuery(document).click( function(e) {
				var target				=	jQuery( e.target );
				if( ! target.parents( dropdown ).length || target.attr('name') != input.attr('name') ) {
					dropdown.close();
				}
			});
		 // When the window is resized...
			jQuery(window).resize( function() {
			 // Where is the input now?
				inputTop = input.offset().top - form.offset().top;
				inputLeft = input.offset().left - form.offset().left;
			 // move the dropdown
				dropdown.css({
					top: inputTop + input.outerHeight(),
					left: inputLeft,
					width: input.innerWidth()
				});
			});
		 
			dropdown.change(function(){
			 // If the dropdown has contents...
				if( dropdown.html().length ) {
				 // Where is the input now?
					inputTop = input.offset().top - form.offset().top;
					inputLeft = input.offset().left - form.offset().left;
					if( inputTop < 0 ) 
						inputTop = input.offset().top;
					if( inputLeft < 0 ) 
						inputLeft = input.offset().left;
				 // move the dropdown
					dropdown.css({
						top: inputTop + input.outerHeight(),
						left: inputLeft,
						width: input.innerWidth()
					});
				 // Add the keyhover class to the first shown list item
					jQuery('ul li:first',dropdown).addClass('keyhover');
				}
			 // When the option is clicked...
				jQuery('a.option', dropdown ).bind('click',function() {
				 // Variables
					var href 					=	jQuery(this).attr('href');
					var id						=	jQuery(this).parents('.dropdown').id().replace('-dropdown','');
					var input					=	jQuery('input#'+id);
				 // Get the query array
					if( href.match(/\?/) && href.match(/=/) ) {
						var query				=	gaFramework.deserialize( href.split('\?')[1] );
						var value				=	query.value;
					}
					else if( href.match('\?') ) {
						var value				=	href.substr( 1 );
					}
				 // Log the activity
					console.log( "Autosuggested option selected: ", value );
				 // Update our field
					input.val( value ).change();
				 // Close the dropdown
					dropdown.close();
				 // Stop the link from bubbling up
					return false;
				});
			});
		}
	 // Return the dropdown so we can reference it easier
		return dropdown;
	};

})(jQuery);