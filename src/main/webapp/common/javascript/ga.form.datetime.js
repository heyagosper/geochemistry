(function(jQuery) {

	/* Set up the dateTime namespace
	 *
	 */	
		
	jQuery.dateTime = {};
	
	/* Date Picker
	 *
	 */	
		
	jQuery.fn.dateField = function() {
	 // If there are inputs to be messed with
		jQuery.each( jQuery(this).not('.hasDatePicker'), function() {
	 // note that the jQuery datePicker itself adds the .hasDatePicker marker class  
	 // Variables
		var input								=	jQuery(this);
	 // If there are inputs to be messed with
		if( input.length ) {
		 // Variables
			var dateFormat = 'dd-mm-yy';
			var inputClass = input.attr('class');
			if ( typeof inputClass !== 'undefined' ){
			   dateFormat =	( matches = inputClass.match(/dateFormat:(\S+)/) ) ? matches[1] : 'dd-mm-yy';
			}
		 // Apply the jQuery UI datepicker
			if (typeof dojo === undefined) {
					input.datepicker( {
						dateFormat : dateFormat
					});
				} else {
					// dojo listeners don't see the jQuery triggered events
					// so we need to open up a dojoGate to the other universe
					// and feed 'em through
					input
						.datepicker( {
							dateFormat : dateFormat,
							changeMonth : true,
							changeYear : true,
							constrainInput : true,
							onSelect : function() {
								gaFramework.triggerDomEvent(this,
										"change");
							}
						});
				}
			}
		});
	 //
		return this;
	};

	/* Time Picker
	 *
	 */	
		
	jQuery.fn.timeField = function() {
	 // Variables
		var input								=	jQuery(this).not('.hasTimePicker');
	 // If there are inputs to be messed with
		if( input.length ) {
			var timeList		=	jQuery('<ul></ul>');
			for( var i=1; i<=24; i++ ) {
			 // Variables
				var h			=	new String( ( i != 24 ) ? i : 0 );
				var hh			=	new String( ( h.length == 1 ) ? '0'+h : h );
				var g			=	new String( ( i <= 12 ) ? i : ( i-12 ) );
				var gg			=	new String( ( g.length == 1 ) ? '0'+g : g );
				var a			=	new String( ( i+1 <= 12 || i == 24 ) ? 'am' : 'pm'); 
			 // Hour
				jQuery('<li></li>')
					.html( g + ':00 ' + a )
					.wrapInner('<a href="?value=' + hh + ':00" class="option"></a>')
					.appendTo( timeList );
			 // Half Hour
				jQuery('<li></li>')
					.html( g + ':30 ' + a )
					.wrapInner('<a href="?value=' + hh + ':30" class="option"></a>')
					.appendTo( timeList );
			}
		 // Variables
			var dropdown						=	input.dropdown();
		 // And now alter it for our mischevious purposes
			dropdown.addClass('timepicker').html( timeList ).change();
		 // When the user enters the field we want to show the dropdown.
			input.focus( function() {
				dropdown.show(100);
			});
		 // Apply a 'hasTimePicker' class to stop reapplication
			input.addClass('hasTimePicker');
		}
		return this;
	};

	/* Combined Date and Time Pickers
	 *
	 */	
		
	jQuery.fn.dateTimeField = function() {
		jQuery(this).not('.hasDateTimePicker').map( function() {
		 // Apply a 'hasDateTimePicker' class to stop reapplication
			jQuery(this).addClass('hasDateTimePicker');
		 //	Variables
			var inputRow						=	jQuery(this);
			var input							=	jQuery('input', this );
			var inputSpan						=	jQuery('.input', this );
		 // Hide the original input
			inputSpan.hide();
		 // Build Overlay
			var inputGroup						=	jQuery('<span></span>').addClass('group').appendTo(this);
			var dateSpan						=	jQuery('<span></span>').addClass('input').addClass('date').appendTo( inputGroup );
			var timeSpan						=	jQuery('<span></span>').addClass('input').addClass('time').appendTo( inputGroup );
			var dateInput						=	jQuery('<input />').attr({ type: 'text' }).appendTo( dateSpan ).dateField();
			var timeInput						=	jQuery('<input />').attr({ type: 'text' }).appendTo( timeSpan ).timeField();
		 // Set the value
			if( input.val() ) {
				dateInput.val( input.val().split(' ')[0] );
				timeInput.val( input.val().split(' ')[1] );
			}
		 // When a field is changed, we update all the others
			jQuery('span.date input, span.time input', inputRow ).change(function(){
				var dateVal			=	dateInput.val();
				if( ! dateVal ) {
					var d 			=	new Date();
					var date		=	new String( d.getDate() ).lpad('0',2);
					var month		=	new String( d.getMonth() ).lpad('0',2);
					var year		=	d.getFullYear();
					dateVal			=	date+'-'+month+'-'+year;
					dateInput.val( dateVal );
				}
				var timeVal			=	timeInput.val();
				if( ! timeVal ) {
					timeVal			=	'00:00';
					timeInput.val( timeVal );
				}
				input.val( dateVal + ' ' + timeVal ).change();
			});
		});
	};

	/* DateTime init
	 *
	 */	
		
	jQuery.dateTime.init = function() {
	 // Setup the date fields
		jQuery(".form_input.date input").dateField();
	 // Setup the time fields
		jQuery(".form_input.time input").timeField();
	 // Setup the combined date and time fields
		jQuery(".form_input.dateTime").dateTimeField();
	 // Do it all again on gaFramework.modified.
		gaFramework.modified(function(){
			jQuery(".form_input.date input").dateField();
			jQuery(".form_input.time input").timeField();
			jQuery(".form_input.dateTime").dateTimeField();
		});
	};

})(jQuery);
jQuery(document).ready(function(){
	jQuery.dateTime.init();
});	
