(function(jQuery) {
	
	/* 
	 *
	 */	
	
	jQuery.placeholder = {};
	
	/* 
	 *
	 */	
	
	jQuery.fn.placeholder = function() {
		jQuery.each( jQuery(this).not('.hasPlaceholder'), function() {
		 // Apply a 'hasPlaceholder' class to stop reapplication
			jQuery(this).addClass('hasPlaceholder');
		 // Variables
			var input			=	jQuery(this);
			var placeholder		=	jQuery(this).attr('placeholder');
			var overlay			=	jQuery('<div class="ui-placeholder-overlay" unselectable="true"></div>').html(placeholder).appendTo('body');
		 //	Style the overlay
			jQuery(overlay).css({
				'position': 'absolute',
				'top': input.offset().top,
				'left': input.offset().left,
				'width': 'auto',
				'height': input.height(),
				'line-height': input.css('line-height'),
				'padding-top': new Number( input.css('padding-top').replace('px','') ) + new Number( input.css('border-top-width').replace('px','') ),
				'padding-right': new Number( input.css('padding-right').replace('px','') ) + new Number( input.css('border-right-width').replace('px','') ),
				'padding-bottom': new Number( input.css('padding-bottom').replace('px','') ) + new Number( input.css('border-bottom-width').replace('px','') ),
				'padding-left': new Number( input.css('padding-left').replace('px','') ) + new Number( input.css('border-left-width').replace('px','') ),
				'opacity': 0.5
			});
		 // Apply the placeholder on page load
			if( jQuery(this).val() != '' ) {
				jQuery(overlay).css({ 'display': 'none' });
			}
		 // Move the overlay
			jQuery(window).bind('resize',function() {
				jQuery(overlay).css({
					'top': input.offset().top,
					'left': input.offset().left,
					'line-height': input.css('line-height'),
					'padding-top': new Number( input.css('padding-top').replace('px','') ) + new Number( input.css('border-top-width').replace('px','') ),
					'padding-right': new Number( input.css('padding-right').replace('px','') ) + new Number( input.css('border-right-width').replace('px','') ),
					'padding-bottom': new Number( input.css('padding-bottom').replace('px','') ) + new Number( input.css('border-bottom-width').replace('px','') ),
					'padding-left': new Number( input.css('padding-left').replace('px','') ) + new Number( input.css('border-left-width').replace('px','') )
				});
			});
		 // When the overlay is clicked, place focus on the input
			jQuery(overlay).bind('click',function() {
				jQuery(input).focus();
			});
		 // When the field has focus, remove the placeholder
			jQuery(this).bind('focus',function() {
				jQuery(overlay).css({ 'display': 'none' });
			});
		 // When the field is empty, add the placeholder
			jQuery(this).bind('blur',function() {
				if( jQuery(this).val() == '' ) {
					jQuery(overlay).css({
						'display': 'block'
					});
				}
			});
		});
	 // Return Object for chainability
		return this;
	};

	/* 
	 *
	 */	
	
	jQuery.placeholder.init = function() {
		if( ! jQuery.browser.webkit ) {
			jQuery('input[placeholder],textarea[placeholder]').placeholder();
			gaFramework.modified(function(){
				jQuery('input[placeholder],textarea[placeholder]').placeholder();
			});
		}
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.placeholder.init();
});	
