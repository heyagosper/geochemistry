(function(jQuery) {

	/* 
	 *
	 */	
	
	jQuery.search = {};

	/* 
	 *
	 */	
	
	jQuery.fn.search = function() {
		jQuery.each( jQuery(this).not('.hasSearch'), function() {
		 // Apply a 'hasPlaceholder' class to stop reapplication
			jQuery(this).addClass('hasSearch');
		 // Wrap the input so the button is placed correctly.
			jQuery(this).wrap('<span class="gaSearchInput"></span>');
		 // Variables
			var input				=	jQuery(this);
			var placeholder			=	jQuery(input).attr('placeholder');
			var span				=	jQuery(this).parents('span.gaSearchInput');
			var cross				=	jQuery('<a href="#" class="gaSearchClear"></a>').html('&times;').appendTo(span);
		 // We don't want the clear button for blank inputs onload
			if( jQuery(input).val() == '' || jQuery(input).val() == placeholder ) {
				jQuery(cross).hide();
			}
		 // The clear button should clear the input's value
			jQuery(cross).bind('click',function() {
				jQuery(input).val('').change().focus();
				return false;
			});
		 // When we enter something into the field, we show a clear button
			jQuery(input).bind('keypress change',function() {
			 // Hide if input is blank
				if( jQuery(input).val() == '' || jQuery(input).val() == placeholder ) {
					jQuery(cross).fadeOut();
				}
			 // Show if input has value
				else {
					jQuery(cross).fadeIn();
				}
			});
		});
	 // Return Object for chainability
		return this;
	};

	/* 
	 *
	 */	
	
	jQuery.search.init = function() {
		if( ! jQuery.browser.webkit ) {
			jQuery('input[type="search"]').search();
			gaFramework.modified(function(){
				jQuery('input[type="search"]').search();
			});
		}
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.search.init();
});	
