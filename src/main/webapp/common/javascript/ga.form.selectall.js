(function(jQuery) {
	
	/* Set up the menu namespace
	 *
	 */	
		
	jQuery.selectAll = {};
	
	/* 
	 * This jQuery extend function is created to auto check all check boxes if
	 * 'selectAll' check box were checked
	 * this function has dependancy on check(), uncheck() and toggleCheck() function in 
	 * ga.utilities.js
	 */
		
	jQuery.fn.selectAll = function() {
	 // Start the timer
		//console.time('jQuery.grid.selectAll');
	 // Iterate through the tables not prepared for selectable rows
		jQuery(this).not('.hasSelectAll').map( function( i ) {
		    // Add a class to 'selectAll' checkbox so we don't reprepare the checkbox again.
			var groupToggle = jQuery(this).addClass('hasSelectAll');
			// Get matadata from element (selectBooleanCheckbox) class - {key:value} pair
			var metadata = groupToggle.metadata();
			// get value with key = 'withClass'
			var groupClass = metadata.withClass;
		    // Get all checkboxes elements except the one 'selectAll' checkbox
			var groupBoxes = jQuery('input:checkbox.'+groupClass).not('.hasSelectAll');    
			// if all of check boxes are checked then check 'selectAll' checkbox
			// if none of check boxes are checked then uncheck 'selectAll' checkbox
			if( ! groupBoxes.not(':checked').length ) {
				groupToggle.attr({'checked':'checked'});
			}
			else {
				groupToggle.removeAttr('checked');
			}
		    // We can't proceed without some groupBoxes
			// process group check boxes based on 'selectAll' check box status
			if( groupClass || groupBoxes.length ) {
			 // check the rest of the checkboxes with 'selectAll' chcek box			
				groupToggle.toggleCheck( function() {
					groupBoxes.not(':hidden').check();
				}, function() {
					groupBoxes.not(':hidden').uncheck();
				});
			 // check  the 'selectAll' check box when we check the rest of the boxes
				groupBoxes.toggleCheck( function() {
					if( ! groupBoxes.not(':checked').length ) {
						groupToggle.attr({'checked':'checked'});
					}
				}, function() {
					if( groupBoxes.not(':checked').length ) {
						groupToggle.removeAttr('checked');
					}
				});
			}
		});
	 // Log the setup time
		//console.timeEnd('jQuery.grid.selectAll');
	};
	
	/* 
	 * This jQuery extend function is created to auto change all related fields if
	 * 'changeAll' field is changed
	 *  e.g. < input class="changeAll {withClass:'preferenceOrder',includeBlank:false} integer"/>
	 *  means... change all input fields with class="preferenceOrder" when this field changes; except when this field becomes blank
	 */
		
	jQuery.fn.changeAll = function() {
	 // Iterate through the input fields marked as .changeAll
		jQuery(this).not('.hasChangeAll').each( function() {
		    // Add a class to 'changeAll' field so we don't repeat this code
			var groupInput = jQuery(this).addClass('hasChangeAll');
			// apply change from groupToggle to all connected fields
			jQuery(this).change(function(){
				var groupInput = jQuery(this);
				var metadata = groupInput.metadata();
				// get value with key = 'withClass'
				var groupClass = metadata.withClass;
				var includeEmpty = metadata.includeEmpty;
				
				var val = groupInput.val();
				if (includeEmpty || (val != null && val != "")) {
				    // Get all elements except the one 'changeAll' field
					jQuery('input.'+groupClass+':visible:enabled').not('.hasChangeAll').val(val);    
				}
			});
		});
	};

	
	/* 
	 * init checkbox selector with 'selectAll' check box (class include 'selectAll')
	 * init change fields with 'changeAll' 
	 */	
		
	jQuery.selectAll.init = function() {
		jQuery('input.selectAll[type=checkbox]').selectAll();
		jQuery('input.changeAll').changeAll();		
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.selectAll.init();
});	
