(function(jQuery) {

	/* Set up the sliderInput namespace
	 *
	 */	
	
	jQuery.sliderInput = {};
			
	/* 
	 *
	 */	
	
	jQuery.fn.sliderInput = function( fn ) {
	 // If there are inputs to be messed with
		jQuery.each( jQuery(this).not('.hasSlider'), function() {
		 // Variables
			var inputField					=	jQuery(this).addClass('hasSlider');
			var inputSpan					=	inputField.parent().addClass('gaSliderInput');
			var inputRow					=	inputField.parents('.form_input');
			var id							=	inputField.attr('id');
			var name						=	inputField.attr('name');
		 // Setup markup
			inputSpan.wrap('<span class="group gaSlider">'+"\n\n"+'</span>');
			var inputGroup					=	jQuery('.group.gaSlider',inputRow);
			inputGroup.append("\n\n"+'<span class="input gaSliderBar"><span class="gaSliderBar-pad"><span></span></span></span>');				
			var inputSlider					=	jQuery('.gaSliderBar-pad span',inputGroup);
		 // Minimum Value
			var minClass					=	inputField.attr('class').match(/min:(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)/);
			var minVal						=	new Number( minClass[1] );
		 // Maximum Value
			var maxClass					=	inputField.attr('class').match(/max:(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)/);
			var maxVal						=	new Number( maxClass[1] );
		 // Width
			if( inputSpan.hasClass('veryMini') ) {
				inputGroup.addClass('veryMini');
			}
			else if( inputSpan.hasClass('mini') ) {
				inputGroup.addClass('mini');
			}
			else if( inputSpan.hasClass('half') ) {
				inputGroup.addClass('half');
			}
			else if( ! inputSpan.hasClass('full') ) {
				if( maxVal <= 10 )
					inputGroup.addClass('veryMini');
				else if( maxVal <= 100 )
					inputGroup.addClass('mini');
				else if( maxVal <= 500 )
					inputGroup.addClass('half');
			}
		 // Setup the slider
			inputSlider
				.slider({
					animate:	500,
					min:		minVal,
					max:		maxVal 
				})
				.bind('slidestart slide slidechange slidestop', function(){
					inputField.val( inputSlider.slider('value') ).trigger('validate');
				});
			inputField
				.bind('focus blur change', function(){
					var parent		=	jQuery(this).parent();
					inputSlider.slider('value',inputField.val());
				})
				.map( function( i ) {
					var parent		=	jQuery(this).parent();
					var value		=	Math.round( jQuery(this).val() );
					inputSlider.slider('value',value);
				});
				
		 // */
		});
	};

	/* Slider init
	 *
	 */	
	
	jQuery.sliderInput.init = function() {
		jQuery('input.slider').sliderInput();
		gaFramework.modified(function(){
			jQuery('input.slider').sliderInput();
		});
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.sliderInput.init();
});	
