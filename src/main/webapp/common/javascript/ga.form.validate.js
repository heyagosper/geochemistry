(function(jQuery) {
		  
	jQuery.validate = {};

	/* Custom event for when we want a field to be forced to validate.
	 *
	 */	
		
	jQuery.fn.validate = function( fn ) {
		if( typeof fn == 'function' )
			jQuery(this).bind('validate', fn);
		else if( ! fn )
			jQuery(this).trigger('validate', fn);
		return this;
	};
	
	/* Custom event for when we want a field to be forced to validate.
	 * note triggerHandler returns undefined if there are no handlers
	 */	
		
	jQuery.fn.validSubmit = function( fn ) {
		if( typeof fn == 'function' )
			jQuery(this).bind('validSubmit',fn);
		else if( ! fn )
			return jQuery(this).triggerHandler('validSubmit'); 
		return this;
	};
	
	/* Custom event for when we want a field to be forced to validate.
	 * note triggerHandler returns undefined if there are no handlers
	 */	
		
	jQuery.fn.beforeValidSubmit = function( fn ) {
		if( typeof fn == 'function' )
			jQuery(this).bind('beforeValidSubmit', fn);
		else if( ! fn )
			return jQuery(this).triggerHandler('beforeValidSubmit');
		return this;
	};
		
	/* Bind Validator to Form
	 *
	 */	
		
	jQuery.fn.prepareForm = function() {
  	  console.time("ga.form.validate:prepareForm");
	 // Iterate through the .form_input rows, and we mark required ones
  	    console.time("ga.form.validate:prepareForm:required");
		jQuery( '.form_input' ).map(function( key, object ) {
			if( jQuery(this).hasClass('required') && ! jQuery('span.star',this) ) {
				jQuery('label',this).append('<span class="star" title="Fields within this row may be required.">*</span>');
			}
		});
	  	console.timeEnd("ga.form.validate:prepareForm:required");
	 // Apply validation to input fields
  	    console.time("ga.form.validate:prepareForm:prepareInput");
		jQuery('input[type=text],input[type=password],textarea,select',this).prepareInput();
  	    console.timeEnd("ga.form.validate:prepareForm:prepareInput");
  	    console.time("ga.form.validate:prepareForm:delegations");
		jQuery(this).delegate('input[type=text],input[type=password],textarea,select','mouseenter',function() {
			jQuery(this).parent().addClass('hover');
		});
		jQuery(this).delegate('input[type=text],input[type=password],textarea,select','mouseleave',function() {
			jQuery(this).parent().removeClass('hover');
		});
		jQuery(this).delegate('input[type=text],input[type=password],textarea,select','focus',function() {
			jQuery(this).addClass('focus');
			jQuery(this).parent().addClass('focus');
		});
		jQuery(this).delegate('input[type=text],input[type=password],textarea,select','change blur',function() {
			jQuery(this).removeClass('focus');
			jQuery(this).parent().removeClass('focus');
		});
		jQuery(this).delegate('input[type=text],input[type=password],textarea,select','textchange change validate',function() {
			jQuery(this).validateInput();
		});
  	    console.timeEnd("ga.form.validate:prepareForm:delegations");
		

	 // Iterate through the grid inputs and apply validation
		jQuery('table.grid').prepareGrid();
	 // TESTING GROUPS OF CHECKBOXES AND RADIO BUTTONS
	 // IS NOT YET IMPLEMENTED
	 /* / Loop through the checkboxes and radio boxes and collect an temporary array of names
		jQuery.each( jQuery( 'input[type=checkbox],input[type=radio]', this ), function( key, object ) {
			if( ! jQuery(this).parents('table.grid') ) {
				var name				=	jQuery(this).attr('name');
				if( ! tempNames.toString().match( name ) ) {
					tempNames.push( jQuery(this).attr('name') );
				}
			}
		});
	 // Then we loop through the name array and collect
		jQuery.each( tempNames, function( key, value ) {
			var input					=	jQuery('[name=' + value + ']');
		});
	 // */
	 // On clicking a button
  	    console.time("ga.form.validate:prepareForm:buttons");
		jQuery('button,input[type=button],input[type=submit],input[type=reset],a.button',this).not('.hasClick').map( function() {
		 // Stop this from being bound again
			jQuery(this).addClass('hasClick');
		 // On click...
			jQuery(this).click(function( e ) {
			 // Confirm the action
				if( jQuery(this).hasClass('negative') ) {
					jQuery(this).parents('form').data('validateNegative',true);
				}
			 // A class of 'immediate' will bypass the validation
				if( jQuery(this).hasClass('immediate') ) {
					jQuery(this).parents('form').data('validateImmediate',true);
				}				
			});
		});
  	    console.timeEnd("ga.form.validate:prepareForm:buttons");
	 // On submission of the form
  	    console.time("ga.form.validate:prepareForm:submit");
		jQuery(this).not('.hasValidation').not('.noValidation').each( function() {
		 // Stop this from being bound again
			jQuery(this).addClass('hasValidation');
		 // On submit, validate the form.
			jQuery(this).bind('submit',function( evt ) {
				if (jQuery(this).validateForm() == false){
			        // stop form from submitting normally
			        if (evt.preventDefault) {
			            evt.preventDefault();
			        } else {
			            evt.returnValue = false;
			        }
			        return false;				    
				};
			});
		 // When a validate is triggered...
			jQuery(this).bind('validate',function( e ) {
			 // Validate any input fields in the form.
				jQuery('input[type=text],input[type=password],textarea,select',this).validateInput();
			 // Validate any table.grid in the form.
				jQuery('table.grid',this).not('.noValidation').validateGrid();
			});
		});
  	    console.timeEnd("ga.form.validate:prepareForm:submit");
  	  console.timeEnd("ga.form.validate:prepareForm");
	 // Return this for chaining
		return this;
	};
		
	/* Validate Value
	 * Custom event for when we want a field to be forced to validate.
	 */	

	jQuery.fn.validateForm = function() {
  	  console.time("ga.form.validate:validateForm");
  	  try {
	 // Trigger the beforeValidSubmit event, which allows us to run code before validation.		
		if( jQuery(this).beforeValidSubmit() == false ) {
			return false;
		}
	 // If negative is true, we confirm the action
		if( jQuery(this).data('validateNegative') ) {
			jQuery(this).data('validateNegative',false);
			if( ! confirm( 'Are you sure you want to do that?\n\nPress OK to continue, or Cancel to stop.' ) ) {
				return false;
			}
		}
	 // If immediate is true, we submit the form.
		if( jQuery(this).data('validateImmediate') ) {
			jQuery(this).data('validateImmediate',false);
			return jQuery(this).validSubmit();
		}
	 // Validate any input fields in the form.
		jQuery('input[type=text],input[type=password],textarea,select',this).validateInput();
	 // Validate any table.grid in the form.
		jQuery('table.grid',this).not('.noValidation').validateGrid();
	 // We get the first available error bubble
		var errorBubble				=	jQuery('li.ga-error-bubble',this).first();
	 // If an error exists (in this form)...
		if( errorBubble.length ) {
			var errorMsg			=	jQuery('.ga-error-bubble-message',errorBubble).text();
			var inputId				=	errorBubble.id().replace('-error','');
			//console.log( jQuery('#'+inputId ) );
			var inputLabel			=	jQuery('#'+inputId ).label().text;
		 // Interrupt the user...
			alert( "'" + inputLabel + "' " + errorMsg );
		 // Put focus on the field with an error.
			if( jQuery('#'+inputId ).is('input,textarea,select') ) {
				jQuery('#'+inputId ).focus();
			}
		 // Return false to stop the submit.
			return false;
		}
	 // If no errors exist (in this form)...
		else {
		 // Return true to allow the submit to go through.
			return jQuery(this).validSubmit();
		}
  	  } finally {
  	  	 console.timeEnd("ga.form.validate:validateForm");
  	  }
	};
		
	/* Bind Validator to Input
	 *
	 */	
		
	jQuery.fn.prepareInput = function() {
	 // Iterate through the unprepared inputs
		console.log('preparing input on '+ jQuery(this).not('.hasValidation').length + ' fields');
		jQuery(this).not('.hasValidation').each( function() {
		 // Variables
			var inputField = jQuery(this);
			inputField.addClass('hasValidation');
		 // Mark the field's label for required field.
			if (inputField.isRequired()) {
				var inputLabel = inputField.label();
				if (! jQuery('span.star',inputLabel).length ) {
				   if( inputLabel.is('td,th') ) {
					  inputLabel.append('<span class="star" title="The fields in this column are required.">*</span>');
				   } else {
					  inputLabel.append('<span class="star" title="This is a required field.">*</span>');
				   }
			    }
			}
		});
	 // Return this for chaining
		return this;
	};
		
	/* Validate Value
	 *
	 */	
	
	jQuery.fn.validate.remoteValidate = [];

	jQuery.fn.validateInput = function() {
		jQuery(this).map( function() {
		 // Variables
			var inputField = jQuery(this);
			var inputSpan = inputField.parents('span.input');
			var inputRow = inputField.parents('.form_input');
			var id = inputField.attr('id');
			var name = inputField.attr('name');
		 // Variables
			var errors = 0;
			var inputValue = jQuery( inputField ).val();
		 // Validations on values can only be run when there is a value
			if( inputValue) {
			 // Minimum/Maximum Values
				if( inputField.min() || inputField.max() ) {
				 // Variables
					var minVal = inputField.min();
					var maxVal = inputField.max();
				 // If the value is less than the minimum...
					if( minVal && inputValue < minVal ) {
						inputField.addError('must be greater than or equal to ' + minVal +'.');
						errors++;
					}
				 // If the value is greater than the maximum...
					else if( maxVal && inputValue > maxVal ) {
						inputField.addError('must be less than or equal to ' + maxVal +'.');
						errors++;
					}
				 // Now we automatically validate for numeric values with a min/max validation
				 // We do it at this point so it can be overridden with specific validation classes
				 //	Numeric
					if( jQuery.isNumeric( minVal ) || jQuery.isNumeric( maxVal ) ) {
						if( ! jQuery.isNumeric( inputValue ) ) {
							inputField.addError('must be numeric (e.g. 37.5).');
							errors++;
						}
					}
				 //	Integer
					else if( jQuery.isInteger( minVal ) || jQuery.isInteger( maxVal ) ) {
						if( ! jQuery.isInteger( inputValue ) ) {
							inputField.addError('must be an integer (e.g. 264).');
							errors++;
						}
					}
				}
			 //	Ajax Validation
				if( inputField.hasClass('autoValidate') && inputField.attr('src') ) {
				 // If an ajax call exists for this field, we abort.
					try {
						if( jQuery.fn.validate.remoteValidate[name] && jQuery.fn.validate.remoteValidate[name].status == 0 ) {
							jQuery.fn.validate.remoteValidate[name].abort();
						}
					} catch(e){}		
				 // Kick off the new ajax call.
					jQuery.fn.validate.remoteValidate[name] = jQuery.ajax({
							type:		'POST',
							url:		inputField.attr('src'),
							data:		'q=' + inputField.val(),
							dataType:	'text',
							success: 	function( txt ) {
							 // A blank or 'true' string means no errors.
								if( ! txt )
									txt = 'true';
							 // Or place them if we are returned one.
								else if( txt ) {
									inputField.addError(txt);
									errors++;
								}
							}
						});
				}
			 //	Numeric
				else if( inputField.hasClass('numeric') || inputSpan.hasClass('numeric') ) {
					if( ! jQuery.isNumeric( inputValue ) ) {
						inputField.addError('must be numeric (e.g. 37.5).');
						errors++;
					}
				}
			 //	Integer
				else if( inputField.hasClass('integer') || inputSpan.hasClass('integer') ) {
					if( ! jQuery.isInteger( inputValue ) ) {
						inputField.addError('must be an integer (e.g. 264).');
						errors++;
					}
				}
			 //	Alpha Characters Only (A-Z)
				else if( inputField.hasClass('alphabetical') || inputSpan.hasClass('alphabetical') ) {
					if( ! jQuery.isAlphabetical( inputValue ) ) {
						inputField.addError('must be alphabetical (A-Z) characters only.');
						errors++;
					}
				}
			 //	Alphanumeric Characters Only (A-Z)
				else if( inputField.hasClass('alphanumeric') || inputSpan.hasClass('alphanumeric') ) {
					if( ! jQuery.isAlphanumeric( inputValue ) ) {
						inputField.addError('must be alphanumeric (A-Z or 0-9) characters only.');
						errors++;
					}
				}
			 //	Date Format
				else if( inputField.hasClass('date') || inputSpan.hasClass('date') || inputRow.hasClass('date') ) {
				 // We try to parse the date with the datepicker's parsing function.
					try {
						jQuery.datepicker.parseDate( inputField.dateFormat(), inputValue );
				 // If it fails, we add the arror.
					} catch( e ) {
						inputField.addError('must be in the correct format, e.g. ' + jQuery.datepicker.formatDate( inputField.dateFormat(), new Date() ) );
						errors++;
					}
				}
			 //	Time Format
				else if( inputField.hasClass('time') || inputSpan.hasClass('time') || inputRow.hasClass('time') ) {
				 // Variables
					var segments				=	jQuery( inputField ).val().match(/^([0-9]{2,2}):([0-9]{2,2})$/);
				 // 
					if( ! segments || ( segments[1] < 0 || segments[1] > 23 ) || ( segments[2] < 0 || segments[2] > 59 ) ) {
						inputField.addError('must be in 24hr HH:MM format (e.g. 17:36).');
						errors++;
					}
				}
			}
		 //	Required Fields
			else if( inputField.isRequired()) {
				inputField.addError('is required<span style="display: none;"> and must have a value</span>.');
				errors++;
			}
		 // If there are no errors, remove the error tip.
			if( errors <= 0 ) {
				inputField.removeError();
			}
		});
	};
	
	/* Bind Validator to Grid
	 *
	 */	
	
	jQuery.fn.prepareGrid = function() {
	  	console.time("ga.form.validate:prepareGrid");
	 // Iterate through the unprepared inputs
		jQuery(this).not('.hasValidation').map( function() {
		 // Variables
			var inputGrid = jQuery(this).addClass('hasValidation');
		 // Bind the actual validation to when the grid is changed.
			inputGrid.bind('change validate', function() {
				jQuery(this).not('.noValidation').validateGrid();
			});
		 // Find the required cells...
			var requireCells = inputGrid.find('.requireCell');
		 // If there are no requireCells already...											  
			if( ! requireCells.length ) {
			 // Get the tbody
				var tbody = inputGrid.find('tbody,tfoot').not('table table *');
			 // Iterate through all the column headers marked as required...
				inputGrid.find('thead .required').map( function() {
				 // Get the column number from the th class
					var columnId = jQuery(this).attr('class').match(/c[0-9]+/)[0];
				 // Add the selectorCell class to the column cells
					tbody.find('.' + columnId ).addClass('requireCell');
				 // Mark the column as required.
				 	if( ! jQuery(this).find('span.star').length && inputGrid.hasClass('selectable') ) {
						jQuery(this).append('<span class="star" title="The fields in this column may be required.">*</span>');
					}
				 	else if( ! jQuery(this).find('span.star').length ) {
						jQuery(this).append('<span class="star" title="The fields in this column are required.">*</span>');
					}
				});
			}
		 // Apply validation to input fields
		//	jQuery('input[type=text],input[type=password],textarea,select',this).prepareInput();
		});
	  	console.timeEnd("ga.form.validate:prepareGrid");
	};
	
	/* Validate Grid
	 *
	 */	
		
	jQuery.fn.validateGrid = function() {
	 // Iterate through the unprepared inputs
		jQuery(this).each( function() {
		 // Validate any input fields in the grid.
			jQuery('input[type=text],input[type=password],textarea,select',this).validateInput();
		 // Variables
			var inputGrid					=	jQuery(this);
			var inputRow					=	inputGrid.parents('.form_input');
			var errors						=	0;
		 // Required Selection
			if( inputGrid.hasClass('selectable') && inputRow.hasClass('required') && ! jQuery('.selectorCell input:checked',inputGrid).length ) {
				jQuery(inputGrid).addError('must have at least one row selected.','This grid');
				errors++;
			}
		 //	Required Fields
		 // If there are no errors, remove the error tip.
			if( errors <= 0 ) {
				jQuery(inputGrid).removeError();
			}
		});
	};
	
	/* Add an error bubble to an input.
	 *
	 */	
	
	jQuery.fn.addError = function( errorMsg, thisField ) {
	 // Variables
		var input =	jQuery(this);
		var form =	input.parents('form');
		var id = jQuery(this).id();
		if( ! id ) {
			id = 'input-' + gaFramework.identifier( this );
			input.id( id );
		}
	 // Somewhere for us to place our errors.
		var container = jQuery('ul.ga-error-container',form);
		if( ! container.length ) {
			container = jQuery('<ul class="ga-error-container"></ul>').appendTo(form);
		}
	 // Add an error class to the input
		input.addClass('error');
	 // Add one to the parent cell if it exists.
		if( input.parents('td,th').length ) {
			input.parents('td,th').addClass('error');
		}
	 // If an error bubble does not exist...
		if( ! jQuery('li.ga-error-bubble#'+id+'-error',container).length ) {
		 // Build the markup
			var bubble = jQuery('<li></li>').addClass('ga-error-bubble');
			bubble.id( id + '-error' );
			bubble.appendTo(container);
			var bubblePad = jQuery('<span><span class="ga-error-bubble-field"></span><span class="ga-error-bubble-label"></span> <span class="ga-error-bubble-message"></span></span>').addClass('ga-error-bubble-pad').appendTo(bubble);
		 // When the window is resized...
			input.bind('focus',function() {
				input.addClass('focus');
				if( ! bubble.is(':visible') ) {
					bubble.show();
				}
			});
			input.bind('change',function() {
				input.removeClass('focus');
				if( ! input.hasClass('mouseover') && bubble.is(':visible') ) {
					bubble.hide();
				}
			});
			input.bind('blur',function() {
				input.removeClass('focus');
				if( ! input.hasClass('mouseover') && bubble.is(':visible') ) {
					bubble.hide();
				}
			});
		 // When the window is resized...
			if( ! input.parents('table.grid .selectorCell').length ) {
				input.bind('mouseover',function() {
					input.addClass('mouseover');
					if( ! bubble.is(':visible') ) {
						bubble.show();
					}
				});
				input.bind('mouseout',function() {
					input.removeClass('mouseover');
					if( ! input.hasClass('focus') && bubble.is(':visible') ) {
						bubble.hide();
					}
				});
			} else {
				input.parents('table.grid').hover(function() {
					input.addClass('mouseover');
					if( ! bubble.is(':visible') ) {
						bubble.show();
					}
				}, function() {
					input.removeClass('mouseover');
					if( ! input.hasClass('focus') && bubble.is(':visible') ) {
						bubble.hide();
					}
				});
			}
		 // Where is the input, relative to the form?
			var inputTop = input.offset().top - form.offset().top;
		 // Checking the browser and reducing header height from form
			if( jQuery.browser.msie && new Number( jQuery.browser.version ) < 8 && form.parent('body').length ) {
				inputTop = inputTop - gaFramework.barHeightTop; 
			}
			var inputLeft = input.offset().left - form.offset().left;
		 // Align the error bubble
			bubble.css({
				top: inputTop + input.outerHeight() + 'px',
				left: inputLeft + 'px'
			});
		 // When the window is resized...
			jQuery(window).bind('resize realignErrorBubbles', function() {
				if( ! input.is(':hidden') ) {
				 // Where is the input now?
					inputTop = input.offset().top - form.offset().top;
					inputLeft = input.offset().left - form.offset().left;
				 // Checking the browser and reducing header height from form
					if( jQuery.browser.msie && new Number( jQuery.browser.version ) < 8 && form.parent('body').length ) {
						inputTop = inputTop - gaFramework.barHeightTop; 
					}
				 // We shift the error bubble so it aligns correctly with it's input.
					bubble.css({
						top: inputTop + input.outerHeight() + 'px',
						left: inputLeft + 'px'
					});
				}
			});
		 // When the document is modified...
			jQuery(document).bind('modified', function() {
				if( ! input.is(':hidden') ) {
				 // We shift the error bubble so it aligns correctly with it's input.
					bubble.css({
						top: inputTop + input.outerHeight() + 'px',
						left: inputLeft + 'px'
					});
				}
			});
		 // Show the bubble
			//bubble.show();
		}
	 // Update the message
		jQuery('li.ga-error-bubble#'+ id +'-error span.ga-error-bubble-field',container).html( ( thisField ) ? thisField : 'This field' );
		jQuery('li.ga-error-bubble#'+ id +'-error span.ga-error-bubble-message',container).html( errorMsg );
	};
	
	/* Remove the error bubble from an input.
	 *
	 */	
	
	jQuery.fn.removeError = function() {
	 // Get the Id
		var id = jQuery(this).id();
	 // If the input has an id, we attempt to remove the error
		if( id ) {
		 // Variables 
			var input =	jQuery(this);
			var form = input.parents('form');
			var container = jQuery('ul.ga-error-container',form);
			var bubble = jQuery('li.ga-error-bubble#'+ id +'-error',container);
		 // Remove the error class from the input.
			input.removeClass('error');
		 // Remove it from the parent cell if it exists.
			if( input.parents('td,th').length ) {
				input.parents('td,th').removeClass('error');
			}
		 // If an error bubble exists...
			if( bubble.length ) {
				bubble.remove();
			}
		}
	};
	
	/* Find out if a field is required or not.
	 *
	 */	
	
	jQuery.fn.isRequired = function() {
	 // Variables
		var inputField = jQuery(this);
	 // As long as the input is not a button of any kind...
		if( inputField.attr('type') != 'button' && inputField.attr('type') != 'submit' && inputField.attr('type') != 'reset' ) {
		 // If the field has the required attribute...
			if( typeof inputField.attr('required') == 'string' ){
				return true;
		    }
		 // If the field has a class of required...
			if( inputField.hasClass('required') ){
				return true;
	        }
		 // If the parent span.input has a class of required...
           	var inputSpan = inputField.closest('span.input');
			if( inputSpan.hasClass('required') ){
				return true;
            }
           var inputGrid = inputField.closest('table.grid');
           var inputRow = inputField.closest('.form_input');

		 // If the .form_input row is required and we're not in a table.grid.
			if( inputRow.hasClass('required') && ! inputGrid.length ){
				return true;
			}
		 // If we're in a selectable table.grid and the row is selected...
			if( inputGrid.length && inputGrid.hasClass('selectable') && inputRow.hasClass('selected') ) {
			 // If the column is marked as required...
				if( inputField.closest('.requireCell').length ){
					return true;
				}
			}
		 // If we're in a non-selectable table.grid...
			if( inputGrid.length && ! inputGrid.hasClass('selectable') ) {
			 // If the .form_input row is required and there are no columns marked as required...
				if( inputRow.hasClass('required') && ! inputGrid.find('.required,.requireCell').length ){
					return true;
				}
			 // If the column is marked as required...
				if( inputField.closest('.requireCell').length ){
					return true;
				}
			}
		}
	 // Otherwise, the field is not required.
		return false;
	};
	
	/* Test to see if a value is numeric (optional decimal point).
	 *
	 */	
	
	jQuery.isNumeric = function( value ) {
		if( value.toString().match(/^[\s]*(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)[\s]*$/i) ) {
			return true;
		}
		return false;
	};
	
	/* Test to see if a value is a valid integer (NO decimal point).
	 *
	 */	
	
	jQuery.isInteger = function( value ) {
		if(	value.toString().match(/^[\s]*(-?[0-9]+)?(e[-+]?[0-9]+)?[\s]*$/i) ) {
			return true;
		}
		return false;
	};
	
	/* Test to see if a value is alphabetical (A-Z only, case insensitive).
	 *
	 */	
	
	jQuery.isAlphabetical = function( value ) {
		if( value.toString().match(/^[a-zA-Z]+$/i) ) {
			return true;
		}
		return false;
	};
	
	/* Test to see if a value is alphanumeric (A-Z and 0-9 only, case insensitive).
	 *
	 */	
	
	jQuery.isAlphanumeric = function( value ) {
		if( value.toString().match(/^[a-zA-Z0-9]+$/) ) {
			return true;
		}
		return false;
	};
	
	/* Minimum Possible Value
	 * 
	 */
	  
	jQuery.fn.min = function( value ) {
	 // If a value is passed, we set the min value
		if( value ) {
		 // Check that the value is a valid number
			if( value.toString().match(/^(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)$/i) ) {
			 // Remove the current value, if it exists.
				if( currentValue = jQuery(this).min() ) {
					jQuery(this).removeClass( 'min:' + currentValue );
				}
			 // Add the new class.
				jQuery(this).addClass( 'min:' + value );
			}
			else {
				console.error("The minimum value you're trying to set is an invalid number.");
			}
		 // We return the object for chaining.
			return this;
		}
	 // Otherwise we get it from the input
		else if( minClass = jQuery(this).attr('class').match(/min:(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)/i) ) {
			return new Number( minClass[1] );
		}
	 // Otherwise we return false (which means there is no min value)
		else {
			return false;
		}
	};
		
	/* Maximum Possible Value
	 * 
	 */
	
	jQuery.fn.max = function( value ) {
	 // If a value is passed, we set the max value
		if( value ) {
		 // Check that the value is a valid number
			if( value.toString().match(/^(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)$/i) ) {
			 // Remove the current value, if it exists.
				if( currentValue = jQuery(this).max() ) {
					jQuery(this).removeClass( 'max:' + currentValue );
				}
			 // Add the new class.
				jQuery(this).addClass( 'max:' + value );
			}
			else {
				console.error("The maximum value you're trying to set is an invalid number.");
			}
		 // We return the object for chaining.
			return this;
		}
	 // Otherwise we get it from the input
		else if( maxClass = jQuery(this).attr('class').match(/max:(([-+]?[\.0-9]+)?(e[-+]?[0-9]+)?)/) ) {
			return new Number( maxClass[1] );
		}
	 // Otherwise we return false (which means there is no max value)
		else {
			return false;
		}
	};
		
	/* Date Format
	 * 
	 */
	
	jQuery.fn.dateFormat = function( value ) {
	 // If a value is passed, we set the max value
		if( value ) {
		 // Remove the current value, if it exists.
			if( currentValue = jQuery(this).attr('class').match(/dateFormat:(\S+)/) ) {
				jQuery(this).removeClass( 'dateFormat:' + currentValue[1] );
			}
		 // Add the new class.
			jQuery(this).addClass( 'dateFormat:' + value );
		 // We return the object for chaining.
			return this;
		}
	 // Otherwise we get it from the input
		else {
			var formatClass = jQuery(this).attr('class').match(/dateFormat:(\S+)/);
			return ( formatClass ) ? formatClass[1] : 'dd-mm-yy';
		}
	};
	
	/* Initialise validation
	 *
	 */	
		
	jQuery.validate.init = function() {
		jQuery('form').prepareForm();
		gaFramework.modified(function(){
			jQuery('form').prepareForm();
		});
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.validate.init();
});	
