(function(jQuery) {
	
	/* 
	 *
	 */	
		
	jQuery.thumbnails = {};
		
	/* 
	 *
	 */	
		
	jQuery.fn.thumbnails = function() {
	 // Start the timer
		//console.time('jQuery.fn.thumbnails');
	 // Iterate through the containers
		jQuery(this).map( function() {
			if( jQuery(this).is(":visible") ) {	
		 // Get a set of the thumbnails
			var thumbnails = jQuery('.ga-thumbnail-row',this);
			var maxWidth = 0, maxHeight = 0;
		 // Iterate through unprepared images
			thumbnails.map( function() {
				maxWidth = ( jQuery(this).width() > maxWidth ) ? jQuery(this).width() : maxWidth;
				maxHeight = ( jQuery(this).height() > maxHeight ) ? jQuery(this).height() : maxHeight;
			});
		 // Set the height and width of all the thumbnails
			thumbnails.css({
				height: maxHeight,
				width: maxWidth
			});
		 // Iterate through unprepared images
			thumbnails.map( function() {
				var image = jQuery('img.ga-thumbnail-image',this);
				var margin = ( jQuery(this).innerWidth() - image.outerWidth() ) / 2;
				image.css({
					'margin-left': margin,
					'margin-right': margin
				});
			});
			}
		});
	 // Start the timer
		//console.timeEnd('jQuery.fn.thumbnails');
	 // Return for chaining
		return this;
	};
	
	/* 
	 *
	 */	
		
	jQuery.thumbnails.init = function() {
		jQuery('.ga-thumbnail-container').thumbnails();
		gaFramework.modified(function(){
			jQuery('.ga-thumbnail-container').thumbnails();
		});
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.thumbnails.init();
});	
