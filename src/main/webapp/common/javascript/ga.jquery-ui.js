/* gaFramework jqueryui integration
/* -------------------------------------------------------------------------------------------------------------------------- */

gaFramework.jqueryui = {
	init : function() {
		gaFramework.jqueryui.autocomplete.init();
	}
};

gaFramework.jqueryui.autocomplete = {
	// when autocomplete list gets modified
	onChange : function(evt) {
		var input = jQuery(this);
		var parent = input.closest('span.rf-au');
		var parentId = parent.id();
		var button = parent.find('.rf-au-btn-arrow');
		if (button.length) { // if we have a button then hide/show depending
			// on list size
			var items = jQuery('#' + parentId + 'List').find(
					'ul > li:not(:empty)');
			if (items.length) {
				button.show();
			} else {
				button.hide();
			}
			// repeat in a while in case the ajax call is slow
			window.setTimeout(function() {
				var items = jQuery('#' + parentId + 'List').find(
						'ul > li:not(:empty)');
				console.log(items.length);
				if (items.length) {
					button.show();
				} else {
					button.hide();
				}
			}, 500);
		}

	},
	toggle : function(evt) {
		var input = jQuery(this);
		var button = input.closest('span.rf-au-fld-btn').find('.rf-au-btn');
		if (button.length) { // if we have a button then hide/show depending
			// on list size
			var items = input.autocomplete('widget').find('li:not(:empty)');
			if (items.length) {
				button.show();
			} else {
				button.hide();
			}
			// repeat in a while in case the ajax call is slow
			window.setTimeout(
					function() {
						var items = input.autocomplete('widget').find(
								'li:not(:empty)');
						if (items.length) {
							button.show();
						} else {
							button.hide();
						}
					}, 500);
		}
		return true;
	},
	click : function(evt) {
		var input = jQuery(this).parent().find('input.ui-autocomplete-input');
		var widget = input.autocomplete('widget');
		if (widget.is(":visible")) {
			widget.hide();
		} else {
			widget.show();
			window.setTimeout(function() { // delayed show to avoid open/close flicker
				widget.show();
			}, 50);
		}
		evt.stopPropagation();
		return false;
	},
	init : function() {
		var auInputs = jQuery('input.ui-autocomplete-input:not(.uiAutoMarker)');
		if (auInputs.length) {
			auInputs
					.each(function() {
						var input = jQuery(this);
						input.addClass('uiAutomarker').wrap(
								"<span class='rf-au-fld-btn'/>");
						var button = jQuery(
								"<span class='rf-au-btn'><span class='rf-au-btn-arrow'/></span>")
								.insertAfter(input);
						button.bind('click',
								gaFramework.jqueryui.autocomplete.click);
						// set initial visibility of the button depending on
						// list size
						var items = input.autocomplete('widget').find(
								'li:not(:empty)');
						if (items.length) {
							button.show();
						} else {
							button.hide();
						}
						input.bind("autocompletesearch",
								gaFramework.jqueryui.autocomplete.toggle);
					});
		}
	}
};
jQuery(document).ready(function() {
	window.setTimeout(function() {
		gaFramework.jqueryui.init();
	}, 100);
	// gaFramework.jqueryui.init();
});
