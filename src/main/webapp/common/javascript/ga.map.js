/*
 *  Common code for OpenLayers usage
 *
 */
if (!gaMap) {
    var gaMap = {};
    if (!gaFramework) {
        var gaFramework = {};
    }
    if (typeof openLayersDir == 'undefined') {
        openLayersDir = 'http://www.openlayers.org/api';
    }
    if (typeof OpenLayers != 'undefined') {
        OpenLayers.Control.BoundingBox = OpenLayers.Class(OpenLayers.Control.DrawFeature, {
            lastFeature : null,
            initialize : function(layer, handler, options) {
                OpenLayers.Control.DrawFeature.prototype.initialize.apply(this, [ layer, handler, options ]);

            },
            setMap : function(map) {
                this.handler.setMap(map);
                OpenLayers.Control.prototype.setMap.apply(this, arguments);
            },
            featureAdded : function(feature) {
                // one poly at a time... so remove the last one
                if (this.lastFeature != null) {
                    this.deleteFeature(this.lastFeature);
                }
                this.lastFeature = feature;
                this.notifyBoundaries(feature.geometry);
            },
            notifyBoundaries : function(geometry) {
                // override this with something useful
                alert(geometry.toString());
            },
            // bounds is of type OpenLayers.Bounds
            setBoundaries : function(bounds) {
                var rect = new OpenLayers.Feature.Vector(bounds.toGeometry());
                this.layer.addFeatures(rect);
                this.featureAdded(rect);
            },
            clearBoundaries : function() {
                if (this.lastFeature != null) {
                    this.deleteFeature(this.lastFeature);
                }
                this.lastFeature = null;
            },
            deleteFeature : function(feature) {
                feature.state = OpenLayers.State.DELETE;
                this.layer.events.triggerEvent("afterfeaturemodified", {
                    feature : feature
                });
                this.layer.drawFeature(feature);
            },
            CLASS_NAME : "OpenLayers.Control.BoundingBox"
        });

        OpenLayers.Control.PointMarker = OpenLayers.Class(OpenLayers.Control.DrawFeature, {
            lastFeature : null,
            initialize : function(layer, handler, options) {
                OpenLayers.Control.DrawFeature.prototype.initialize.apply(this, [ layer, handler, options ]);

            },
            setMap : function(map) {
                this.handler.setMap(map);
                OpenLayers.Control.prototype.setMap.apply(this, arguments);
            },
            featureAdded : function(feature) {
                // one point at a time... so remove the last one
                if (this.lastFeature != null) {
                    this.deleteFeature(this.lastFeature);
                }
                this.lastFeature = feature;
                this.notifyPoint(feature.geometry);
            },
            notifyPoint : function(coords) {
                // override this with something useful
                alert(coords.toString());
            },
            // bounds is of type OpenLayers.LATLON?
            setPoint : function(coords) {
                var point = new OpenLayers.Geometry.Point(coords.lon, coords.lat);
                this.drawFeature(point);
            },
            clearPoint : function() {
                if (this.lastFeature != null) {
                    this.deleteFeature(this.lastFeature);
                }
                this.lastFeature = null;
            },
            deleteFeature : function(feature) {
                this.layer.removeFeatures([ feature ], {
                    silent : true
                });
            },
            CLASS_NAME : "OpenLayers.Control.PointMarker"
        });
        if (typeof gaMap.markers == 'undefined') {
            gaMap.markers = {};
        }
        gaMap.jsDirectory = {};
        jQuery.each(jQuery('script'), function(i, script) {
            var src = jQuery(script).attr('src');
            if (src && src.match(/(.*?)ga\.map\.js$/)) {
                var matches = src.match(/(.*?)ga\.map\.js$/);
                gaMap.jsDirectory = matches[1];
            }
        });
        gaMap.markers.markerSize = new OpenLayers.Size(10, 15);
        gaMap.markers.markerOffset = new OpenLayers.Pixel(-(gaMap.markers.markerSize.w / 2), -gaMap.markers.markerSize.h);
        gaMap.markers.blueMarker = new OpenLayers.Icon(gaMap.jsDirectory + '../images/marker-blue.png', gaMap.markers.markerSize,
                gaMap.markers.markerOffset);
        gaMap.markers.goldMarker = new OpenLayers.Icon(gaMap.jsDirectory + '../images/marker-gold.png', gaMap.markers.markerSize,
                gaMap.markers.markerOffset);
        gaMap.markers.redMarker = new OpenLayers.Icon(gaMap.jsDirectory + '../images/marker.png', gaMap.markers.markerSize,
                gaMap.markers.markerOffset);

        gaMap.markers.asPoint = function(element) {
            var lonLat = gaMap.markers.asLonLat(element);
            if (!lonLat) {
                return null;
            } else {
                return new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);
            }
        }

        gaMap.markers.asLonLat = function(element) {
            if (!element) {
                return null;
            }
            var jElement = jQuery(element);
            if (!jElement.length) {
                return null;
            }
            var metadata = jElement.metadata();
            var lat = parseFloat(metadata.lat, 10);
            var lon = parseFloat(metadata.lon, 10);
            var markerLonLat = new OpenLayers.LonLat(lon, lat);
            return markerLonLat;
        }
        // add a marker to the markerLayer to represent a mapMarkerCheckBox
        gaMap.markers.addMarker = function(element, markerLayer, highlight) {
            try {
                if (!highlight) {
                    highlight = false;
                }
                var jElement = jQuery(element);
                var metadata = jElement.metadata();
                var markerLonLat = gaMap.markers.asLonLat(element);
                markerLonLat.transform(gaMap.map.displayProjection, gaMap.map.baseLayer.projection);
                var oldMarker = gaMap.markers.findMarkerAt(markerLonLat, markerLayer);
                if (oldMarker != null) {
                    jElement.data('marker', oldMarker); // connect to the existing
                    // marker
                } else {
                    var icon;
                    if (element.type == 'checkbox' || element.type == 'radio') {
                        if (element.checked) { // if we have a real checkbox
                            icon = gaMap.markers.redMarker.clone();
                        } else {
                            icon = gaMap.markers.goldMarker.clone();
                        }
                    } else {
                        if (highlight) {
                            icon = gaMap.markers.redMarker.clone();
                        } else {
                            icon = gaMap.markers.goldMarker.clone();
                        }
                    }
                    var marker = new OpenLayers.Marker(markerLonLat, icon);
                    marker.layer = markerLayer;
                    marker.checkbox = element;

                    markerLayer.addMarker(marker);
                    jElement.data('marker', marker);
                    gaMap.tidyMapElements(icon.imageDiv);
                }
            } catch (e) {
                console.log('exception occured in addMarker [%o], %o', element, e);
            }
        };

        // replace add a marker to the markerLayer to represent a mapMarkerCheckBox
        gaMap.markers.replaceMarker = function(checkbox, markerLayer) {
            var marker = jQuery(checkbox).data('marker');
            markerLayer.removeMarker(marker);
            gaMap.markers.addMarker(checkbox, markerLayer);
        };

        // return the first marker found at the specified lonlat
        gaMap.markers.findMarkerAt = function(markerLonLat, markerLayer) {
            if (markerLayer.markers && (markerLayer.markers.length > 0)) {
                for ( var i = 0, len = markerLayer.markers.length; i < len; i++) {
                    var marker = markerLayer.markers[i];
                    if (marker.lonlat.lon == markerLonLat.lon && marker.lonlat.lat == markerLonLat.lat) {
                        return marker;
                    }
                }
            }
            return null;
        };

        gaMap.markers.displayMarker = function(marker, asyncron) {

            var jCheckbox = jQuery(marker.checkbox);
            // the TD of the accordionPanel
            var panelTD = jCheckbox.parent().next();
            // the accordionPanel itself
            var panel = jQuery('.panelHead', panelTD).closest('.accordionPanel');
            var link = jCheckbox.closest('tr').find("a.link");

            var checkStatus = jCheckbox.attr("checked");
            if (checkStatus == 'checked') {// toggle the check status
                checkStatus = false;
            } else {
                checkStatus = true;
            }
            // 1. Set the checkbox of the selected marker to "checked" (TRUE)
            jCheckbox.attr("checked", checkStatus);
            gaMap.markers.replaceMarker(marker.checkbox, marker.layer);

            if (panel.length) { // we have an accordion panel with details
                // 2. Open the accordionPanel
                jQuery.accordion.openPanel(panel, 0, asyncron);

                // 3. Remove "notselected" class and add "selected" class to the row for
                // css styling
                var row = jQuery('.panelBody', panelTD).parent().parent().parent();
                if (checkStatus) {
                    row.removeClass('notselected');
                    row.addClass('selected');
                } else {
                    row.addClass('notselected');
                    row.removeClass('selected');
                }
                // 4. Scroll to the selected row
                var container = panelTD.parent().parents('.content');
                container.scrollTo(panelTD.parent().position().top);
            } else if (link.length) {
                link.trigger('click'); // click it
            } else { // hopefully we already have the detail in the row.
                // 3. Remove "notselected" class and add "selected" class to the row for
                // css styling
                var row = jCheckbox.closest('tr');
                if (checkStatus) {
                    row.removeClass('notselected');
                    row.addClass('selected');
                } else {
                    row.addClass('notselected');
                    row.removeClass('selected');
                }
                // 4. Scroll to the selected row
                var container = jCheckbox.closest('tr').closest('.content');
                container.scrollTo(row.position().top);
            }

        };

        gaMap.markers.displayMarkers = function(markerArray) {
            if (markerArray == null || markerArray.length == 0) {
                return;
            }

            // Note: set async of ajax call to false in order to make the for loop
            // work properly and avoid flooding the web server
            for ( var i = 0; i < markerArray.length; i++) {
                gaMap.markers.displayMarker(markerArray[i], false);
            }
        };
        gaMap.setLayerVisibility = function(regex, visible) {
            if (!gaMap.map) {
                return;
            }
            var layers = gaMap.map.getLayersByName(regex);
            for ( var i = 0; i < layers.length; i++) {
                layers[i].setVisibility(visible); // show/hide
            }
        }
        gaMap.destroyLayers = function(regex) {
            if (!gaMap.map) {
                return;
            }
            var layers = gaMap.map.getLayersByName(regex);
            for ( var i = 0; i < layers.length; i++) {
                layers[i].destroy();
            }
        }

        // ZoomToPrevious control
        OpenLayers.Control.ZoomToPrevious = OpenLayers.Class(OpenLayers.Control.DrawFeature, {
            initialize : function(layer, handler, options) {
                OpenLayers.Control.DrawFeature.prototype.initialize.apply(this, [ layer, handler, options ]);
            },

            setMap : function(map) {
                this.handler.setMap(map);
                OpenLayers.Control.prototype.setMap.apply(this, arguments);
            },
            CLASS_NAME : "OpenLayers.Control.ZoomToPrevious"
        });

        // ZoomToPrevious support
        gaMap.savedPositions = [];

        gaMap.mapPosition = function(center, zoom) {
            this.center = center;
            this.zoom = zoom;
        };

        gaMap.saveBounds = function() {
            if (gaMap.savedPositions.length > 0) {
                if (!((gaMap.savedPositions[gaMap.savedPositions.length - 1].center.lat == gaMap.map.getCenter().lat)
                        && (gaMap.savedPositions[gaMap.savedPositions.length - 1].center.lon == gaMap.map.getCenter().lon) && (gaMap.savedPositions[gaMap.savedPositions.length - 1].zoom == gaMap.map
                        .getZoom()))) {
                    // Map has been moved (not just resized)
                    gaMap.savedPositions.push(new gaMap.mapPosition(gaMap.map.getCenter(), gaMap.map.getZoom()));
                }
            }
        };
        gaMap.gotoPrevious = function() {
            if (gaMap.savedPositions.length > 0) {
                if (gaMap.savedPositions.length > 1)
                    gaMap.savedPositions.pop();
                var lastPosition = gaMap.savedPositions[gaMap.savedPositions.length - 1];
                if (lastPosition) {
                    gaMap.map.events.unregister("moveend", gaMap.map, gaMap.saveBounds);
                    gaMap.map.setCenter(lastPosition.center);
                    gaMap.map.zoomTo(lastPosition.zoom);
                    gaMap.map.events.register("moveend", gaMap.map, gaMap.saveBounds);
                }
            }
        };

        gaMap.addWmsLayer = function(wmsTitle, wmsLayers, wmsURI, options) {
            defaultOptions = {
                isBaseLayer : false,
                projection : gaMap.map.projection,
                visibility : false,
                singleTile : false
            };
            if (typeof options == 'object') {
                options = jQuery.extend(defaultOptions, options);
            } else {
                options = defaultOptions;
            }
            var wmsLayer = new OpenLayers.Layer.WMS(wmsTitle, wmsURI, {
                transparent : 'true',
                layers : wmsLayers + ''
            }, options);
            gaMap.map.addLayer(wmsLayer);
            return wmsLayer;
        };

        /*
         * retrieves the marker layer by name (creating and adding it to the map
         * if necessary)
         */
        gaMap.getPointLayer = function(map, layername, selectPointZoomLevel) {
            var pointLayer = map.getLayersByName(layername);
            if (pointLayer.length) {
                pointLayer = pointLayer[0];
            } else {
                var styleMap = new OpenLayers.StyleMap(OpenLayers.Util.applyDefaults({
                    fillColor : "red",
                    fillOpacity : 1,
                    strokeColor : "#000000"
                }, OpenLayers.Feature.Vector.style["default"]));
                pointLayer = new OpenLayers.Layer.Vector(layername, {
                    styleMap : styleMap,
                    displayInLayerSwitcher : true
                });
                gaMap.pointControl = new OpenLayers.Control.PointMarker(pointLayer, OpenLayers.Handler.Point, {
                    title : layername,
                    handlerOptions : {
                        mousemove : function() {
                            return false;
                        }
                    }
                });
                OpenLayers.Util.extend(gaMap.pointControl, {
                    zoomLevel : selectPointZoomLevel,
                    /*
                     * this gets called by openlayers after a point has been
                     * clicked
                     */
                    notifyPoint : function(point) {
                        // To convert the geometry from Google
                        // Coordinates system to
                        // EPSG:4326
                        var coords = point.clone();
                        coords.transform(gaMap.map.baseLayer.projection, gaMap.map.displayProjection);
                        var zoom = gaMap.map.getZoom();
                        if (typeof gaMap.pointControl.zoomLevel == 'function')
                            zoom = gaMap.pointControl.zoomLevel(zoom);
                        else if (gaMap.pointControl.zoomLevel !== undefined && gaMap.pointControl.zoomLevel !== null) {
                            zoom = gaMap.pointControl.zoomLevel;
                        }
                        gaMap.map.setCenter(new OpenLayers.LonLat(point.x, point.y), zoom);
                        var latInput = jQuery('#locationCoordinates input.latitude');
                        var lonInput = jQuery('#locationCoordinates input.longitude');
                        var lat = parseFloat(coords.y, 10).toFixed(2);
                        var lon = parseFloat(coords.x, 10).toFixed(2);
                        var latVal = latInput.val();
                        if (lat != latVal) {
                            latInput.val(lat);
                            latInput.trigger('change');
                        }
                        var lonVal = lonInput.val();
                        if (lon != lonVal) {
                            lonInput.val(lon);
                            lonInput.trigger('change');
                        }
                    }
                });
                gaMap.pointControl.events.register("activate", gaMap.pointControl, gaMap.pointControl.clearPoint);
                map.addLayer(pointLayer);
            }
            return pointLayer;
        };

        gaMap.showMapMarker = function(map) {
            // show result items on the map
            var markerLayer = gaMap.getPointLayer(map, "Point Marker");
            markerLayer.removeAllFeatures();
            var latInput = jQuery('#locationCoordinates input.latitude');
            var lonInput = jQuery('#locationCoordinates input.longitude');
            if (jQuery.isNumeric(latInput.val()) && jQuery.isNumeric(lonInput.val())) {
                var lat = parseFloat(latInput.val(), 10);
                var lon = parseFloat(lonInput.val(), 10);
                if (isNaN(lat) || isNaN(lon)) {
                    // ignore
                } else {
                    lat = lat.toFixed(2);
                    lon = lon.toFixed(2);

                    var coords = new OpenLayers.LonLat(lon, lat);
                    coords.transform(gaMap.map.displayProjection, gaMap.map.baseLayer.projection);
                    gaMap.pointControl.setPoint(coords);
                }
            }
        };

        gaMap.tidyMapElements = function(scope) {
            // tidy up map elements for wcag compliance
            if (!scope) {
                var scope = gaMap.mapDiv;
            }
            if (scope) {
                scope = jQuery(scope);
                var ai = jQuery("img:not([alt])", scope);
                if (ai.length) {
                    //console.log('adding alt attrs to ' + ai.length + ' images');
                    ai.attr("alt", "");
                }
                ai = null;
                var ti = jQuery("input:not([title])", scope);
                if (ti.length) {
                    //console.log('adding title attrs to ' + ti.length + ' input elements');
                    ti.attr("title", "mapping-component controls");
                }
                ti = null;
                scope.find('input').each(function() {
                    var elem = jQuery(this);
                    if (elem.prop('name') != null) {
                        if (elem.prop('name').substr(0, 1) != '_') {
                            elem.prop('name', '_' + elem.prop('name')); // prefix with '_' so frameworks like spring binding ignore it
                            elem.prop('name', elem.prop('name').replace(/(?![A-Za-z0-9._])./g, "")); // clean out any unusual chars
                            // if the next element is an orphaned label adopt it
                            elem.next('label:not([for])').each(function() {
                                var label = jQuery(this);
                                label.attr('for', elem.prop('id'));
                            });
                        }
                    }
                });
            }
        };

        /*
         * sets up a standard/basic map of australia with nominated controls
         * 
         */
        gaMap.initBasicMap = function(options) {
            defaultOptions = {
                zoomIn : true,
                zoomOut : true,
                fullZoom : true,
                zoomToPrev : true,
                pan : true,
                zoomBox : true,
                boundingBox : true,
                selectPoint : false,
                selectPointZoomLevel : 10,
                info : true, // adds info tool
                infoSelectFirst : false, // selects first item only
                overviewMap : true,
                baseLayer : 'googleStreet',
                defaultControl : null, // title of the control to select by default, eg 'Bounding Box' 
                numZoomLevels : 19,
                minScale : 30000
            };
            if (typeof options == 'object') {
                options = jQuery.extend(defaultOptions, options);
            } else {
                options = defaultOptions;
            }
            var mapDiv = jQuery('#map');
            if (!mapDiv.length) {
                return; // no div to put the map in
            }
            if (mapDiv.hasClass('olMap')) {
                return; // already initialized
            }
            gaMap.mapDiv = mapDiv;
            if (typeof google == 'undefined') {
                gaMap.mapDiv
                        .html('<div class="warning"><h3>Unable to access Google Maps.</h3>The map portion of this application has been disabled.</div>');
                return null;
            }

            gaMap.proj4326 = new OpenLayers.Projection("EPSG:4326");
            gaMap.projWGS84 = gaMap.proj4326;
            gaMap.proj900913 = new OpenLayers.Projection("EPSG:900913");
            gaMap.proj3857 = new OpenLayers.Projection("EPSG:3857"); // 
            gaMap.projGlobalMercator = gaMap.proj3857;
            gaMap.proj4283 = new OpenLayers.Projection("EPSG:4283"); // GDA94 in latlong
            gaMap.projGDA94 = gaMap.proj4283;

            gaMap.maxExtent = new OpenLayers.Bounds(112, -44, 157, -2).transform(gaMap.proj4326, gaMap.proj900913);
            gaMap.map = new OpenLayers.Map("map", {
                controls : [],
                projection : "EPSG:3857", // google
                displayProjection : gaMap.projWGS84,
                minScale : options.minScale,
                numZoomLevels : options.numZoomLevels,
                units : "m",
                maxExtent : gaMap.maxExtent,
                wrapDateLine : true,
                restrictedExtent : null
            });
            gaFramework.map = gaMap.map; // expose as gaFramework.map for backward compatability
            var googleStreet = new OpenLayers.Layer.Google("Google (Streets)", {
                sphericalMercator : true,
                wrapDateLine : true
            }, {
                isBaseLayer : true,
                visibility : false
            });
            googleStreet.projection = gaMap.proj3857; // override with the standard epsg id

            var googlePhysical = new OpenLayers.Layer.Google("Google (Physical)", {
                sphericalMercator : true,
                type : "terrain",
                wrapDateLine : true
            }, {
                isBaseLayer : true,
                visibility : false
            });
            googlePhysical.projection = gaMap.proj3857; // override with the standard epsg id

            var googleSatellite = new OpenLayers.Layer.Google("Google (Satellite)", {
                sphericalMercator : true,
                type : "satellite",
                wrapDateLine : true
            }, {
                isBaseLayer : true,
                visibility : false
            });
            googleSatellite.projection = gaMap.proj3857; // override with the standard epsg id

            var googleHybrid = new OpenLayers.Layer.Google("Google (Satellite with Labels)", {
                sphericalMercator : true,
                type : "hybrid",
                wrapDateLine : true
            }, {
                isBaseLayer : true,
                visibility : false
            });
            googleHybrid.projection = gaMap.proj3857; // override with the standard epsg id

            // set transformation functions so openlayers knows transformations to/from EPSG:3857 are the spehericalmercator defaults
            if (!OpenLayers.Projection.transforms["EPSG:4326"] || !OpenLayers.Projection.transforms["EPSG:4326"]["EPSG:3857"]) {
                OpenLayers.Projection.addTransform("EPSG:4326", "EPSG:3857", OpenLayers.Layer.SphericalMercator.projectForward);
            }
            if (!OpenLayers.Projection.transforms["EPSG:3857"] || !OpenLayers.Projection.transforms["EPSG:3857"]["EPSG:4326"]) {
                OpenLayers.Projection.addTransform("EPSG:3857", "EPSG:4326", OpenLayers.Layer.SphericalMercator.projectInverse);
            }

            var zoomInControl = null;
            if (options.zoomIn) {
                zoomInControl = new OpenLayers.Control.ZoomIn({
                    title : 'Zoom in'
                });
            }
            var zoomOutControl = null;
            if (options.zoomOut) {
                zoomOutControl = new OpenLayers.Control.ZoomOut({
                    title : 'Zoom out'
                });
            }
            var zoomToFullControl = null;
            if (options.fullZoom) {
                zoomToFullControl = new OpenLayers.Control.ZoomToMaxExtent({
                    title : 'Zoom to full map extent'
                });
                OpenLayers.Util.extend(zoomToFullControl, {
                    trigger : function(bounds) {
                        gaMap.map.zoomToExtent(gaMap.maxExtent);
                    }
                });
            }
            var panControl = null;
            if (options.pan) {
                panControl = new OpenLayers.Control.Navigation({
                    title : 'Pan'
                });
            }
            var zoomToPreviousControl = null;
            if (options.zoomToPrev) {
                var ztpLayer = new OpenLayers.Layer.Vector("Zoom to Previous", {
                    displayInLayerSwitcher : false
                });
                zoomToPreviousControl = new OpenLayers.Control.ZoomToPrevious(ztpLayer, OpenLayers.Handler.RegularPolygon);
                OpenLayers.Util.extend(zoomToPreviousControl, {
                    title : 'Zoom to the previous extent',
                    type : OpenLayers.Control.TYPE_BUTTON,
                    response : function(bounds) {
                    },
                    trigger : function(bounds) {
                        gaMap.gotoPrevious();
                    }
                });
            }
            if (options.selectPoint) {
                var pointLayer = gaMap.getPointLayer(gaMap.map, "Point Marker", options.selectPointZoomLevel);
            }
            if (options.boundingBox) {
                gaMap.boundingboxLayer = new OpenLayers.Layer.Vector("Bounding Box", {
                    displayInLayerSwitcher : true
                });
                gaMap.boundingboxControl = new OpenLayers.Control.BoundingBox(gaMap.boundingboxLayer, OpenLayers.Handler.RegularPolygon, {
                    title : 'Bounding Box',
                    handlerOptions : {
                        sides : 4,
                        irregular : true,
                        citeComplaint : true
                    }
                });
                OpenLayers.Util.extend(gaMap.boundingboxControl, {
                    notifyBoundaries : function(geometry) {

                        // To convert the geometry from Google Coordinates system to
                        // EPSG:4326
                        var bounds = geometry.clone().getBounds();
                        bounds.transform(//gaMap.map.baseLayer.projection,
                        gaMap.proj900913, gaMap.map.displayProjection);

                        var inputLatN = jQuery('#boundingBoxExtents input.latN');
                        var inputLatS = jQuery('#boundingBoxExtents input.latS');
                        var inputLonE = jQuery('#boundingBoxExtents input.lonE');
                        var inputLonW = jQuery('#boundingBoxExtents input.lonW');
                        var latN = parseFloat(inputLatN.val(), 10);
                        var latS = parseFloat(inputLatS.val(), 10);
                        var lonE = parseFloat(inputLonE.val(), 10);
                        var lonW = parseFloat(inputLonW.val(), 10);

                        if (isNaN(latN) //
                                || isNaN(latS)// 
                                || isNaN(lonE)//
                                || isNaN(lonW) //
                                || latN != bounds.top.toFixed(4)//
                                || latS != bounds.bottom.toFixed(4)//
                                || lonE != bounds.right.toFixed(4)//
                                || lonW != bounds.left.toFixed(4)) {
                            inputLatN.val(bounds.top.toFixed(4));
                            inputLatS.val(bounds.bottom.toFixed(4));
                            inputLonE.val(bounds.right.toFixed(4));
                            inputLonW.val(bounds.left.toFixed(4));
                            inputLatN.trigger('change');
                        }
                    }
                });
                gaMap.boundingboxControl.events.register("activate", gaMap.boundingboxControl, gaMap.boundingboxControl.clearBoundaries);

            }
            var zoomBoxControl = null;
            if (options.zoomBox) {
                zoomBoxControl = new OpenLayers.Control.ZoomBox({
                    title : 'Zoom box'
                });
            }
            if (options.info) {
                // the iTool control
                gaMap.infoControl = new OpenLayers.Control();
                OpenLayers.Util.extend(gaMap.infoControl, {
                    title : 'Select a marker or area of interest',
                    displayClass : 'olControlInformation',
                    type : OpenLayers.Control.TYPE_TOOL,
                    draw : function() {
                        this.box = new OpenLayers.Handler.Box(zoomBoxControl, {
                            "done" : this.response
                        }, {
                            'boxDivClassName' : 'olHandlerBoxInfoBox',
                            'keyMask' : OpenLayers.Handler.MOD_NONE
                        });
                        this.box.activate();
                    },
                    response : function(bounds) {
                        // For a bounding box...
                        if (bounds instanceof OpenLayers.Bounds) {
                            // Create a bounding box
                            var bbox = new OpenLayers.Bounds();
                            bbox.extend(gaMap.map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.left, bounds.bottom)));
                            bbox.extend(gaMap.map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.right, bounds.top)));

                            var polygon = bounds.toGeometry();

                            var markerLayer = gaMap.map.getLayersByName("Results");
                            if (markerLayer.length) {

                                markerLayer = markerLayer[0];
                                var markers = markerLayer.markers;

                                var markersInsidePolygon = [];

                                if (markers.length >= 1) {
                                    for ( var i = 0; i < markers.length; i++) {
                                        var pixs = gaMap.map.getPixelFromLonLat(markers[i].lonlat);
                                        var point = new OpenLayers.Geometry.Point(pixs.x, pixs.y);
                                        var inside = polygon.containsPoint(point);

                                        if (inside == true) {
                                            markersInsidePolygon.push(markers[i]);
                                        }
                                    }

                                    if (markersInsidePolygon.length > 0) {
                                        if (options.infoSelectFirst) {
                                            gaMap.markers.displayMarker(markersInsidePolygon[0]); // just the first one
                                        } else {
                                            gaMap.markers.displayMarkers(markersInsidePolygon);
                                        }
                                    }

                                } // End of markers.length
                            } // End of markerLayer
                        }
                        // For a click...
                        else {
                            var closestMarker = null;
                            var markerLayer = gaMap.map.getLayersByName("Results");

                            if (markerLayer.length) {

                                markerLayer = markerLayer[0];
                                var markers = markerLayer.markers;

                                if (markers.length >= 1) {
                                    // if clicking on the marker icon...
                                    // Note: the offset of a marker: x-5, y-15 pixels
                                    for ( var i = 0; i < markers.length; i++) {
                                        var pixs = gaMap.map.getPixelFromLonLat(markers[i].lonlat);
                                        if (bounds.x >= pixs.x - 5 && bounds.x <= pixs.x + 5 && bounds.y >= pixs.y - 15 && bounds.y <= pixs.y) {
                                            closestMarker = markers[i];
                                            break;
                                        }
                                    }

                                    // if clicking outside of the marker icon... do the best guess...
                                    if (closestMarker == null) {
                                        var pixs = gaMap.map.getPixelFromLonLat(markers[0].lonlat);
                                        var adjustY = pixs.y;
                                        // Adjust the y pixels of the marker icon
                                        if (bounds.y < pixs.y) {
                                            adjustY = pixs.y - 10;
                                        }

                                        // set the first marker as the closest marker
                                        var shortest = Math.sqrt(Math.abs(bounds.x - pixs.x) * Math.abs(bounds.x - pixs.x)
                                                + Math.abs(bounds.y - adjustY) * Math.abs(bounds.y - adjustY));
                                        // if the distance <=10, the marker is treated as clicked
                                        if (shortest <= 10.0) {
                                            closestMarker = markers[0];
                                        }

                                        // Compare with the rest of selected markers
                                        for ( var i = 1; i < markers.length; i++) {
                                            var markerPixs = gaMap.map.getPixelFromLonLat(markers[i].lonlat);

                                            var adjustY = markerPixs.y;
                                            // Adjust the y pixels of the marker icon
                                            if (bounds.y < markerPixs.y) {
                                                adjustY = markerPixs.y - 10;
                                            }

                                            var distance = Math.sqrt(Math.abs(bounds.x - markerPixs.x) * Math.abs(bounds.x - markerPixs.x)
                                                    + Math.abs(bounds.y - adjustY) * Math.abs(bounds.y - adjustY));
                                            // Reset the shortest distance and the closest marker
                                            if (distance < shortest) {
                                                shortest = distance;
                                                // if the distance <=10, the marker is treated as clicked
                                                if (shortest <= 10.0) {
                                                    closestMarker = markers[i];
                                                }
                                            }
                                        } // End of For loop
                                    } // End of If
                                } // markers > 0
                            } // markerlayer

                            if (closestMarker != null) {
                                gaMap.markers.displayMarker(closestMarker);
                            }
                        }// End of else
                    } // End of response function
                }); // End of OpenLayers.Util.extend
            }
            gaMap.map.addLayers([ googleStreet, googlePhysical, googleSatellite, googleHybrid ]);
            if ('googlePhysical' == options.baseLayer) {
                gaMap.map.setBaseLayer(googlePhysical);
            } else if ('googleSatellite' == options.baseLayer) {
                gaMap.map.setBaseLayer(googleSatellite);
            } else if ('googleHybrid' == options.baseLayer) {
                gaMap.map.setBaseLayer(googleHybrid);
            } else {
                gaMap.map.setBaseLayer(googleStreet);
            }
            if (options.boundingBox) {
                gaMap.map.addLayer(gaMap.boundingboxLayer);
            }

            var controls = [];
            if (options.zoomIn) {
                controls.push(zoomInControl);
            }
            if (options.zoomOut) {
                controls.push(zoomOutControl);
            }
            if (options.fullZoom) {
                controls.push(zoomToFullControl);
            }
            if (options.zoomToPrev) {
                controls.push(zoomToPreviousControl);
            }
            if (options.pan) {
                controls.push(panControl);
            }
            if (options.zoomBox) {
                controls.push(zoomBoxControl);
            }
            if (options.boundingBox) {
                controls.push(gaMap.boundingboxControl);
            }
            if (options.selectPoint) {
                controls.push(gaMap.pointControl);
            }
            if (options.info) {
                controls.push(gaMap.infoControl);
            }

            var defControl = null;
            if (options.defaultControl == null) {
                if (options.boundingBox) {
                    options.defaultControl = gaMap.boundingboxControl.title;
                } else if (options.selectPoint) {
                    options.defaultControl = gaMap.pointControl.title;
                }
            }
            if (options.defaultControl != null) {
                for ( var i = 0; i < controls.length; i++) {
                    if (options.defaultControl == controls[i].title) {
                        defControl = controls[i];
                    }
                }
            }

            var panel = new OpenLayers.Control.Panel({
                defaultControl : defControl
            });

            panel.addControls(controls);
            gaMap.map.addControl(panel);
            gaMap.map.addControl(new OpenLayers.Control.LayerSwitcher({
                roundedCorner : false
            }));
            gaMap.map.addControl(new OpenLayers.Control.MousePosition({
                prefix : '<a target="_blank" ' + 'href="http://spatialreference.org/ref/epsg/4326/">' + 'EPSG:4326</a>: '
            }));
            gaMap.map.addControl(new OpenLayers.Control.ScaleLine());
            //gaMap.map.addControl(new OpenLayers.Control.KeyboardDefaults({
            //    observeElement : gaMap.mapDiv
            //}));

            if (options.overviewMap) {
                // reference map
                var ovMap = new OpenLayers.Layer.Google("Google (Streets)", {
                    sphericalMercator : true,
                    wrapDateLine : true
                }, {
                    isBaseLayer : true,
                    visibility : true
                });
                ovMap.projection = gaMap.proj3857; // override with the standard epsg id

                var ovsize = Math.floor(gaMap.map.size.w / 7); // approx 15% of map
                // size
                if (ovsize > 80) { // but limit to 80 pixels
                    ovsize = 80;
                }
                var refMap = new OpenLayers.Control.OverviewMap({
                    size : new OpenLayers.Size(ovsize, ovsize),
                    projection : gaMap.map.baseLayer.projection,
                    units : "m",
                    maxExtent : gaMap.maxExtent,
                    layers : [ ovMap ],
                    autoPan : true
                });

                gaMap.map.addControl(refMap);
                refMap.maximizeControl();
            }

            gaMap.map.zoomToExtent(gaMap.maxExtent);

            // track changes to the boundingBoxExtents & apply them to the map
            if (options.boundingBox) {
                jQuery('#boundingBoxExtents').delegate('input:text', 'change', function() {
                    // var input = jQuery(this);
                    // console.log(input.attr('id') + 'set to ' +
                    // input.val());
                    var latN = parseFloat(jQuery('#boundingBoxExtents .latN').val(), 10);
                    var latS = parseFloat(jQuery('#boundingBoxExtents .latS').val(), 10);
                    var lonE = parseFloat(jQuery('#boundingBoxExtents .lonE').val(), 10);
                    var lonW = parseFloat(jQuery('#boundingBoxExtents .lonW').val(), 10);

                    if (isNaN(latN) || isNaN(latS) || isNaN(lonE) || isNaN(lonW)) {
                        // ignore
                    } else {
                        var bounds = new OpenLayers.Bounds(//
                        lonW.toFixed(4),//
                        latS.toFixed(4),//
                        lonE.toFixed(4),//
                        latN.toFixed(4));

                        // Convert the bounds back from display to base projection coordinate systems
                        bounds.transform(gaMap.map.displayProjection, gaMap.map.baseLayer.projection);

                        gaMap.boundingboxControl.setBoundaries(bounds);
                    }
                });

                //gaMap.map.events.register("changebaselayer", gaMap.map, gaMap.windowResize);
            }
            // Zoom to Previous
            // --------------------------------------------------------------------------------------------------------------------------
            // Begin zoom to previous support functions

            gaMap.savedPositions.push(new gaMap.mapPosition(gaMap.map.getCenter(), gaMap.map.getZoom()));

            gaMap.map.events.register("moveend", gaMap.map, gaMap.saveBounds);

            /*
             * Stylesheet switching /*
             * --------------------------------------------------------------------------------------------------------------------------
             */
            jQuery('#switchCSS').toggle(function() {
                gaMap.oldBounds = {
                    center : gaMap.map.getCenter(),
                    zoom : gaMap.map.getZoom(),
                    extent : gaMap.map.getExtent()
                };
                var oldCenter = gaMap.map.center;
                gaMap.map.setCenter(oldCenter);
                jQuery(window).resize();
                return false;
            }, function() {
                jQuery(window).resize();
                gaMap.map.setCenter(gaMap.oldBounds.center);
                gaMap.map.zoomToExtent(gaMap.oldBounds.extent);
                gaMap.map.zoomTo(gaMap.oldBounds.zoom);
                gaMap.map.updateSize();
                jQuery(window).resize();
                return false;
            });
            gaMap.map.events.register("changebaselayer", function() {
                //console.log('tidying map elements after changebaseleyer');
                this, gaMap.tidyMapElements();
            });
            //gaMap.map.events.register("redraw", this, gaMap.tidyMapElements);
            //console.log('tidying map elements on startup');
            gaMap.tidyMapElements(gaMap.mapDiv);
            window.setTimeout(function() {
                //console.log('tidying map elements after 500ms');
                gaMap.tidyMapElements();
                window.setTimeout(function() {
                    //console.log('tidying map elements after 1000ms');
                    gaMap.tidyMapElements();
                    window.setTimeout(function() {
                        //console.log('tidying map elements after 2000ms');
                        gaMap.tidyMapElements();
                        window.setTimeout(gaMap.tidyMapElements, 2000);
                    }, 1000);
                }, 500);
            }, 500);
            // resize map when window resized
            if (jQuery.throttle) {
                jQuery(window).resize(jQuery.throttle(250, function() {
                    gaMap.map.updateSize();
                }));
            } else {
                jQuery(window).resize(function() {
                    gaMap.map.updateSize();
                });
            }
            return gaMap.map;
        }; // end of initBasicMap

        //console.warn('checking Control.LayerSwitcher input name and id attributes');
        if (typeof dojo != 'undefined') {
            dojo.connect(OpenLayers.Control.LayerSwitcher.prototype, 'redraw', null, function() {
                gaMap.tidyMapElements(this.div);
                if (!this.div == gaMap.mapDiv) {
                    gaMap.tidyMapElements(gaMap.mapDiv);
                }
            });
        } else if (typeof jQuery.aop != 'undefined') {
            jQuery.aop.after({
                target : OpenLayers.Control.LayerSwitcher.prototype,
                method : 'redraw'
            }, function(index) {
                gaMap.tidyMapElements(this.div);
                if (!this.div == gaMap.mapDiv) {
                    gaMap.tidyMapElements(gaMap.mapDiv);
                }
            });
        } else {
            console.warn(' ---- skipping redraw fix as neither dojo.connect or jQuery.aop are available');
        }
        //    jQuery(document).modified(function() {
        //        console.log('tidy after docmodified');
        //        gaMap.tidyMapElements(gaMap.mapDiv);
        //    });

        if ('OpenLayers 2.10' <= OpenLayers.VERSION_NUMBER && 'Release 2.11' > OpenLayers.VERSION_NUMBER) {
            console.warn('applying openlayers-2.10 changeset 11581 for ticket 2828');
            // apply changeset 11581 for ticket
            // http://trac.osgeo.org/openlayers/ticket/2828
            // as it causes our map to disappear when switching between print and
            // standard css if map has been zoomed

            OpenLayers.Layer.Google.v3.onMapResize = function() {
                if (this.visibility) {
                    google.maps.event.trigger(this.mapObject, "resize");
                } else {
                    var cache = OpenLayers.Layer.Google.cache[this.map.id];
                    if (!cache.resized) {
                        var layer = this;
                        google.maps.event.addListenerOnce(this.mapObject, "tilesloaded", function() {
                            google.maps.event.trigger(layer.mapObject, "resize");
                            layer.moveTo(layer.map.getCenter(), layer.map.getZoom());
                            delete cache.resized;
                        });
                    }
                    cache.resized = true;
                }
            };

            OpenLayers.Layer.Google.v3.setGMapVisibility = function(visible) {
                var cache = OpenLayers.Layer.Google.cache[this.map.id];
                if (cache && !cache.resized) {
                    var type = this.type;
                    var layers = this.map.layers;
                    var layer;
                    for ( var i = layers.length - 1; i >= 0; --i) {
                        layer = layers[i];
                        if (layer instanceof OpenLayers.Layer.Google && layer.visibility === true && layer.inRange === true) {
                            type = layer.type;
                            visible = true;
                            break;
                        }
                    }
                    var container = this.mapObject.getDiv();
                    if (visible === true) {
                        this.mapObject.setMapTypeId(type);
                        container.style.left = "";
                        if (cache.termsOfUse && cache.termsOfUse.style) {
                            cache.termsOfUse.style.left = "";
                            cache.termsOfUse.style.display = "";
                            cache.poweredBy.style.display = "";
                        }
                        cache.displayed = this.id;
                    } else {
                        delete cache.displayed;
                        container.style.left = "-9999px";
                        if (cache.termsOfUse && cache.termsOfUse.style) {
                            cache.termsOfUse.style.display = "none";
                            // move ToU far to the left in addition to setting
                            // display to "none", because at the end of the GMap
                            // load sequence, display: none will be unset and ToU
                            // would be visible after loading a map with a google
                            // layer that is initially hidden.
                            cache.termsOfUse.style.left = "-9999px";
                            cache.poweredBy.style.display = "none";
                        }
                    }
                }
            };
        }
        if ('OpenLayers 2.10' <= OpenLayers.VERSION_NUMBER && 'Release 2.11' > OpenLayers.VERSION_NUMBER) {
            console.warn('applying openlayers-2.10 fix for ticket 3003');
            // http://trac.osgeo.org/openlayers/ticket/3003
            OpenLayers.Handler.Box = OpenLayers.Class(OpenLayers.Handler, {

                /**
                 * Property: dragHandler {<OpenLayers.Handler.Drag>}
                 */
                dragHandler : null,

                /**
                 * APIProperty: boxDivClassName {String} The CSS class to use
                 * for drawing the box. Default is olHandlerBoxZoomBox
                 */
                boxDivClassName : 'olHandlerBoxZoomBox',

                /**
                 * Property: boxCharacteristics {Object} Caches some box
                 * characteristics from css. This is used by the
                 * getBoxCharacteristics method.
                 */
                boxCharacteristics : null,

                /**
                 * Constructor: OpenLayers.Handler.Box
                 * 
                 * Parameters: control - {<OpenLayers.Control>} callbacks -
                 * {Object} An object containing a single function to be called
                 * when the drag operation is finished. The callback should
                 * expect to receive a single argument, the point geometry.
                 * options - {Object}
                 */
                initialize : function(control, callbacks, options) {
                    OpenLayers.Handler.prototype.initialize.apply(this, arguments);
                    var callbacks = {
                        "down" : this.startBox,
                        "move" : this.moveBox,
                        "out" : this.removeBox,
                        "up" : this.endBox
                    };
                    this.dragHandler = new OpenLayers.Handler.Drag(this, callbacks, {
                        keyMask : this.keyMask
                    });
                },

                /**
                 * Method: destroy
                 */
                destroy : function() {
                    if (this.dragHandler) {
                        this.dragHandler.destroy();
                        this.dragHandler = null;
                    }
                    OpenLayers.Handler.prototype.destroy.apply(this, arguments);
                },

                /**
                 * Method: setMap
                 */
                setMap : function(map) {
                    OpenLayers.Handler.prototype.setMap.apply(this, arguments);
                    if (this.dragHandler) {
                        this.dragHandler.setMap(map);
                    }
                },

                /**
                 * Method: startBox
                 * 
                 * Parameters: evt - {Event}
                 */
                startBox : function(xy) {
                    this.zoomBox = OpenLayers.Util.createDiv('zoomBox', this.dragHandler.start);
                    this.zoomBox.className = this.boxDivClassName;
                    this.zoomBox.style.zIndex = this.map.Z_INDEX_BASE["Popup"] - 1;
                    this.map.viewPortDiv.appendChild(this.zoomBox);

                    OpenLayers.Element.addClass(this.map.viewPortDiv, "olDrawBox");
                },

                /**
                 * Method: moveBox
                 */
                moveBox : function(xy) {
                    var startX = this.dragHandler.start.x;
                    var startY = this.dragHandler.start.y;
                    var deltaX = Math.abs(startX - xy.x);
                    var deltaY = Math.abs(startY - xy.y);
                    this.zoomBox.style.width = Math.max(1, deltaX) + "px";
                    this.zoomBox.style.height = Math.max(1, deltaY) + "px";
                    this.zoomBox.style.left = xy.x < startX ? xy.x + "px" : startX + "px";
                    this.zoomBox.style.top = xy.y < startY ? xy.y + "px" : startY + "px";

                    // depending on the box model, modify width and
                    // height to take borders
                    // of the box into account
                    var box = this.getBoxCharacteristics();
                    if (box.newBoxModel) {
                        if (xy.x > startX) {
                            this.zoomBox.style.width = Math.max(1, deltaX - box.xOffset) + "px";
                        }
                        if (xy.y > startY) {
                            this.zoomBox.style.height = Math.max(1, deltaY - box.yOffset) + "px";
                        }
                    }
                },

                /**
                 * Method: endBox
                 */
                endBox : function(end) {
                    var result;
                    if (Math.abs(this.dragHandler.start.x - end.x) > 5 || Math.abs(this.dragHandler.start.y - end.y) > 5) {
                        var start = this.dragHandler.start;
                        var top = Math.min(start.y, end.y);
                        var bottom = Math.max(start.y, end.y);
                        var left = Math.min(start.x, end.x);
                        var right = Math.max(start.x, end.x);
                        result = new OpenLayers.Bounds(left, bottom, right, top);
                    } else {
                        result = this.dragHandler.start.clone(); // i.e.
                        // OL.Pixel
                    }
                    this.removeBox();

                    this.callback("done", [ result ]);
                },

                /**
                 * Method: removeBox Remove the zoombox from the screen and
                 * nullify our reference to it.
                 */
                removeBox : function() {
                    this.map.viewPortDiv.removeChild(this.zoomBox);
                    this.zoomBox = null;
                    this.boxCharacteristics = null;
                    OpenLayers.Element.removeClass(this.map.viewPortDiv, "olDrawBox");

                },

                /**
                 * Method: activate
                 */
                activate : function() {
                    if (OpenLayers.Handler.prototype.activate.apply(this, arguments)) {
                        this.dragHandler.activate();
                        return true;
                    } else {
                        return false;
                    }
                },

                /**
                 * Method: deactivate
                 */
                deactivate : function() {
                    if (OpenLayers.Handler.prototype.deactivate.apply(this, arguments)) {
                        if (this.dragHandler) {
                            this.dragHandler.deactivate();
                        }
                        return true;
                    } else {
                        return false;
                    }
                },

                /**
                 * Method: getCharacteristics Determines offset and box model
                 * for a box.
                 * 
                 * Returns: {Object} a hash with the following properties: -
                 * xOffset - Corner offset in x-direction - yOffset - Corner
                 * offset in y-direction - newBoxModel - true for all browsers
                 * except IE in quirks mode
                 */
                getBoxCharacteristics : function() {
                    if (!this.boxCharacteristics) {
                        var xOffset = parseInt(OpenLayers.Element.getStyle(this.zoomBox, "border-left-width"))
                                + parseInt(OpenLayers.Element.getStyle(this.zoomBox, "border-right-width")) + 1;
                        var yOffset = parseInt(OpenLayers.Element.getStyle(this.zoomBox, "border-top-width"))
                                + parseInt(OpenLayers.Element.getStyle(this.zoomBox, "border-bottom-width")) + 1;
                        // all browsers use the new box model,
                        // except IE in quirks mode
                        var newBoxModel = OpenLayers.Util.getBrowserName() == "msie" ? document.compatMode != "BackCompat" : true;
                        this.boxCharacteristics = {
                            xOffset : xOffset,
                            yOffset : yOffset,
                            newBoxModel : newBoxModel
                        };
                    }
                    return this.boxCharacteristics;
                },

                CLASS_NAME : "OpenLayers.Handler.Box"
            });
        }
        if ('OpenLayers 2.10' <= OpenLayers.VERSION_NUMBER && 'Release 2.13' > OpenLayers.VERSION_NUMBER) {
            // careful they changed the version number technique!

            console.warn('applying openlayers-2.10,11,12 fix for ticket 3040');
            // see http://trac.osgeo.org/openlayers/ticket/3040
            // ARCGIS WFS responses do not bother specifying the dimensionionality of the geometry
            // this fix is to assume it is 2 dimensional 

            OpenLayers.Format.GML.prototype.parseGeometry.linestring = function(node, ring) {
                /**
                 * Two coordinate variations to consider: 1) <gml:posList
                 * dimension="d">x0 y0 z0 x1 y1 z1</gml:posList> 2)
                 * <gml:coordinates>x0, y0, z0 x1, y1, z1</gml:coordinates>
                 */
                var nodeList, coordString;
                var coords = [];
                var points = [];

                // look for <gml:posList>
                nodeList = this.getElementsByTagNameNS(node, this.gmlns, "posList");
                if (nodeList.length > 0) {
                    coordString = this.getChildValue(nodeList[0]);
                    coordString = coordString.replace(this.regExes.trimSpace, "");
                    coords = coordString.split(this.regExes.splitSpace);
                    var dim = parseInt(nodeList[0].getAttribute("dimension"));
                    //Applying dim fix from  http://trac.osgeo.org/openlayers/ticket/3040
                    if (!dim) {
                        dim = 2;
                    }
                    //end of dim fix from  http://trac.osgeo.org/openlayers/ticket/3040
                    var j, x, y, z;
                    for ( var i = 0; i < coords.length / dim; ++i) {
                        j = i * dim;
                        x = coords[j];
                        y = coords[j + 1];
                        z = (dim == 2) ? null : coords[j + 2];
                        if (this.xy) {
                            points.push(new OpenLayers.Geometry.Point(x, y, z));
                        } else {
                            points.push(new OpenLayers.Geometry.Point(y, x, z));
                        }
                    }
                }

                // look for <gml:coordinates>
                if (coords.length == 0) {
                    nodeList = this.getElementsByTagNameNS(node, this.gmlns, "coordinates");
                    if (nodeList.length > 0) {
                        coordString = this.getChildValue(nodeList[0]);
                        coordString = coordString.replace(this.regExes.trimSpace, "");
                        coordString = coordString.replace(this.regExes.trimComma, ",");
                        var pointList = coordString.split(this.regExes.splitSpace);
                        for ( var i = 0; i < pointList.length; ++i) {
                            coords = pointList[i].split(",");
                            if (coords.length == 2) {
                                coords[2] = null;
                            }
                            if (this.xy) {
                                points.push(new OpenLayers.Geometry.Point(coords[0], coords[1], coords[2]));
                            } else {
                                points.push(new OpenLayers.Geometry.Point(coords[1], coords[0], coords[2]));
                            }
                        }
                    }
                }

                var line = null;
                if (points.length != 0) {
                    if (ring) {
                        line = new OpenLayers.Geometry.LinearRing(points);
                    } else {
                        line = new OpenLayers.Geometry.LineString(points);
                    }
                }
                return line;
            }
        }
    }
}
