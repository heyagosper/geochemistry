(function(jQuery) {
	
	/* Set up the menu namespace
	 *
	 */	
		
	jQuery.gaMenu = {};
	
	/* 
	 *
	 */	
		
	jQuery.fn.gaMenu = function() {
	 // Get our toggle
		var menuToggle = jQuery(this).addClass('gaMenuToggle');
	 // We wrap it in a menu container
		menuToggle.wrap('<div class="gaMenuContainer"></div>');
		var menuContainer = menuToggle.parent('.gaMenuContainer');
	 // And we put our menu dropdown inside our container
		var menuDropdown = jQuery('<ul></ul>').addClass('gaMenuDropdown').appendTo(menuContainer).hide();
	 // If we click anywhere on the page...
		jQuery(document).click( function(e) {
		 // Hide our menuDropdown
			menuContainer.removeClass('gaMenuOpen');
			menuDropdown.hide();
		});
	 // If we click inside the menuDropdown...
		menuDropdown.click( function(e) {
		 // Stop the click from propogating up to the document.
			e.stopPropagation();
		});
	 // When clicking on the menu button...
		menuToggle.click( function(e) {
		 // If the menuDropdown is hidden...
			if( menuDropdown.is(':hidden') ) {
			 // Hide other menus on the page.
				jQuery('.gaMenuContainer').removeClass('gaMenuOpen');
				jQuery('.gaMenuDropdown').hide();
			 // And open this one
				menuContainer.addClass('gaMenuOpen');
				menuDropdown.show();
			}
		 // If the menuDropdown is visible...
			else {
			 // Hide our menuDropdown
				menuContainer.removeClass('gaMenuOpen');
				menuDropdown.hide();
			}
		 //
			return false;
		});
	 // Return the optionsListItem
		return menuDropdown;
	};
	
	/* Add a sepearator to our menu
	 *
	 */	
		
	jQuery.gaMenu.addSeparator = function( menuDropdown ) {
	 // Create and append the separator
		var separator = jQuery('<li></li>').addClass('gaMenuSeparator').appendTo( menuDropdown );
	 // Adjust the width so we don't get weird wrapping
		separator.width( menuDropdown.width() );
	 // Output the listItem
		return separator;
	};
			
	/* Add an item to our menu
	 *
	 */	
		
	jQuery.gaMenu.addListItem = function( menuDropdown, label, fn ) {
	 // Create and append the listItem
		var listItem = jQuery('<li></li>').addClass('gaMenuItem').appendTo( menuDropdown );
	 // Make the label and apply it
		var label = jQuery('<span></span>').html(label).appendTo( listItem );
	 // Bind it to the click
		listItem.click( fn );
	 // Adjust the width so we don't get weird wrapping
	 	if( label.text().length * 6 + 40 > menuDropdown.width() ) {
			var newWidth = label.text().length * 6 + 40;
			menuDropdown.width( newWidth );
			jQuery('li',menuDropdown).width( ( newWidth ) - 10 );
			jQuery('li.gaMenuSeparator',menuDropdown).width( newWidth );
		}
		else {
			listItem.width( menuDropdown.width() - 10 );
		}
	 // Output the listItem
		return listItem;
	};
			
	/* Add a toggle item to our menu
	 *
	 */	
		
	jQuery.gaMenu.addToggleItem = function( menuDropdown, label, id, fnCheck, fnUncheck ) {
	 // Create and append the listItem
		var toggleItem = jQuery('<li></li>').addClass('gaMenuItem').addClass('gaMenuToggleItem').appendTo( menuDropdown );
	 // Make the checkbox
		var checkbox = jQuery('<input type="checkbox">').id( id ).attr({ 'checked': 'checked', 'name': id, 'title':'Visibility toggle for '+id}).appendTo( toggleItem );
	 // Make the label and apply it
		var label = jQuery('<span></span>').html(label).appendTo( toggleItem );
	 // Bind it to the click
		checkbox.toggleCheck( fnCheck, fnUncheck );
	 // If we click on the listItem...
		toggleItem.click( function(e) {
		 // Toggle the checkbox
			checkbox.toggleCheck();
		});
	 // If we click on the checkbox...
		checkbox.click( function(e) {
		 // Stop the click from propogating up to the listItem.
			e.stopPropagation();
		});
	 // Adjust the width so we don't get weird wrapping
	 	if( label.text().length * 6 + 40 > menuDropdown.width() ) {
			var newWidth = label.text().length * 6 + 40;
			menuDropdown.width( newWidth );
			jQuery('li',menuDropdown).width( ( newWidth ) - 10 );
			jQuery('li.gaMenuSeparator',menuDropdown).width( newWidth );
		}
		else {
			toggleItem.width( menuDropdown.width() - 10 );
		}
	 // Output the listItem
		return toggleItem;
	};
		
})(jQuery);