 /* Form Validation
 /* ---------------------------------------------------------------------------------------------------------------------------- */
	(function(jQuery) {
			  
		jQuery.modal = {};
			  
	 // 
		jQuery.fn.modal = function() {
		};
		
	 // 
		jQuery.modal.create = function( id ) {
		 // Variables
			var modal = {};
			modal.window = jQuery('<div class="ga-modal"></div>').id(id).appendTo('body');
			modal.tools = jQuery('<ul class="ga-modal-tools" style="display: none;"></ul>').appendTo(modal.window);
			modal.closeTool = jQuery('<li class="ga-modal-close"><span>Close</span></li>').appendTo(modal.tools);
			modal.windowPad = jQuery('<div class="ga-modal-pad"></div>').appendTo(modal.window);
			modal.content = jQuery('<div class="ga-modal-content"></div>').appendTo(modal.windowPad);
		 // Set the size and position to animate from
			modal.window.css({ 'opacity': 0 });
		 // Make the modal draggable
		 	modal.window.draggable();
		 // Function to set a title
			modal.setTitle = function( value ) {
				if( value ) {
					if( ! modal.title ) {
						modal.title = jQuery('<div class="ga-modal-title"></div>').prependTo(modal.windowPad);
						modal.tools.fadeIn(500);
						modal.window.draggable( "option", {
							handle: ( modal.title ) ? modal.title : false
						});
					}
					modal.title.text(value);
				}
			};
		 // Function to set a caption
			modal.setCaption = function( value ) {
				if( value ) {
					if( ! modal.caption ) {
						modal.caption = jQuery('<div class="ga-modal-caption"></div>').appendTo(modal.windowPad);
					}
					modal.caption.text(value);
				}
			};
		 // Make the modal draggable
		 	modal.window.hover(function(){
				if( ! modal.title ) {
					modal.tools.fadeIn(500);
				}
			},function(){
				if( ! modal.title ) {
					modal.tools.fadeOut(500);
				}
			});
		 // Return
		 	return modal;
		};

	 // 
		jQuery.modal.show = function( modal, param ) {
		 // Default parameters
		 	if( ! param ) { param = {}; }
		 // Set the opacity
		 	if( ! param.opacity ) { param.opacity = 0.5; }
		 // Get the original height and width
			var modalWidth = modal.window.width();
			var modalHeight = modal.window.height();
			var imageWidth = jQuery('img',modal.content).width();
			var imageHeight = jQuery('img',modal.content).height();
		 // Set the size and position to animate from
			modal.window.css(param);
		 // And set the image too, if necessary.
			if( modal.window.hasClass('ga-modal-image-viewer') ) {
				jQuery('img.ga-modal-image-viewer-img',modal.content).css({
					'width': param.width,
					'height': param.height
				});
			}
		 // Animate the zoom...
			modal.window.animate({
				'width': modalWidth,
				'height': modalHeight,
				'left': ( ( jQuery(window).width() - modalWidth ) / 2 ),
				'top': ( ( jQuery(window).height() - modalHeight ) / 2 ),
				'opacity': 1
			},300,'swing');
		 // And also on the image...
			if( modal.window.hasClass('ga-modal-image-viewer') ) {
				jQuery('img.ga-modal-image-viewer-img',modal.content).animate({
					'width': imageWidth,
					'height': imageHeight
				},300,'swing');
			}
		 // And hide the title, tool, and caption bars
		 	if( modal.title ) {
				modal.title.show(300,'swing');
				modal.tools.show(300,'swing');
			}
		};

	 // 
		jQuery.modal.hide = function( modal, param ) {
		 // Default parameters
		 	if( ! param ) { param = {}; }
		 // Set the opacity
		 	if( ! param.opacity ) { param.opacity = 0.5; }
		 // Animate the zoom
			modal.window.animate( param, 300, 'swing', function(){ jQuery(this).remove(); });
		 // And zoom the image too, if necessary.
			if( modal.window.hasClass('ga-modal-image-viewer') ) {
				jQuery('img.ga-modal-image-viewer-img',modal.content).animate({
					'width': param.width,
					'height': param.height
				},300,'swing');
			}
		 // And hide the title, tool, and caption bars
		 	if( modal.title ) {
				modal.title.hide(300,'swing');
				modal.tools.hide(300,'swing');
			}
		};

	 // 
		jQuery.fn.imageModal = function() {
		 // Start the timer
		 	console.time('jQuery.fn.imageModal');
		 // Iterate through unprepared images
			jQuery(this).not('.hasImageModal').each( function() {
				var imageLink = jQuery(this); 
			 // Add a class so we don't reprepare the link again.
				imageLink.addClass('hasImageModal');
			 // Variables
				var thumbnail = jQuery('img',this);
				var src = imageLink.attr('href');
				var id = imageLink.id();
				if (!id) {
					id = 'image-' + gaFramework.identifier( this );
					imageLink.id(id);
				}
			 // Bind functionality to the clicking of the link.
				imageLink.bind('click',function( e ) {
					e.preventDefault();
					if( ! jQuery( '#' + id + '-viewer' ).length ) {
					 // Create the modal window
						var modal = jQuery.modal.create( id + '-viewer' );
						modal.window.addClass('ga-modal-image-viewer');
					 // Set the content
						var image = jQuery('<img class="ga-modal-image-viewer-img" src="' + src + '" />').appendTo(modal.content);
					 // Set up a loading animation while the image loads.
						var loading = jQuery('<span class="ga-thumbnail-loading"></span>').insertAfter(thumbnail);
						loading.css({
							width: jQuery(thumbnail).outerWidth(),
							height: jQuery(thumbnail).outerHeight()
						});
					 // When the image has loaded...
						jQuery(image).load(function(){
						 // Remove the loading animation
							jQuery(loading).remove();
							if( image.height() > jQuery(window).height() - 100 ) {
								var percent = ( ( jQuery(window).height() - 100 ) / image.height() );
								image.css({
									width: Math.round( image.width() * percent ),
									height: Math.round( image.height() * percent )
								});
							}
							if( image.width() > jQuery(window).width() - 100 ) {
								var percent = ( ( jQuery(window).width() - 100 ) / image.width() );
								image.css({
									width: Math.round( image.width() * percent ),
									height: Math.round( image.height() * percent )
								});
							}
							modal.setTitle( thumbnail.attr('title') );
							modal.setCaption( thumbnail.attr('alt') );
						 // The param for animating to and from
							modal.toggleParam = function() {
								return {
								'width': thumbnail.width(),
								'height': thumbnail.height(),
								'left': thumbnail.offset().left,
								'top': thumbnail.offset().top
								};
							};
						 // Find the group
							var container = jQuery(this).parents('ga-thumbnail-container');
							var group = jQuery('ga-modal-image',container);
						 // Make the close tool do stuff
							modal.closeTool.bind('click',function(){
								jQuery.modal.hide( modal, modal.toggleParam() );
							});
						 // Get the next and previous modals
							modal.group = {};
							modal.group.prev = thumbnail.parents('li').first().prev().find('.ga-image-modal');
							modal.group.next = thumbnail.parents('li').first().next().find('.ga-image-modal');
						 // Add a 'next' tool
							modal.nextTool = jQuery('<li class="ga-modal-next"><span>Next</span></li>').prependTo(modal.tools);
							if( modal.group.next.length ) {
								modal.nextTool.bind('click',function(){
									jQuery.modal.hide( modal, modal.toggleParam() );
									modal.group.next.click();
								});
							}
						 // Fade when there is no next modal
							else {
								modal.nextTool.addClass('disabled');
							}
						 // Add a 'previous' tool
							modal.prevTool = jQuery('<li class="ga-modal-prev"><span>Previous</span></li>').prependTo(modal.tools);
							if( modal.group.prev.length ) {
								modal.prevTool.bind('click',function(){
									jQuery.modal.hide( modal, modal.toggleParam() );
									modal.group.prev.click();
								});
							}
						 // Fade when there is no previous modal
							else {
								modal.prevTool.addClass('disabled');
							}
						 // Open the modal
							setTimeout(function(){ jQuery.modal.show( modal, modal.toggleParam() ); },10);
						});
					}
				 // Stop the page from being redirected
					return false;
				});
			});
		 // Start the timer
		 	console.timeEnd('jQuery.fn.imageModal');
		 // Stop the page from being redirected
			return this;
		};
		
	})(jQuery);
	
 /* Apply jQuery Functionality.
 /* ---------------------------------------------------------------------------------------------------------------------------- */
	gaFramework.modified( function(){ 
		jQuery('.ga-image-modal').imageModal();
	});
