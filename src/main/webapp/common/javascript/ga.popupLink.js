/*
 *  Common code for popup displays of entities like samples
 *
 */
if (!gaPopupLink) {
	var gaPopupLink = {};
}
if (!gaFramework) {
	var gaFramework = {};
}
gaPopupLink.onclick = function(evt) {
	evt.preventDefault();
	evt.stopPropagation();
	var pl = jQuery(this);
	var nodeId = 'popupContent';
	var href = pl.attr('href');
	jQuery.get(href, function(data) {
		gaPopupLink.dialog.attr('content',data);
		gaPopupLink.dialog.show();
	});
	return false;
};
gaPopupLink.once = true;
gaPopupLink.popupLinks = function() {

	/*
	 * looks for a elements with class="popupLink"
	 * eg <a class="popupLink href='showSampleDetails/12345/details.html'}">xxxxx1234</a>
	 * and convert them to display in a popup window using ajax
	 * 
	 */
	var popupLinks = jQuery("a.popupLink:not(.popupLinkMarker)");
	if (popupLinks.length) { // if we have some
		console.log('styling popup links');
		popupLinks.addClass('popupLinkMarker'); // add a marker so we know they have been processed
		popupLinks.unbind('click').bind('click', gaPopupLink.onclick);
		if (gaPopupLink.once) {
			dojo.require("dijit.Dialog");
			//jQuery('body').delegate('a.popupLink', 'click', gaPopupLink.onclick);
			gaPopupLink.once = false;
			gaPopupLink.dialog = new dijit.Dialog({
				//style : "overflow:auto; width: 800px; height: 700px;"
			});
			dojo.body().appendChild(gaPopupLink.dialog.domNode);
		}
	}
};
// run on page startup and when the dom is modified
gaPopupLink.init = function() {
	gaPopupLink.popupLinks();
	gaFramework.ready(gaPopupLink.popupLinks);
	gaFramework.modified(gaPopupLink.popupLinks);
};
jQuery(document).ready(function(){
	gaPopupLink.init();
});	
