/* gaFramework richfaces integration
/* -------------------------------------------------------------------------------------------------------------------------- */

gaFramework.richfaces = {
	init : function() {
		gaFramework.richfaces.Autocomplete.init();
	}
};

gaFramework.richfaces.Autocomplete = {
	// when autocomplete list gets modified
	onChange : function(evt) {
		var input = jQuery(this);
		var parent = input.closest('span.rf-au');
		var parentId = parent.id();
		var button = parent.find('.rf-au-btn-arrow');
		if (button.length) { // if we have a button then hide/show depending
								// on list size
			var items = jQuery('#' + parentId + 'List').find(
					'ul > li:not(:empty)');
			if (items.length) {
				button.show();
			} else {
				button.hide();
			}
			// repeat in a while in case the ajax call is slow
			window.setTimeout(function() {
				var items = jQuery('#' + parentId + 'List').find(
						'ul > li:not(:empty)');
				console.log(items.length);
				if (items.length) {
					button.show();
				} else {
					button.hide();
				}
			}, 500);
		}

	},
	init : function() {
		var auInputs = jQuery('span.rf-au:not(.rfAutoMarker) input:visible');
		if (auInputs.length) {
			auInputs.each(function() {
				var input = jQuery(this);
				input.addClass('rfAutomarker');
				// input.bind('change keyup keydown',
				// gaFramework.richfaces.Autocomplete.onChange);
				// set initial visibility of the button
				var parent = input.closest('span.rf-au');
				var parentId = parent.id();
				var button = parent.find('.rf-au-btn-arrow');
				if (button.length) { // if we have a button then hide/show
										// depending on list size
					var items = jQuery('#' + parentId + 'List').find(
							'ul > li:not(:empty)');
					if (items.length) {
						button.show();
					} else {
						button.hide();
					}
				}
			});
			// delegating the event handler up to the body hopefully means the
			// list has been updated by the time we are notified
			jQuery('body').delegate('span.rf-au input:visible',
					'change keyup keydown',
					gaFramework.richfaces.Autocomplete.onChange);
		}
	}
};
jQuery(document).ready(function() {
	gaFramework.richfaces.init();
});
