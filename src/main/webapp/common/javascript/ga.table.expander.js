(function(jQuery) {
			
	/* 
	 *
	 */	
		
	jQuery.tableExpander = {};
			
	/* 
	 *
	 */	
		
	jQuery.fn.tableExpander = function() {
	 // Iterate through the tables not applied
		jQuery.each( jQuery(this).not('.hasTableExpander'), function() {
		 // Apply a 'hasTableExpander' class to stop reapplication
			jQuery(this).addClass('hasTableExpander');
		 // Create wrapper
			jQuery(this).wrap('<span class="gaTableExpander gaWrapper"></span>');
			var wrapper = jQuery(this).parent();
		 // Make sure it responds to the 'input' class.
			if( jQuery(this).hasClass('input') ) {
				jQuery(wrapper).addClass('input');
				jQuery(this).removeClass('input');
			}
		 // Create toggle button
			var toggle = jQuery('<span class="gaTableExpander gaToggle">Toggle</span>');
			jQuery(wrapper).append(toggle);
		 // Toggle the table size
			jQuery(toggle).toggle(function(){
				jQuery(wrapper)
					.addClass('maximised');
/*					.css({
						width: jQuery(window).width(),
						height: jQuery(window).height()
					});
*/			},function(){
				jQuery(wrapper)
					.removeClass('maximised');
/*					.css({
						width: 'auto',
						height: 'auto'
					});
*/			});
		});
	 // Return Object for chainability
		return this;
	};

	/* 
	 *
	 */	
		
	jQuery.tableExpander.init = function() {
		jQuery('table.expandable').tableExpander();
	};

})(jQuery);