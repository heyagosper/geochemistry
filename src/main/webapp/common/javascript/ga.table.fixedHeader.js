(function(jQuery) {
	
	/* 
	 *
	 */	
		
	jQuery.fixedHeader = {};
	
	/* 
	 *
	 */	
		
	jQuery.fn.fixedHeader = function() {
	 // If there are inputs to be messed with
		jQuery.each( jQuery(this).not('.hasFixedHeader'), function(i) {
		 // Variables
			var table = jQuery(this).addClass('hasFixedHeader');
		 // If we're not in IE7 or previous...
			if ( ! ( jQuery.browser == 'msie' && jQuery.browser.version < 7 ) ) {
			 // Get the thead
				var thead = table.find('thead');

			 // Now when we scroll...
			    jQuery(window).scroll( function( e ) {
			    	
			    if(thead.offset()) {
					// Find the top and bottom of the thead
					var top = thead.offset().top - parseFloat( thead.css('margin-top').replace(/auto/, 0));
					var bottom = top + table.height();
					    
			        // what the y position of the scroll is
				    var y = jQuery(this).scrollTop() + gaFramework.barHeightTop;
					// Create the helper table id
			        var tempTableId = 'helper-' + table.id();
				    // Find the helper table
				    var tempTable = jQuery('#'+tempTableId);
				    // If the helper table doesn't exist and we've scrolled past the top
				    if( ! tempTable.length && y >= top && y < bottom ) {
				    	// Create a helper table
				   
					 	tempTable = jQuery('<table class="input grid gaFixedHeaderHelper"></table>').id(tempTableId).appendTo('body');
						 // Put the content of the row into the helper table
							tempTable.append( '<thead>'+thead.html()+'</thead>' );
						 // Set the cell widths
							var cells = thead.find('td,th');
							tempTable.find('td,th').map( function(i) {
								jQuery(this).width( jQuery(cells[i]).width() );
							});
						 // Set the starting CSS for the row
							tempTable.css({
								'position': 'fixed',
								'width': thead.width(),
								'top': gaFramework.barHeightTop - 2,
								'left': thead.offset().left
							});
			        } 
			    	else if( tempTable.length && ( y < top || y >= bottom ) ) {
			    		// 
			    		tempTable.remove();
			    	}
			    }
			  });
			}  
		});
	};
	
	/* 
	 *
	 */	
	
	jQuery.fixedHeader.init = function() {
		jQuery('table.fixedHeader').fixedHeader();
		gaFramework.modified(function(){
			jQuery('table.fixedHeader').fixedHeader();
		});		
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.fixedHeader.init();
});	
