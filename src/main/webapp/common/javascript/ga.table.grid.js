(function(jQuery) {

	jQuery.grid = {};
	jQuery.fn.grid = function() {
	 // Start the timer.
		//console.time("ga.table.grid:fn");
	 // Format the table
		//console.time("ga.table.grid:formatTable");
		jQuery.grid.formatTable( this );
		//console.timeEnd("ga.table.grid:formatTable");
	 // Iterate through our tables
		//console.time("ga.table.grid:first");
		var once = true;
		jQuery(this).each( function( i ) {
			if (once) {
				once = false;
				//console.timeEnd("ga.table.grid:first");
				//console.time("ga.table.grid:rest");
			}
			var grid = jQuery(this);
			//console.time("ga.table.grid:selectable");
			if( grid.hasClass('selectable') ) {
				jQuery.grid.selectableRows( this );
			}
			//console.timeEnd("ga.table.grid:selectable");
			//console.time("ga.table.grid:sortable");
			if( grid.hasClass('sortable') ) {
				jQuery.grid.sortableRows( this );
			}
			//console.timeEnd("ga.table.grid:sortable");
			//console.time("ga.table.grid:links");
			if( grid.hasClass('links') ) {
				jQuery.grid.linkRows( this );
			}
			//console.timeEnd("ga.table.grid:links");
			//console.time("ga.table.grid:collapsible");
			if( grid.hasClass('collapsible') ) {
				grid.addClass('collapsibleRows');
				grid.addClass('collapsibleColumns');
				console.warn('class .collapsible has been deprecated. Use .collapsibleRows and/or .collapsibleColumns instead');
			}
			if( grid.hasClass('collapsibleRows') ) {
				//console.time("ga.table.grid:collapsibleRows");
				jQuery.grid.collapsibleRows( this );
				//console.timeEnd("ga.table.grid:collapsibleRows");
			}
			if( grid.hasClass('collapsibleColumns') ) {
				//console.time("ga.table.grid:collapsibleColumns");
				jQuery.grid.collapsibleColumns( this );
				//console.timeEnd("ga.table.grid:collapsibleColumns");
			}
			//console.timeEnd("ga.table.grid:collapsible");
		});
		//console.timeEnd("ga.table.grid:rest");

		//console.timeEnd("ga.table.grid:fn");
	};
		
	/* 
	 *
	 */	
		
	jQuery.grid.formatTable = function( table ) {
	 // Iterate through the unprepared tables
		jQuery(table).not('.hasGridFormat').each( function( i ) {
		 // Add a class so we don't reprepare the table again.
			var table = jQuery(this).addClass('hasGridFormat');
			var tbody = table.children('tbody,tfoot');
			var inputs = tbody.find('input[type=text],input[type=password],select,textarea');
			var checkboxes = tbody.find('input[type=checkbox],input[type=radio]');
		 // Add some handle classes
			inputs.closest('td,th').addClass('inputCell');
			var checkboxCells = checkboxes.closest('td,th').addClass('checkboxCell');
		 // Wrap the inputs themselves
			inputs.wrap('<span></span>');
			if (!jQuery(table).hasClass('nohover')){
				var tableId = table.id();
				if (!tableId){
					tableId = gaFramework.identifier( table );
					table.id(tableId);
				}

				// note: jquery delegate will not accept a selector starting with > so we need to reselect the table by id 
				 //	Row hover
  				jQuery(table).delegate('#'+tableId+'>tbody >tr, #'+tableId+'>tfoot> tr','mouseenter',function() {
						jQuery(this).addClass('hover');
					});
  				jQuery(table).delegate('#'+tableId+'>tbody >tr, #'+tableId+'>tfoot> tr','mouseleave',function() {
  						jQuery(this).removeClass('hover');
  					});
				 //	Input cell hover
					jQuery(table).delegate('input[type=text],input[type=password],select,textarea','mouseenter',function() {
						jQuery(this).addClass('hover').closest('th,td').addClass('hover');
					});
					jQuery(table).delegate('input[type=text],input[type=password],select,textarea','mouseleave',function() {
						jQuery(this).removeClass('hover').closest('th,td').removeClass('hover');
					});
				 //	Input cell focus
					jQuery(table).delegate('input[type=text],input[type=password],select,textarea','focus',function() {
						jQuery(this).addClass('focus').closest('th,td').addClass('focus');
					});
				 //	Input cell blur
					jQuery(table).delegate('input[type=text],input[type=password],select,textarea','blur',function() {
						jQuery(this).removeClass('focus').closest('th,td').removeClass('focus');
					});
			}
		});
	};
	
	/* 
	 *
	 */	
		
	jQuery.grid.selectableRows = function( table ) {
	 // Private method for selecting a row
		var selectTableRowForCheckbox = function( checkbox ) {
		 // Variables
			var tr = jQuery(checkbox).parents('tr');
			var inputs = tr.find('input[type=text],input[type=password],select,textarea');
		 // Add the row highlighting	
			tr.removeClass('notselected').addClass('selected');
		};
	 // Private method for deselecting a row
		var deselectTableRowForCheckbox = function( checkbox ) {
		 // Variables
			var tr = jQuery(checkbox).parents('tr');
			var inputs = tr.find('input[type=text],input[type=password],select,textarea');
		 // Remove the row highlighting	
			tr.removeClass('selected').addClass('notselected');
		};
	 // Iterate through the tables not prepared for selectable rows
		jQuery(table).not('.hasSelectableRows').each( function( i ) {
		 // Add a class so we don't reprepare the table again.
			var table = jQuery(this).addClass('hasSelectableRows');
		 // Prepare the selectorCells
			jQuery.grid.prepareSelectorCells( this );
		 // Variables
			var tbody = table.children('tbody,tfoot');
			var tableRows = tbody.children('tr');
			var selectorCells = tableRows.children('.selectorCell');
			var checkboxes = selectorCells.find('input[type=checkbox],input[type=radio]');
		 // Select all the prechecked rows
			var precheckedRows = selectorCells.find('input[type=checkbox]:checked,input[type=radio]:checked');
			jQuery( precheckedRows ).each( function( i ) {
				selectTableRowForCheckbox( this );
			});
		 // Set up the select all menu
			var selectAllCheckbox = table.children('thead').find('> tr > .selector.checkboxCell input');
			if( selectAllCheckbox.length ) {
			 // Wrap the selectAll checkbox to make a menuToggle
				selectAllCheckbox.wrap('<span class="gaGridSelectAllMenuLabel"></span>');
				var menuToggle = selectAllCheckbox.parent('.gaGridSelectAllMenuLabel');
			 // Apply a menu to our menuToggle
				var menuDropdown = menuToggle.gaMenu();
			 // If we click inside the selectAllCheckbox...
				selectAllCheckbox.click( function(e) {
				 // Stop the click from propogating up to the document.
					e.stopPropagation();
				});
			 // Item to select all rows
				var selectAll = jQuery.gaMenu.addListItem( menuDropdown, 'All', function(){
					tbody.children('tr').not('.hiddenRow').find('> .selectorCell input').not(':checked').check();
				});
			 // Item to unselect all rows
				var selectNone = jQuery.gaMenu.addListItem( menuDropdown, 'None', function(){
					tbody.children('tr').not('.hiddenRow').find('> .selectorCell input:checked').uncheck();
				});
			 // Get a list of row groups
				//console.time('ga.table.grid.prepareRowGroups');
				var groups = jQuery.grid.prepareRowGroups( table );
				//console.timeEnd('ga.table.grid.prepareRowGroups');
			 // If the table has groups of rows defined
				if( groups.length > 1 ) {
				 // Add a separator
					var separator = jQuery.gaMenu.addSeparator( menuDropdown );
				 // Loop through our groups
					jQuery.each( groups, function( i, group ) {
					 // Toggle for Selected Rows
						var selectGrouped = jQuery.gaMenu.addListItem( menuDropdown, group.label, function(){
							group.obj.not('.hiddenRow').find('> .selectorCell input').not(':checked').check();
						});
					 // When we hover over the menu item, light up the columns
						selectGrouped.hover( function() {
							group.obj.addClass('highlightRow');
						}, function() {
							group.obj.removeClass('highlightRow');
						});
					});
				}
			}
			 // Make it so if you click the row, toggle the selector checkbox (disable with class=noAutoRowSelect)
			if (!table.hasClass('noAutoRowSelect')){
				 // We do this the hard way so that if we're trying to drag, we don't toggle the checkbox
					var tbodyCells = tbody.children('tr').children('td,th');
					tbodyCells.click( function(e) {
						var target = jQuery(e.target);
						var cell = target.is('td,th')? target:target.closest('td,th');
						 // If we aren't clicking inside the selector cell...
						if( cell.not('.selectorCell').length ) {
						 // If we aren't clicking on inputs or links...
							if( target.not('a,input,textarea,select').length ) {
							 // Toggle the selector checkbox
								var selectorCell = cell.closest('tr').children('.selectorCell');
								if (selectorCell.length){
									selectorCell.find('input[type=checkbox],input[type=radio]').toggleCheck();
								}
							}
						}
					});
			}
		 // When the selector checkbox is toggled, toggle the row selection
			checkboxes.toggleCheck( function(e) {
			 // ON CHECK
				var checkbox = jQuery(this);
			 // Select the table row
				selectTableRowForCheckbox( checkbox );
			 // If the checkbox is a radio box...
				if( checkbox.attr('type') == 'radio' ) {
				 // We find all the unchecked boxes
					var selectedRows = tbody.find('tr.selected');
					var selectorCells = selectedRows.find('.selectorCell');
					var unchecked = selectorCells.find('input[type=checkbox],input[type=radio]').not(':checked');
				 // And deselect them
					deselectTableRowForCheckbox( unchecked );
					if (!table.hasClass('noValidation')){
				       // Validate the table
					    table.trigger('validate');
					}
				}
			}, function(e) {
			 // ON UNCHECK
				var checkbox = jQuery(this);
			 // Deselect the table row
				deselectTableRowForCheckbox( checkbox );
				if (!table.hasClass('noValidation')){
			        // Validate the table
				    table.trigger('validate');
				}
			});
		});
	};
	
	/* 
	 *
	 */	
		
	jQuery.grid.sortableRows = function( table ) {
	 // Start the timer
		//console.time('jQuery.grid.sortableRows');
	 // Iterate through the tables not prepared for sortable rows
		jQuery(table).not('.hasSortableRows').each( function( i ) {
		 // Add a class so we don't reprepare the table again.
			var table = jQuery(this).addClass('hasSortableRows');
		 // Prepare the sequenceCells
			jQuery.grid.prepareSequenceCells( this );
		 // Variables
			var tbody = table.find('tbody,tfoot');
			var sequenceCells = tbody.find('.sequenceCell');
		 // Remove the inputCell class (we don't need it)
			sequenceCells.removeClass('inputCell').unbind('click');
		 // Reorganise the contents of the cell
			sequenceCells.find('span').addClass('gaGridSequenceInput');
			sequenceCells.append('<span class="gaGridSequenceHandle"></span>');
			sequenceCells.append('<span class="gaGridSequenceLabel">1</span>');
		 // Iterate through the tables not prepared for sortable rows
			sequenceCells.each( function( i ) {
			 // Variables
				var sequenceCell = jQuery(this);
			 // Mark the row as draggable
				sequenceCell.closest('tr').addClass('draggable');
			});
		 // Any rows not marked as draggable by now we will explicitly set as nodrag
			jQuery('tr',table).not('.draggable').addClass('nodrag');
		 // Set up a private method for applying the sort order
			var determineRowSorting = function() {
			 // Iterate through the rows
			 // We specifically find the rows here (and don't just use the premade variable) so we get the new sorting order.
				tbody.find('.sequenceCell').each( function( i ) {
				 // Variables
					var sequenceCell = jQuery(this);
				 // Mark the row as draggable
					sequenceCell.find('input').val(i+1);
					sequenceCell.find('.gaGridSequenceLabel').html(i+1);
				});
			};
		 // Then run it.
			determineRowSorting();
		 // And let tableDnd do it's thingy
			table.tableDnD({
				dragHandle: 'sequenceCell',
				onDragClass: 'dragging',
				onDragStart: function( table, row ) {
				 // Create a helper table
					var tempTable = jQuery('<table class="input grid gaGridSortableHelper"></table>').appendTo('body');
				 // Find the row we're dragging
					var row = jQuery(table).find('tr.hover');
				 // If we find a row...
					if( row.length ) {
					 // Put the content of the row into the helper table
						tempTable.append( '<tbody>'+row.html()+'</tbody>' );
					 // Set the cell widths
						var cells = row.find('td,th');
						tempTable.find('td,th').each( function(i) {
							jQuery(this).width( jQuery(cells[i]).width() );
						});
					 // Set the starting CSS for the row
						tempTable.css({
							'width': row.width(),
							'top': row.offset().top,
							'left': row.offset().left
						});
					 // Move the helper table with the mouse
						jQuery(document).mousemove(function(e){
							var pageY = e.pageY;
							tempTable.css({
								'top': pageY - (tempTable.height()/2)
							});
						});
					}
				 // Remove the helper table
					jQuery(table).trigger('gaGridRowDragStart');
				},
				onDrop: function( table, row ){
				 // Remove the helper table
					jQuery('.gaGridSortableHelper').remove();
				 // Remove the helper table
					jQuery(table).trigger('gaGridRowDragStop');
				 // Redetermine the sequence numbers
					determineRowSorting();
				}
			});
		});
	 // Log the setup time
		//console.timeEnd('jQuery.grid.sortableRows');
	};
	
	/* 
	 * links tables operate like a menu.
	 * We expect to find a real hyperlink somewhere in the row and clicking anywhere on the row 
	 * (except any other hyperlink or input field .. though not sure why you'd have an input field)
	 * behaves as if the user clicked on the hyperlink itself
	 * so the tr element itself behaves like an anchor element
	 * 
	 */	
		
	jQuery.grid.linkRows = function( table ) {
		//console.time('jQuery.grid.linkRows');
		// Iterate through the tables not already prepared for link rows
		jQuery(table).not('.hasLinkRows').each(function(i) {
		 // Add a class so we don't reprepare the table again.
			var table = jQuery(this).addClass('hasLinkRows');
			var tbody = table.children('tbody,tfoot');
		 // Loop through each row...
			tbody.children('tr').each(function() {
			 // Variables
			 	var row					=	jQuery(this);
				var firstCell			=	row.children('td:first, th:first');
				var firstLink			=	firstCell.find('a:first');
			 // Only for linked rows...
				if( firstLink.length ) {
					row.addClass('link');
				 // apply hover/selected/notselected classes to row to give hover and click feedback
					row.hover(function() {
						jQuery(this).addClass('hover');
					}, function() {
						jQuery(this).removeClass('hover').removeClass('selected').addClass('notselected');				   
					});
					row.mousedown(function() {
						jQuery(this).addClass('selected').removeClass('notselected');				   
					});
					row.mouseup(function() {
						jQuery(this).removeClass('selected').addClass('notselected');				   
					});
				 // Row click activates link in first column.
					row.click(function(e) {
						var row					=	jQuery(this);
						var firstCell			=	row.children('td:first, th:first');
						var firstLink			=	firstCell.find('a:first');
						var target				=	jQuery( e.target );
						// If the target is not a link, trigger the click.
						if( target.not('a').length ) {
							firstLink.trigger('click');
						}
					});
				};
			});
		});
		 // Log the setup time
		//console.timeEnd('jQuery.grid.linkRows');
	};	

	/* 
	 *
	 */	
		
	jQuery.grid.collapsibleRows = function( table ) {
	 // Iterate through the tables not prepared for collapsible rows
		jQuery(table).not('.hasCollapsibleRows').each(function(i) {
			try {
				 // Add a class so we don't reprepare the table again.
				var table = jQuery(this).addClass('hasCollapsibleRows');
				var tbody = table.children('tbody,tfoot');
			 // Get a list of row groups
				//console.time('ga.table.grid.prepareRowGroups');
				var groups = jQuery.grid.prepareRowGroups( table );
			 // If the table is a selectable table
				if( table.hasClass('selectable') || groups.length > 1 ) {
				 // Get a menu for our visibility toggles
					var collapseMenu = jQuery.tableOptions.addMenu( table, 'Visible Rows' );
				 // Private method for selecting a row
					var hideAndShowRowGroups = function() {
					 // Get the starting scroll position
						var togglePos = Math.floor( jQuery(table).tableOptions().position().top );
						var scrollTop = jQuery(window).scrollTop();
						var scrollOffset = togglePos - scrollTop;
						if( groups.length > 1 ) { // If the table is grouped...
						 // Iterate through our groups...
							jQuery.each( groups, function( i, group ) {
							 // If the group is marked as visible...
								if( jQuery('input#' + group.id + ':checked',collapseMenu).length ) {
								 // Show all rows in this group.
									tbody.children('.'+group.id).removeClass('hiddenRow');
								} else if( jQuery('input#gaGridCollapsibleRowsSelected:checked',collapseMenu).length ) {
								 // else If the group is marked as hidden and selected is marked as visible...
								 // Hide unselected rows in this group.
									tbody.children('.'+group.id+':not(.selected)').addClass('hiddenRow');
								} else {
							 // If the group is marked as hidden and selected is marked as hidden...
							 // Or if the table isn't selectable and the group is marked as hidden...
								 // Hide all rows in this group.
									tbody.children('.'+group.id).addClass('hiddenRow');
								}
							});
						} else { // If the table is selectable... 
							// If selected is marked as visible...
							var selectedRows = tbody.children('tr.selected');
							if( collapseMenu.find('input#gaGridCollapsibleRowsSelected:checked').length ) {
								selectedRows.removeClass('hiddenRow'); // Show selected rows.
							} else { // If selected is marked as hidden...
								selectedRows.addClass('hiddenRow'); // Hide selected rows.
							}
							var unselectedRows = tbody.children('tr:not(.selected)');
						 // If unselected is marked as visible...
							if( collapseMenu.find('input#gaGridCollapsibleRowsUnselected:checked').length ) {
								unselectedRows.removeClass('hiddenRow'); // Show unselected rows.
							} else { // If unselected is marked as hidden...
								unselectedRows.addClass('hiddenRow'); // Hide unselected rows.
							}
						}
					 // Update the scroll position
						var newTogglePos = Math.floor( jQuery(table).tableOptions().position().top );
						var newScrollTop = newTogglePos - scrollOffset;
						jQuery(window).scrollTop( newScrollTop );
					};
				 // Function to add a visibility toggle to the menu
					var addCollapseMenuItem = function( id, label, obj ) {
					 // Get the menu item
					 	var menuItem = jQuery.gaMenu.addToggleItem( collapseMenu, label, id, hideAndShowRowGroups, hideAndShowRowGroups );
					 // When we hover over the menu item, light up the columns
					 	if( obj ) {
							menuItem.hover( function() {
								obj.addClass('highlightRow');
							}, function() {
								obj.removeClass('highlightRow');
							});
						}
					 // Toggle for Selected Rows
						return menuItem;
					};
				 // If the table is a selectable table
					if( table.hasClass('selectable') ) {
					 // Variables
						var tbody = table.children('tbody,tfoot');
						var tbodyRows = tbody.children('tr');
					 // Toggle for Selected Rows
						var toggleSelected = addCollapseMenuItem( 'gaGridCollapsibleRowsSelected', 'Selected' );
					 // If there are selected rows and they're all hidden...
						var selectedRows = tbody.children('tr.selected');
						if( selectedRows.length && ! selectedRows.not('.hiddenRow').length ) {
							toggleSelected.find('input').removeAttr('checked');
						}
					 // If the table has groups of rows defined
						if( groups.length <= 1 ) {
						 // Toggle for Unselected Rows
							var toggleUnselected = addCollapseMenuItem( 'gaGridCollapsibleRowsUnselected', 'Unselected' );
						 // If there are unselected rows and they're all hidden...
							var unselectedRows = tbody.children('tr:not(.selected)');
							if( unselectedRows.length && ! unselectedRows.not('.hiddenRow').length ) {
								toggleUnselected.find('input').removeAttr('checked');
							}
						}
					}
				 // If the table has groups of rows defined
					if( groups.length > 1 ) {
					 // If the table is a selectable table
						if( table.hasClass('selectable') ) {
						 // Add a separator
							var separator = jQuery.gaMenu.addSeparator( collapseMenu );
						}
					 // Loop through our groups
						jQuery.each( groups, function( i, group ) {
						 // Toggle for the group's rows
							var toggleGrouped = addCollapseMenuItem( group.id, group.label, group.obj );
						 // If there are rows in the group and they're all hidden...
							if( group.obj.length && ! group.obj.not('.hiddenRow').length ) {
								toggleGrouped.find('input').removeAttr('checked');
							}
						});
					}
			 }				
			} catch(e) {
				console.error('exception caught in table.grid.collapsibleRows [%o],%o',i,e);
			}
		});
	 // Log the setup time
		//console.timeEnd('jQuery.grid.collapsibleRows');
	};

	/* 
	 *
	 */	
		
	jQuery.grid.collapsibleColumns = function( table ) {
	 // Start the timer
		//console.time('jQuery.grid.collapsibleColumns');
	 // Iterate through the tables not prepared for collapsible rows
		jQuery(table).not('.hasCollapsibleColumns').each(function(i) {
		 // Add a class so we don't reprepare the table again.
			var table = jQuery(this).addClass('hasCollapsibleColumns');
			var tbody = table.find('tbody,tfoot');
		 // Get a list of row groups
			//console.time('ga.table.grid.prepareColumnGroups');
			var groups = jQuery.grid.prepareColumnGroups( table );
			//console.timeEnd('ga.table.grid.prepareColumnGroups');
		 // If the table is a selectable table
			if( groups.length > 1 ) {
			 // Get a menu for our visibility toggles
				var collapseMenu = jQuery.tableOptions.addMenu( table, 'Visible Columns' );
			 // Private method for selecting a row
				var hideAndShowColumnGroups = function() {
				 // Get the starting scroll position
					var togglePos = Math.floor( jQuery(table).tableOptions().position().top );
					var scrollTop = jQuery(window).scrollTop();
					var scrollOffset = togglePos - scrollTop;					
				 // Iterate through our groups...
					jQuery.each( groups, function( i, group ) {
					 //
						if( jQuery('input#' + group.id + ':checked',collapseMenu).length ) {
							group.obj.removeClass('hiddenColumn');
						}
						else {
							group.obj.addClass('hiddenColumn');
						}
					});
				 // Update the scroll position
					var newTogglePos = Math.floor( jQuery(table).tableOptions().position().top );
					var newScrollTop = newTogglePos - scrollOffset;
					jQuery(window).scrollTop( newScrollTop );
					
				};
			 // Function to add a visibility toggle to the menu
				var addCollapseMenuItem = function( id, label, obj ) {
				 // Get the menu item
				 	var menuItem = jQuery.gaMenu.addToggleItem( collapseMenu, label, id, hideAndShowColumnGroups, hideAndShowColumnGroups );
				 // When we hover over the menu item, light up the columns
				 	if( obj ) {
						menuItem.hover( function() {
							obj.addClass('highlightColumnOn').removeClass('highlightColumnOff');
						}, function() {
							obj.removeClass('highlightColumnOn').addClass('highlightColumnOff');
						});
					}
				 // Toggle for Selected Rows
					return menuItem;
				};
			 // Loop through our groups
				jQuery.each( groups, function( i, group ) {
				 // Toggle for the group's columns
					var toggleGrouped = addCollapseMenuItem( group.id, group.label, group.obj );
				 // If there are cells in the group and they're all hidden...
					if( group.obj.length && ! group.obj.not(':hidden').length ) {
						toggleGrouped.find('input').removeAttr('checked');
					}
				});
			}
		});
	 // Log the setup time
		//console.timeEnd('jQuery.grid.collapsibleColumns');
	};
	
	/* 
	 *
	 */	
		
	jQuery.grid.prepareSequenceCells = function( table ) {
		jQuery(table).each( function( i ) {
		 // Variables
			var table = jQuery(this);
			var tbody = table.find('tbody,tfoot');
			var sequenceCells = jQuery(this).find('.sequenceCell');
		 // If there are no sequenceCells already...											  
			if( ! sequenceCells.length ) {
			 // Find the column number for the sequenceCells, we default to the first column if we can't find it
				var sequence = table.find('thead .sequence');
			 // If there is a sequence column marked...
				if( sequence.length ) {
				 // Get the column number from the th class
					var columnId = sequence.attr('class').match(/c[0-9]+/)[0];
				 // Find the checkboxes in the matched column
					columnCells = tbody.find('.' + columnId );
				 // If they are checkboxCells...
					if( columnCells.is('.inputCell') ) {
					 // Add the sequenceCells class
						sequenceCells = columnCells.addClass('sequenceCell');
					}
				}
			}
		});
	};
	
	/* 
	 *
	 */	
		
	jQuery.grid.prepareSelectorCells = function( table ) {
		jQuery(table).each( function( i ) {
		 // Variables
			var table = jQuery(this);
			var tbody = table.children('tbody,tfoot');
			var selectorCells = table.find('> tr > td.selectorCell');
		 // If there are no selectorCells already...											  
			if( ! selectorCells.length ) {
			 // Find the column number for the selector cells, we default to the first column if we can't find it
				var selector = table.find('> thead > tr > th.selector, > thead > tr > td.selector');
			 // Get the column number from the th class
				var columnId = ( selector.length ) ? selector.attr('class').match(/c[0-9]+/)[0] : 'c1';
			 // Find the checkboxes in the matched column
				columnCells = tbody.find('> tr > td.checkboxCell.' + columnId );
				 // Add the selectorCell class
		   	    columnCells.addClass('selectorCell');
			}
		});
	};
	
	/* 
	 *
	 */	
		
	jQuery.grid.prepareRowGroups = function( table ) {
	 // 
		var table = jQuery(table).first();
		var tempGroups = {};
		var groups = jQuery.makeArray();
		var tableId = table.id();
		if (!tableId){
			tableId = gaFramework.identifier( table );
			table.id(tableId);
		}
		var ungrouped = { label: 'Ungrouped', id: 'rowungrouped-' + tableId };
		var tbodyRows = table.find('tbody tr,tfoot tr').not('table table tr');
	 // Iterate through the rows in the body
		tbodyRows.each( function( i ) {
		 // Add a class so we don't reprepare the table again.
			var row = jQuery(this);
			var data = row.metadata();
		 // If a group is set...
			if( data.group ) {
			 // If the group is not already set up... 
				if( ! tempGroups[data.group] ) {
				 // Create a unique identifier for the group
					var groupId = 'rowgroup' + jQuery.md5(data.group) + tableId;
				 // Apply the class to this row
					row.addClass( groupId );
				 // And add the details to the groups object
					tempGroups[data.group] = {
						label: data.group,
						id: groupId
					};
				}
			 // If the group is already set up... 
				else {
				 // Get the unique group class
					var groupId = tempGroups[data.group].id;
				 // Apply the class to this row
					row.addClass( groupId );
				}
			}
		 // If no group is set...
			else {
			 // Apply the class to this row
				row.addClass( ungrouped.id );
			}
		});
	 // And put this group into the output array
		tempGroups['Ungrouped'] = ungrouped;
	 // Loop through our groups
		jQuery.each( tempGroups, function( i, group ) {
		 // Select the rows in that group
			group.obj = jQuery('.'+group.id,table).not('table table *');
		 // And put this group into the output array
		 	if( group.obj.length ) {
				groups.push( group );
			}
		});
	 // Return the list of groups
		return groups;
	};
		
	/* 
	 *
	 */	
		
	jQuery.grid.prepareColumnGroups = function( table ) {
	 // 
		var table = jQuery(table).first();
		var tableId = table.id();
		if (!tableId){
			tableId = gaFramework.identifier( table );
			table.id(tableId);
		}
		var tempGroups = {};
		var groups = jQuery.makeArray();
		var ungrouped = { label: 'Ungrouped', id: 'colungrouped-' + tableId };
		var theadCells = table.find('thead th').not('table table th');
	 // Iterate through the rows in the body
		theadCells.each( function( i ) {
		 // Add a class so we don't reprepare the table again.
			var cell = jQuery(this);
			var data = cell.metadata();
		 // If a group is set...
			if( data.group ) {
			 // If the group is not already set up... 
				if( ! tempGroups[data.group] ) {
				 // Create a unique class for the group
					var groupId = 'colgroup' + jQuery.md5( data.group) + tableId;
				 // Apply the class to this row
					cell.addClass( groupId );
				 // Get the column number from the th class
					var column = cell.attr('class').match(/c[0-9]+/)[0];
				 // Add the group class to all the cells in the matched column
					table.find('.' + column ).not('table table *').addClass( groupId );
				 // And add the details to the groups object
					tempGroups[data.group] = {
						label: data.group,
						id: groupId
					};
				}
			 // If the group is already set up... 
				else {
				 // Get the unique group class
					var groupId = tempGroups[data.group].id;
				 // Apply the class to this row
					cell.addClass( groupId );
				 // Get the column number from the th class
					var column = cell.attr('class').match(/c[0-9]+/)[0];
				 // Add the group class to all the cells in the matched column
					table.find('.' + column ).not('table table *').addClass( groupId );
				}
			}
		 // If no group is set...
			else {
			 // Apply the class to this row
				cell.addClass( ungrouped.id );
			 // Get the column number from the th class
				var column = cell.attr('class').match(/c[0-9]+/)[0];
			 // Add the group class to all the cells in the matched column
				table.find('.' + column ).not('table table *').addClass( ungrouped.id );
			}
		});
	 // And put this group into the output array
		tempGroups['Ungrouped'] = ungrouped;
	 // Loop through our groups
		jQuery.each( tempGroups, function( i, group ) {
		 // Select the rows in that group
			group.obj = jQuery('.'+group.id,table).not('table table *');
		 // And put this group into the output array
		 	if( group.obj.length ) {
				groups.push( group );
			}
		});
	 // Return the list of groups
		return groups;
	};

	/* 
	 *
	 */	
		
	jQuery.grid.init = function() {
		console.time('jQuery.grid.init');
		jQuery('table.grid').grid();
		gaFramework.modified(function(){
			jQuery('table.grid').grid();
		});
		console.timeEnd('jQuery.grid.init');		
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.grid.init();
});	
