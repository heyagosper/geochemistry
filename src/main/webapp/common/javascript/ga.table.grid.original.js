/* Tables 
/* -------------------------------------------------------------------------------------------------------------------------- */
	(function(jQuery) {
		jQuery.fn.extend({
						 
		 // Functionality for Grid Inputs.
			prepareGridTables: function() {
				jQuery(this).not('.hasGridFormat').map( function( i ) {
					console.time('grid.js.prepareGridTables.setup');
				 // Variables
					var table					=	jQuery( this ).addClass('hasGridFormat');
					var tableId					=	'table'+( i+1 );
					var rows					=	new Array();
					var form_input				=	table.parents('.form_input');
					var bodyrows				=	table.find('tbody tr,tfoot tr');
					var inputs					=	table.find('input[type=text],input[type=password],select');
					var checkboxes				=	table.find('input[type=checkbox],input[type=radio]');
					var cells					=	table.find('td,th');
					var cellCount				=	0;
					// as wrapping the input elements is very slow it can be turned off on a per-table basis by adding the class skipGridInputSpans 
					var skipGridInputSpans      = table.hasClass('skipGridInputSpans');
				 // Add some handle classes
					inputs.parents('td,th').addClass('inputCell');
					checkboxes.parents('td,th').addClass('checkboxCell');
				 // Place a unique id on the table.
					table.attr({ id: tableId });
				 // Iterate through the rows in the table.
					table.find('tr').map( function( r ) {
					 // Variables
						row = jQuery(this);
						cellCount				=	( cellCount > row.find('th,td').length ) ? cellCount : row.find('th,td').length;
					 // Place an id on the row
						row.attr({ id: tableId + ':' + (r+1) });
					 // Add an array for each row to the rows variable.
						rows[r]					=	new Array();
					});
				 // Iterate through the rows in the table again
					table.find('tr').map( function( r ) {
					 // We set up an array with a boolean for each cell, which we use to account for whether that cell has been used already.
						for( var i=0; i < cellCount; i++ ) {
							rows[r][i]			=	false;
						}
					});
					console.timeEnd('grid.js.prepareGridTables.setup');
				 // Iterate through the rows in the table again
					console.time('grid.js.prepareGridTables.addSpans');
					table.find('tr').map( function( r ) {
					 // Variables
						var row 				=   jQuery(this);
						var c					=	0;
						var rowCells			=	row.find('th,td');
					 //	Loop through the boolean array we created
						jQuery.each( rows[r], function( k, value ) {
						 // And if the cell is false
							if( value == false && rowCells[c] ) {
							 // Variables
								var cell		=	jQuery( rowCells[c] );
								var colSpan		=	( cell.attr('colspan') ) ? Math.floor( cell.attr('colspan') ) : 1;
								var rowSpan		=	( cell.attr('rowspan') ) ? Math.floor( cell.attr('rowspan') ) : 1;
							 // Put a unique id onto each cell.
								cell
									//.html( tableId )
									.attr({ id: tableId + ':' + (r+1) + ':' + (k+1) });
								if (!skipGridInputSpans) { 
									cell.find('input,select').map( function(ci) { // wrap only the input elements
										jQuery(this).wrap('<span><span></span></span>');
									});
								   //cell.wrapInner( '<span><span></span></span>' );
								}
							 // Then we iterate to make sure the classes cover columns and rows the cell spans into
								for( var ri=0; ri<rowSpan; ri++ ) {
									for( var ci=0; ci<colSpan; ci++ ) {
									 // Mark our boolean array as true for each merge area
										rows[(r+ri)][(k+ci)] = true;
									 // And add a class for reference
										cell
											//.append( ' r' + (r+ri+1) + ' c' + (k+ci+1) )
											.addClass( 'r' + (r+ri+1) )
											.addClass( 'c' + (k+ci+1) );
									}
								}
							 // 
								c++;
							}
						});
					});
					console.timeEnd('grid.js.prepareGridTables.addSpans');
				 // Trigger the selectable rows functionality if the column is a selector
				 // this should be something like grid.find('thead th.selector, thead td.selector')
					if( table.find('.checkboxCell.c1 input').length ) {
						console.time('grid.js.prepareGridTables.selectable');
						table.prepareSelectableTable();
						console.timeEnd('grid.js.prepareGridTables.selectable');
					}
				 // Mark cells as required.
					console.time('grid.js.prepareGridTables.required');
					table.find('thead th, thead td').map( function() {
					 // Variables
						var columnHead			=	jQuery(this);
						var columnClass			=	columnHead.attr('class');
					 // Put a star in the header cell if it's required
						if( columnHead.hasClass('required') || columnHead.hasClass('requiredCell') )
							columnHead.find('span span').append('<span class="star" title="This is a required column.">*</span>');
					 // And then go through and mark each cell in that column
						if( columnClass.match(/c[0-9]+/g) ) {
							jQuery.each( columnClass.match(/c[0-9]+/g), function( key, value ){
							 // If the column is required
								if( columnHead.hasClass('required') ) {
								 // Variables
									var reqCells	=	table.find('th.' + value + ', td.' + value);
								 // Mark the td/th as a required cell, this doesn't trigger the required validation.
									reqCells.addClass('requiredCell');
								 // If rows aren't selectable, then we mark the inputs as required
									if( ! table.find('.selectorCell').length ) {
										 reqCells .find('input,select').addClass('required');
									}
								}
							});
						}
					});
					console.timeEnd('grid.js.prepareGridTables.required');
				 // Trigger a resize to move dropdowns and error tips
					console.time('grid.js.prepareGridTables.resize');
					jQuery(window).resize();
					console.timeEnd('grid.js.prepareGridTables.resize');
				 //	Input cell hover
					console.time('grid.js.prepareGridTables.hover');
					inputs.hover(function(){
						inputs.removeClass('hover').parents('th,td').removeClass('hover');
						jQuery(this).addClass('hover').parents('th,td').addClass('hover');
					},
					function(){
						inputs.removeClass('hover').parents('th,td').removeClass('hover');
					});
				 //	Input cell hover
					bodyrows.hover(function(){
						bodyrows.removeClass('hover');
						jQuery(this).addClass('hover');
					},
					function(){
						bodyrows.removeClass('hover');
					});
				 //	Input cell focus
					inputs.focus(function(){
						inputs.removeClass('focus').parents('th,td,tr').removeClass('focus');
						jQuery(this).addClass('focus').parents('th,td,tr').addClass('focus');
					});
				 //	Input cell blur
					inputs.blur(function(){
						inputs.removeClass('focus').parents('th,td,tr').removeClass('focus');
					});
				 // Remove focus and blur if we exit the grid
					jQuery(document).hover( function( e ) {
						var target				=	jQuery( e.target );
						if( ! target.parents('table.grid').length ) {
							inputs.removeClass('hover').parents('th,td,tr').removeClass('hover');
						}
					});
					jQuery(document).focus( function( e ) {
						var target				=	jQuery( e.target );
						if( ! target.parents('table.grid').length ) {
							inputs.removeClass('focus').parents('th,td,tr').removeClass('focus');
						}
					});
					console.timeEnd('grid.js.prepareGridTables.hover');
				});
			 //
				return this;
			},

		 // Functionality for Link Tables.
			prepareLinksTables: function() {
				jQuery(this).not('.hasLinksFormat').map( function( i ) {
				 // Variables
				 	var table					=	jQuery(this).addClass('hasLinksFormat');
				 // Loop through each row...
					table.find('tbody tr').each(function() {
					 // Variables
					 	var row					=	jQuery(this);
						var firstCell			=	row.find('td:first, th:first');
						var firstLink			=	firstCell.find('a:first');
					 // Only for linked rows...
						if( firstLink.length ) {
						 // Row hover class.
							row.hover(function() {
								jQuery(this).addClass('hover');
							}, function() {
								jQuery(this).removeClass('hover').removeClass('selected').addClass('notselected');				   
							});
						 // Selected class used as an "active" state to give click feedback.
							row.mousedown(function() {
								jQuery(this).addClass('selected').removeClass('notselected');				   
							});
						 // Remove selected class on mouseup.
							row.mouseup(function() {
								jQuery(this).removeClass('selected').addClass('notselected');				   
							});
						}
					});
				 // Row click activates link in first column.
					table.find('tbody tr').click(function(e) {
					 // Variables
					 	var row					=	jQuery(this);
						var firstCell			=	row.find('td:first, th:first');
						var firstLink			=	firstCell.find('a:first');
						var target				=	jQuery( e.target );
					 // If the target is not a link, trigger the click.
						if( target.not('a').length ) {
							firstLink.trigger('click');
						}
					});
				});
			},
			
			// Functionality for DragDropSequence Tables.
			/*
			 * DragDropSequence tables use a drag drop paradigm to manage sequencing of table rows
			 * This means table rows have a cell containing an input field with some sort of sequence number 
			 * and we want to drag/drop rows up and down within the table to order them and have the 
			 * order reflected in the sequence number input fields
			 *   
			 * To setup this behaviour for a table
			 *   (0. have an input field within a table column for the sequence number
			 *       and have the rows already in the order indicated by the sequence numbers) 
			 *   1. add the class 'dragDropSequence' to the table for which we want this behaviour 
			 *   2. add the class 'sequenceCell' to the cell element which contains the sequence input field (presumably one per row)
			 *   
			 *           
			 *  @DependsOn: 
			 *   - class definitions in style.css for 'tDnD_whileDrag'
			 */
			prepareDragDropSequenceTables : function() {
				jQuery(this).not('.hasDragDropSequenceFormat').map(function(i) {
					var table = jQuery(this);
					table.addClass('hasDragDropSequenceFormat'); // prevent repeat styling
					// add dragDrop sequencing if we have columns marked with class sequenceCell
					var sequenceCells =  table.find('tbody td.sequenceCell');
					if (sequenceCells.length) { // if we have any
						sequenceCells.map(function() {
							// hide the input field and display the sequence value itself for each cell
							var column = jQuery(this);
							var inputField= column.find('input');
							inputField.css( {display : 'none'})
								.after('<span class="seqValue">'+inputField.val()+'</span>')
								.change(function() {
									var input = jQuery(this);
									var v = input.val();
									var cell = input.parents('.sequenceCell');
									cell.find('.seqValue').html(v);
								});
							// mark the row as draggable
							column.closest('tr').addClass('draggable');
							// and the column gets styled as a dragHandle
							column.addClass('tDnD_dragHandle');
							/* remove class 'inputCell' we conflict with grid-table code (and dragging will not work)
							 *  due to the css definitions on table.grid tbody .inputCell > span
							 *  and table.grid tbody .inputCell > span > span
							 */
							column.removeClass('inputCell');
						});
					}
					// any rows not marked as draggable by now we will explicitly set as nodrag
					jQuery("tr", table).each(function() {
						var row = jQuery(this);
						if (!row.hasClass("draggable")) {
							row.addClass('nodrag');
						}
					});
					// and let tableDnd do it's thingy
					table.tableDnD({
						onDragClass: 'tDnD_dragHandle',
						onDragStart: function(table, row) {
							table.addClass('dragging');
					    },
					    onDrop: function(table, row){
							table.removeClass('dragging');
					    	var rows=table.tBodies[0].rows;
					    	console.log('dropped row ');
					        for (var i=0; i<rows.length; i++) {
					        	jQuery(rows[i]).find('.sequenceCell input').val(i+1).change();
					        }
					    }
					});
				});
			},
			
			// Functionality for CollapsibleColumn Tables.
			/*
			 * Collapsible column tables have columns that can have their visibility toggled on or off.
			 * To setup this behaviour for a column
			 *   1. add the class 'collapsible' to the th and corresponding td elements of the column 
			 *   2. add the class 'showColumns' to the table element to have the table display with the columns initially visible
			 *   3. or add the class 'hideColumns' to the table element to have the table display with the columns initially hidden
			 *   
			 * Optional extras (as in do you want fries with that?)
			 * 
			 * Multiple columns:
			 *    just add the 'collapsible' class to as many columns as you like
			 *    
			 * Multiple column groups:
			 *    if you have two groups of columns that you want to hide/show independantly you can add a numeric suffix [1-9] to the classes 
			 *    to identify the two groups.
			 *     eg add 'collapsible1' to one group of columns and 'collapsible2' to the other
			 *     and 'showColumns1 hideColumns2' (or a variation therof) to the table
			 *    There are currently up to 10 such groups available (ie 'collapsible' and 'collapsible1' through 'collapsible9'
			 *    Behaviour if you try adding multiple 'collapsiblex' classes to the same column is undefined.
			 *    If you use multiple column groups you will need to specify appropriate labels for the hide/show toggles (so read on)      
			 * 
			 * Setting the label for the show/hide toggle:
			 *   By default the label will be "Show Detail Columns" or "Hide Detail Columns" as appropriate.
			 *   This can be overridden by a slightly cludgy mechanism of adding a class of the form 'columnGroup:My_Column_Group_Label' to the table.
			 *   If you have multiple collapible groups you will need a coresponding class to specify the label for each
			 *    eg for columns tagged with class='columnGroup9' we would have a table class='columnGroup9:Blah'
			 *   The label parameter:
			 *      Standard behaviour
			 *        A single string which we will prefix with "Show " or "Hide " as appropriate
			 *        eg  'columnGroup:Blah' would result in toggle labels
			 *           'Show Blah' and 'Hide Blah'
			 *        To avoid accidental matchups against classes we don't want embedded spaces so use underscore characters as placeholders 
			 *        for spaces.
			 *        eg  'columnGroup:Detail_Columns' would result in toggle labels
			 *           'Show Detail Columns' and 'Hide Detail Columns' (the default)
			 *           
			 *      Explicit show/hide labels
			 *        If you specify a label in the format ':label_for_show:label_for_hide'
			 *        the code will use the two labels (without any prefixing) as the show/hide labels.
			 *        eg  'columnGroup:Show_All:Show_Important_Stuff_Only' would result in toggle labels
			 *           'Show All' and 'Show Important Stuff Only'
			 *           
			 *           
			 *  @DependsOn: 
			 *   - class definitions in style.css for 'collapsible'
			 *  @Todo 
			 *   - we need the style meister to work out appropriate styling for hide/showeable columns 
			 *   - fix IE compatability issue: when the starting state is hidden IE7 and IE8 will refuse to display the columns 
			 *           (yet it works if they start in a shown state???) 
			 *           As a workaround we show the columns and trigger a delayed hide event
			 *   - check whether there are any issues using ':' within a classname. It's convenient but I'm not sure it's strictly valid.
			 */
			prepareCollapsibleColumnTables : function() {
				jQuery(this).not('.hasCollapsibleColumnFormat').map(function(i) {
					var table = jQuery(this);
					table.addClass('hasCollapsibleColumnFormat'); // prevent repeat styling
					/* check the table for a class columnGroup+groupSuffix[label]
					 * If we don't a columnGroup we assume there is only one group and the label is 'Detail Columns'
					 * otherwise create a toggle for each group 
					 */
					var tableClass			=	table.attr('class');					
					var columnGroups  = 0;
					jQuery.each(tableClass.match(/(columnGroup[1-9]?\:)([^ \-]+(\:)?)([^ \:]+(\:)?)?/gi), function( key, value ) {
						 var tokens = value.split(/columnGroup|\:/);
						 if (tokens.length > 2) {
						    table.prepareRowColToggle('Columns',tokens[1],tokens[2],tokens[3]);
						    columnGroups++;
						 } else {
							 console.log('unable to extract columnGroup label from '+ value +', will ignore');
						 }
					});			 
					if (columnGroups == 0) {
					  table.prepareRowColToggle('Columns','','Detail Columns');
					}
				});
			},
			prepareRowColToggle : function(rowCol, groupSuffix, groupShowLabel, groupHideLabel) {
				// create toggle link for user to change row/column visibility
				var table = jQuery(this);
				if (groupHideLabel == null || groupHideLabel.length == 0) {
					groupHideLabel = 'Hide '+groupShowLabel;
					groupShowLabel = 'Show '+groupShowLabel;
				}
				groupHideLabel = groupHideLabel.replace(/_/g,' '); // switch placeholder underscores to spaces (note replace(string,string) only matches once)
				groupShowLabel = groupShowLabel.replace(/_/g,' ');
				var show = 'show'+rowCol+groupSuffix;
				var hide = 'hide'+rowCol+groupSuffix;
				var toggleGroup = 'toggle'+rowCol+groupSuffix;
				var toggleLink = jQuery('<a></a>')
				.addClass('toggle')
				.addClass(toggleGroup)
				.html(table.hasClass(show) ? groupHideLabel : groupShowLabel)
				.bind('click',function(){
					var togglePos = Math.floor(toggleLink.position().top);
					var scrollTop = jQuery(window).scrollTop();
					var scrollOffset = togglePos - scrollTop;
					if (table.hasClass(show)) {
						table.removeClass(show);
						table.addClass(hide);
					} else {
						table.removeClass(hide);
						table.addClass(show);
					}					
					toggleLink.html(table.hasClass(show) ? groupHideLabel : groupShowLabel);
					var newTogglePos = Math.floor(toggleLink.position().top);
					var newTop=newTogglePos - scrollOffset;
					console.log('adjusting toggle'+', togglePos='+togglePos+', scrollTop='+scrollTop+', scrollOffset='+scrollOffset+', newTogglePos='+newTogglePos+', newTop='+newTop+', height='+jQuery(window).height());
					jQuery(window).scrollTop(newTop); // reposition the page to put the toggle back in place
					//jQuery(window).resize(); // Trigger a resize to force IE to rethink the display
				});
				toggleLink.insertAfter(table).wrap('<span class="display"/>');
				// Cludge For IE compatability. Open with columns showing and then hide otherwise we can't show them ????
				if ('Columns' == rowCol) {
					if (table.hasClass(hide)) {
						table.removeClass(hide);
						table.addClass(show);
						window.setTimeout(function() {
							table.removeClass(show);
							table.addClass(hide);
						}, 1);
					}
				}
			},
			
			
			// Functionality for CollapsibleRow Tables.
			/*
			 * Collapsible Row tables have rows that can have their visibility toggled on or off.
			 * To setup this behaviour for a row
			 *   1. add the class 'collapsible' to the tr element 
			 *   2. add the class 'showRows' to the table element to have the table display with the rows initially visible
			 *   3. or add the class 'hideRows' to the table element to have the table display with the rows initially hidden
			 *   
			 * Optional extras (as in do you want fries with that?)
			 * 
			 * Multiple rows:
			 *    just add the 'collapsible' class to as many rows as you like
			 *    
			 * Multiple row groups:
			 *    if you have two groups of rows that you want to hide/show independantly you can add a numeric suffix [1-9] to the classes 
			 *    to identify the two groups.
			 *     eg add 'collapsible1' to one group of rows and 'collapsible2' to the other
			 *     and 'showRows1 hideRows2' (or a variation therof) to the table
			 *    There are currently up to 10 such groups available (ie 'collapsible' and 'collapsible1' through 'collapsible9'
			 *    Behaviour if you try adding multiple 'collapsiblex' classes to the same row is undefined.
			 *    If you use multiple row groups you will need to specify appropriate labels for the hide/show toggles (so read on)      
			 * 
			 * Setting the label for the show/hide toggle:
			 *   By default the label will be "Show Detail Rows" or "Hide Detail Rows" as appropriate.
			 *   This can be overridden by a slightly cludgy mechanism of adding a class of the form 'rowGroup:My_Row_Group_Label' to the table.
			 *   If you have multiple collapible groups you will need a coresponding class to specify the label for each
			 *    eg for rows tagged with class='rowGroup9' we would have a table class='rowGroup9:Blah'
			 *   The label parameter:
			 *      Standard behaviour
			 *        A single string which we will prefix with "Show " or "Hide " as appropriate
			 *        eg  'rowGroup:Blah' would result in toggle labels
			 *           'Show Blah' and 'Hide Blah'
			 *        To avoid accidental matchups against classes we don't want embedded spaces so use underscore characters as placeholders 
			 *        for spaces.
			 *        eg  'rowGroup:Detail_Rows' would result in toggle labels
			 *           'Show Detail Rows' and 'Hide Detail Rows' (the default)
			 *           
			 *      Explicit show/hide labels
			 *        If you specify a label in the format ':label_for_show:label_for_hide'
			 *        the code will use the two labels (without any prefixing) as the show/hide labels.
			 *        eg  'rowGroup:Show_All:Show_Important_Stuff_Only' would result in toggle labels
			 *           'Show All' and 'Show Important Stuff Only'
			 *           
			 *           
			 *  @DependsOn: 
			 *   - class definitions in style.css for 'collapsible'
			 *  @Todo 
			 *   - we need the style meister to work out appropriate styling for hide/showeable rows 
			 *   - fix IE compatability issue: when the starting state is hidden IE7 and IE8 will refuse to display the rows 
			 *           (yet it works if they start in a shown state???) 
			 *           As a workaround we show the rows and trigger a delayed hide event
			 *   - check whether there are any issues using ':' within a classname. It's convenient but I'm not sure it's strictly valid.
			 *   - consider using the jquery metadata plugin to pick up configuration requirements
			 */
			prepareCollapsibleRowTables : function() {
				jQuery(this).not('.hasCollapsibleRowFormat').map(function(i) {
					var table = jQuery(this);
					table.addClass('hasCollapsibleRowFormat'); // prevent repeat styling
					/* check the table for a class rowGroup+groupSuffix[label]
					 * If we don't have a rowGroup we assume there is only one group and the label is 'Detail Rows'
					 * otherwise create a toggle for each group 
					 */
					var tableClass			=	table.attr('class');					
					var rowGroups  = 0;
					jQuery.each(tableClass.match(/(rowGroup[1-9]?\:)([^ \-]+(\:)?)([^ \:]+(\:)?)?/gi), function( key, value ) {
						 var tokens = value.split(/rowGroup|\:/);
						 if (tokens.length > 2) {
						    table.prepareRowColToggle('Rows',tokens[1],tokens[2],tokens[3]);
						    rowGroups++;
						 } else {
							 console.log('unable to extract rowGroup label from '+ value +', will ignore');
						 }
					});			 
					if (rowGroups == 0) {
					  table.prepareRowColToggle('Rows','','Detail Rows');
					}
				});
			},
			
		 // Functionality for Link Tables.
			prepareSelectableTable: function() {
			 // Variables
				//console.time('grid.js.prepareGridTables.selectable.setup');
				var grid					=	jQuery(this);
				var formInput				=	grid.parents('.form_input');
				var bodyrows				=	grid.find('tbody tr,tfoot tr');
				var inputs					=	grid.find('input[type=text],input[type=password],select');
				var checkboxes				=	bodyrows.find('.checkboxCell input');
				var selectors				=	grid.find('th.checkboxCell.c1,td.checkboxCell.c1').addClass('selectorCell');
				var selectableRows			=	selectors.parents('tr').addClass('selectableRow');
					selectableRows			=	grid.find('tbody tr.selectableRow, tfoot tr.selectableRow');
				var selectorCellInputs 		= 	selectors.find('input');
				//console.timeEnd('grid.js.prepareGridTables.selectable.setup');
 			    if( selectors.length && bodyrows.length > 1 ) {
					grid.after('<span class="display"><a class="toggleUnselected toggle">Hide Unselected Rows</a></span>');
				}
			 // Toggle All Checkboxes/Radio Boxes
				//console.time('grid.js.prepareGridTables.selectable.toggle');
				var checkAll = grid.find('thead .checkboxCell input');
				if (checkAll.length) {
					checkAll.attr({ title: 'Check/Uncheck All'})
					.each( function() {
					 // Variables
						var headerCheckbox		=	jQuery(this);
						var parentCell		=	headerCheckbox.parents('th,td');
						var columnClass		=	parentCell.attr('class');
						var columnId 		= 	columnClass.match(/c[0-9]+/g);
						headerCheckbox.click( function() {
							// Go through and mark each cell in that column
							var selCells	=	selectableRows.find('th.' + columnId + ', td.' + columnId);
							if( parentCell.find(':checked').length ) { // select all rows
								selCells.find('input[type=checkbox]').check();
							} else {// Deselect all rows
								selCells.find('input[type=checkbox]').uncheck();
							}
							// Show unselected rows
							grid.showUnselected();
						});
					// Update the toggle all based on whether all the checkboxes in that column are checked or not.
						bodyrows.find('.checkboxCell.'+columnId+' input').bind('check uncheck', function(){
							//console.time('grid.js.prepareGridTables.selectable.toggle.handler');
							var checkbox = jQuery(this);
							if (checkbox.is(':checked')) { // this one has been checked
								if (!headerCheckbox.is(':checked')) {// if header already checked then this cannot change its state
									var uncheckedFound = false;
									selectableRows.find('th.' + columnId + ',td.' + columnId).map(function(){
										if( !jQuery(this).find(':checked').length ) {
											uncheckedFound = true;
											return false; // stop looking
										}
									});
									if (uncheckedFound) { // at least one unchecked
										headerCheckbox.removeAttr('checked');
									} else { // all checked?
										headerCheckbox.attr('checked','checked');
									}
								}
							} else {//this one has been unchecked
								headerCheckbox.removeAttr('checked'); // at least one is unchecked (this one) 
							}
							//console.timeEnd('grid.js.prepareGridTables.selectable.toggle.handler');
						});
					});
				}
				//console.time('grid.js.prepareGridTables.selectable.toggle');
			 // Update the row to be highlighted as selected.
				//console.time('grid.js.prepareGridTables.selectable.highlight');
				selectorCellInputs.bind('check', function(){
					//console.time('grid.js.prepareGridTables.selectable.highlight.check');
				 // Variables
					var input 				= jQuery(this);
					var parent				=	input.parent();
					var parentRow			=	input.parents('tr');
				 // Radio boxes should clear highlighting first.
					if( input.attr('type') == 'radio' ) {
						grid.find('tr.selected').removeClass('selected').addClass('notselected');
						grid.find('.requiredCell input, .requiredCell select').removeClass('required');
						grid.find('.inputCell input').validate();
					}
				 // If this row's selector is checked
					if( ! parentRow.parents('thead').length ) {
						parentRow.addClass('selected').removeClass('notselected');
						parentRow.find('.requiredCell input, .requiredCell select').addClass('required');
						parentRow.find('.inputCell input').validate();
					}
					//console.timeEnd('grid.js.prepareGridTables.selectable.highlight.check');
				});
			 // Update the row to be highlighted as unselected.
				selectorCellInputs.bind('uncheck', function(){
					//console.time('grid.js.prepareGridTables.selectable.highlight.uncheck');
				 // Variables
					var input				= jQuery(this);
					var parent				=	input.parent();
					var parentRow			=	input.parents('tr');
				 // If this row's selector is NOT checked
					if( ! parentRow.parents('thead').length ) {
						parentRow.removeClass('selected').addClass('notselected');
						parentRow.find('.requiredCell input, .requiredCell select').removeClass('required');
						parentRow.find('.sequenceCell input').val('').change();
						parentRow.find('.inputCell input').validate();
					}
					//console.timeEnd('grid.js.prepareGridTables.selectable.highlight.uncheck');
				});
			 // manage auto resequenced rows as their selection status changes
				if (selectorCellInputs.length && selectableRows.find('.sequenceCell input').length) {
					selectorCellInputs.bind('check uncheck', function(){
						//console.time('grid.js.prepareGridTables.selectable.resequence');
						// Variables
						var input 				=   jQuery(this);
						var parent				=	input.parent();
						var parentRow			=	input.parents('tr');
						// If a sequence column exists, we update the sequence.
						if( parentRow.find('.sequenceCell input').length ) {
							// Variables
							var selectedRows		=	selectableRows.find('.selectorCell input:checked').parents('tr');
							var a				=	1;
							//var selectableRows	=	grid.find('tr.selectableRow');
							var array			=	new Array();
							// Loop through the selected rows
							jQuery.each( selectedRows, function() {
								// Get the sequence number from the cell
								var row			= jQuery(this);
								var seqCell		=	row.find('.sequenceCell input');
								var seqNo		=	( seqCell.val() != '' ) ? Math.floor( seqCell.val() ) : ( selectableRows.length );
								array[seqNo] 	=	row;
							});
							// Loop through the sequence array
							jQuery.each( array, function( i, val ) {
								if( val ) {
									jQuery(this).find('.sequenceCell input').val( a++ ).change();
								}
							});
						}
					});
				}

				//console.timeEnd('grid.js.prepareGridTables.selectable.highlight');
			 // Toggle the selection of rows by clicking anywhere in the row
				//console.time('grid.js.prepareGridTables.selectable.rowselect');
				//console.time('grid.js.prepareGridTables.selectable.rowselect.tbody');
				grid.find('tbody, tfoot').click(function( e ){
				 // Variables										 
					var target				=	jQuery( e.target );
					var parentRow			=	target.parents('tr');
					var selectorCell		=	parentRow.find('.selectorCell');
					var selectorInput		=	parentRow.find('.selectorCell input');
				 // We only change the state if we're clicking outside of the selector itself
					if( jQuery.param( target ) == jQuery.param( selectorInput ) ) {
					} else if(target.parents('.inputCell').length) { // and not in an input cell
					} else if(target.attr('type') == 'checkbox') { // and not a checkbox
					} else if(target.attr('type') == 'radio') { // and not a radio button
					} else {
							// Change state
							if( target.parents('span.error').length ) {
								target.validate();
							} else if( selectorCell.find('input:checked').length )
								selectorInput.uncheck();
							else
								selectorInput.check();
					}
				});
				//console.timeEnd('grid.js.prepareGridTables.selectable.rowselect.tbody');
			 // Toggle unselected rows.
				//console.time('grid.js.prepareGridTables.selectable.rowselect.unselected');
				formInput.find('a.toggleUnselected').click(function(){
					if( jQuery(this).hasClass('rowsHidden') ) {
						grid.showUnselected();
					}
					else if( formInput.find('tbody tr.selected, tfoot tr.selected').length ) {
						grid.hideUnselected();
					}
					else {
						alert( 'Select some rows first!' );
					}
				});
				grid.not('.hideUnselectedRows').addClass('showUnselectedRows'); // start with unselected visible unless explicitly overridden
				//console.timeEnd('grid.js.prepareGridTables.selectable.rowselect.unselected');
				 // adjust visibility of toggleUnselected on uncheck
				selectorCellInputs.bind('uncheck', function(){
					var input				= jQuery(this);
					var parent				=	input.parent();
					var parentRow			=	input.parents('tr');
					if (! parentRow.parents('thead').length) { // ignore unselection of thead rows
						var toggleLink				=	formInput.find('a.toggleUnselected');
						if (toggleLink.length) {
							if (toggleLink.is(':visible')) { // if visible
								if(!formInput.find('tbody tr.selected, tfoot tr.selected').length ) { // but no selected rows 
									if (toggleLink.hasClass('rowsHidden')) { // if we have hidden rows
										toggleLink.show(); // then show the toggle so the user can get the table back
									} else { // else no hidden rows
										toggleLink.hide(); // then hide the toggle
									}
								}
							}
						}
					}
				});				    
			 // adjust visibility of toggleUnselected on check
				selectorCellInputs.bind('check', function(){
					var input				= jQuery(this);
					var parent				=	input.parent();
					var parentRow			=	input.parents('tr');
				    if (! parentRow.parents('thead').length) { // ignore selection of thead rows
				    	var toggleLink	=	formInput.find('a.toggleUnselected');
				    	if (toggleLink.length) { // if we have a toggle
						   toggleLink.show(); // then it should be visible as we have at least one selected row now  
				    	}
					}
				});				    

				// Map the checked boxes when the window loads, and set the row as "selected"
				//console.time('grid.js.prepareGridTables.selectable.rowselect.checked');
				var checkedSelectorCells = grid.find('.selectorCell input:checked');
				if (checkedSelectorCells.length) {
					checkedSelectorCells.first().check();
					checkedSelectorCells.each(function() {
						jQuery(this).parents('tr').addClass('selected').removeClass('notselected');
					});
				} else {
					formInput.find('a.toggleUnselected').hide(); // hide toggle until they select something
				}
				grid.find('tr.selectableRow').not('.selected').addClass('notselected');
				//console.timeEnd('grid.js.prepareGridTables.selectable.rowselect.checked');
				//console.timeEnd('grid.js.prepareGridTables.selectable.rowselect');
			},

		 // Functionality for Link Tables.
			showUnselected: function() {
			 // Variables
				var formInput				=	jQuery(this).parents('.form_input');
				var toggleLink				=	formInput.find('a.toggleUnselected');
				var togglePos = Math.floor(toggleLink.position().top);
				var scrollTop = jQuery(window).scrollTop();
				var scrollOffset = togglePos - scrollTop;
			 // 
				toggleLink.removeClass('rowsHidden');
				toggleLink.html('Hide Unselected Rows');
				formInput.find('table').removeClass('hideUnselectedRows').addClass('showUnselectedRows');
				//formInput.find('tbody tr, tfoot tr').show();
				var newTogglePos = Math.floor(toggleLink.position().top);
				var newTop=newTogglePos - scrollOffset;
				console.log('adjusting toggle'+', togglePos='+togglePos+', scrollTop='+scrollTop+', scrollOffset='+scrollOffset+', newTogglePos='+newTogglePos+', newTop='+newTop+', height='+jQuery(window).height());
				jQuery(window).scrollTop(newTop); // reposition the page to put the toggle back in place
			},

		 // Functionality for Link Tables.
			hideUnselected: function() {
			 // Variables
				var formInput				=	jQuery(this).parents('.form_input');
				var toggleLink				=	formInput.find('a.toggleUnselected');
				var togglePos = Math.floor(toggleLink.position().top);
				var scrollTop = jQuery(window).scrollTop();
				var scrollOffset = togglePos - scrollTop;
			 // 
				toggleLink.addClass('rowsHidden');
				toggleLink.html('Show Unselected Rows');
				formInput.find('table').addClass('hideUnselectedRows').removeClass('showUnselectedRows');
				//formInput.find('tbody tr, tfoot tr').hide();
				//formInput.find('tbody tr.selected, tfoot tr.selected').show();
				var newTogglePos = Math.floor(toggleLink.position().top);
				var newTop=newTogglePos - scrollOffset;
				console.log('adjusting toggle'+', togglePos='+togglePos+', scrollTop='+scrollTop+', scrollOffset='+scrollOffset+', newTogglePos='+newTogglePos+', newTop='+newTop+', height='+jQuery(window).height());
				jQuery(window).scrollTop(newTop); // reposition the page to put the toggle back in place
			},

		 // Functionality for Link Tables.
			grid: {
				
				initComplete: false,
				
				init: function() { /*
					//console.time('grid.js:modified');
				 //
					jQuery('table.grid').prepareGridTables();
					jQuery('table.links').prepareLinksTables();
					jQuery('table.dragDropSequence').prepareDragDropSequenceTables();
					jQuery('table[class*="showColumns"],table[class*="hideColumns"]').prepareCollapsibleColumnTables();
					jQuery('table[class*="showRows"],table[class*="hideRows"]').prepareCollapsibleRowTables();
				 //
				 	if( ! jQuery.fn.grid.initComplete ) {
						jQuery.fn.grid.initComplete = true;
						gaFramework.modified(function(){
							jQuery.fn.grid.init();
						});
					}
					//console.timeEnd('grid.js:modified');
					
					*/
				}

			}

		});
	})(jQuery);
	
/* Apply Table Functionality
/* -------------------------------------------------------------------------------------------------------------------------- */	
 /* / Define functions.
	(function(jQuery) {
			  
 // Date Selector
	jQuery.fn.gridSearch = function() {
		var parent					=	jQuery(this);
		var id						=	jQuery(this).find('label').attr('id');
		jQuery(this).find('label').attr('id')
		jQuery(this).find('label').after('<span class="input group search"><label for="'+id+':gridSearch">Search:</label><span class="input"><input id="'+id+':gridSearch" type="text"></span><span class="button"><button>Show All</button></span></span>');
		jQuery(this).find('.input.search input').doSearch();
		jQuery(this).find('.input.search input').keyup(function(){
			parent.find('tbody tr input').removeAttr('checked');
			parent.find('tbody tr').removeClass('selected');
			jQuery( this ).doSearch();
		});
		jQuery(this).find('.input.search button').click(function() {
			parent.find('tbody tr').show();
			return false;
		});
	}
	
 // Date Selector
	jQuery.fn.doSearch = function() {
		var parent					=	jQuery(this).parents('.form_input.gridSearch');
		var searchValue				=	jQuery(this).val();
		parent.find('tbody tr').map(function() {
			if( ! jQuery(this).find('td').attr('colspan') ) {
				jQuery(this).hide();
			}
		});
		if( searchValue ) {
			parent.find('tbody tr').map(function() {
				var regexp			=	new RegExp( searchValue );
				if( regexp.test( jQuery(this).text() ) ) {
					jQuery(this).show();
				}
			});
		}
	}

	})(jQuery);
	
	jQuery(document).ready(function(){
		jQuery('.form_input.gridSearch').gridSearch();
	});

 // */