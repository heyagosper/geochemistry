(function(jQuery) {

	/* 
	 *
	 */	
		
	jQuery.tableIdentify = {};
	
	/* 
	 *
	 */	
		
	jQuery.fn.tableIdentify = function() {
		console.time("ga.table.identify:tableIdentify");
	 // Iterate through the unprepared tables
		jQuery(this).not('.hasGridIds').not('.noGridIds').each( function( i ) {
		 // Add a class so we don't reprepare the table again.
			var table = jQuery(this).addClass('hasGridIds');
		 // Make sure the table has a unique id
			var tableId = table.id();
			if( ! tableId ) {
				tableId = 'table' + gaFramework.identifier( table );
				table.id( tableId );
			}
		 // Get all the rows in this table
			var tr = table.children().children('tr');
		 // Variables
			var maxCellsInRow = 0;
		 // Iterate through cells in the first row of the table counting them.
			tr.first().children('td,th').each( function() {
				maxCellsInRow += new Number( jQuery(this).attr('colspan') );
			});
		 // We set up a matrix with a boolean for each cell, which we use to account
		 // for whether that cell has been used already.
			var theMatrix = new Array();
			for( var r=0; r < tr.length; r++ ) {
				var cellMatrix = new Array();
				for( var c=0; c < maxCellsInRow; c++ ) {
					cellMatrix[c] = false;
				}
				theMatrix[r] = cellMatrix;
			}
		 // Iterate through the rows in the table again
			tr.each( function( r ) {
				var row = jQuery(this);
				//console.info("processing row "+r+" of table "+tableId);
				if (r >= theMatrix.length){
					console.error('attempting to process a row %o we have not setup a matrix for in %o',r,table);
					alert('attempting to process row '+r +' of '+ theMatrix.length+' in table '+tableId);
					return true;
				} else {
				   var td = row.children('td,th');
				   if( td.length < maxCellsInRow ) { // If there are less cells in this row than the maximum...
					   var c = 0;
					   jQuery.each( theMatrix[r], function( k, value ) { //	Loop through the tdMatrix we created
						   if( value == false && td[c] ) { // And if the cell is false
							   var cell		=	jQuery( td[c] );
							   var colSpan		=	( tempColSpan = cell.attr('colspan') ) ? Math.floor( tempColSpan ) : 1;
							   var rowSpan		=	( tempRowSpan = cell.attr('rowspan') ) ? Math.floor( tempRowSpan ) : 1;
							   if (cell.attr('id') == null){ 
								   cell.id( tableId + ':' + (r+1) + ':' + (c+1) ); // Put a unique id onto each cell.
							   }
							   // Then we iterate to make sure the classes cover columns and rows the cell spans into
							   for( var ri=0; ri<rowSpan; ri++ ) {
								   for( var ci=0; ci<colSpan; ci++ ) {
									   theMatrix[(r+ri)][(k+ci)] = true; // Mark our boolean array as true for each merge area 
									   cell.addClass( 'r' + (r+ri+1) ).addClass( 'c' + (k+ci+1) ); //  And add a class for reference
								   }
							   }
							   c++;
						   }
					   });
				   } else {  // else there are the correct number of cells in this row... 
					   td.each( function( c ) {// Iterate through the cells in the row
						   var cell = jQuery(this);
						   if (cell.attr('id') == null){
							   cell.id( tableId + ':' + (r+1) + ':' + (c+1) ); // Put a unique id onto each cell.
						   }
						   cell.addClass( 'r' + (r+1) ).addClass( 'c' + (c+1) ); // And a class on each cell to reference it's position
					   });
				   }
				}
			});
			theMatrix = null;
		});
		console.timeEnd("ga.table.identify:tableIdentify");
	};
		
	/* 
	 *
	 */	
		
	jQuery.tableIdentify.init = function() {
		jQuery('table.grid').tableIdentify();
		gaFramework.modified(function(){
			jQuery('table.grid').tableIdentify();
		});
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.tableIdentify.init();
});	
