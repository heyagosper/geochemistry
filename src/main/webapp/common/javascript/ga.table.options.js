(function(jQuery) {

	/* 
	 *
	 */	
		
	jQuery.tableOptions = {};
	
	/* 
	 *
	 */	
		
	jQuery.fn.tableOptions = function() {
	 // Limit the tables to one
		var table = jQuery(this).first();
		var metadata = table.metadata();
	 // Find an existing optionsList
		var optionsListId = 'options-'+table.id();
		var optionsList = jQuery('ul#'+optionsListId);
	 // Create an optionsList if one doesn't exist already
		if( ! jQuery('ul#'+optionsListId).length ) {
		 // Create the optionsList
			optionsList = jQuery('<ul></ul>').id(optionsListId).addClass('gaGridOptionsList');
		 // If we've set the location in a metadata class
			if( metadata.optionsDiv ) {
				jQuery('div#'+metadata.optionsDiv).append( optionsList );
			}
		 // If we're in a form_input row, add it at the end...
			else if( table.parents('.form_input').length ) {
				optionsList.addClass('display');
				table.parents('.form_input').append( optionsList );
			}
		 // Otherwise insert directly after the table
			else {
				table.wrap('<div></div>');
				optionsList.addClass('display');
				table.parent().append( optionsList );
			}
		}
	 // Return the optionsListItem
		return optionsList;
	};
		
	/* 
	 *
	 */	
		
	jQuery.tableOptions.addItem = function( table, label, href ) {
	 // Limit the tables to one
		var table = jQuery(table).first();
	 // Get the optionsList
		var optionsList = table.tableOptions();
	 // Default the href to a hash.
		href = ( href ) ? href : '#';			
	 // Append the toggle into the optionsList
		var optionsListItem = jQuery('<li></li>').addClass('gaGridOptionsListItem').appendTo(optionsList);
		var listItemLink = jQuery('<a href="#">'+label+'</a>').addClass('gaGridOptionsToggle').appendTo(optionsListItem);
	 // Return the optionsListItem
		return listItemLink;
	};

	/* 
	 *
	 */	
		
	jQuery.tableOptions.addMenu = function( table, label ) {
	 // Limit the tables to one
		var table = jQuery(table).first();
	 // Add the list item
		var optionsListItem = jQuery.tableOptions.addItem( table, label );
	 // Append the toggle into the optionsList
		var optionsMenu = optionsListItem.gaMenu();
	 // Return the optionsListItem
		return optionsMenu;
	};

})(jQuery);