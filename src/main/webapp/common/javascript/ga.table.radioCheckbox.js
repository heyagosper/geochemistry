(function(jQuery) {

	jQuery.radioCheckbox = {};
	/**********************************************************************************
	* This function is to make a set of checkboxes behave like a radio button group.
    * The need for this is because JSF is not able to render radio button groups where the 
    * individual radio buttons are separated by other structural elements 
    * (eg in separate cells or rows of a table)
    *
    * usage: render each checkbox with class='radioCheckboxGroup' to indicate we want 
    *        the radiobutton behaviour
    *        Add a metadata element named 'group' to associate all of the chechboxes that
    *        should behave as a single radiobutotn group.
    *        identify The radio button group is 
    * eg   class='radioCheckboxGroup {group:group1"}
    *      to indicate this checkbox shoudl behave like a radio button 
    *      grouped with all other checkboxes in 'group1'
	***********************************************************************************/		
	jQuery.fn.radioCheckbox = function() {	
		jQuery(this).click(function(){
				var checkbox = jQuery(this).addClass("radioCheckboxSelected");
				var metadata = checkbox.metadata();
				var group = metadata.group;
				if (typeof metadata.minChecked == 'undefined') {
					metadata.minChecked = 1;
				}
				jQuery(".radioCheckboxGroup:checked").each(function(){
					var data = jQuery(this).metadata();
					var eachGroup = data.group;
					if ( eachGroup == group && (jQuery(this).hasClass('radioCheckboxSelected')<=0)) {
						// uncheck checkbox (UI operation)
						jQuery(this).attr('value',"off");	
						jQuery(this).removeAttr('checked');						
					}
				});
								
				if (metadata.minChecked == 0){
					// the last mohican is allowed to be unchecked.....
					if (jQuery(this).attr('value') == 'off'){
						jQuery(this).removeAttr('checked');						
					}
				} else { // radiogroup behaviour means this one gets checked even if the action was to uncheck it
					checkbox.attr('value','on');
					checkbox.attr('checked',true); 
				}
				jQuery(this).removeClass("radioCheckboxSelected");
		});	
	};
	
	
	jQuery.radioCheckbox.init = function() {
		console.log("start table.radioCheckbox init func ...");
		jQuery('.radioCheckboxGroup').radioCheckbox();
		gaFramework.modified(function(){
			jQuery('.radioCheckboxGroup').radioCheckbox();
		});
	};
})(jQuery);
jQuery(document).ready(function(){
	jQuery.radioCheckbox.init();
});	
