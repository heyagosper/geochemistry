(function(jQuery) {
	
	/* 
	 *
	 */	
		
	jQuery.rowAlterColors = {};
	
	/* 
	 *
	 */	
		
	jQuery.fn.rowAlterColors = function() {
	 // If there are inputs to be messed with
		jQuery.each( jQuery(this).not('.hasRowAlterColors').not('.noRowAlterColors'), function(i) {
		 // Variables
			var table = jQuery(this).addClass('hasRowAlterColors');
		 // Apply the even/odd classes now...
			jQuery.rowAlterColors.applyColorsTo( table );
		 // And apply when we drop the row during a sort...
		 	jQuery(table).bind('gaGridRowDragStop',function(e) {
				jQuery.rowAlterColors.applyColorsTo(jQuery(this));
			});
		});
	};
	
	/* 
	 *
	 */	
		
	jQuery.rowAlterColors.applyColorsTo = function( table ) {
	 // Variables
		var rows = table.find('> tbody > tr,> tfoot > tr');
	 // Iterate through the rows in the table.
		rows.removeClass('odd').removeClass('even').each(function(i) {
		 // Variables
			var row = jQuery(this);
		 // If the row is odd...
			if( i%2 == 1 ) {
				row.addClass('odd');
			}
		 // If the row is even...
			else {
				row.addClass('even');
			}
		});
	};
	
	/* 
	 *
	 */	
	
	jQuery.rowAlterColors.init = function() {
		jQuery('table.grid, table.rowAlterColors').rowAlterColors();
		gaFramework.modified(function(){
			jQuery('table.grid, table.rowAlterColors').rowAlterColors();
		});
	};
		
})(jQuery);
jQuery(document).ready(function(){
	jQuery.rowAlterColors.init();
});	
