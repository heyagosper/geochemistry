/* gaFramework Utilities
/* -------------------------------------------------------------------------------------------------------------------------- */ 

if (!gaFramework) {
	var gaFramework = {};
}
if (!gaFramework.misc) {
	gaFramework.misc = {};
}


 /* Loading Throbber
  *
  */
  
 // Set up the loading throbber
	gaFramework.throbber = {
	 // Variables
		count: 0,
	 // Show Throbber
		show: function() {
		 // If the throbber isn't already being shown
			if( gaFramework.throbber.count <= 0 && ! jQuery('div.gaLoading').length ) {
			 // Create and show the throbber
				jQuery('<div></div>')
					.html('<div class="gaLoading-pad">Loading...</div>')
					.addClass('gaLoading')
					.appendTo('body')
					.fadeIn('fast');
				// If the page is scrolled adjust the position of the loading icon 
				var div = jQuery('div.gaLoading-pad');
				if (jQuery(document).scrollTop() > 0) {
					div.css("top", parseInt(div.css("top")) + jQuery(document).scrollTop());
				}
			}
		 // Increment the counter
			gaFramework.throbber.count++;
		},
	 // Hide Throbber
		hide: function() {
		 // Deincrement the counter
			gaFramework.throbber.count--;
		 // If the counter falls below zero
			if( gaFramework.throbber.count <= 0 ) {
			 // Hide and remove the throbber
				jQuery('div.gaLoading').fadeOut('fast',function(){
					jQuery(this).remove();
				});
			 // Reset the counter
				gaFramework.throbber.count = 0;
			}
		}
	};
	
 // Automatically show and hide our throbber for AJAX calls
	jQuery(document).ready(function(){
	 // When they start							
		jQuery(document).bind('ajaxStart', function() {
			gaFramework.throbber.show();
		});
	 // When they end
		jQuery(document).bind('ajaxComplete modified', function() {
			gaFramework.throbber.hide();
		});
	});
	
 /* Autofocus
  *
  */

	jQuery(document).ready(function(){
		if( ! jQuery.browser.webkit ) {
			setTimeout( "jQuery('input[autofocus],select[autofocus],textarea[autofocus]').last().focus();", 100 );
		}
	});

 /* Scroll page to error messages
  *
  */
	gaFramework.showMessages = function() {
	    if (jQuery('#messagesInsertionPoint ul li').length) {// if we have any
		// messages
	    	jQuery(document).scrollTop(0); // scroll to the top so the messages are visible
	    }
	};
	
	gaFramework.modified(gaFramework.showMessages);

 // Global handlers for non-jQuery AJAX calls
	gaFramework.ajax = {
	 // Run functions when a non-jQuery AJAX call is initiated
		onRequest: function() {
		 // Trigger event
			jQuery(document).trigger('ajaxStart');
		},
	 // Reapply functionality when a non-jQuery AJAX call is completed
		onResponse: function() {
		 // Trigger events
			jQuery(document).trigger('ajaxComplete');
			gaFramework.modified();
		},
	 // Collection of ajax calls (used when we want to abort calls)
	 	calls: {}
	};
	
 /*
  * trigger an event using garden variety DOM event handling
  * for use when we could have observers from differing javascript packages  
  */
	gaFramework.triggerDomEvent = function(element, eventName) {
		//console.log('triggering %t on %o',eventName,element);
		if (document.createEvent)  {
			var evt = document.createEvent('HTMLEvents');
			evt.initEvent(eventName, false, true);
			return element.dispatchEvent(evt);
		}

		if (element.fireEvent) {
			return element.fireEvent('on' + eventName);
		}
	};

 /*
  *
  */

 // Deserialize a query string
	gaFramework.deserialize = function( q ){
		if( q.match('&') )
			var x						=	q.replace(/&amp;/g, '&').replace(/\+/g, ' ').split('&'), i, name, t;
		else
			var x						=	new Array( q );
		for( q={}, i=0; i<x.length; i++ ) {
			t							=	x[i].split('=', 2);
			name						=	unescape(t[0]);
			if( ! q[name] )
				q[name]					=	[];
			if (t.length > 1)
				q[name]					=	unescape(t[1]);
			else
				q[name]					=	true;
		}
		return q;
	};

 /*
  *
  */

	gaFramework.switchCSS = function( styleName ) {
		jQuery('link[rel*=style][title]').each(function(i){
			this.disabled = true;
			if (this.getAttribute('title') == styleName) {
				this.disabled = false;
			}
		});
	};

 /*
  *
  */
	gaFramework.identifierCount = 0;
	gaFramework.identifierPrefix = "gaFrameworkId";
	gaFramework.identifier = function(elem) {
		var id = gaFramework.identifierPrefix+ (++gaFramework.identifierCount);
		while (jQuery('#'+id).length){
			id = gaFramework.identifierPrefix+ (++gaFramework.identifierCount); // loop until find an unused id
		}
		return id;
	};
	
 /*
 * This is to escape dom identifiers for use in JQuery finds as jQuery will choke on any embedded colon or decimal point characters 
 */
	gaFramework.idSelector = function( id ) {
		return (id == null) ? null : '#' + id.replace(/(:|\.)/g,'\\$1');;
	};

 /*
  *
  */

	gaFramework.logHandlers = function( elem ) {
		// Log ALL handlers for ALL events on elem:
		jQuery.each(jQuery(elem).data('events'), function(i, event){
			var element = jQuery(this);
		    jQuery.each(event, function(i, handler){
		        console.log('handler',handler );
		    });
		});
	};
	
	gaFramework.misc.checkboxInit = function() {
	 // We use a live event binding to bind to ALL future checkboxes too. This means we don't have to rebind on modified.
		jQuery('input[type=checkbox],input[type=radio]').live('change', function(){
			if( jQuery(this).is(':checked') ) {
				jQuery(this).trigger('check');
			}
			else {
				jQuery(this).trigger('uncheck');
			}
		});
	};

	
/* Extend jQuery
/* -------------------------------------------------------------------------------------------------------------------------- */ 
(function(jQuery) {
		 
	/* Custom event for when the document is modified
	 *
	 */	
		
	jQuery.fn.modified = function( fn ) {
		if( typeof fn == 'function' )
			jQuery(this).bind('modified',fn);
		else if( ! fn )
			jQuery(this).trigger('modified');
		return this;
	};

	/* Set up the checkbox namespace.
	 * 
	 */
	
	jQuery.checkbox = {};
		
	/* Custom event for checking checkboxes/radio buttons
	 *
	 */	
		
	jQuery.fn.check = function( fn ) {
	 // Variables
		var checkbox = jQuery(this);
	 // For binding...
		if( typeof fn == 'function' ) {
			checkbox.bind('check', fn);
		}
	 // For triggering...
		else if( ! fn ) {
			checkbox.attr('checked',true);
			checkbox.trigger('check').trigger('change');
		}
	 // Output for chaining
		return this;
	};

	/* Custom event for unchecking checkboxes/radio buttons
	 *
	 */	
		
	jQuery.fn.uncheck = function( fn ) {
	 // Variables
		var checkbox = jQuery(this);
	 // For binding...
		if( typeof fn == 'function' ) {
			checkbox.bind('uncheck', fn);
		}
	 // For triggering...
		else if( ! fn ) {
			checkbox.attr('checked',false);
			checkbox.trigger('uncheck').trigger('change');
		}
	 // Output for chaining
		return this;
	};

	/* Custom event for unchecking checkboxes/radio buttons
	 *
	 */	
		
	jQuery.fn.toggleCheck = function( fnCheck, fnUncheck ) {
	 // Variables
		var checkbox = jQuery(this);
	 // For binding...
		if( fnCheck || fnUncheck ) {
			checkbox.check( fnCheck );
			checkbox.uncheck( fnUncheck );
		}
	 // For triggering...
		else {
		 // Get the checkbox type...
			var checkboxType = checkbox.attr('type');
		 // If the checkbox is checked...
			if( checkboxType == 'checkbox' && checkbox.checked ) {
				checkbox.uncheck();
			} else if( ! checkbox.checked ) {
				checkbox.check();
			}
		}
	 // Output for chaining
		return this;
	};

	/* ID
	 * Get or set the id attribute of a DOM element.
	 */
	
	jQuery.fn.id = function( value ) {
	 // We need something to get the id from first.
		if( jQuery(this).length ) {
		 // If a value is passed, we set the id
			if( value ) {
				value = value.replace(/\\:/g,':').replace(/\\./g,'.');
				this[0].id = value;
				return jQuery(this);
			}
		 // Otherwise we get it from the element
			else {
				var id = this[0].id;
				return (id == '') ? null : id.replace(/(:|\.)/g,'\\$1');
			}
		}
	 // Return the object by default for chaining
		return jQuery(this);
	};
	
	/* Label
	 * 
	 */
	
	jQuery.fn.label = function() {
	 // Variables
		var inputField					=	jQuery(this).first();
		var inputLabel					=	false;
	 // Get the label element
	 // If an actual label exists...
		var inputFieldId			=	inputField.id();
		if( inputFieldId ) {
		 // Get the label
			inputLabel					=	jQuery('label[for=' + inputFieldId + ']');
		}
	 // Otherwise, in a grid we use the column header...
		if( ! inputLabel || ! inputLabel.length && inputField.parents('table.grid').length ) {
		 // Variables
			var inputGrid				=	inputField.parents('table');
			var inputCell				=	inputField.parents('td,th');
		 // If the inputCell has an id...
			if( inputCell.id() ) {
			 // Get the inputCell id
				inputCellId				=	inputCell.id().toString().replace(/\\:/g,':').replace(/\\./g,'.').split(/:/);
			 // Get the topmost header
				try {
					inputLabel			=	inputGrid.find('thead .c' + inputCellId[2] + ':last');
				} catch(e) {
					console.error('unable to get inputLabel for %o',inputCell,e);
				}
			}
		}
		// try the inputfield.title
        if( ! inputLabel || ! inputLabel.length ) {
            if (inputField.attr('title')){
               inputLabel.text = inputField.attr('title');
            } else {
  			   inputLabel					=	inputField.parents('.form_input').children("label,.label").first();
  	         // Get the text (use the title if it exists)
               inputLabel.text                    =   ( inputLabel.attr('title') ) ? inputLabel.attr('title') : inputLabel.text();
		    }
        }else {
	     // Get the text (use the title if it exists)
		     inputLabel.text					=	( inputLabel.attr('title') ) ? inputLabel.attr('title') : inputLabel.text();
        }
	 // A little reformating to make the text more pleasant.
		inputLabel.text					=	jQuery.trim( inputLabel.text.replace(/[\s]+/mg,' ') );
	 // Remove the asterisk (if it exists)
		if( jQuery('span.star',inputLabel).length )
			inputLabel.text				=	inputLabel.text.substr( 0, inputLabel.text.length - 1 );
	 // Trim the spaces
		inputLabel.text					=	jQuery.trim( inputLabel.text );
	 // Remove the colon (if it exists)
		if( inputLabel.text.substr( inputLabel.text.length - 1 ) == ':' )
			inputLabel.text				=	inputLabel.text.substr( 0, inputLabel.text.length - 1 );
	 // Trim the spaces
		inputLabel.text					=	jQuery.trim( inputLabel.text );
	 // Store the label
		return inputLabel;				
	};

	/* Input Value Updates
	 *
	 */	
	
	jQuery.fn.valUpdate = function() {
	 // When the link is clicked...
		jQuery(this).bind('click',function() {
		 // Get the query array
			href 						=	jQuery(this).attr('href');
			if( href.match(/\?/) )
				query					=	gaFramework.deserialize( href.split('\?')[1] );
		 // Loop though and update inputs
			jQuery.each( query, function( key, value ) {
				jQuery('[name="'+key+'"]').val( value ).change();
			});
		 // Stop the link from bubbling up
			return false;
		});
	 // 
		return this;
	};

	/* Check that user has saved form data before moving on.
	 *
	 */	
		
	jQuery.fn.ifNotSaved = function() {
	
	};
		
	/* Prepare a form.notSaved to show a confirmation when input values are changed.
	 *
	 */	
	
	jQuery.fn.notSaved = function() {
	 // Iterate through the unprepared forms
		jQuery(this).not('.hasNotSaved').map( function() {
		 // Bind it to any changed inputs	
			jQuery('input,textarea,select',this).bind('textchange change',function() {
				jQuery.fn.notSaved.setNotSaved( jQuery(this).parents('form') );
			});
		 // Bind it to any changed forms	
			jQuery(this).bind('change',function() {
				jQuery.fn.notSaved.setNotSaved( jQuery(this) );
			});
		});
	 // Return this for chaining purposes	
		return this;
	};
	
	/* Manually set a form to show a confirmation.
	 *
	 */	
	
	jQuery.fn.notSaved.setNotSaved = function( object ) {
	 // Iterate through the provided forms
		jQuery(this).map( function() {
		 // Set the unload message	
			window.onbeforeunload = function() {
				return "If you leave now, any unsaved changes will be lost!";
			};
		 // Turn off the unload message when the form is submitted.	
			jQuery(this).submit( function() {
				window.onbeforeunload	=	function(){};
				jQuery('input,textarea,select',this).unbind('textchange change');
				jQuery(this).unbind('change');
			});
		});
	 // Return this for chaining purposes	
		return this;
	};
	
	/* 
	 *
	 */
		
	gaFramework.misc.initComplete = false;
	
	gaFramework.misc.init = function( object ) {
	 // Prepare tab groups	
	    var tabgroups = jQuery('.ga-tabs-group');
	    if (tabgroups.length) { // if we have any
	       tabgroups.each(function() {// for each group
		      jQuery(this).tabs();
	          // Select the default tab for this group
 		      var selectedDiv = jQuery(this).find('.ga-tabs-content.selected');
		      if (selectedDiv.length){
			     jQuery(this).tabs('select','#'+selectedDiv.first().id());
		      }
		   });
		}
	 // Apply Accordion Panels
		jQuery('form.notSaved').notSaved();
	 // Input Value Update Links
		jQuery('a.valUpdate').valUpdate();
	 // Select the input marked for autofocus
		if( ! jQuery.browser.webkit ) {
			setTimeout( "jQuery('input[autofocus],select[autofocus],textarea[autofocus],button[autofocus]').last().focus();", 100 );
		}
	 // 
		if( ! gaFramework.misc.initComplete ) {
			gaFramework.misc.initComplete = true;
			gaFramework.modified(function(){
				gaFramework.misc.init();
			});
			
			// connect to the print-friendly button
			jQuery('#switchCSS').toggle(function() {
				jQuery('body').addClass('print-friendly');
				jQuery('#header').css({
					position : 'static'
				});
				jQuery(this).html('Back to Application');
				gaFramework.switchCSS('Print');
				jQuery(window).resize();
				return false;
			}, function() {
				jQuery('body').removeClass('print-friendly');
				jQuery('#header').css({
					position : 'absolute'
			});
				jQuery(this).html('Print Friendly');
				gaFramework.switchCSS('Standard');
				jQuery(window).resize();
				return false;
			});			
		}
	};

})(jQuery);

 /* Event propogation to other JS frameworks
  *
  */

	try {
		if ( typeof Spring != 'undefined' && typeof Spring.RemotingHandler != 'undefined' ) {
		 // For Dojo Calls
			if ( typeof dojo != 'undefined' ) {
				dojo.connect(Spring.RemotingHandler.prototype, 'submitForm', gaFramework.ajax, 'onRequest');
				dojo.connect(Spring.RemotingHandler.prototype, 'handleResponse', gaFramework.ajax, 'onResponse');
			}
		 // For A4J Calls
			if ( typeof A4J != 'undefined' && typeof A4J.AJAX != 'undefined' ) {
				dojo.connect( A4J.AJAX, 'SubmitQuery', gaFramework.ajax, 'onRequest' );
				dojo.connect( A4J.AJAX, 'finishRequest', gaFramework.ajax, 'onResponse' );
			}
			 // For JSF2 Calls
			if ( typeof jsf != 'undefined' && typeof jsf.ajax != 'undefined' ) {
				dojo.connect( jsf.ajax, 'request', gaFramework.ajax, 'onRequest' );
				dojo.connect( jsf.ajax, 'response', gaFramework.ajax, 'onResponse' );
			}
			
		}
	} catch( e ) {
		console.error( 'Error hooking jQuery events into non-jQuery AJAX calls. ' + e );
	}
	
	// decrepid old browsers like IE7 and 8 have no string trim method
	if (!String.prototype.trim) {
	    String.prototype.trim=function(){return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');};
	}
	
	
jQuery(document).ready(function(){
	gaFramework.misc.checkboxInit();
	gaFramework.misc.init();
});	