if (!gaFramework) {
    var gaFramework = {};
}
if (!gaFramework.workflow) {
    gaFramework.workflow = {};
}

(function(jQuery) {

    /*
     * Variables
     * 
     */

    var tabsLeft;
    var tabsRight;
    var extentLeft;
    var extentRight;

    /*
     * 
     */

    /*
     * Initialise the Workflow View
     * 
     */

    gaFramework.workflow.tabBar = function(selector, context) {
        // Get the container
        gaFramework.workflow.container = jQuery(selector, context);
        // If the container exists...
        if (gaFramework.workflow.container.length) {
            // Get the tabGroup
            gaFramework.workflow.tabGroup = gaFramework.workflow.container.find('ol');
            // If the tabeGroup exists...
            if (gaFramework.workflow.tabGroup.length) {
                // Get the tabs
                gaFramework.workflow.tabs = gaFramework.workflow.container.find("li, li.group > a");
                gaFramework.workflow.tabsWidth = 0;
                // Do some things to the tabs to make them work properly.
                var windowWidth = jQuery(window).width();
                var zindex = 500;
                jQuery.each(gaFramework.workflow.tabs, function() {
                    // Set the z-index
                    jQuery(this).css({
                        zIndex : zindex--
                    });
                    // derive the width of the gaFramework.workflow.tabGroup from the last
                    // tab
                    gaFramework.workflow.tabsWidth = Math.floor(jQuery(this).position().left + jQuery(this).width() - 18);
                });
                // Apply the correct width to the tab group --- no, leave it the
                // full width to avoid wrapping
                // gaFramework.workflow.tabGroup.width( gaFramework.workflow.tabsWidth );
                // Finish changing some of the variables
                tabsLeft = gaFramework.workflow.tabGroup.position().left;
                tabsRight = windowWidth - (tabsLeft + gaFramework.workflow.tabsWidth);
                extentLeft = 0;
                extentRight = gaFramework.workflow.tabsWidth - windowWidth + 5;
                if (extentRight < 0) {
                    extentRight = 0;
                }
                // Scroll the tabs to show the selected one
                gaFramework.workflow.scrollTabs('selected');
                // Toggle the scroll buttons when the window is resized
                jQuery(window).bind('resize', function() {
                    gaFramework.workflow.toggleScrollButtons();
                });
            }
        }
    };

    /*
     * 
     */
    gaFramework.workflow.viewArea = function(selector, context) {
        // Get the workflow view
        gaFramework.workflow.view = jQuery(selector, context);
        // Variables
        var marginTop = 0;
        var marginBottom = 0;
        var containerSide = 'pre';
        // Loop through the structure blocks on the page
        jQuery('div.structure').each(function() {
            if (jQuery(this).hasClass('container')) {
                containerSide = 'post';
                // Fix for the way IE renders the header and footer over our resizable columns structure.
                if (jQuery.browser.msie && new Number(jQuery.browser.version) < 8 && jQuery('div.structure.container').hasClass('columns')) {
                    jQuery(this).css({
                        top : marginTop
                    });
                }
            }
            // Loop through the previous structure blocks
            else if (containerSide == 'pre') {
                // We position everything to stop it scrolling with the screen.
                jQuery(this).css({
                    position : 'fixed',
                    top : marginTop
                });
                // Fix for the way IE renders the header and footer over our resizable columns structure.
                if (jQuery.browser.msie && new Number(jQuery.browser.version) < 8 && jQuery('div.structure.container').hasClass('columns')) {
                    jQuery(this).css({
                        position : 'absolute'
                    });
                }
                // Get the combined height of the gaFramework.workflow.view's siblings
                marginTop = marginTop + jQuery(this).outerHeight();
                // 
                // console.log( 'Adjusting structure block in header... (div.%s)', jQuery.trim( jQuery(this).attr('class').replace('structure ', '') ) );
            }
            // Loop through the following structure blocks
            else if (containerSide == 'post') {
                // We position everything to stop it scrolling with the screen.
                jQuery(this).css({
                    position : 'fixed',
                    bottom : marginBottom
                });
                // Fix for the way IE renders the header and footer over our resizable columns structure.
                if (jQuery.browser.msie && new Number(jQuery.browser.version) < 8 && jQuery('div.structure.container').hasClass('columns')) {
                    jQuery(this).css({
                        position : 'absolute'
                    });
                }
                /*
                 * // Fix for the way Opera renders the header and footer over
                 * our resizable columns structure. if( jQuery.browser.opera &&
                 * !jQuery('div.structure.workflow').length ) {
                 * alert("working"); jQuery(this).css({ marginTop: '-10' }); }
                 * else { alert("umut"); }
                 */
                // Get the combined height of the gaFramework.workflow.view's siblings
                marginBottom = marginBottom + jQuery(this).outerHeight();
                // 
                //console.log( 'Adjusting structure block in footer... (div.%s)', jQuery.trim( jQuery(this).attr('class').replace('structure ', '') ) );
            }
        });
        // Boom.
        gaFramework.barHeightTop = marginTop;
        gaFramework.barHeightBottom = marginBottom;
        gaFramework.barHeight = (marginTop + marginBottom);
        // For resizable columns, we set the height and width
        jQuery('div.structure.container').each(function() {
            // Update the padding of the view container to make room for the siblings
            jQuery('div.structure.container').css({
                marginTop : marginTop,
                marginBottom : marginBottom
            });
            // If we're using resizable columns...
            if (jQuery(this).hasClass('columns')) {
                // Variables
                container = jQuery(this);
                // Give the page a 'resizableColumns' class so we fix CSS issues.
                jQuery('body').addClass('resizableColumns');
                // Reset the height of our container
                container.width(jQuery(window).width());
                container.height(jQuery(window).height() - gaFramework.barHeight);
                container.children().height(jQuery(window).height() - gaFramework.barHeight);
                // On window resize...
                jQuery(window).bind('resize', function() {
                    container.width(jQuery(window).width());
                    container.height(jQuery(window).height() - gaFramework.barHeight);
                    container.children().height(jQuery(window).height() - gaFramework.barHeight);
                });
            }
        });
    };

    /*
     * Adjust the width of a column
     * 
     */

    gaFramework.workflow.adjustColumn = function(column, width, limit) {
        var limit = limit ? limit : 100;
        // Get the column
        var column = jQuery(column);
        // We only do this if we're given a single column.
        if (column.length == 1) {
            // Variables
            var container = column.parent();
            var nextColumn = column.next();
            var handlebar = jQuery('div.handlebar', column);
            // Determine the column width as a percentage of the two we're focused on
            var containerWidth = container.width();
            var combinedWidth = column.width() + nextColumn.width();
            var combinedPercent = combinedWidth / container.width();
            var handlebarWidth = jQuery('.handlebar', this).outerWidth();
            // Manipulate the width we get
            if (width == 'default') {
                width = column.attr('defaultWidth');
            } else if (width == 'min') {
                width = limit / containerWidth;
            } else if (width == 'max') {
                width = 1.0 - (limit / containerWidth);
            }
            if (width > 1.0) {
                width = width / containerWidth;
            } else if (width < 0.0) {
                width = limit / containerWidth;
            }
            if (width > 1) {
                width = 1.0 - (limit / containerWidth);
            }
            // Stop the dragging if we go beyond our extent
            if (width * containerWidth < limit) {
                width = (handlebarWidth - 1) / containerWidth;
                jQuery(this).trigger('dragstop');
            } else if (width * containerWidth > combinedWidth - limit && jQuery('div.handlebar', nextColumn).length) {
                width = (combinedWidth - handlebarWidth + 1) / containerWidth;
                jQuery(this).trigger('dragstop');
            } else if (width * containerWidth > combinedWidth - limit) {
                width = (combinedWidth) / containerWidth;
                jQuery(this).trigger('dragstop');
            }
            // Figure out the new widths
            var newColumnPercent = width;
            var newNextColumnPercent = 1 /* combinedPercent */- width;
            // Resize the columns
            console.log('adjusting width to %o', (newColumnPercent * 100) + '%');
            jQuery(column).width((newColumnPercent * 100) + '%');
            console.log('adjusting nextcolwidth to %o', (newNextColumnPercent * 100) + '%');
            jQuery(nextColumn).width((newNextColumnPercent * 100) + '%');
            // Iterate through the columns
            jQuery('.column', container).each(function(i) {
                if (jQuery('.handlebar', this).length) {
                    jQuery('.handlebar', this).css({
                        left : jQuery(this).outerWidth() - jQuery('.handlebar', this).outerWidth()
                    });
                }
            });
            //
            jQuery(window).resize();
        }
    };

    /*
     * 
     */

    gaFramework.workflow.resizableColumns = function(selector, context) {
        jQuery(selector, context)
                .not('.hasHandlebar')
                .each(
                        function() {
                            // Variables
                            var container = jQuery(this).addClass('hasHandlebar');
                            // Iterate through the columns
                            jQuery('.column', container)
                                    .each(
                                            function(i) {
                                                if (jQuery(this).length > i) {
                                                    // Variables
                                                    var column = jQuery(this);
                                                    column.attr('defaultWidth', column.width() / container.width());
                                                    var nextColumn = jQuery(this).next();
                                                    nextColumn.attr('defaultWidth', nextColumn.width() / container.width());
                                                    // Add the handlebar
                                                    jQuery(
                                                            '<div class="handlebar"><img class="handlebar-min" src="common/css/images/handlebar_close.gif"><img class="handlebar-max" src="common/css/images/handlebar_open.gif"></div>')
                                                            .appendTo(column).draggable({
                                                                axis : 'x',
                                                                containment : container,
                                                                helper : 'clone',
                                                                scroll : false
                                                            });
                                                    jQuery('.handlebar-min', this).on('click', function(evt) {
                                                        console.log('minclick');
                                                        gaFramework.workflow.adjustColumn(jQuery('.column', container).first(), 'min', 6);
                                                        evt.stopPropagation();
                                                    });
                                                    jQuery('.handlebar-max', this).on('click', function(evt) {
                                                        console.log('maxclick');
                                                        gaFramework.workflow.adjustColumn(jQuery('.column', container).first(), 'max', 4);
                                                        evt.stopPropagation();
                                                    });
                                                    // Reposition the handlebar
                                                    jQuery('.handlebar', this).css({
                                                        height : jQuery(this).innerHeight(),
                                                        left : jQuery(this).width() - jQuery('.handlebar', this).outerWidth()
                                                    }).bind(
                                                            'drag',
                                                            function(e, ui) {
                                                                // Variables
                                                                var clone = jQuery('div.handlebar.ui-draggable-dragging', jQuery(this).parent());
                                                                // Adjust the column
                                                                gaFramework.workflow.adjustColumn(column, jQuery(clone).offset().left
                                                                        - jQuery(column).offset().left + jQuery(this).outerWidth());
                                                            }).bind('click', function(e, ui) {
                                                        console.log('barclick');
                                                        gaFramework.workflow.adjustColumn(column, 'default');
                                                    });
                                                } else {
                                                    jQuery(this).addClass('last');
                                                }
                                            });
                            if (jQuery.throttle) {
                                jQuery(window).resize(jQuery.throttle(250, function() {
                                    // Loop through our columns
                                    jQuery('.column', container).each(function(i) {
                                        if (jQuery('.handlebar', this).length) {
                                            // Reposition the handlebar
                                            jQuery('.handlebar', this).css({
                                                height : jQuery(this).innerHeight(),
                                                left : jQuery(this).outerWidth() - jQuery('.handlebar', this).outerWidth()
                                            });
                                        }
                                    });
                                }));
                            } else {
                                jQuery(window).resize(function() {
                                    // Loop through our columns
                                    jQuery('.column', container).each(function(i) {
                                        if (jQuery('.handlebar', this).length) {
                                            // Reposition the handlebar
                                            jQuery('.handlebar', this).css({
                                                height : jQuery(this).innerHeight(),
                                                left : jQuery(this).outerWidth() - jQuery('.handlebar', this).outerWidth()
                                            });
                                        }
                                    });
                                });
                            }
                        });
    };

    /*
     * 
     */

    gaFramework.workflow.toggleScrollButtons = function(event) {
        // Variables
        //tabsLeft                      =   gaFramework.workflow.tabGroup.position().left;
        tabsRight = jQuery(window).width() - (tabsLeft + gaFramework.workflow.tabsWidth);
        extentLeft = 0;
        extentRight = gaFramework.workflow.tabsWidth - jQuery(window).width() + 5;
        if (extentRight < 0) {
            extentRight = 0;
        }
        // console.log('toggleScrollButtons tabsLeft:'+tabsLeft+', tabsRight:'+tabsRight+', extentleft:'+extentLeft+', extentRight:'+extentRight);     

        // Left
        if (tabsLeft >= 0) {
            if (jQuery('div.scroll-left', gaFramework.workflow.container).length) {
                jQuery('div.scroll-left', gaFramework.workflow.container).fadeOut('fast', function() {
                    jQuery(this).remove();
                });
            }
        } else if (!jQuery('div.scroll-left', gaFramework.workflow.container).length) {
            var scrollLeft = jQuery('<div></div>').addClass('scroll-left').css({
                display : 'none'
            }).appendTo(gaFramework.workflow.container);
            scrollLeft.fadeIn('fast');
            scrollLeft.click(function() {
                gaFramework.workflow.scrollTabs('left');
            });
            scrollLeft.dblclick(function() {
                gaFramework.workflow.scrollTabs('left-extent');
            });
        }
        // Right
        if (tabsRight >= 0) {
            if (jQuery('div.scroll-right', gaFramework.workflow.container).length) {
                jQuery('div.scroll-right', gaFramework.workflow.container).fadeOut('fast', function() {
                    jQuery(this).remove();
                });
            }
        } else if (!jQuery('div.scroll-right', gaFramework.workflow.container).length) {
            var scrollRight = jQuery('<div></div>').addClass('scroll-right').css({
                display : 'none'
            }).appendTo(gaFramework.workflow.container);
            scrollRight.fadeIn('fast');
            scrollRight.click(function() {
                gaFramework.workflow.scrollTabs('right');
            });
            scrollRight.dblclick(function() {
                gaFramework.workflow.scrollTabs('right-extent');
            });
        }
        return false;
    };

    /*
     * 
     */

    gaFramework.workflow.scrollTabs = function(direction) {
        // Variables
        var scrollAmount = Math.floor(jQuery(window).width() * .9); //300;
        //console.log( "Scrolling: " + direction +", tabsLeft:" +tabsLeft +", tabsRight:"+tabsRight+", scrollAmount:"+scrollAmount );
        // Stop all current animations. It gives a much smoother animation on double clicks.
        gaFramework.workflow.tabGroup.stop();
        // Figure out what direction we're scrolling and base the new position on that.
        if (direction == 'left') {
            tabsLeft = Math.floor(tabsLeft + scrollAmount);
            tabsRight = Math.floor(tabsRight - scrollAmount);
        } else if (direction == 'right') {
            tabsLeft = Math.floor(tabsLeft - scrollAmount);
            tabsRight = Math.floor(tabsRight + scrollAmount);
        } else if (direction == 'selected') {
            // Variables
            var selectedTab = jQuery('li.selected', gaFramework.workflow.tabGroup);
            if (selectedTab.length == 1) { // only if we have exactly one selected tab
                var selectTabLeft = Math.floor(tabsLeft + jQuery('li.selected', gaFramework.workflow.tabGroup).position().left);
                var selectTabRight = Math.floor(jQuery(window).width()
                        - (selectTabLeft + jQuery('li.selected', gaFramework.workflow.tabGroup).width() - 18));
                // If the tab is off the left side
                if (selectTabLeft < 0) {
                    var scrollAmount = 0 - selectTabLeft;
                    tabsLeft = Math.floor(tabsLeft + scrollAmount);
                    tabsRight = Math.floor(tabsRight - scrollAmount);
                }
                // If the tab is off the right side
                else if (selectTabRight < 0) {
                    var scrollAmount = 0 - selectTabRight;
                    tabsLeft = Math.ceil(tabsLeft - scrollAmount);
                    tabsRight = Math.ceil(tabsRight + scrollAmount);
                }
            } else {
                //console.log('workflow:scrolltabs('+direction+') but selectedTab.length='+selectedTab.length);
            }
        }
        // We can move to the extents on demand, or if a scroll is going to take us beyond the extent.
        if (direction == 'left-extent' || (tabsLeft > 0 && tabsRight < 0)) {
            tabsLeft = extentLeft;
            tabsRight = -extentRight;
        } else if (direction == 'right-extent' || (tabsLeft < 0 && tabsRight > 0)) {
            tabsLeft = -extentRight;
            tabsRight = extentLeft;
        }
        // If the position has actually changed...
        if (gaFramework.workflow.tabGroup.position().left != tabsLeft) {
            //console.log( "Scrolling the tabs... to adjust tabsLeft from " +gaFramework.workflow.tabGroup.position().left +" to " + tabsLeft );
            gaFramework.workflow.tabGroup.css({
                left : tabsLeft + "px"
            });
            gaFramework.workflow.toggleScrollButtons();
        } else {
            //console.log( "No scrolling required. Everything's shiny, cap'n. tabsLeft:"+tabsLeft+", tabsRight:"+tabsRight);
        }
    };

    /*
     * 
     */
    gaFramework.screenArea = null;
    gaFramework.structureCount = null;
    gaFramework.contentArea = null;
    gaFramework.workflow.initComplete = false;
    gaFramework.workflow.initCount = 0;

    /*
     * Initialise the Workflow Tabs
     * 
     */

    gaFramework.workflow.init = function() {
        //console.log('gaFramework.workflow.init header position: %o', jQuery('#header').css('position'));
        if (jQuery('#header').css('position') == 'static') {
            // css has not loaded yet (or it would be either absolute or fixed
            window.setTimeout(function() {
                //console.log('gaFramework.workflow.init waiting for ga.base.css to load');
                gaFramework.screenArea = null;
                gaFramework.workflow.init();
            }, 200);
            if (++gaFramework.workflow.initCount < (5 * 15)) {// keep trying for about 15 seconds then give up 
                return;
            }
        }
        // Get the screenarea and content area
        var screenArea = jQuery(window).height() * jQuery(window).width();
        var contentArea = jQuery(document).height() * jQuery(document).width();
        console.log('gaFramework.workflow.init screenheight:%o, documentheight:%o', jQuery(window).height(), jQuery(document).height());
        var structureCount = jQuery('.structure').length;
        // If the screen area or content area has changed...
        if (screenArea != gaFramework.screenArea || screenArea != gaFramework.screenArea || structureCount != gaFramework.structureCount) {
            // Store the changed values
            gaFramework.screenArea = screenArea;
            gaFramework.contentArea = contentArea;
            gaFramework.structureCount = structureCount;
            // Setup workarea
            gaFramework.workflow.tabBar('.structure.workflow');
            gaFramework.workflow.viewArea('.structure.container');
            gaFramework.workflow.resizableColumns('.structure.columns');
        }
        // If we haven't already set up the bind...
        if (!gaFramework.workflow.initComplete) {
            gaFramework.workflow.initComplete = true;
            // Reorganise on modified
            gaFramework.modified(function() {
                gaFramework.workflow.init();
            });
        }
    };

})(jQuery);

jQuery(document).ready(function() {
    gaFramework.workflow.init();
});
