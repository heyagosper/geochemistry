// Google Mercator projection
// Used in combination with GoogleMercator layer type in OpenLayers
//+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs

// This is supposed to be the 'official' google/spherical-mercator projection
// http://wiki.openstreetmap.org/wiki/EPSG:3857
// proj4js 1.1.0 has this incorrectly listed as EPSG:3875 which will be corrected in the next release http://trac.osgeo.org/proj4js/changeset/2220
// deprecated synonyms include EPSG:900913, EPSG:3875, EPSG:3785 and GOOGLE
Proj4js.defs["EPSG:3857"]= "+title=GoogleMercator +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs";

