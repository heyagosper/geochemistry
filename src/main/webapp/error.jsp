<%@ taglib
  prefix="sec"
  uri="http://www.springframework.org/security/tags"%>
<%@ taglib
  uri="http://java.sun.com/jsp/jstl/core"
  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page import="java.lang.Exception,java.io.*"%>
<%@ page import="au.gov.ga.oemd.framework.context.SystemContext"%>
<%@ page isErrorPage="true"%>
<html>
<head>
<title>Application Framework Index Page</title>
<script
  type="text/javascript"
  src="<%=request.getContextPath()%>/resources/common/javascript/jquery.js"></script>
<script
  type="text/javascript"
  src="<%=request.getContextPath()%>/resources/common/javascript/framework.js"></script>
<link
  type="text/css"
  href="<%=request.getContextPath()%>/resources/common/css/style.css"
  rel="stylesheet" />
</head>
<body>
  <div class="structure bulkhead" id="header">
    <div class="bulkhead-pad">
      <a
        href="http://www.ga.gov.au/"
        class="logo"
        title="Australian Government Geoscience Australia logo">Australian
        Government - Geoscience Australia</a>
      <h1>System Error</h1>
      <ul id="nav">
      </ul>
      <ul class="options">
      </ul>
      <div
        class="bulkhead-info"
        style="float: right;">
        <p>${user.name}</p>
        <p>${systemContext.environmentName}</p>
      </div>
    </div>
  </div>
  <div class="structure workflow">
    <div class="workflow-pad">
      <ol>

      </ol>
    </div>
  </div>
  <div class="structure container">
    <div class="container-pad">
      <div class="view intro">

        <h2>${pageContext.errorData.statusCode} Error</h2>

        <div class="columns">
          <div
            class="column"
            style="width: 100%;">

            <p style="margin-bottom: 10px;">
              An unrecovarable error has occurred. To start over use the
              link below.<br /> <a
                href='<%=request.getContextPath()+ "/"%>'><%=request.getContextPath()%></a><br />or<br />
              <a href="${systemContext.configProperties['homeUri']}">${systemContext.configProperties['homeLabel']}</a><br /><br />
              Contact the OEMD IS support team if errors keep recurring.
            </p>

            <c:if test="${systemContext.nonProduction}">
              <p style="margin-bottom: 10px;">
                <b>Exception message:</b><br /> <br />
                <%=(exception == null) ? "" : exception.getMessage()%></p>
              <%
      String stStack = "";
          if (exception != null) {
              java.io.StringWriter sw = new java.io.StringWriter();
              exception.printStackTrace(new java.io.PrintWriter(sw));
              stStack = sw.toString();
          }
  %>
              <p style="margin-bottom: 10px;">
                <b>Stack trace: </b>
              </p>
              <pre>
<%=stStack%>
</pre>
            </c:if>
          </div>
        </div>

      </div>
    </div>
  </div>
</body>
</html>
